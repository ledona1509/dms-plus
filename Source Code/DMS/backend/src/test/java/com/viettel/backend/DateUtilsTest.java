package com.viettel.backend;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtilsTest {

    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Date date = new Date(1426071133964l);
        System.out.println(sdf.format(date));
        System.out.println(date.getTime());
    }

}
