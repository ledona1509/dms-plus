package com.viettel.backend.dto;

import com.viettel.backend.domain.User;

/**
 * @author thanh
 */
public class UserDto extends UserSimpleDto {

    private static final long serialVersionUID = 1L;

    private String role;

    public UserDto() {
        super();
    }

    public UserDto(User user) {
        super(user);

        if (user.getRoles() != null && !user.getRoles().isEmpty()) {
            this.role = user.getRoles().get(0);
        }
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
