package com.viettel.backend.oauth2.core;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.NoSuchClientException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JdbcClientDetailsService implements ClientDetailsService {
 
	private ObjectMapper objectMapper = new ObjectMapper();
	
	private final JdbcTemplate jdbcTemplate;

	private final Log logger = LogFactory.getLog(getClass());
	
	public JdbcClientDetailsService(DataSource dataSource) {
		Assert.notNull(dataSource, "DataSource required");
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
     
    @Override
    public ClientDetails loadClientByClientId(String clientId)
            throws OAuth2Exception {
    	ApplicationInfo app = null;
    	try {
    		app = jdbcTemplate.queryForObject(selectAccessTokenSql, rowMapper, clientId);
    	} catch (EmptyResultDataAccessException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Failed to find application with id: " + clientId);
			}
		} catch (IllegalArgumentException e) {
			logger.error("Could not extract application for id " + clientId, e);
		}
 
        if (app != null) {
            List<String> authorizedGrantTypes = extractListFromString(app.getAuthorizedGrantTypes());
            List<String> scopes = extractListFromString(app.getScope());
            Set<String> redirectUris = new HashSet<String>(1);
            redirectUris.add(app.getWebServerRedirectUrl());
            
            BaseClientDetails clientDetails = new BaseClientDetails();
            clientDetails.setClientId(app.getAppId());
            clientDetails.setClientSecret(app.getAppSecret());
            clientDetails.setAuthorizedGrantTypes(authorizedGrantTypes);
            clientDetails.setScope(scopes);
            clientDetails.setRefreshTokenValiditySeconds(app.getRefreshTokenValidity());
            clientDetails.setAccessTokenValiditySeconds(app.getAccessTokenValidity());
            clientDetails.setRegisteredRedirectUri(redirectUris);
            clientDetails.setAuthorities(extractAuthorities(app.getAuthorities()));
            clientDetails.setAdditionalInformation(extractAdditionalInformation(app.getAdditionalInformation()));
            
            return clientDetails;
        }
        else {
        	if (logger.isDebugEnabled()) {
				logger.debug("Unable to find application with id: " + clientId);
			}
            throw new NoSuchClientException("No client recognized with id: "
                    + clientId);
        }
    }
 
    private List<String> extractListFromString(String value) {
    	if (StringUtils.isEmpty(value)) {
    		return null;
    	}
    	String[] arrs = value.split(",");
    	return Arrays.asList(arrs);
    }
    
    private List<GrantedAuthority> extractAuthorities(String value) {
    	if (StringUtils.isEmpty(value)) {
    		return null;
    	}
    	String[] arrs = value.split(",");
    	List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(arrs.length);
    	for (int i = 0; i < arrs.length; i++) {
			SimpleGrantedAuthority authority = new SimpleGrantedAuthority(arrs[i]);
			authorities.add(authority);
		}
    	return authorities;
    }
    
    @SuppressWarnings("unchecked")
	private Map<String, ?> extractAdditionalInformation(String value) {
    	if (StringUtils.isEmpty(value)) {
    		return Collections.emptyMap();
    	}
    	Map<String, ?> data = Collections.emptyMap();
		try {
			data = objectMapper.readValue(value, HashMap.class);
		} catch (IOException e) {
			throw new IllegalStateException(
					"Cannot convert JSON to information Map", e);
		}
    	return data;
    }

	private final RowMapper<ApplicationInfo> rowMapper = new ApplicationRowMapper();
	
	private String selectAccessTokenSql = "SELECT C_APPLICATION_ID, NAME, VALUE, APP_ID,"
			+ " 	APP_SECRET, AUTHORITIES, AUTHORIZED_GRANT_TYPES, SCOPE, ACCESS_TOKEN_VALIDITY,"
			+ " 	REFRESH_TOKEN_VALIDITY, WEB_SERVER_REDIRECT_URL, ADDITIONAL_INFORMATION"
			+ " FROM C_APPLICATION"
			+ " WHERE APP_ID = ?";
    
    private static class ApplicationRowMapper implements RowMapper<ApplicationInfo> {

		@Override
		public ApplicationInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Long id = rs.getLong("C_APPLICATION_ID");
			String name = rs.getString("NAME");
			String value = rs.getString("VALUE");
			String appId = rs.getString("APP_ID");
			String appSecret = rs.getString("APP_SECRET");
			String authorizedGrantTypes = rs.getString("AUTHORIZED_GRANT_TYPES");
			String scope = rs.getString("SCOPE");
			String webServerRedirectUrl = rs.getString("WEB_SERVER_REDIRECT_URL");
			String authorities = rs.getString("AUTHORITIES");
			Integer accessTokenValidity = rs.getInt("ACCESS_TOKEN_VALIDITY");
			Integer refreshTokenValidity = rs.getInt("REFRESH_TOKEN_VALIDITY");
			String additionalInformation = rs.getString("ADDITIONAL_INFORMATION");
			
			ApplicationInfo app = new ApplicationInfo();
			app.setId(id);
			app.setName(name);
			app.setValue(value);
			app.setAppId(appId);
			app.setAppSecret(appSecret);
			app.setAuthorizedGrantTypes(authorizedGrantTypes);
			app.setScope(scope);
			app.setWebServerRedirectUrl(webServerRedirectUrl);
			app.setAuthorities(authorities);
			app.setAccessTokenValidity(accessTokenValidity);
			app.setRefreshTokenValidity(refreshTokenValidity);
			app.setAdditionalInformation(additionalInformation);
			
			return app;
		}
	}
}
