package com.viettel.backend.repository.impl;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;

import com.viettel.backend.domain.NameCategory;
import com.viettel.backend.domain.PO;
import com.viettel.backend.repository.BasicNameCategoryRepository;
import com.viettel.backend.util.CriteriaUtils;

public class BasicNameCategoryRepositoryImpl<D extends NameCategory> extends BasicCategoryRepositoryImpl<D> implements
        BasicNameCategoryRepository<D> {

    private static final long serialVersionUID = -8958421384263600937L;

    @Override
    public boolean existsByName(ObjectId clientId, ObjectId id, String name) {
        Criteria otherCriteria = null;
        if (id != null) {
            otherCriteria = Criteria.where(PO.COLUMNNAME_ID).ne(id);
        }

        Criteria nameCriteria = CriteriaUtils.getSearchInsensitiveCriteria(NameCategory.COLUMNNAME_NAME, name);
        return super._countWithDraft(clientId, true, null, CriteriaUtils.andOperator(otherCriteria, nameCriteria)) > 0;
    }
}
