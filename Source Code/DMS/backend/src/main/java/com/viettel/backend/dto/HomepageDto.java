package com.viettel.backend.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public class HomepageDto implements Serializable {

    private static final long serialVersionUID = -2425918035587920200L;

    private String today;

    // MONTHLY
    private int orderByDayThisMonth;
    private int orderByDayLastMonth;
    private BigDecimal revenueByOrderThisMonth;
    private BigDecimal revenueByOrderLastMonth;
    private BigDecimal outputByOrderThisMonth;
    private BigDecimal outputByOrderLastMonth;
    private BigDecimal revenueThisMonth;
    private BigDecimal revenueTargetThisMonth;
    private BigDecimal outputThisMonth;
    private BigDecimal outputTargetThisMonth;

    // DAILY
    private BigDecimal revenueToday;
    private BigDecimal outputToday;
    private int numberVisit;
    private int numberVisitHasOrder;
    private int numberOrderNoVisit;
    private int numberVisitErrorTime;
    
    private List<HashMap<String, Object>> top5products;
    private List<HashMap<String, Object>> worst5salesmen;
    
    public HomepageDto() {
        this.orderByDayThisMonth = 0;
        this.orderByDayLastMonth = 0;
        this.revenueByOrderThisMonth = BigDecimal.ZERO;
        this.revenueByOrderLastMonth = BigDecimal.ZERO;
        this.outputByOrderThisMonth = BigDecimal.ZERO;
        this.outputByOrderLastMonth = BigDecimal.ZERO;
        this.revenueThisMonth = BigDecimal.ZERO;
        this.revenueTargetThisMonth = BigDecimal.ZERO;
        this.outputThisMonth = BigDecimal.ZERO;
        this.outputTargetThisMonth = BigDecimal.ZERO;
        
        this.revenueToday = BigDecimal.ZERO;
        this.outputToday = BigDecimal.ZERO;
        this.numberVisit = 0;
        this.numberVisitHasOrder = 0;
        this.numberOrderNoVisit = 0;
        this.numberVisitErrorTime = 0;
    
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    public int getOrderByDayThisMonth() {
        return orderByDayThisMonth;
    }

    public void setOrderByDayThisMonth(int orderByDayThisMonth) {
        this.orderByDayThisMonth = orderByDayThisMonth;
    }

    public int getOrderByDayLastMonth() {
        return orderByDayLastMonth;
    }

    public void setOrderByDayLastMonth(int orderByDayLastMonth) {
        this.orderByDayLastMonth = orderByDayLastMonth;
    }

    public BigDecimal getRevenueByOrderThisMonth() {
        return revenueByOrderThisMonth;
    }

    public void setRevenueByOrderThisMonth(BigDecimal revenueByOrderThisMonth) {
        this.revenueByOrderThisMonth = revenueByOrderThisMonth;
    }

    public BigDecimal getRevenueByOrderLastMonth() {
        return revenueByOrderLastMonth;
    }

    public void setRevenueByOrderLastMonth(BigDecimal revenueByOrderLastMonth) {
        this.revenueByOrderLastMonth = revenueByOrderLastMonth;
    }
    
    public BigDecimal getOutputByOrderThisMonth() {
        return outputByOrderThisMonth;
    }
    
    public void setOutputByOrderThisMonth(BigDecimal outputByOrderThisMonth) {
        this.outputByOrderThisMonth = outputByOrderThisMonth;
    }
    
    public BigDecimal getOutputByOrderLastMonth() {
        return outputByOrderLastMonth;
    }
    
    public void setOutputByOrderLastMonth(BigDecimal outputByOrderLastMonth) {
        this.outputByOrderLastMonth = outputByOrderLastMonth;
    }

    public BigDecimal getRevenueThisMonth() {
        return revenueThisMonth;
    }

    public void setRevenueThisMonth(BigDecimal revenueThisMonth) {
        this.revenueThisMonth = revenueThisMonth;
    }

    public BigDecimal getRevenueTargetThisMonth() {
        return revenueTargetThisMonth;
    }

    public void setRevenueTargetThisMonth(BigDecimal revenueTargetThisMonth) {
        this.revenueTargetThisMonth = revenueTargetThisMonth;
    }

    public BigDecimal getOutputThisMonth() {
        return outputThisMonth;
    }

    public void setOutputThisMonth(BigDecimal outputThisMonth) {
        this.outputThisMonth = outputThisMonth;
    }

    public BigDecimal getOutputTargetThisMonth() {
        return outputTargetThisMonth;
    }

    public void setOutputTargetThisMonth(BigDecimal outputTargetThisMonth) {
        this.outputTargetThisMonth = outputTargetThisMonth;
    }

    public BigDecimal getRevenueToday() {
        return revenueToday;
    }

    public void setRevenueToday(BigDecimal revenueToday) {
        this.revenueToday = revenueToday;
    }

    public BigDecimal getOutputToday() {
        return outputToday;
    }

    public void setOutputToday(BigDecimal outputToday) {
        this.outputToday = outputToday;
    }

    public int getNumberVisit() {
        return numberVisit;
    }

    public void setNumberVisit(int numberVisit) {
        this.numberVisit = numberVisit;
    }

    public int getNumberVisitHasOrder() {
        return numberVisitHasOrder;
    }

    public void setNumberVisitHasOrder(int numberVisitHasOrder) {
        this.numberVisitHasOrder = numberVisitHasOrder;
    }

    public int getNumberOrderNoVisit() {
        return numberOrderNoVisit;
    }

    public void setNumberOrderNoVisit(int numberOrderNoVisit) {
        this.numberOrderNoVisit = numberOrderNoVisit;
    }

    public int getNumberVisitErrorTime() {
        return numberVisitErrorTime;
    }

    public void setNumberVisitErrorTime(int numberVisitErrorTime) {
        this.numberVisitErrorTime = numberVisitErrorTime;
    }
    
    public List<HashMap<String, Object>> getTop5products() {
        return top5products;
    }
    
    public void setTop5products(List<HashMap<String, Object>> top5products) {
        this.top5products = top5products;
    }
    
    public List<HashMap<String, Object>> getWorst5salesmen() {
        return worst5salesmen;
    }
    
    public void setWorst5salesmen(List<HashMap<String, Object>> worst5salesmen) {
        this.worst5salesmen = worst5salesmen;
    }

}
