package com.viettel.backend.service;

import java.io.Serializable;

import org.springframework.data.domain.Pageable;

import com.viettel.backend.dto.ExhibitionReportDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.OrderDto;
import com.viettel.backend.dto.OrderSimpleDto;
import com.viettel.backend.dto.SalesmanSalesReportMonthlyDto;
import com.viettel.backend.dto.SurveyResultDto;
import com.viettel.backend.dto.VisitReportDailyDto;
import com.viettel.backend.dto.VisitReportDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface CommonReportService extends Serializable {

    public VisitReportDailyDto getVisitReportDaily(UserLogin userLogin, String salesmanId);

    /**
     * Trả về báo cáo bán hàng của nhân viên bán hàng theo tháng
     * 
     * @param salesmanId
     *            Nếu = null thì trả về báo cáo của tất cả NVBH của GSBH
     */
    public ListJson<SalesmanSalesReportMonthlyDto> getSalesmanSalesReportMonthly(UserLogin userLogin,
            String salesmanId, int month, int year);

    public ListJson<VisitReportDto> getSalesmanVisitReport(UserLogin userLogin, String salesmanId,
            String fromDateIsoDate, String toDateIsoDate);

    public SurveyResultDto getSurveyReport(UserLogin userLogin, String surveyId);

    public byte[] exportSurveyReport(UserLogin userLogin, String surveyId);

    public ExhibitionReportDto getExhibitionReport(UserLogin userLogin, String exhibitionId);

    public byte[] exportExhibitionReport(UserLogin userLogin, String exhibitionId);

    public ListJson<OrderSimpleDto> getOrders(UserLogin userLogin, String _distributorId, String _fromDate, String _toDate,
            Pageable pageable);
    
    public OrderDto getOrderById(UserLogin userLogin, String _orderId);

}
