package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Exhibition;
import com.viettel.backend.domain.User;
import com.viettel.backend.dto.ExhibitionDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.ExhibitionRatingRepository;
import com.viettel.backend.repository.ExhibitionRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.service.SalesmanExhibitionService;

/**
 * @author thanh
 */
@Service
public class SalesmanExhibitionServiceImpl extends AbstractSalesmanService implements SalesmanExhibitionService {

    private static final long serialVersionUID = -5402109549735065233L;

    @Autowired
    private ExhibitionRepository exhibitionRepository;

    @Autowired
    private ExhibitionRatingRepository exhibitionRatingRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public ListJson<ExhibitionDto> getExhibitionsByCustomer(UserLogin userLogin, String _customerId) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, _customerId);

        ObjectId clientId = userLogin.getClientId();

        Sort sort = new Sort(Exhibition.COLUMNNAME_END_DATE_VALUE);
        List<Exhibition> exhibitions = exhibitionRepository.getExhibitionsByCustomer(clientId, customer.getId(), null,
                null, sort);
        if (exhibitions == null || exhibitions.isEmpty()) {
            return ListJson.emptyList();
        }

        List<ExhibitionDto> dtos = new ArrayList<ExhibitionDto>(exhibitions.size());
        for (Exhibition exhibition : exhibitions) {
            ExhibitionDto dto = new ExhibitionDto(exhibition);
            dtos.add(dto);
        }

        return new ListJson<ExhibitionDto>(dtos, Long.valueOf(dtos.size()));
    }

}
