package com.viettel.backend.restful.supervisor;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.TargetCreateDto;
import com.viettel.backend.dto.TargetDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.SupervisorTargetService;

@RestController
@RequestMapping(value = "/supervisor/target")
public class TargetController extends AbstractController {

    private static final long serialVersionUID = 1086937096431940275L;
    
    @Autowired
    private SupervisorTargetService supervisorTargetService;

    // LIST
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> list(
            @RequestParam(required = true) int month, 
            @RequestParam(required = true) int year,
            @RequestParam(required = false) Integer page, 
            @RequestParam(required = false) Integer size) {
        ListJson<TargetDto> results = supervisorTargetService.getTargets(getUserLogin(), month, year,
                getPageRequest(page, size));
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> detail(@PathVariable String id) {
        TargetDto result = supervisorTargetService.getById(getUserLogin(), id);
        return new Envelope(result).toResponseEntity();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> create(@RequestBody @Valid TargetCreateDto dto) {
        TargetDto result = supervisorTargetService.create(getUserLogin(), dto);
        return new Envelope(result).toResponseEntity();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@PathVariable String id, @RequestBody TargetCreateDto dto) {
        TargetDto result = supervisorTargetService.update(getUserLogin(), id, dto);
        return new Envelope(result).toResponseEntity();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable String id) {
        supervisorTargetService.delete(getUserLogin(), id);
        return new Envelope(Meta.OK).toResponseEntity();
    }

}
