package com.viettel.backend.oauth2.mvc;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author thanh
 */
@Controller
public class LoginController {

	@RequestMapping(value = "/account/login", method = RequestMethod.GET)
	public ModelAndView login() throws Exception {
		return new ModelAndView("login");
	}
	
	@RequestMapping(value = "/account/ping")
	@ResponseBody
	public ResponseEntity<?> autoPing() {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null && auth.isAuthenticated()) {
	        return new ResponseEntity<PingResult>(PingResult.ALIVE, HttpStatus.OK);
	    } else {
	        return new ResponseEntity<PingResult>(PingResult.NOT_ALIVE, HttpStatus.OK);
	    }
	}
	
	public static class PingResult {

	    public static final PingResult ALIVE = new PingResult(true);
	    public static final PingResult NOT_ALIVE = new PingResult(false);
	    
	    private boolean sessionAlive;
	    
	    public PingResult() {
            this(false);
        }
	    
	    public PingResult(boolean sessionAlive) {
            this.sessionAlive = sessionAlive;
        }

        public boolean isSessionAlive() {
            return sessionAlive;
        }

        public void setSessionAlive(boolean sessionAlive) {
            this.sessionAlive = sessionAlive;
        }
	}
}
