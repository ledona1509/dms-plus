package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.domain.User;
import com.viettel.backend.dto.UserLoginDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface AuthenticationService extends Serializable {

    public User authenticate(String username, String password);

    public UserLoginDto getUserLoginDto(UserLogin userLogin);

    public void changePassword(UserLogin userLogin, String userId, String oldPassword, String newPassword);

}
