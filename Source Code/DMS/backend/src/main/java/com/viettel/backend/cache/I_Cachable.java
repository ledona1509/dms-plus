package com.viettel.backend.cache;

public interface I_Cachable<V> {

    public V cloneForCache();
    
}
