package com.viettel.backend.restful.supervisor;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ExhibitionReportDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.SalesmanSalesReportMonthlyDto;
import com.viettel.backend.dto.SurveyResultDto;
import com.viettel.backend.dto.VisitReportDailyDto;
import com.viettel.backend.dto.VisitReportDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SupervisorReportService;

@RestController(value="supervisorReportController")
@RequestMapping(value = "/supervisor/report")
public class ReportController extends AbstractController {

    private static final long serialVersionUID = -2181009688465494927L;

    @Autowired
    private SupervisorReportService supervisorReportService;

    /**
     * Báo cáo ghé thăm theo một khoảng thời gian - lấy theo salesman
     */
    @RequestMapping(value = "/visit", method = RequestMethod.GET, params = { "salesmanId" })
    public ResponseEntity<?> getVisitReportBySalesman(@RequestParam String fromDate, @RequestParam String toDate,
            @RequestParam String salesmanId) {
        ListJson<VisitReportDto> listJson = supervisorReportService.getVisitReportBySalesman(getUserLogin(),
                salesmanId, fromDate, toDate);
        return new Envelope(listJson).toResponseEntity(HttpStatus.OK);
    }

    /**
     * Báo cáo ghé thăm theo một khoảng thời gian - lấy theo customer
     */
    @RequestMapping(value = "/visit", method = RequestMethod.GET, params = { "customerId" })
    public ResponseEntity<?> getVisitReportByCustomer(@RequestParam String fromDate, @RequestParam String toDate,
            @RequestParam String customerId) {
        ListJson<VisitReportDto> listJson = supervisorReportService.getVisitReportByCustomer(getUserLogin(),
                customerId, fromDate, toDate);
        return new Envelope(listJson).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/sales/monthly/salesman", method = RequestMethod.GET)
    public ResponseEntity<?> getSalesmanSalesReportMonthly(@RequestParam(value = "month") int month,
            @RequestParam(value = "year") int year, @RequestParam(required = false) String salesmanId) {
        ListJson<SalesmanSalesReportMonthlyDto> listJson = supervisorReportService.getSalesmanSalesReportMonthly(
                getUserLogin(), salesmanId, month, year);
        return new Envelope(listJson).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/visit/daily", method = RequestMethod.GET)
    public ResponseEntity<?> getVisitReportDaily(@RequestParam(value = "salesmanId", required = false) String salesmanId) {
        VisitReportDailyDto dto = supervisorReportService.getVisitReportDaily(getUserLogin(), salesmanId);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/survey/{surveyId}", method = RequestMethod.GET)
    public ResponseEntity<?> getSurveyReport(@PathVariable String surveyId) {
        SurveyResultDto dto = supervisorReportService.getSurveyReport(getUserLogin(), surveyId);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/survey/{surveyId}/export", method = RequestMethod.GET)
    public ResponseEntity<?> exportSurveyResult(@PathVariable String surveyId) throws IOException {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"Survey.xlsx\"");
        ResponseEntity<byte[]> result = new ResponseEntity<byte[]>(supervisorReportService.exportSurveyReport(
                getUserLogin(), surveyId), header, HttpStatus.OK);
        return result;
    }

    @RequestMapping(value = "/exhibition/{exhibitionId}", method = RequestMethod.GET)
    public ResponseEntity<?> getExhibitionReport(@PathVariable String exhibitionId) {
        ExhibitionReportDto dto = supervisorReportService.getExhibitionReport(getUserLogin(), exhibitionId);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/exhibition/{exhibitionId}/export", method = RequestMethod.GET)
    public ResponseEntity<?> exportExhibitionReport(@PathVariable String exhibitionId) throws IOException {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"Exhibition.xlsx\"");
        ResponseEntity<byte[]> result = new ResponseEntity<byte[]>(supervisorReportService.exportExhibitionReport(
                getUserLogin(), exhibitionId), header, HttpStatus.OK);
        return result;
    }
}
