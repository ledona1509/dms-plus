package com.viettel.backend.cache;


public interface I_Cache<K, V extends I_Cachable<V>>{

    public void clear();
    
    public V remove(K key);
    
    public V put(K key, V value);
    
    public V get(K key);
    
}
