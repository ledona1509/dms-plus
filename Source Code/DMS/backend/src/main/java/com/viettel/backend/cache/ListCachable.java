package com.viettel.backend.cache;

import java.util.ArrayList;
import java.util.Collection;

public class ListCachable<E> extends ArrayList<E> implements I_Cachable<ListCachable<E>> {

    private static final long serialVersionUID = 1L;

    public ListCachable(Collection<E> list) {
        super(list);
    }

    @Override
    public ListCachable<E> cloneForCache() {
        ArrayList<E> newList = new ArrayList<E>(this.size());
        for (E e : this) {
            newList.add(e);
        }
        return new ListCachable<E>(newList);
    }
    
}
