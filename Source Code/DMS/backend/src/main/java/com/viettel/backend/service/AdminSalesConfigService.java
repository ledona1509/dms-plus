package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.SalesConfigDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface AdminSalesConfigService extends Serializable {

    public SalesConfigDto getSalesConfigDto(UserLogin userLogin);
    
    public String save(UserLogin userLogin, SalesConfigDto dto);
    
}
