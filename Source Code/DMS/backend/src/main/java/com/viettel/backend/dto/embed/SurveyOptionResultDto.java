package com.viettel.backend.dto.embed;

import com.viettel.backend.dto.NameCategoryDto;

public class SurveyOptionResultDto extends NameCategoryDto {

    /**
     * 
     */
    private static final long serialVersionUID = 4085000556181222126L;
    
    public SurveyOptionResultDto() {
        super();
        this.result = 0;
    }
    
    private int result;
    
    public int getResult() {
        return result;
    }
    
    public void setResult(int result) {
        this.result = result;
    }

}
