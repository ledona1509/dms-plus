package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Survey;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.SurveySimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.SurveyRepository;
import com.viettel.backend.service.SupervisorSurveyService;

@Service
public class SupervisorSurveyServiceImpl extends AbstractSupervisorService implements SupervisorSurveyService {

    private static final long serialVersionUID = 4822917139294672054L;

    @Autowired
    private SurveyRepository surveyRepository;

    @Override
    public ListJson<SurveySimpleDto> getSurveys(UserLogin userLogin, String search, Pageable pageable) {
        checkIsSupervisor(userLogin);

        Sort sort = new Sort(Direction.DESC, Survey.COLUMNNAME_END_DATE_VALUE);
        List<Survey> surveys = surveyRepository.getSurveys(userLogin.getClientId(), search, pageable, sort);

        if (surveys == null || surveys.isEmpty()) {
            return ListJson.emptyList();
        }

        List<SurveySimpleDto> dtos = new ArrayList<SurveySimpleDto>(surveys.size());
        for (Survey survey : surveys) {
            dtos.add(new SurveySimpleDto(survey));
        }

        long size = Long.valueOf(dtos.size());
        if (pageable != null) {
            if (pageable.getPageNumber() > 0 || size == pageable.getPageSize()) {
                size = surveyRepository.count(userLogin.getClientId(), search);
            }
        }

        return new ListJson<SurveySimpleDto>(dtos, size);
    }

}
