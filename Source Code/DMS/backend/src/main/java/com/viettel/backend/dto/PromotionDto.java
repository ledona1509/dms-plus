package com.viettel.backend.dto;

import java.util.ArrayList;
import java.util.List;

import com.viettel.backend.domain.Promotion;
import com.viettel.backend.domain.embed.PromotionDetail;
import com.viettel.backend.dto.embed.ProductEmbedDto;
import com.viettel.backend.dto.embed.PromotionDetailDto;
import com.viettel.backend.engine.promotion.I_Promotion;

public class PromotionDto extends PromotionSimpleDto implements I_Promotion<String, ProductEmbedDto> {

    private static final long serialVersionUID = -1603649145435793534L;

    private List<PromotionDetailDto> details;

    public PromotionDto(Promotion promotion, String defaultProductPhoto) {
        super(promotion);

        if (promotion.getDetails() != null) {
            this.details = new ArrayList<PromotionDetailDto>(promotion.getDetails().size());
            for (PromotionDetail detail : promotion.getDetails()) {
                this.details.add(new PromotionDetailDto(detail, defaultProductPhoto));
            }
        }
    }

    public void setDetails(List<PromotionDetailDto> details) {
        this.details = details;
    }

    @SuppressWarnings("unchecked")
    public List<PromotionDetailDto> getDetails() {
        return details;
    }

}
