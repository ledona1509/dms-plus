package com.viettel.backend.restful.superadmin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.domain.Client;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.restful.BasicCategoryController;
import com.viettel.backend.service.BasicCategoryService;
import com.viettel.backend.service.SuperAdminClientService;

@RestController
@RequestMapping(value = "/superadmin/client")
public class ClientController extends
        BasicCategoryController<Client, NameCategoryDto, NameCategoryDto, NameCategoryDto> {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private SuperAdminClientService superAdminClientService;

    @Override
    protected BasicCategoryService<Client, NameCategoryDto, NameCategoryDto, NameCategoryDto> getService() {
        return superAdminClientService;
    }

}
