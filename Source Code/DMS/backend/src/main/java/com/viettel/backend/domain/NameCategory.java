package com.viettel.backend.domain;

/**
 * @author trung
 */
public abstract class NameCategory extends POSearchable {

    private static final long serialVersionUID = 5438408178211293923L;

    public static final String COLUMNNAME_NAME = "name";

    private String name;

    public NameCategory() {
        super();
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String[] getSearchValues() {
        return new String[] { name };
    }

}
