package com.viettel.backend.dto.report;

import java.io.Serializable;
import java.math.BigDecimal;

public class DaySalesReportDto implements Serializable {

    private static final long serialVersionUID = -4613095523957567352L;

    private String date;
    private BigDecimal revenue;
    private BigDecimal productivity;
    private long nbOrder;
    private long nbDistributor;
    private long nbCustomer;
    private long nbSalesman;

    public DaySalesReportDto(String date, BigDecimal revenue, BigDecimal productivity, long nbOrder,
            long nbDistributor, long nbCustomer, long nbSalesman) {
        super();

        this.date = date;
        this.revenue = revenue;
        this.productivity = productivity;
        this.nbOrder = nbOrder;
        this.nbDistributor = nbDistributor;
        this.nbCustomer = nbCustomer;
        this.nbSalesman = nbSalesman;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public long getNbOrder() {
        return nbOrder;
    }

    public void setNbOrder(long nbOrder) {
        this.nbOrder = nbOrder;
    }

    public long getNbDistributor() {
        return nbDistributor;
    }

    public void setNbDistributor(long nbDistributor) {
        this.nbDistributor = nbDistributor;
    }

    public long getNbCustomer() {
        return nbCustomer;
    }

    public void setNbCustomer(long nbCustomer) {
        this.nbCustomer = nbCustomer;
    }

    public long getNbSalesman() {
        return nbSalesman;
    }

    public void setNbSalesman(long nbSalesman) {
        this.nbSalesman = nbSalesman;
    }

}
