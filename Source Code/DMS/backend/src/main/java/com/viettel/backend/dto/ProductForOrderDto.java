package com.viettel.backend.dto;

import com.viettel.backend.domain.Product;

public class ProductForOrderDto extends ProductDto {

    /**
	 * 
	 */
    private static final long serialVersionUID = -7661118391476167272L;

    private int seqNo;

    public ProductForOrderDto(Product product, String defaultProductPhoto) {
        super(product, defaultProductPhoto);
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

}
