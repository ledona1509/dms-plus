package com.viettel.backend.restful.supervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.VisitFeedbackDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SupervisorFeedbackService;

@RestController(value="supervisorFeedbackController")
@RequestMapping(value = "/supervisor/feedback")
public class FeedbackController extends AbstractController {

    private static final long serialVersionUID = -3492046947585355471L;

    @Autowired
    private SupervisorFeedbackService supervisorFeedbackService;

    /**
     * Lấy danh sách feedback cho GSBH hiện tại - có phân trang (theo thời gian
     * và chưa đọc lên đầu)
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getFeedbacks(@RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size) {
        ListJson<VisitFeedbackDto> results = supervisorFeedbackService.getFeedbacks(getUserLogin(),
                getPageRequest(page, size));
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    /** Lấy chi tiết 1 feedback và cập nhật trạng thái đã đọc */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> readFeedback(@PathVariable String id) {
        VisitFeedbackDto dto = supervisorFeedbackService.readFeedback(getUserLogin(), id);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

}
