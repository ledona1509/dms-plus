package com.viettel.backend.domain.embed;

import java.util.List;

import com.viettel.backend.domain.NameCategory;

public class SurveyQuestion extends NameCategory {

    /**
     * 
     */
    private static final long serialVersionUID = 3227127774937212395L;

    private boolean multipleChoice;
    private boolean required;
    
    private List<SurveyOption> options;
    
    public boolean isMultipleChoice() {
        return multipleChoice;
    }
    
    public void setMultipleChoice(boolean multipleChoice) {
        this.multipleChoice = multipleChoice;
    }
    
    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
    
    public List<SurveyOption> getOptions() {
        return options;
    }
    
    public void setOptions(List<SurveyOption> options) {
        this.options = options;
    }
    
}
