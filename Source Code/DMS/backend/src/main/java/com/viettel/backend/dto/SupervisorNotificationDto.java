package com.viettel.backend.dto;

import java.io.Serializable;

public class SupervisorNotificationDto implements Serializable {

    private static final long serialVersionUID = -6508740234889263658L;

    private long nbFeedbackToRead;
    private long nbPurchaseToApprove;
    private long nbCustomerToApprove;

    public SupervisorNotificationDto(long nbFeedbackToRead, long nbPurchaseToApprove, long nbCustomerToApprove) {
        super();
        this.nbFeedbackToRead = nbFeedbackToRead;
        this.nbPurchaseToApprove = nbPurchaseToApprove;
        this.nbCustomerToApprove = nbCustomerToApprove;
    }

    public long getNbFeedbackToRead() {
        return nbFeedbackToRead;
    }

    public void setNbFeedbackToRead(long nbFeedbackToRead) {
        this.nbFeedbackToRead = nbFeedbackToRead;
    }

    public long getNbPurchaseToApprove() {
        return nbPurchaseToApprove;
    }

    public void setNbPurchaseToApprove(long nbPurchaseToApprove) {
        this.nbPurchaseToApprove = nbPurchaseToApprove;
    }

    public long getNbCustomerToApprove() {
        return nbCustomerToApprove;
    }

    public void setNbCustomerToApprove(long nbCustomerToApprove) {
        this.nbCustomerToApprove = nbCustomerToApprove;
    }

}
