package com.viettel.backend.restful.salesman;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ExhibitionDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SalesmanExhibitionService;

/**
 * @author thanh
 */
@RestController(value="salesmanExhibitionController")
@RequestMapping(value = "/salesman/exhibition")
public class ExhibitionController extends AbstractController {

    private static final long serialVersionUID = 8047048300443030912L;

    @Autowired
    private SalesmanExhibitionService salesmanExhibitionService;

    // LIST AVAILABLE FOR CUSTOMER
    @RequestMapping(value = "/available", method = RequestMethod.GET)
    public ResponseEntity<?> getSurveyForCustomer(@RequestParam(value = "customerId", required = true) String customerId) {
        ListJson<ExhibitionDto> results = salesmanExhibitionService
                .getExhibitionsByCustomer(getUserLogin(), customerId);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

}
