package com.viettel.backend.repository;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.UOM;

public interface UOMRepository extends BasicNameCategoryRepository<UOM> {

    public boolean existsByCode(ObjectId clientId, ObjectId id, String code);
    
}
