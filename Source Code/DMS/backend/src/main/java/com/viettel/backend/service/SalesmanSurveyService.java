package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.SurveyDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SalesmanSurveyService extends Serializable {

    public ListJson<SurveyDto> getSurveysByCustomer(UserLogin userLogin, String customerId);
    
}
