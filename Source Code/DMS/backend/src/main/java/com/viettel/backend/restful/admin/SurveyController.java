package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.domain.Survey;
import com.viettel.backend.dto.SurveyDto;
import com.viettel.backend.dto.SurveySimpleDto;
import com.viettel.backend.restful.BasicCategoryController;
import com.viettel.backend.service.AdminSurveyService;
import com.viettel.backend.service.BasicCategoryService;

@RestController(value="adminSurveyController")
@RequestMapping(value = "/admin/survey")
public class SurveyController extends
        BasicCategoryController<Survey, SurveySimpleDto, SurveyDto, SurveyDto> {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private AdminSurveyService adminSurveyService;

    @Override
    protected BasicCategoryService<Survey, SurveySimpleDto, SurveyDto, SurveyDto> getService() {
        return adminSurveyService;
    }

}
