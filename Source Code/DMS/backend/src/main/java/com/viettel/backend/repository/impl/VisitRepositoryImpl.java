package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.Visit;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.util.CriteriaUtils;
import com.viettel.backend.util.DateTimeUtils;

@Repository
public class VisitRepositoryImpl extends BasicRepositoryImpl<Visit> implements VisitRepository {

    private static final long serialVersionUID = -8401381924226354121L;

    @Override
    protected Criteria getDefaultCriteria() {
        Criteria isVisitCriteria = Criteria.where(Visit.COLUMNNAME_IS_VISIT).is(true);
        return isVisitCriteria;
    }

    // ** ANY VISIT STATUS **
    public Visit getVisitByCustomerToday(ObjectId clientId, ObjectId customerId) {
        SimpleDate today = DateTimeUtils.getToday();
        SimpleDate tomorow = DateTimeUtils.getTomorrow();

        Criteria dateCriteria = Criteria.where(Visit.COLUMNNAME_START_TIME_VALUE).gte(today.getValue())
                .lt(tomorow.getValue());
        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).is(customerId);

        Criteria criteria = CriteriaUtils.andOperator(customerCriteria, dateCriteria);

        return super._getFirst(clientId, true, criteria, null);
    }

    public Map<ObjectId, Visit> getMapVisitByCustomerIdsToday(ObjectId clientId, Collection<ObjectId> customerIds) {
        SimpleDate today = DateTimeUtils.getToday();
        SimpleDate tomorow = DateTimeUtils.getTomorrow();

        Criteria dateCriteria = Criteria.where(Visit.COLUMNNAME_START_TIME_VALUE).gte(today.getValue())
                .lt(tomorow.getValue());
        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).in(customerIds);

        Criteria criteria = CriteriaUtils.andOperator(customerCriteria, dateCriteria);

        List<Visit> visits = super._getList(clientId, true, criteria, null, null);

        if (visits == null || visits.isEmpty()) {
            return Collections.<ObjectId, Visit> emptyMap();
        }

        Map<ObjectId, Visit> results = new HashMap<ObjectId, Visit>();
        for (Visit visit : visits) {
            results.put(visit.getCustomer().getId(), visit);
        }

        return results;
    }

    // ** VISITED ONLY **
    public List<Visit> getVisitedsBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Period period,
            Sort sort) {
        Criteria visitStatusCriteria = Criteria.where(Visit.COLUMNNAME_VISIT_STATUS).is(Visit.VISIT_STATUS_VISITED);
        Criteria salesmanCriteria = Criteria.where(Visit.COLUMNNAME_SALESMAN_ID).in(salesmanIds);
        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Visit.COLUMNNAME_END_TIME_VALUE, period);

        Criteria criteria = CriteriaUtils.andOperator(visitStatusCriteria, salesmanCriteria, dateCriteria);

        return super._getList(clientId, true, criteria, null, sort);
    }

    public List<Visit> getVisitedsByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Period period,
            Sort sort) {
        Criteria visitStatusCriteria = Criteria.where(Visit.COLUMNNAME_VISIT_STATUS).is(Visit.VISIT_STATUS_VISITED);
        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).in(customerIds);
        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Visit.COLUMNNAME_END_TIME_VALUE, period);

        Criteria criteria = CriteriaUtils.andOperator(visitStatusCriteria, customerCriteria, dateCriteria);

        return super._getList(clientId, true, criteria, null, sort);
    }

    @Override
    public List<Visit> getLastVisitedsByCustomer(ObjectId clientId, ObjectId customerId, int size) {
        if (customerId == null) {
            return Collections.<Visit> emptyList();
        }

        Criteria visitStatusCriteria = Criteria.where(Visit.COLUMNNAME_VISIT_STATUS).is(Visit.VISIT_STATUS_VISITED);
        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).is(customerId);

        Criteria criteria = CriteriaUtils.andOperator(visitStatusCriteria, customerCriteria);

        PageRequest pageable = new PageRequest(0, size, Direction.DESC, Visit.COLUMNNAME_END_TIME_VALUE);

        return super._getList(clientId, true, criteria, pageable, null);
    }

}
