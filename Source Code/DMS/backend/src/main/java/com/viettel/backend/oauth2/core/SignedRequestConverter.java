package com.viettel.backend.oauth2.core;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author thanh
 */
public interface SignedRequestConverter {
    
    public final String PARAMETER_TICKET = "ticket";
    
    public final String URL = "url";
    
    public final String QUERY = "query";

    public final String EXP = "exp";

    /**
     * @return redirect URL
     */
    String signRequest(HttpServletRequest request, HttpServletResponse response, Long validity) throws IOException;

    Map<String, Object> verifyRequest(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
