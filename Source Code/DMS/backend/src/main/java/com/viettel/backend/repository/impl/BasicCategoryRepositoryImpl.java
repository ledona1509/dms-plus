package com.viettel.backend.repository.impl;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;

import com.viettel.backend.domain.POSearchable;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.util.CriteriaUtils;

public abstract class BasicCategoryRepositoryImpl<D extends POSearchable> extends BasicRepositoryImpl<D> implements
        BasicCategoryRepository<D> {

    private static final long serialVersionUID = -8958421384263600937L;

    @Override
    protected Boolean getDefaultDraftFilter() {
        return false;
    }

    @Override
    public List<D> getAll(ObjectId clientId) {
        return _getListWithDraft(clientId, true, false, null, null, null);
    }

    @Override
    public List<D> getListWithDraft(ObjectId clientId, String search, Boolean draft, Pageable pageable, Sort sort) {
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);

        return _getListWithDraft(clientId, true, draft, searchCriteria, pageable, sort);
    }

    @Override
    public long countWithDraft(ObjectId clientId, String search, Boolean draft) {
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);

        return _countWithDraft(clientId, true, draft, searchCriteria);
    }
    
    @Override
    public D getByIdWithDraft(ObjectId clientId, ObjectId id) {
        return _getByIdWithDraft(clientId, true, id);
    }

    @Override
    public boolean enable(ObjectId clientId, ObjectId id) {
        return _enableDraft(clientId, id);
    }

}
