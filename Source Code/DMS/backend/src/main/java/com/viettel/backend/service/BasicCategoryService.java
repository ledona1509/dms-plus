package com.viettel.backend.service;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;

import com.viettel.backend.domain.POSearchable;
import com.viettel.backend.dto.DTO;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.oauth2.core.UserLogin;

public interface BasicCategoryService<DOMAIN extends POSearchable, SIMPLEDTO extends DTO, DETAILDTO extends SIMPLEDTO, CREATEDTO extends Serializable>
        extends Serializable {

    /** Get all active PO */
    public ListJson<SIMPLEDTO> getAll(UserLogin userLogin);

    /** Get list active PO */
    public ListJson<SIMPLEDTO> getList(UserLogin userLogin, String search, Boolean draft, Pageable pageable);

    /** Find active PO */
    public DETAILDTO getById(UserLogin userLogin, String id);

    /** Create PO */
    public ObjectId create(UserLogin userLogin, CREATEDTO dto);

    /** Update PO */
    public ObjectId update(UserLogin userLogin, String id, CREATEDTO dto);

    /** Enable PO */
    public boolean enable(UserLogin userLogin, String id);

    /** Delete PO */
    public boolean delete(UserLogin userLogin, String id);

}
