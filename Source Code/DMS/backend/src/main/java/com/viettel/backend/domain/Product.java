package com.viettel.backend.domain;

import java.math.BigDecimal;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.engine.promotion.I_Product;

@Document(collection = "Product")
public class Product extends NameCategory implements I_Product<ObjectId> {

    private static final long serialVersionUID = -4897491701019327268L;

    public static final String COLUMNNAME_CATEGORY_ID = "productCategory.id";
    public static final String COLUMNNAME_DESCRIPTION = "description";
    public static final String COLUMNNAME_CODE = "code";
    public static final String COLUMNNAME_PRICE = "price";
    public static final String COLUMNNAME_UOM_CODE = "uom.code";
    public static final String COLUMNNAME_UOM_NAME = "uom.name";
    public static final String COLUMNNAME_UOM_ID = "uom.id";
    public static final String COLUMNNAME_PHOTO = "photo";

    private String code;
    private String photo;
    private UOM uom;
    private BigDecimal price;
    private BigDecimal output;
    private ProductCategory productCategory;
    private String description;

    public Product() {
        super();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public UOM getUom() {
        return uom;
    }

    public void setUom(UOM uom) {
        this.uom = uom;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getOutput() {
        return output;
    }

    public void setOutput(BigDecimal output) {
        this.output = output;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String[] getSearchValues() {
        return new String[] { getCode(), getName() };
    }
}
