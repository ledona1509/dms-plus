package com.viettel.backend.dto;

import java.util.List;

import com.viettel.backend.dto.embed.CustomerEmbedDto;

/**
 * @author thanh
 */
public class ExhibitionWithParticipantsDto extends ExhibitionDto {

    private static final long serialVersionUID = 4294936505380930621L;

    private List<CustomerEmbedDto> participants;

    public List<CustomerEmbedDto> getParticipants() {
        return participants;
    }

    public void setParticipants(List<CustomerEmbedDto> participants) {
        this.participants = participants;
    }
    
}
