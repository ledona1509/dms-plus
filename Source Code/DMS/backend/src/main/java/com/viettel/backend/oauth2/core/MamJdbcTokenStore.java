package com.viettel.backend.oauth2.core;

import javax.sql.DataSource;

import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

public class MamJdbcTokenStore extends JdbcTokenStore {
	
	private static final String ACCESS_TOKEN_TABLENAME = "o_oauth_access_token";
	
	private static final String REFRESH_TOKEN_TABLENAME = "o_oauth_refresh_token";
	
	private static final String ACCESS_TOKEN_INSERT_STATEMENT = String.format("insert into %s (token_id, token, authentication_id, user_name, client_id, authentication, refresh_token) values (?, ?, ?, ?, ?, ?, ?)", ACCESS_TOKEN_TABLENAME);

	private static final String ACCESS_TOKEN_SELECT_STATEMENT = String.format("select token_id, token from %s where token_id = ?", ACCESS_TOKEN_TABLENAME);

	private static final String ACCESS_TOKEN_AUTHENTICATION_SELECT_STATEMENT = String.format("select token_id, authentication from %s where token_id = ?", ACCESS_TOKEN_TABLENAME);

	private static final String ACCESS_TOKEN_FROM_AUTHENTICATION_SELECT_STATEMENT = String.format("select token_id, token from %s where authentication_id = ?", ACCESS_TOKEN_TABLENAME);

	private static final String ACCESS_TOKENS_FROM_USERNAME_AND_CLIENT_SELECT_STATEMENT = String.format("select token_id, token from %s where user_name = ? and client_id = ?", ACCESS_TOKEN_TABLENAME);

	private static final String ACCESS_TOKENS_FROM_CLIENTID_SELECT_STATEMENT = String.format("select token_id, token from %s where client_id = ?", ACCESS_TOKEN_TABLENAME);

	private static final String ACCESS_TOKEN_DELETE_STATEMENT = String.format("delete from %s where token_id = ?", ACCESS_TOKEN_TABLENAME);

	private static final String ACCESS_TOKEN_DELETE_FROM_REFRESH_TOKEN_STATEMENT = String.format("delete from %s where refresh_token = ?", ACCESS_TOKEN_TABLENAME);

	private static final String REFRESH_TOKEN_INSERT_STATEMENT = String.format("insert into %s (token_id, token, authentication) values (?, ?, ?)", REFRESH_TOKEN_TABLENAME);

	private static final String REFRESH_TOKEN_SELECT_STATEMENT = String.format("select token_id, token from %s where token_id = ?", REFRESH_TOKEN_TABLENAME);

	private static final String REFRESH_TOKEN_AUTHENTICATION_SELECT_STATEMENT = String.format("select token_id, authentication from %s where token_id = ?", REFRESH_TOKEN_TABLENAME);

	private static final String REFRESH_TOKEN_DELETE_STATEMENT = String.format("delete from %s where token_id = ?", REFRESH_TOKEN_TABLENAME);

	public MamJdbcTokenStore(DataSource dataSource) {
		super(dataSource);
		
		setInsertAccessTokenSql(ACCESS_TOKEN_INSERT_STATEMENT);

		setSelectAccessTokenSql(ACCESS_TOKEN_SELECT_STATEMENT);

		setSelectAccessTokenAuthenticationSql(ACCESS_TOKEN_AUTHENTICATION_SELECT_STATEMENT);

		setSelectAccessTokenFromAuthenticationSql(ACCESS_TOKEN_FROM_AUTHENTICATION_SELECT_STATEMENT);

		setSelectAccessTokensFromUserNameAndClientIdSql(ACCESS_TOKENS_FROM_USERNAME_AND_CLIENT_SELECT_STATEMENT);

		setSelectAccessTokensFromClientIdSql(ACCESS_TOKENS_FROM_CLIENTID_SELECT_STATEMENT);

		setDeleteAccessTokenSql(ACCESS_TOKEN_DELETE_STATEMENT);

		setDeleteAccessTokenFromRefreshTokenSql(ACCESS_TOKEN_DELETE_FROM_REFRESH_TOKEN_STATEMENT);

		setInsertRefreshTokenSql(REFRESH_TOKEN_INSERT_STATEMENT);

		setSelectRefreshTokenSql(REFRESH_TOKEN_SELECT_STATEMENT);

		setSelectRefreshTokenAuthenticationSql(REFRESH_TOKEN_AUTHENTICATION_SELECT_STATEMENT);

		setDeleteRefreshTokenSql(REFRESH_TOKEN_DELETE_STATEMENT);
	}
}
