package com.viettel.backend.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.embed.CustomerScheduleDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SupervisorVisitScheduleService {

    public ListJson<CustomerScheduleDto> getCustomerSchedulesByDistributor(UserLogin userLogin, String customerSearch,
            String distributorId, String salesmanId, boolean all, Pageable pageable);

    public void saveByDistributor(UserLogin userLogin, String distributorId, List<CustomerScheduleDto> dtos);

    public void saveByCustomer(UserLogin userLogin, String customerId, CustomerScheduleDto dto);

}
