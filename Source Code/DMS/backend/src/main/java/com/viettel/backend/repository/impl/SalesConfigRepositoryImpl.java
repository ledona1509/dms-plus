package com.viettel.backend.repository.impl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.viettel.backend.cache.CacheManager;
import com.viettel.backend.domain.SalesConfig;
import com.viettel.backend.repository.SalesConfigRepository;

@Repository
public class SalesConfigRepositoryImpl extends AbstractRepository<SalesConfig> implements SalesConfigRepository {

    private static final long serialVersionUID = -1702535078849812226L;

    @Autowired
    private CacheManager cacheManager;
    
    @Override
    public SalesConfig getSalesConfig(ObjectId clientId) {
        SalesConfig salesConfig = cacheManager.getSalesConfigCache().get(clientId);
        if (salesConfig == null) {
            salesConfig = _getFirst(clientId, true, null, null);
            cacheManager.getSalesConfigCache().put(clientId, salesConfig);
        }
        return salesConfig;
    }

    @Override
    public SalesConfig save(ObjectId clientId, SalesConfig domain) {
        SalesConfig salesConfig = _save(clientId, domain);
        cacheManager.getSalesConfigCache().put(clientId, salesConfig);
        return salesConfig;
    }

}
