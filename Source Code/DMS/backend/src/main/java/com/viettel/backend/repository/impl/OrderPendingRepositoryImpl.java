package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.repository.OrderPendingRepository;
import com.viettel.backend.util.CriteriaUtils;

@Repository
public class OrderPendingRepositoryImpl extends BasicRepositoryImpl<Order> implements OrderPendingRepository {

    private static final long serialVersionUID = -2035785564064751389L;

    @Override
    protected Criteria getDefaultCriteria() {
        Criteria isOrderCriteria = Criteria.where(Visit.COLUMNNAME_IS_ORDER).is(true);
        return isOrderCriteria;
    }

    @Override
    public List<Order> getOrderBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Integer approveStatus,
            String searchCode, Pageable pageable, Sort sort) {
        if (salesmanIds == null || salesmanIds.isEmpty()) {
            return Collections.<Order> emptyList();
        }

        Criteria salesmanCriteria = Criteria.where(Visit.COLUMNNAME_SALESMAN_ID).in(salesmanIds);

        Criteria approveStatusCriteria = null;
        if (approveStatus != null) {
            approveStatusCriteria = Criteria.where(Visit.COLUMNNAME_APPROVE_STATUS).is(approveStatus);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Visit.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils.andOperator(salesmanCriteria, approveStatusCriteria, searchCriteria);

        return super._getList(clientId, true, criteria, pageable, sort);
    }

    @Override
    public long countOrderBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Integer approveStatus,
            String searchCode) {
        if (salesmanIds == null || salesmanIds.isEmpty()) {
            return 0l;
        }

        Criteria salesmanCriteria = Criteria.where(Visit.COLUMNNAME_SALESMAN_ID).in(salesmanIds);

        Criteria approveStatusCriteria = null;
        if (approveStatus != null) {
            approveStatusCriteria = Criteria.where(Visit.COLUMNNAME_APPROVE_STATUS).is(approveStatus);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Visit.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils.andOperator(salesmanCriteria, approveStatusCriteria, searchCriteria);

        return super._count(clientId, true, criteria);
    }

    @Override
    public List<Order> getOrderByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Integer approveStatus,
            Period period, String searchCode, Pageable pageable, Sort sort) {
        if (customerIds == null || customerIds.isEmpty()) {
            return Collections.<Order> emptyList();
        }
        
        Criteria dateCriteria = null;
        if (period != null) {
            dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_CREATED_TIME_VALUE, period);
        }

        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).in(customerIds);
        
        Criteria approveStatusCriteria = null;
        if (approveStatus != null) {
            approveStatusCriteria = Criteria.where(Visit.COLUMNNAME_APPROVE_STATUS).is(approveStatus);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Visit.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils.andOperator(dateCriteria, customerCriteria, approveStatusCriteria, searchCriteria);

        return super._getList(clientId, true, criteria, pageable, sort);
    }

    @Override
    public long countOrderByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Integer approveStatus,
            Period period, String searchCode) {
        if (customerIds == null || customerIds.isEmpty()) {
            return 0l;
        }
        
        Criteria dateCriteria = null;
        if (period != null) {
            dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_CREATED_TIME_VALUE, period);
        }

        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).in(customerIds);
        
        Criteria approveStatusCriteria = null;
        if (approveStatus != null) {
            approveStatusCriteria = Criteria.where(Visit.COLUMNNAME_APPROVE_STATUS).is(approveStatus);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Visit.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils.andOperator(dateCriteria, customerCriteria, approveStatusCriteria, searchCriteria);

        return super._count(clientId, true, criteria);
    }

    @Override
    public List<Order> getOrderByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds,
            Integer approveStatus, String searchCode, Pageable pageable, Sort sort) {

        Assert.notEmpty(distributorIds);
        Criteria distributorCriteria = null;
        if (distributorIds != null) {
            distributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);
        }

        Criteria approveStatusCriteria = null;
        if (approveStatus != null) {
            approveStatusCriteria = Criteria.where(Visit.COLUMNNAME_APPROVE_STATUS).is(approveStatus);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Visit.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, approveStatusCriteria, searchCriteria);

        return super._getList(clientId, true, criteria, pageable, sort);
    }

    @Override
    public long countOrderByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, Integer approveStatus,
            String searchCode) {

        Assert.notEmpty(distributorIds);
        Criteria distributorCriteria = null;
        if (distributorIds != null) {
            distributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);
        }

        Criteria approveStatusCriteria = null;
        if (approveStatus != null) {
            approveStatusCriteria = Criteria.where(Visit.COLUMNNAME_APPROVE_STATUS).is(approveStatus);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Visit.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, approveStatusCriteria, searchCriteria);

        return super._count(clientId, true, criteria);
    }
}
