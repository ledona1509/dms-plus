package com.viettel.backend.dto;

import java.util.List;

/**
 * @author thanh
 */
public class ExhibitionWithRatingDto extends ExhibitionSimpleDto {

    private static final long serialVersionUID = 4294936505380930621L;

    private List<ExhibitionCategoryWithRatingDto> category;
    
    public List<ExhibitionCategoryWithRatingDto> getCategory() {
        return category;
    }

    public void setCategory(List<ExhibitionCategoryWithRatingDto> category) {
        this.category = category;
    }

    /**
     * @author thanh
     */
    public static class ExhibitionCategoryWithRatingDto {

        private String id;
        private String name;
        private List<ExhibitionItemWithRatingDto> details;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<ExhibitionItemWithRatingDto> getDetails() {
            return details;
        }

        public void setDetails(List<ExhibitionItemWithRatingDto> details) {
            this.details = details;
        }

    }

    /**
     * @author thanh
     */
    public static class ExhibitionItemWithRatingDto {

        private String id;
        private String name;
        private float rate;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getRate() {
            return rate;
        }

        public void setRate(float rate) {
            this.rate = rate;
        }

    }
}
