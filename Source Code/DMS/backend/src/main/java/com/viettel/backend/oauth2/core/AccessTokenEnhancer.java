package com.viettel.backend.oauth2.core;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

/**
 * @author thanh
 */
public class AccessTokenEnhancer implements TokenEnhancer {
    
    public static final String KEY_USER = "user";
    public static final String KEY_LANGUAGE = "lang";
    
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken,
            OAuth2Authentication authentication) {
        DefaultOAuth2AccessToken newToken = new DefaultOAuth2AccessToken(accessToken);
        if (!authentication.isClientOnly()) {
        	Map<String, Object> info = new LinkedHashMap<String, Object>(accessToken.getAdditionalInformation());
            //info.put(KEY_USER, ((UserAuthenticationToken)authentication.getUserAuthentication()).getUser());
        	
        	// If grant_type is implicit, pass current user's language selection
        	if ("implicit".equals(authentication.getOAuth2Request().getGrantType())) {
        	    info.put(KEY_LANGUAGE, LocaleContextHolder.getLocaleContext().getLocale());
        	}
            newToken.setAdditionalInformation(info);
        }
        return newToken;
    }
}
