package com.viettel.backend.engine.promotion;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromotionReward<ID extends Serializable, P extends I_Product<ID>> implements Serializable {

    private static final long serialVersionUID = 7495282081559264914L;
    
    private BigDecimal percentage;
    private BigDecimal quantity;
    private P product;

    public PromotionReward() {
        super();
    }

    public PromotionReward(BigDecimal percentage, BigDecimal quantity, P product) {
        super();
        this.percentage = percentage;
        this.quantity = quantity;
        this.product = product;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public P getProduct() {
        return product;
    }

    public void setProduct(P product) {
        this.product = product;
    }

}
