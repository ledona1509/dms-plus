package com.viettel.backend.restful.supervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.SupervisorNotificationDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SupervisorNotificationService;

@RestController
@RequestMapping(value = "/supervisor/notification")
public class NotificationController extends AbstractController {

    private static final long serialVersionUID = -3492046947585355471L;

    @Autowired
    private SupervisorNotificationService supervisorNotificationService;

    /** Lây các thông tin notification của GSBH */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getNotification() {
        SupervisorNotificationDto dto = supervisorNotificationService.getNotification(getUserLogin());
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

}
