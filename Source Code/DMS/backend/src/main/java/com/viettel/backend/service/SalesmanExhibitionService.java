package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.ExhibitionDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SalesmanExhibitionService extends Serializable {

    public ListJson<ExhibitionDto> getExhibitionsByCustomer(UserLogin userLogin, String customerId);

}
