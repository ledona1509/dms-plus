package com.viettel.backend.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.POSearchable;

public interface BasicCategoryRepository<D extends POSearchable> extends BasicRepository<D> {

    /** draft = true */
    public List<D> getAll(ObjectId clientId);

    /** draft = both */
    public List<D> getListWithDraft(ObjectId clientId, String search, Boolean draft, Pageable pageable, Sort sort);

    public long countWithDraft(ObjectId clientId, String search, Boolean draft);

    public D getByIdWithDraft(ObjectId clientId, ObjectId id);

    public boolean enable(ObjectId clientId, ObjectId id);

}
