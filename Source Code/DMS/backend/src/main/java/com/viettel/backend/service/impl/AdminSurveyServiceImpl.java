package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Survey;
import com.viettel.backend.domain.embed.SurveyOption;
import com.viettel.backend.domain.embed.SurveyQuestion;
import com.viettel.backend.dto.SurveyDto;
import com.viettel.backend.dto.SurveySimpleDto;
import com.viettel.backend.dto.embed.SurveyOptionDto;
import com.viettel.backend.dto.embed.SurveyQuestionDto;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.SurveyRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminSurveyService;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class AdminSurveyServiceImpl extends BasicCategoryServiceImpl<Survey, SurveySimpleDto, SurveyDto, SurveyDto>
        implements AdminSurveyService {

    private static final long serialVersionUID = -5771173067596328856L;

    @Autowired
    private SurveyRepository surveyRepository;

    @Override
    public BasicCategoryRepository<Survey> getRepository() {
        return surveyRepository;
    }
    
    @Override
    protected Sort getSort() {
        return new Sort(Direction.DESC, Survey.COLUMNNAME_END_DATE_VALUE);
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN);
    }
    
    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.ADMIN_ONLY;
    }

    @Override
    public Survey createDomain(UserLogin userLogin, SurveyDto createdto) {
        Survey domain = new Survey();
        initPOWhenCreate(Survey.class, userLogin, domain);
        domain.setDraft(true);

        updateDomain(userLogin, domain, createdto);

        return domain;
    }

    @Override
    public void updateDomain(UserLogin userLogin, Survey domain, SurveyDto createdto) {
        if (!domain.isDraft()) {
            throw new BusinessException(ExceptionCode.CANNOT_UPDATE_NO_DRAFT_DATA);
        }

        checkMandatoryParams(createdto, createdto.getName(), createdto.getStartDate(), createdto.getEndDate(),
                createdto.getQuestions());
        
        SimpleDate startDate = SimpleDate.createByIsoDate(createdto.getStartDate(), null);
        SimpleDate endDate = SimpleDate.createByIsoDate(createdto.getEndDate(), null);
        if (startDate == null || endDate == null || startDate.compareTo(endDate) > 0) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        domain.setName(createdto.getName());
        domain.setStartDate(SimpleDate.createByIsoDate(createdto.getStartDate(), null));
        domain.setEndDate(SimpleDate.createByIsoDate(createdto.getEndDate(), null));
        domain.setRequired(true);

        List<SurveyQuestion> questions = new ArrayList<SurveyQuestion>(createdto.getQuestions().size());
        for (SurveyQuestionDto questionDto : createdto.getQuestions()) {
            SurveyQuestion question = new SurveyQuestion();
            question.setId(ObjectIdUtils.getObjectId(questionDto.getId(), new ObjectId()));
            question.setName(questionDto.getName());
            question.setMultipleChoice(questionDto.isMultipleChoice());
            question.setRequired(questionDto.isRequired());

            List<SurveyOptionDto> optionDtos = questionDto.getOptions();
            if (optionDtos != null && !optionDtos.isEmpty()) {
                List<SurveyOption> options = new ArrayList<SurveyOption>(optionDtos.size());
                for (SurveyOptionDto optionDto : optionDtos) {
                    SurveyOption option = new SurveyOption();
                    option.setId(ObjectIdUtils.getObjectId(optionDto.getId(), new ObjectId()));
                    option.setName(optionDto.getName());

                    options.add(option);
                }

                question.setOptions(options);
            }
            questions.add(question);
        }

        domain.setQuestions(questions);
    }

    @Override
    public SurveySimpleDto createSimpleDto(UserLogin userLogin, Survey domain) {
        return new SurveySimpleDto(domain);
    }

    @Override
    public SurveyDto createDetailDto(UserLogin userLogin, Survey domain) {
        return new SurveyDto(domain);
    }

}
