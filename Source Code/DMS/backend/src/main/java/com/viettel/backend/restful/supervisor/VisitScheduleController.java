package com.viettel.backend.restful.supervisor;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.embed.CustomerScheduleDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.SupervisorVisitScheduleService;

@RestController
@RequestMapping(value = "/supervisor/visitschedule")
public class VisitScheduleController extends AbstractController {

    private static final long serialVersionUID = 6710761971578716798L;

    @Autowired
    private SupervisorVisitScheduleService service;

    /** Lấy danh sách lịch trình tuyến */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomerSchedulesByDistributor(
            @RequestParam(value = "distributorId", required = true) String distributorId,
            @RequestParam(value = "salesmanId", required = false) String salesmanId,
            @RequestParam(value = "all", defaultValue = "false") boolean all,
            @RequestParam(value = "q", required = false, defaultValue = "") String search,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        ListJson<CustomerScheduleDto> list = service.getCustomerSchedulesByDistributor(getUserLogin(), search,
                distributorId, salesmanId, all, getPageRequest(page, size));
        return new Envelope(list).toResponseEntity(HttpStatus.OK);
    }

    /** Cập nhật danh sách lịch trình tuyến theo NPP */
    @RequestMapping(value = "/bydistributor", method = RequestMethod.PUT)
    public ResponseEntity<?> saveByDistributor(
            @RequestParam(value = "distributorId", required = true) String distributorId,
            @RequestBody @Valid List<CustomerScheduleDto> dtos) {
        service.saveByDistributor(getUserLogin(), distributorId, dtos);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

    /** Cập nhật lịch trình tuyến cho một khách hàng */
    @RequestMapping(value = "/bycustomer", method = RequestMethod.PUT)
    public ResponseEntity<?> saveByCustomer(@RequestParam(required = true) String customerId,
            @RequestBody @Valid CustomerScheduleDto dto) {
        service.saveByCustomer(getUserLogin(), customerId, dto);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

}
