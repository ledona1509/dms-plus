package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.Order;
import com.viettel.backend.entity.SimpleDate.Period;

public interface OrderRepository extends BasicRepository<Order> {

    public Order getOrderBySalesmanAndId(ObjectId clientId, ObjectId salesmanId, ObjectId orderId);

    public List<Order> getOrderBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Period period,
            String searchCode, Pageable pageable, Sort sort);

    public long countOrderBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Period period,
            String searchCode);

    public List<Order> getOrderBySalesmanAndCustomers(ObjectId clientId, ObjectId salesmanId,
            Collection<ObjectId> customerIds, Period period, String searchCode, Pageable pageable, Sort sort);

    public long countOrderBySalesmanAndCustomers(ObjectId clientId, ObjectId salesmanId,
            Collection<ObjectId> customerIds, Period period, String searchCode);

    public List<Order> getOrderByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, Period period,
            String searchCode, Pageable pageable, Sort sort);

    public long countOrderByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, Period period,
            String searchCode);

    public List<Order> getOrderByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Period period,
            String searchCode, Pageable pageable, Sort sort);

    public long countOrderByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Period period,
            String searchCode);

    public List<Order> getLastOrderByCustomer(ObjectId clientId, ObjectId customerId, int size);

    public List<Order> getOrders(ObjectId clientId, Period period, ObjectId distributorId, ObjectId customerId,
            ObjectId salesmanId);

}
