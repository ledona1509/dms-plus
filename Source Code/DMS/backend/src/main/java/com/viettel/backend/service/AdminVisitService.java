package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.VisitInfoDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface AdminVisitService extends Serializable {

    public VisitInfoDto getVisit(UserLogin userLogin, String id);

}
