package com.viettel.backend.engine.promotion;

import java.io.Serializable;
import java.util.List;

public interface I_Order<ID extends Serializable, P extends I_Product<ID>> {

    public <DETAIL extends I_OrderDetail<ID, P>> List<DETAIL> getDetails();

    public <RESULT extends I_PromotionResult<ID, P>> List<RESULT> getPromotionResults();

}
