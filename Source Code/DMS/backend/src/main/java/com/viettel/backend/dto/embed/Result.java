package com.viettel.backend.dto.embed;

import java.io.Serializable;
import java.math.BigDecimal;

public class Result implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5834285538007891838L;

    private BigDecimal plan;
    private BigDecimal actual;

    public Result() {
        this(-1, -1);
    }

    public Result(BigDecimal plan, BigDecimal actual) {
        super();
        this.plan = (plan == null ? BigDecimal.ZERO : plan);
        this.actual = (actual == null ? BigDecimal.ZERO : actual);
    }
    
    public Result(Integer plan, Integer actual) {
        super();
        this.plan = plan == null ? null : new BigDecimal(plan);
        this.actual = actual == null ? null : new BigDecimal(actual);
    }
    
    public Result(Double plan, Double actual) {
        super();
        this.plan = plan == null ? null : new BigDecimal(plan);
        this.actual = actual == null ? null : new BigDecimal(actual);
    }

    public BigDecimal getPlan() {
        return plan;
    }

    public void setPlan(BigDecimal plan) {
        this.plan = plan;
    }

    public BigDecimal getActual() {
        return actual;
    }

    public void setActual(BigDecimal actual) {
        this.actual = actual;
    }

    public BigDecimal getRemaining() {
        if (plan != null && actual != null) {
            return this.plan.subtract(this.actual);
        } else {
            return null;
        }
    }

    public Integer getPercentage() {
        // KHONG CO PLAN
        if (plan == null || plan.compareTo(BigDecimal.ZERO) <= 0) {
            if (actual == null || actual.compareTo(BigDecimal.ZERO) <= 0) {
                return 0;
            } else {
                return 100;
            }
        } else {
            if (actual == null || actual.compareTo(BigDecimal.ZERO) <= 0) {
                return 0;
            } else {
                BigDecimal percentage = this.actual.divide(this.plan, 2, BigDecimal.ROUND_UP).multiply(new BigDecimal(100));
                return percentage.intValue();
            }
        }
    }

}
