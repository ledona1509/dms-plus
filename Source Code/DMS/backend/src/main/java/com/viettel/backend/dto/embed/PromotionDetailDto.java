package com.viettel.backend.dto.embed;

import com.viettel.backend.domain.embed.PromotionDetail;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.engine.promotion.I_PromotionDetail;
import com.viettel.backend.engine.promotion.PromotionCondition;
import com.viettel.backend.engine.promotion.PromotionReward;

public class PromotionDetailDto extends NameCategoryDto implements I_PromotionDetail<String, ProductEmbedDto> {

    private static final long serialVersionUID = -3036842222216072306L;

    private int type;
    private PromotionCondition<String, ProductEmbedDto> condition;
    private PromotionReward<String, ProductEmbedDto> reward;

    public PromotionDetailDto() {
        super();
    }

    public PromotionDetailDto(PromotionDetail promotionDetail, String defaultProductPhoto) {
        super();

        this.type = promotionDetail.getType();
        if (promotionDetail.getCondition() != null) {

        }

        if (promotionDetail.getCondition() != null) {
            this.condition = new PromotionCondition<String, ProductEmbedDto>();
            if (promotionDetail.getCondition().getProduct() != null) {
                this.condition.setProduct(new ProductEmbedDto(promotionDetail.getCondition().getProduct(),
                        defaultProductPhoto));
            }
            this.condition.setQuantity(promotionDetail.getCondition().getQuantity());
        }

        if (promotionDetail.getReward() != null) {
            this.reward = new PromotionReward<String, ProductEmbedDto>();
            if (promotionDetail.getReward().getProduct() != null) {
                this.reward.setProduct(new ProductEmbedDto(promotionDetail.getReward().getProduct(),
                        defaultProductPhoto));
            }
            this.reward.setPercentage(promotionDetail.getReward().getPercentage());
            this.reward.setQuantity(promotionDetail.getReward().getQuantity());
        }
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public PromotionCondition<String, ProductEmbedDto> getCondition() {
        return condition;
    }

    public void setCondition(PromotionCondition<String, ProductEmbedDto> condition) {
        this.condition = condition;
    }

    public PromotionReward<String, ProductEmbedDto> getReward() {
        return reward;
    }

    public void setReward(PromotionReward<String, ProductEmbedDto> reward) {
        this.reward = reward;
    }

}
