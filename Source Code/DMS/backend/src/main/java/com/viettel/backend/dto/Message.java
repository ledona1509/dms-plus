package com.viettel.backend.dto;

import java.util.Date;

public class Message {

	private Object data;
	private Date clientSentTime;
	private int id;

	public Message() {

	}

	public Message(int id, Object data) {
		this.id = id;
		this.data = data;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getClientSentTime() {
		return clientSentTime;
	}

	public void setClientSentTime(Date clientSentTime) {
		this.clientSentTime = clientSentTime;
	}


}
