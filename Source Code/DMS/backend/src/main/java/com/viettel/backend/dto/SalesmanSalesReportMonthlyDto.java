package com.viettel.backend.dto;

import java.io.Serializable;

import com.viettel.backend.dto.embed.Result;

public class SalesmanSalesReportMonthlyDto implements Serializable {

    private static final long serialVersionUID = 3991523494712638803L;

    private String salesman;
    private String salesmanId;
    private int numberCustomer;
    private Result revenue;
    private Result output;

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

    public int getNumberCustomer() {
        return numberCustomer;
    }

    public void setNumberCustomer(int numberCustomer) {
        this.numberCustomer = numberCustomer;
    }

    public Result getRevenue() {
        return revenue;
    }

    public void setRevenue(Result revenue) {
        this.revenue = revenue;
    }

    public Result getOutput() {
        return output;
    }

    public void setOutput(Result output) {
        this.output = output;
    }

}
