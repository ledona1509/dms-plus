package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.domain.ProductCategory;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.restful.BasicCategoryController;
import com.viettel.backend.service.AdminProductCategoryService;
import com.viettel.backend.service.BasicCategoryService;

@RestController
@RequestMapping(value = "/admin/productcategory")
public class ProductCategoryController extends
        BasicCategoryController<ProductCategory, NameCategoryDto, NameCategoryDto, NameCategoryDto> {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private AdminProductCategoryService adminProductCategoryService;

    @Override
    protected BasicCategoryService<ProductCategory, NameCategoryDto, NameCategoryDto, NameCategoryDto> getService() {
        return adminProductCategoryService;
    }

}
