package com.viettel.backend.oauth2.core;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * @author thanh
 */
public class ExtraSignedRequestConverter extends DefaultSignedRequestConverter {
    
    public static final String USER = "user";

    @Override
    protected Map<String, Object> extractRequest(HttpServletRequest request, Long validity) {
        Map<String, Object> info = super.extractRequest(request, validity);
        // TODO: Declare variable
        if (SecurityContextHelper.getCurrentUser() != null) {
            info.put(USER, SecurityContextHelper.getCurrentUser());
        }
        return info;
    }
}
