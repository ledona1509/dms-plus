package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.Distributor;

public interface DistributorRepository extends BasicCategoryRepository<Distributor> {

    public List<Distributor> getDistributorsBySupervisors(ObjectId clientId, Collection<ObjectId> supervisorIds);

    public Set<ObjectId> getDistributorIdsBySupervisors(ObjectId clientId, Collection<ObjectId> supervisorIds);
    
    public boolean checkSupervisorUsed(ObjectId clientId, ObjectId superviosrId);

}
