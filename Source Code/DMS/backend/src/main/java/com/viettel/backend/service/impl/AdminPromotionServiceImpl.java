package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Product;
import com.viettel.backend.domain.Promotion;
import com.viettel.backend.domain.embed.ProductEmbed;
import com.viettel.backend.domain.embed.PromotionDetail;
import com.viettel.backend.dto.PromotionCreateDto;
import com.viettel.backend.dto.PromotionCreateDto.PromotionDetailCreateDto;
import com.viettel.backend.dto.PromotionDto;
import com.viettel.backend.dto.PromotionSimpleDto;
import com.viettel.backend.engine.promotion.PromotionCondition;
import com.viettel.backend.engine.promotion.PromotionReward;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.repository.PromotionRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminPromotionService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class AdminPromotionServiceImpl extends
        BasicCategoryServiceImpl<Promotion, PromotionSimpleDto, PromotionDto, PromotionCreateDto> implements
        AdminPromotionService {

    private static final long serialVersionUID = -5771173067596328856L;

    @Autowired
    private PromotionRepository promotionRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public BasicCategoryRepository<Promotion> getRepository() {
        return promotionRepository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN);
    }
    
    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.ADMIN_ONLY;
    }

    @Override
    public Promotion createDomain(UserLogin userLogin, PromotionCreateDto createdto) {
        Promotion domain = new Promotion();
        initPOWhenCreate(Promotion.class, userLogin, domain);
        domain.setDraft(true);

        updateDomain(userLogin, domain, createdto);

        return domain;
    }

    @Override
    public void updateDomain(UserLogin userLogin, Promotion domain, PromotionCreateDto createdto) {
        if (!domain.isDraft()) {
            throw new BusinessException(ExceptionCode.CANNOT_UPDATE_NO_DRAFT_DATA);
        }

        checkMandatoryParams(createdto, createdto.getName(), createdto.getDescription(), createdto.getApplyFor(),
                createdto.getStartDate(), createdto.getEndDate(), createdto.getDetails());

        SimpleDate startDate = SimpleDate.createByIsoDate(createdto.getStartDate(), null);
        SimpleDate endDate = SimpleDate.createByIsoDate(createdto.getEndDate(), null);
        if (startDate == null || endDate == null || startDate.compareTo(endDate) > 0) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        domain.setName(createdto.getName());
        domain.setDescription(createdto.getDescription());
        domain.setApplyFor(createdto.getApplyFor());
        domain.setStartDate(startDate);
        domain.setEndDate(endDate);

        List<PromotionDetail> details = new ArrayList<PromotionDetail>(createdto.getDetails().size());
        for (PromotionDetailCreateDto detailDto : createdto.getDetails()) {
            if (detailDto == null || !detailDto.isValid()) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            PromotionDetail detail = new PromotionDetail();
            detail.setId(new ObjectId());
            detail.setType(detailDto.getType());

            Product conditionProduct = getMadatoryPO(userLogin, detailDto.getCondition().getProductId(),
                    productRepository);
            detail.setCondition(new PromotionCondition<ObjectId, ProductEmbed>(new ProductEmbed(conditionProduct),
                    detailDto.getCondition().getQuantity()));

            Product rewardProduct = null;
            if (detailDto.getReward().getProductId() != null) {
                rewardProduct = getMadatoryPO(userLogin, detailDto.getReward().getProductId(), productRepository);
            }
            detail.setReward(new PromotionReward<ObjectId, ProductEmbed>(detailDto.getReward().getPercentage(),
                    detailDto.getReward().getQuantity(), rewardProduct == null ? null : new ProductEmbed(rewardProduct)));

            details.add(detail);
        }
        domain.setDetails(details);
    }

    @Override
    public PromotionSimpleDto createSimpleDto(UserLogin userLogin, Promotion domain) {
        return new PromotionSimpleDto(domain);
    }

    @Override
    public PromotionDto createDetailDto(UserLogin userLogin, Promotion domain) {
        return new PromotionDto(domain, getClientCondig(userLogin).getDefaultProductPhoto());
    }

}
