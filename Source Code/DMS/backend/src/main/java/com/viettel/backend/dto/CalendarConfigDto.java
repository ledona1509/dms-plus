package com.viettel.backend.dto;

import java.util.List;

import com.viettel.backend.entity.SimpleDate;

public class CalendarConfigDto extends DTO {

    private static final long serialVersionUID = 1786366439733982764L;

    private List<Integer> workingDays;
    private List<SimpleDate> everyYearHolidays;
    private List<SimpleDate> exceptionHolidays;
    private List<SimpleDate> exceptionWorkingDays;

    public List<Integer> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<Integer> workingDays) {
        this.workingDays = workingDays;
    }

    public List<SimpleDate> getEveryYearHolidays() {
        return everyYearHolidays;
    }

    public void setEveryYearHolidays(List<SimpleDate> everyYearHolidays) {
        this.everyYearHolidays = everyYearHolidays;
    }

    public List<SimpleDate> getExceptionHolidays() {
        return exceptionHolidays;
    }

    public void setExceptionHolidays(List<SimpleDate> exceptionHolidays) {
        this.exceptionHolidays = exceptionHolidays;
    }

    public List<SimpleDate> getExceptionWorkingDays() {
        return exceptionWorkingDays;
    }

    public void setExceptionWorkingDays(List<SimpleDate> exceptionWorkingDays) {
        this.exceptionWorkingDays = exceptionWorkingDays;
    }

}
