package com.viettel.backend.engine.promotion;

import java.io.Serializable;

public interface I_PromotionEmbed<ID extends Serializable, P extends I_Product<ID>> extends Serializable {

    public ID getId();

}
