package com.viettel.backend.domain.embed;

import java.util.List;

import com.viettel.backend.domain.NameCategory;

/**
 * @author thanh
 */
public class ExhibitionCategory extends NameCategory {

    private static final long serialVersionUID = 3227127774937212395L;

    public static final String COLUMNNAME_ITEMS = "items";

    private List<ExhibitionItem> items;

    public List<ExhibitionItem> getItems() {
        return items;
    }

    public void setItems(List<ExhibitionItem> items) {
        this.items = items;
    }

}
