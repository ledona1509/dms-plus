package com.viettel.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.User;
import com.viettel.backend.dto.UserLoginDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.AuthenticateRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AuthenticationService;
import com.viettel.backend.util.PasswordUtils;

@Service
public class AuthenticateServiceImpl implements AuthenticationService {

    private static final long serialVersionUID = -7521814129330891905L;

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private AuthenticateRepository authenticateRepository;

    @Override
    public User authenticate(String username, String password) {
        User user = authenticateRepository.findByUserName(username);
        
        if (user != null && PasswordUtils.matches(password, user.getPassword())) {
            return user;
        }
        
        return null;
    }

    @Override
    public UserLoginDto getUserLoginDto(UserLogin userLogin) {
        User user = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());

        if (user == null) {
            throw new UnsupportedOperationException("current user not found");
        }

        return new UserLoginDto(user);
    }
    
    @Override
    public void changePassword(UserLogin userLogin, String userId, String oldPassword, String newPassword) {
        if (newPassword == null) {
            throw new BusinessException(ExceptionCode.INVALID_NEW_PASSWORD);
        }
        
        User user = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());
        
        if (user == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        } 
        
        if (PasswordUtils.matches(oldPassword, user.getPassword())) {
            user.setPassword(PasswordUtils.encode(newPassword));
            userRepository.save(userLogin.getClientId(), user);
        } else {
            throw new BusinessException(ExceptionCode.INVALID_OLD_PASSWORD); 
        }
    }

}
