package com.viettel.backend.restful;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.file.DbFile;
import com.viettel.backend.file.DbFileMeta;
import com.viettel.backend.oauth2.core.SecurityContextHelper;
import com.viettel.backend.oauth2.core.SignedRequestConverter;
import com.viettel.backend.oauth2.core.TicketResult;
import com.viettel.backend.service.SupervisorReportService;
import com.viettel.backend.service.engine.FileEngine;

@RestController
public class FileManagerController {

    @Autowired
    private FileEngine fileEngine;

    @Autowired
    private SignedRequestConverter signedRequestConverter;

    @Autowired
    private SupervisorReportService reportSupervisorService;

    @RequestMapping(value = "/file", method = RequestMethod.POST)
    public ResponseEntity<?> uploadFile(@RequestParam(value = "file") MultipartFile file) throws IOException {
        String id = fileEngine.store(SecurityContextHelper.getCurrentUser(), file.getInputStream(),
                file.getOriginalFilename(), file.getContentType(), new DbFileMeta());
        return new Envelope(id).toResponseEntity();
    }

    @RequestMapping(value = "/file", method = RequestMethod.GET, headers = { HttpHeaders.AUTHORIZATION })
    public ResponseEntity<?> getFileTicket(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "id") String id) throws IOException {
        // Validate permission if necessary
        String ticket = signedRequestConverter.signRequest(request, response, 5L * 60 * 1000);
        return new ResponseEntity<TicketResult>(new TicketResult(ticket), HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/file", method = RequestMethod.GET, params = { OAuth2AccessToken.ACCESS_TOKEN })
    public void getFileRedirect(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "id") String id) throws IOException {
        // Validate permission if necessary
        signedRequestConverter.signRequest(request, response, 5L * 60 * 1000);
    }

    @RequestMapping(value = "/file", method = RequestMethod.GET, params = { SignedRequestConverter.PARAMETER_TICKET })
    public ResponseEntity<byte[]> getFileDownload(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "id") String id) throws IOException {

        signedRequestConverter.verifyRequest(request, response);

        DbFile file = fileEngine.get(SecurityContextHelper.getCurrentUser(), id);
        if (file == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.parseMediaType(file.getContentType()));
        header.set("Content-Disposition", "inline; filename=\"" + file.getOriginalFileName() + "\"");
        ResponseEntity<byte[]> result = new ResponseEntity<byte[]>(IOUtils.toByteArray(file.getInputStream()), header,
                HttpStatus.OK);

        return result;
    }

    @RequestMapping(value = "/file", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteFile(@RequestParam(value = "id") String id) throws IOException {
        fileEngine.delete(SecurityContextHelper.getCurrentUser(), id);
        return new Envelope(Meta.OK).toResponseEntity();
    }

    // IMAGE
    @RequestMapping(value = "/image/{id}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImage(@PathVariable(value = "id") String id) throws IOException {
        DbFile file = fileEngine.getImage(id);
        if (file == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.IMAGE_JPEG);
        header.set("Content-Disposition", "inline; filename=\"" + file.getOriginalFileName() + "\"");
        ResponseEntity<byte[]> result = new ResponseEntity<byte[]>(IOUtils.toByteArray(file.getInputStream()), header,
                HttpStatus.OK);

        return result;
    }

    @RequestMapping(value = "/image", method = RequestMethod.POST)
    public ResponseEntity<?> uploadImage(@RequestParam(value = "file") MultipartFile file,
            @RequestParam(value = "sizetype", required = false) String sizetype) throws IOException {
        String id = fileEngine.storeImage(file.getInputStream(), file.getOriginalFilename(), file.getContentType(),
                new DbFileMeta(), sizetype);
        return new Envelope(id).toResponseEntity();
    }
}
