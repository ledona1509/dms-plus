package com.viettel.backend.restful.salesman;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.OrderCreateDto;
import com.viettel.backend.dto.OrderDto;
import com.viettel.backend.dto.OrderSimpleDto;
import com.viettel.backend.dto.embed.OrderPromotionDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SalesmanOrderService;

@RestController(value="salesmanOrderController")
@RequestMapping(value = "/salesman/order")
public class OrderController extends AbstractController {

    private static final long serialVersionUID = -5643500619636563651L;

    @Autowired
    private SalesmanOrderService salesmanOrderService;

    @RequestMapping(value = "/unplanned", method = RequestMethod.POST)
    public ResponseEntity<?> createUnplannedOrder(@RequestBody OrderCreateDto dto) {
        String orderId = salesmanOrderService.createUnplannedOrder(getUserLogin(), dto);
        return new Envelope(orderId).toResponseEntity();
    }

    @RequestMapping(value = "/calculate", method = RequestMethod.POST)
    public ResponseEntity<?> calculatePromotion(@RequestBody OrderCreateDto dto) {
        ListJson<OrderPromotionDto> results = salesmanOrderService.calculatePromotion(getUserLogin(),
                dto);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    // Orders BY CURRENT SALESMAN
    /** GET ALL ORDERS OF SALESMAN SEARCH BY CUSTOMER NAME AND ADDRESS */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getOrders(
            @RequestParam(value = "q", required = false, defaultValue = "") String customerSearch,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        ListJson<OrderSimpleDto> results = salesmanOrderService.getOrders(getUserLogin(),
                customerSearch, getPageRequest(page, size));

        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/today/bycustomer", method = RequestMethod.GET)
    public ResponseEntity<?> getOrdersByCustomerToday(@RequestParam(required = false) String customerId) {
        ListJson<OrderSimpleDto> results = salesmanOrderService.getOrdersByCustomerToday(
                getUserLogin(), customerId);

        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    // ORDER BY CURRENT SALESMAN
    // "/salesman/view/{id}"
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> viewOrderDetail(@PathVariable String id) {
        OrderDto result = salesmanOrderService.getOrderById(getUserLogin(), id);
        return new Envelope(result).toResponseEntity(HttpStatus.OK);
    }

}
