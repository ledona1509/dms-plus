package com.viettel.backend.websocket.core;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.viettel.backend.dto.OutputMessage;

@Component
public class Websocket {
	
	@Autowired
	private SimpMessagingTemplate template;
	
	private static Websocket websocket;
	
	public Websocket() {
	}
	
	@PostConstruct
	void afterInit() {
		if (websocket == null) {
		    websocket = this;
		}
	}
	
	protected SimpMessagingTemplate getMessagingTemplate(){
		return template;
	}

    public static void send(String topic, String sender, Object data) {
        OutputMessage message = new OutputMessage();
        message.setSender(sender);
        message.setData(data);
       
        websocket.getMessagingTemplate().convertAndSend(topic, message);
    }
    
    public static void sendToUser(String userId, String topic, String sender, Object data) {
        OutputMessage message = new OutputMessage();
        message.setSender(sender);
        message.setData(data);
       
        websocket.getMessagingTemplate().convertAndSendToUser(userId, topic, message);
    }
    
}
