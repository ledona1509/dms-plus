package com.viettel.backend.dto.embed;

import com.viettel.backend.domain.embed.SurveyOption;
import com.viettel.backend.dto.NameCategoryDto;

public class SurveyOptionDto extends NameCategoryDto {

    private static final long serialVersionUID = 4085000556181222126L;

    public SurveyOptionDto() {
        super();
    }

    public SurveyOptionDto(SurveyOption surveyOption) {
        super(surveyOption);
    }

}
