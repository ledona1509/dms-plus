package com.viettel.backend.dto.embed;

import java.math.BigDecimal;

import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.engine.promotion.I_OrderDetail;

public class OrderDetailDto implements I_OrderDetail<String, ProductEmbedDto> {
    /**
	 * 
	 */
    private static final long serialVersionUID = 2190025456968539879L;

    private ProductEmbedDto product;
    private BigDecimal quantity;

    public OrderDetailDto(OrderDetail detail, String defaultProductPhoto) {
        super();

        if (detail.getProduct() == null) {
            throw new IllegalArgumentException("product is null");
        }
        this.quantity = detail.getQuantity();
        this.product = new ProductEmbedDto(detail.getProduct(), defaultProductPhoto);
    }

    public ProductEmbedDto getProduct() {
        return product;
    }

    public void setProduct(ProductEmbedDto product) {
        this.product = product;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAmount() {
        if (this.product == null || this.quantity == null || this.quantity.signum() <= 0
                || this.product.getPrice() == null || this.product.getPrice().signum() <= 0)
            return BigDecimal.ZERO;

        return this.product.getPrice().multiply(this.quantity);
    }

}
