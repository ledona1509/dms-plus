package com.viettel.backend.dto;

import java.io.Serializable;

public class LocationDto implements Serializable {

    private static final long serialVersionUID = 5038209025937567636L;
    
    private double latitude;
    private double longitude;
    
    public LocationDto() {
        this(-1, -1);
    }
    
    public LocationDto(double latitude, double longitude) {
        super();
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    public double getLatitude() {
        return latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    public double getLongitude() {
        return longitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    
    public static class LocationDtoWrapper implements Serializable {

        private static final long serialVersionUID = -645713944197310302L;
        
        private LocationDto location;

        public LocationDto getLocation() {
            return location;
        }

        public void setLocation(LocationDto location) {
            this.location = location;
        }
        
    }
    
}
