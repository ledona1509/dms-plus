package com.viettel.backend.repository.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.util.CriteriaUtils;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.PasswordUtils;

@Repository
public class UserRepositoryImpl extends BasicCategoryRepositoryImpl<User> implements UserRepository {

    /**
     * 
     */
    private static final long serialVersionUID = -1746027152695193471L;

    @Autowired
    private DistributorRepository distributorRepository;
    
    @Override
    public List<User> getUsersByRole(ObjectId clientId, String roleCode) {
        Criteria roleCriteria = Criteria.where(User.COLUMNNAME_ROLES).all(roleCode);

        return super._getList(clientId, true, roleCriteria, null, null);
    }
    
    @Override
    public Set<ObjectId> getUserIdsByRole(ObjectId clientId, String roleCode) {
        List<User> users = getUsersByRole(clientId, roleCode);
        return _getIdSet(users);
    }

    @Override
    public List<User> getSalesmenBySupervisors(ObjectId clientId, Collection<ObjectId> supervisorIds) {
        if (supervisorIds == null || supervisorIds.isEmpty()) {
            return Collections.<User> emptyList();
        }

        Collection<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(clientId,
                supervisorIds);
        if (distributorIds == null || distributorIds.isEmpty()) {
            return Collections.<User> emptyList();
        }

        return getSalesmenByDistributors(clientId, distributorIds);
    }

    @Override
    public Set<ObjectId> getSalesmanIdsBySupervisors(ObjectId clientId, Collection<ObjectId> supervisorIds) {
        List<User> users = getSalesmenBySupervisors(clientId, supervisorIds);
        return _getIdSet(users);
    }

    @Override
    public List<User> getSalesmenByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds) {
        if (distributorIds == null || distributorIds.isEmpty()) {
            return Collections.<User> emptyList();
        }

        Criteria roleCriteria = Criteria.where(User.COLUMNNAME_ROLES).all(HardCodeUtils.ROLE_SALESMAN);

        Criteria distributorCriteria = Criteria.where(User.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);

        Criteria criteria = CriteriaUtils.andOperator(roleCriteria, distributorCriteria);

        return super._getList(clientId, true, criteria, null, null);
    }

    @Override
    public Set<ObjectId> getSalesmanIdsByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds) {
        List<User> users = getSalesmenByDistributors(clientId, distributorIds);
        return _getIdSet(users);
    }

    @Override
    public void updateSalesmenOfDistributor(ObjectId clientId, ObjectId distributorId, Collection<ObjectId> salesmanIds) {
        // reset old
        if (distributorId == null) {
            throw new IllegalArgumentException("distributorId is null");
        }
        
        Distributor distributor = distributorRepository.getById(clientId, distributorId);
        if (distributor == null) {
            throw new IllegalArgumentException("distributor is null");
        }

        Update cleanUpdate = new Update();
        cleanUpdate.set(User.COLUMNNAME_DISTRIBUTOR_ID, null);

        Criteria criteria = Criteria.where(User.COLUMNNAME_DISTRIBUTOR_ID).is(distributorId);
        super._updateMulti(clientId, null, criteria, cleanUpdate);

        if (salesmanIds == null || salesmanIds.isEmpty()) {
            return;
        }

        // update new
        Update update = new Update();
        update.set(User.COLUMNNAME_DISTRIBUTOR, new DistributorEmbed(distributor));

        criteria = Criteria.where(PO.COLUMNNAME_ID).in(salesmanIds);
        super._updateMulti(clientId, true, criteria, update);
    }

    @Override
    public User createDistributorAdmin(ObjectId clientId, Distributor distributor, String username) {
        List<String> roles = new ArrayList<String>(1);
        roles.add(HardCodeUtils.ROLE_DISTRIBUTOR_ADMIN);

        User user = new User();
        user.setClientId(clientId);
        user.setActive(true);
        user.setUsername(username);
        user.setFullname(username);
        user.setPassword(PasswordUtils.getDefaultPassword());
        user.setDistributor(new DistributorEmbed(distributor));
        user.setRoles(roles);

        return save(clientId, user);
    }
    
    @Override
    public boolean checkDistributorUsed(ObjectId clientId, ObjectId distributorId) {
        if (distributorId == null) {
            throw new IllegalArgumentException("distributorId is null");
        }

        Criteria criteria = Criteria.where(User.COLUMNNAME_DISTRIBUTOR_ID).is(distributorId);

        return super._exists(clientId, true, criteria);
    }

}
