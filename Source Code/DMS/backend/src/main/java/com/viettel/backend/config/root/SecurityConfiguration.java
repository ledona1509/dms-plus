package com.viettel.backend.config.root;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import com.viettel.backend.oauth2.core.SignedRequestMatcher;
import com.viettel.backend.oauth2.core.UserAuthenticationProvider;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    
    @Value("${url.webview}")
    private String defaultRedirectUrl;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(userAuthenticationProvider());
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(
        		"/images/**", 
        		"/assets/**",
        		"/api/image/**",
        		"/api/notoken/**",
        		"/websocket/**");
        // Ignore check access token with any request start with /api and have "ticket" parameter
        web.ignoring().requestMatchers(new SignedRequestMatcher("/api/**"));
    }
    
    @Bean
    public AuthenticationProvider userAuthenticationProvider() {
    	UserAuthenticationProvider authenticationProvider = new UserAuthenticationProvider();
    	return authenticationProvider;
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	// Note (ThanhNV60): Role should not start with ROLE_ because it
    	// will be inserted automatically
    	
        // @formatter:off
                 http
            .authorizeRequests().antMatchers("/account/login", "/account/ping").permitAll().and()
            .authorizeRequests()
                .anyRequest().hasRole("USER")
                .and()
            .exceptionHandling()
                .accessDeniedPage("/account/login?authorization_error=true")
                .and()
            // TODO: put CSRF protection back into this endpoint
            .csrf()
                .requireCsrfProtectionMatcher(new AntPathRequestMatcher("/oauth/authorize")).disable()
            .logout()
                .logoutSuccessUrl("/account/login")
                .logoutUrl("/account/logout")
                .deleteCookies("JSESSIONID", CookieLocaleResolver.DEFAULT_COOKIE_NAME)
                .permitAll()
                .and()
            .formLogin()
                    .usernameParameter("j_username")
                    .passwordParameter("j_password")
                    .failureUrl("/account/login?authentication_error=true")
                    .loginPage("/account/login")
                    .loginProcessingUrl("/account/login")
                    .defaultSuccessUrl(defaultRedirectUrl);
     
        // @formatter:on
    }
    
    @Bean
    public PasswordEncoder passwordEncoder() {
    	return new org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder();
    }
}
