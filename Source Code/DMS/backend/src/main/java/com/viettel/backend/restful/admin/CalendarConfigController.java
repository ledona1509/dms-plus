package com.viettel.backend.restful.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.CalendarConfigDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.AdminCalendarConfigService;

@RestController
@RequestMapping(value = "/admin/calendarconfig")
public class CalendarConfigController extends AbstractController {

	private static final long serialVersionUID = 2394833065851281843L;
	
	@Autowired
	private AdminCalendarConfigService adminCalendarConfigService;
	
    // SAVE
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@RequestBody @Valid CalendarConfigDto dto) {
        adminCalendarConfigService.saveCalendarConfig(getUserLogin(), dto);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK); 
    }

    // DETAIL
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> detail() {
        CalendarConfigDto dto = adminCalendarConfigService.getCalendarConfig(getUserLogin());
    	return new Envelope(dto).toResponseEntity(HttpStatus.OK); 
    }
    
}
