package com.viettel.backend.dto.embed;

import java.io.Serializable;
import java.util.List;

public class CustomerScheduleItemDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 432213681602083089L;

	private List<Integer> days;
	
	private List<Integer> weeks;

	public List<Integer> getDays() {
		return days;
	}

	public void setDays(List<Integer> days) {
		this.days = days;
	}

	public List<Integer> getWeeks() {
		return weeks;
	}

	public void setWeeks(List<Integer> weeks) {
		this.weeks = weeks;
	}
	
}
