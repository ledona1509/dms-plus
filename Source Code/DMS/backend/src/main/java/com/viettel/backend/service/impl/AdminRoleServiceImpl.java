package com.viettel.backend.service.impl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.RoleDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminRoleService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class AdminRoleServiceImpl extends AbstractService implements AdminRoleService {

    private static final long serialVersionUID = -3301371179583400992L;

    @Override
    public ListJson<RoleDto> getRoles(UserLogin userLogin) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_ADMIN)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }
        
        if (userLogin.getRoles().contains(HardCodeUtils.ROLE_ADMIN)) {
            List<RoleDto> roles = new LinkedList<RoleDto>();
            roles.add(new RoleDto(HardCodeUtils.ROLE_ADMIN, "ADMIN"));
            roles.add(new RoleDto(HardCodeUtils.ROLE_SUPERVISOR, "SUPERVISOR"));
            roles.add(new RoleDto(HardCodeUtils.ROLE_SALESMAN, "SALESMAN"));
            
            return new ListJson<RoleDto>(roles, (long) roles.size());
            
        } else {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }
    }
}
