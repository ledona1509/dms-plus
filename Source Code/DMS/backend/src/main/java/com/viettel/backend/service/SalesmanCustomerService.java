package com.viettel.backend.service;

import java.io.Serializable;

import org.springframework.data.domain.Pageable;

import com.viettel.backend.dto.CustomerCreateDto;
import com.viettel.backend.dto.CustomerForVisitDto;
import com.viettel.backend.dto.CustomerSimpleDto;
import com.viettel.backend.dto.CustomerSummaryDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.LocationDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SalesmanCustomerService extends Serializable {

    // ****************
    // *** SALESMAN ***
    // ****************
    public CustomerSummaryDto getCustomerSummary(UserLogin userLogin, String id);

    public ListJson<CustomerForVisitDto> getCustomersForVisitToday(UserLogin userLogin);

    public ListJson<CustomerForVisitDto> getCustomersForVisitAll(UserLogin userLogin);

    public void updatePhone(UserLogin userLogin, String id, String phone);

    public void updateMobile(UserLogin userLogin, String id, String mobile);

    public void updateLocation(UserLogin userLogin, String id, LocationDto location);

    public String register(UserLogin userLogin, CustomerCreateDto dto);

    /** all customer registered by this salesman **/
    public ListJson<CustomerSimpleDto> getCustomersRegistered(UserLogin userLogin, String search, Pageable pageable);

}
