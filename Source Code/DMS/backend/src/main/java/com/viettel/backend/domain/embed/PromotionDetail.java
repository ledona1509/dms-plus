package com.viettel.backend.domain.embed;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.NameCategory;
import com.viettel.backend.engine.promotion.I_PromotionDetail;
import com.viettel.backend.engine.promotion.PromotionCondition;
import com.viettel.backend.engine.promotion.PromotionReward;

public class PromotionDetail extends NameCategory implements I_PromotionDetail<ObjectId, ProductEmbed> {

    private static final long serialVersionUID = -4712090697721809249L;

    private int type;
    private PromotionCondition<ObjectId, ProductEmbed> condition;
    private PromotionReward<ObjectId, ProductEmbed> reward;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public PromotionCondition<ObjectId, ProductEmbed> getCondition() {
        return condition;
    }

    public void setCondition(PromotionCondition<ObjectId, ProductEmbed> condition) {
        this.condition = condition;
    }

    public PromotionReward<ObjectId, ProductEmbed> getReward() {
        return reward;
    }

    public void setReward(PromotionReward<ObjectId, ProductEmbed> reward) {
        this.reward = reward;
    }

}
