package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.Product;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.domain.embed.ProductEmbed;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.ProductForOrderDto;
import com.viettel.backend.dto.ProductDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.CustomerPendingRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.service.SalesmanProductService;

@Service
public class SalesmanProductServiceImpl extends AbstractSalesmanService implements SalesmanProductService {

    private static final long serialVersionUID = 2812222944238656195L;

    private static final int NUMBER_PO_FOR_SMART_ORDER_PRODUCT = 1;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerPendingRepository customerPendingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Override
    public ListJson<ProductDto> getProducts(UserLogin userLogin, String search, Pageable pageable) {
        checkIsSalesman(userLogin);

        ObjectId clientId = userLogin.getClientId();

        String defaultProductPhoto = getClientCondig(userLogin).getDefaultProductPhoto();

        List<Product> products = productRepository.getProducts(clientId, search, pageable, null);

        List<ProductDto> dtos = new ArrayList<ProductDto>();
        for (Product product : products) {
            // PHOTO
            ProductDto dto = new ProductDto(product, defaultProductPhoto);
            dtos.add(dto);
        }

        long size = Long.valueOf(dtos.size());
        if (pageable != null) {
            if (pageable.getPageNumber() > 0 || size == pageable.getPageSize()) {
                size = productRepository.count(clientId, search);
            }
        }

        return new ListJson<ProductDto>(dtos, size);
    }

    @Override
    public ListJson<ProductForOrderDto> getProductsForOrder(UserLogin userLogin, String _customerId) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, _customerId);

        String defaultProductPhoto = getClientCondig(userLogin).getDefaultProductPhoto();

        Collection<Product> products = productRepository.getAll(userLogin.getClientId());

        Set<ObjectId> productFavoriteIds = new HashSet<ObjectId>();
        List<Order> lastPOs = orderRepository.getLastOrderByCustomer(userLogin.getClientId(),
                customer.getId(), NUMBER_PO_FOR_SMART_ORDER_PRODUCT);
        if (lastPOs != null && !lastPOs.isEmpty()) {
            for (Order order : lastPOs) {
                List<OrderDetail> details = order.getDetails();
                if (details == null || details.isEmpty()) {
                    continue;
                }

                for (OrderDetail detail : details) {
                    ProductEmbed product = detail.getProduct();
                    if (product == null || product.getId() == null) {
                        continue;
                    }

                    productFavoriteIds.add(product.getId());
                }
            }
        }

        List<ProductForOrderDto> dtos = new LinkedList<ProductForOrderDto>();
        int favoriteIndex = 0;
        int noFavoriteIndex = products.size();

        for (Product product : products) {
            ProductForOrderDto dto = new ProductForOrderDto(product, defaultProductPhoto);

            if (productFavoriteIds.contains((product.getId()))) {
                dto.setSeqNo(favoriteIndex);
                favoriteIndex++;
            } else {
                dto.setSeqNo(noFavoriteIndex);
                noFavoriteIndex++;
            }

            dtos.add(dto);
        }

        return new ListJson<ProductForOrderDto>(dtos, Long.valueOf(dtos.size()));
    }

}
