package com.viettel.backend.service.impl;

import java.util.Collection;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.SalesConfig;
import com.viettel.backend.domain.User;
import com.viettel.backend.dto.LocationDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicRepository;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.SalesConfigRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.util.LocationUtils;
import com.viettel.backend.util.ObjectIdUtils;
import com.viettel.backend.util.StringUtils;

public abstract class AbstractService {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private ClientConfigRepository clientConfigRepository;
    
    @Autowired
    private SalesConfigRepository salesConfigRepository;

    protected final <D extends PO> void initPOWhenCreate(Class<D> clazz, UserLogin userLogin, D domain) {
        if (PO.isClientRootFixed(clazz)) {
            domain.setClientId(PO.CLIENT_ROOT_ID);
        } else {
            domain.setClientId(userLogin.getClientId());
        }

        domain.setActive(true);
    }

    protected User getCurrentUser(UserLogin userLogin) {
        User user = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());
        if (user == null) {
            throw new UnsupportedOperationException("current user not found");
        }
        
        return user;
    }
    
    protected final ClientConfig getClientCondig(UserLogin userLogin) {
        ClientConfig clientConfig = clientConfigRepository.getClientConfig(PO.CLIENT_ROOT_ID);
        if (clientConfig == null) {
            throw new UnsupportedOperationException("client config not found");
        }
        
        return clientConfig;
    }
    
    protected final SalesConfig getSalesConfig(UserLogin userLogin) {
        SalesConfig salesConfig = salesConfigRepository.getSalesConfig(userLogin.getClientId());
        if (salesConfig == null) {
            throw new UnsupportedOperationException("sales config not found");
        }
        
        return salesConfig;
    }

    protected <D extends PO> D getMadatoryPO(UserLogin userLogin, String _id, BasicRepository<D> repository) {
        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        D domain = repository.getById(userLogin.getClientId(), id);
        if (domain == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        return domain;
    }

    protected void checkMandatoryParams(Object... params) {
        if (params != null) {
            for (Object param : params) {
                if (param == null) {
                    throw new BusinessException(ExceptionCode.INVALID_PARAM);
                }

                if (param instanceof String) {
                    if (StringUtils.isNullOrEmpty((String) param, true)) {
                        throw new BusinessException(ExceptionCode.INVALID_PARAM);
                    }
                } else if (param instanceof Collection<?>) {
                    Collection<?> items = (Collection<?>) param;

                    if (items.isEmpty()) {
                        throw new BusinessException(ExceptionCode.INVALID_PARAM);
                    }

                    for (Object item : items) {
                        checkMandatoryParams(item);
                    }
                } else if (param instanceof LocationDto) {
                    LocationDto location = (LocationDto) param;

                    if (!LocationUtils.checkLocationValid(location)) {
                        throw new BusinessException(ExceptionCode.INVALID_PARAM);
                    }
                }
            }
        }
    }

}
