package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.ClientConfigDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SuperAdminClientConfigService extends Serializable {

    public ClientConfigDto getClientConfig(UserLogin userLogin);
    
    public String save(UserLogin userLogin, ClientConfigDto dto);
    
}
