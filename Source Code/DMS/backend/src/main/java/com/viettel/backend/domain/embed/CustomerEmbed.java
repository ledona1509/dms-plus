package com.viettel.backend.domain.embed;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.NameCategory;

public class CustomerEmbed extends NameCategory {

    private static final long serialVersionUID = -3158993502257758137L;

    private String code;
    private String address;

    public CustomerEmbed() {
        this(null, null, null, null);
    }

    public CustomerEmbed(Customer customer) {
        this(customer.getId(), customer.getCode(), customer.getName(), customer.getAddress());
    }

    public CustomerEmbed(ObjectId id, String code, String name, String address) {
        super();

        setId(id);
        setName(name);
        this.code = code;
        this.address = address;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
