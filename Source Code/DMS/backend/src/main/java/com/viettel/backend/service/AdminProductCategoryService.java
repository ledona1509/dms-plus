package com.viettel.backend.service;

import com.viettel.backend.domain.ProductCategory;
import com.viettel.backend.dto.NameCategoryDto;

public interface AdminProductCategoryService extends
        BasicCategoryService<ProductCategory, NameCategoryDto, NameCategoryDto, NameCategoryDto> {

}
