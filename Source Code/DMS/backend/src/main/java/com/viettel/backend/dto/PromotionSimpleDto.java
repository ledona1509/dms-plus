package com.viettel.backend.dto;

import com.viettel.backend.domain.Promotion;

public class PromotionSimpleDto extends NameCategoryDto {

    private static final long serialVersionUID = -7572087292184761548L;

    private String startDate;
    private String endDate;

    private String applyFor;
    private String description;

    public PromotionSimpleDto(Promotion promotion) {
        super(promotion);

        this.startDate = promotion.getStartDate() != null ? promotion.getStartDate().getIsoDate() : null;
        this.endDate = promotion.getEndDate() != null ? promotion.getEndDate().getIsoDate() : null;

        this.applyFor = promotion.getApplyFor();
        this.description = promotion.getDescription();
    }

    private int seqNo;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getApplyFor() {
        return applyFor;
    }

    public void setApplyFor(String applyFor) {
        this.applyFor = applyFor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

}
