package com.viettel.backend.dto;

import java.math.BigDecimal;

import com.viettel.backend.domain.Target;
import com.viettel.backend.dto.embed.UserEmbedDto;

public class TargetDto extends DTO {

    private static final long serialVersionUID = -3377268163150327565L;

    private UserEmbedDto salesman;
    private int month;
    private int year;
    private BigDecimal revenue;
    private BigDecimal output;
    
    public TargetDto() {
        
    }
    
    public TargetDto(Target domain) {
        this.setId(domain.getId().toString());
        this.setDraft(domain.isDraft());
        this.setSalesman(new UserEmbedDto(domain.getSalesman()));
        this.setMonth(domain.getMonth());
        this.setYear(domain.getYear());
        this.setRevenue(domain.getRevenue());
        this.setOutput(domain.getOutput());
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    public BigDecimal getOutput() {
        return output;
    }

    public void setOutput(BigDecimal output) {
        this.output = output;
    }

    public UserEmbedDto getSalesman() {
        return salesman;
    }

    public void setSalesman(UserEmbedDto salesman) {
        this.salesman = salesman;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

}
