package com.viettel.backend.dto;

import com.viettel.backend.domain.Distributor;
import com.viettel.backend.dto.embed.UserEmbedDto;

public class DistributorSimpleDto extends NameCategoryDto {

    private static final long serialVersionUID = -4570514747523311583L;

    private String code;
    private String address;
    private UserEmbedDto supervisor;
    
    public DistributorSimpleDto(Distributor distributor) {
        super(distributor);
        
        this.code = distributor.getCode();
        this.address = distributor.getAddress();
        if (distributor.getSupervisor() != null) {
            this.supervisor = new UserEmbedDto(distributor.getSupervisor());
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public UserEmbedDto getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(UserEmbedDto supervisor) {
        this.supervisor = supervisor;
    }
    
}
