package com.viettel.backend.repository;

import com.viettel.backend.domain.CustomerType;

public interface CustomerTypeRepository extends BasicNameCategoryRepository<CustomerType> {

}
