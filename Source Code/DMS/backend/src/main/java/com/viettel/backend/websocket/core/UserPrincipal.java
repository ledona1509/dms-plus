package com.viettel.backend.websocket.core;

import java.io.Serializable;
import java.security.Principal;

/**
 * @author thanh
 */
public class UserPrincipal implements Principal, Serializable {

    private static final long serialVersionUID = -3808357739655345323L;
    
    private String username;

    public UserPrincipal(String name) {
        this.username = name;
    }

    @Override
    public String getName() {
        return username;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserPrincipal other = (UserPrincipal) obj;
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        return true;
    }

}
