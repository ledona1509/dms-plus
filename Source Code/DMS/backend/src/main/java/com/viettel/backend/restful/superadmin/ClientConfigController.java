package com.viettel.backend.restful.superadmin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ClientConfigDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SuperAdminClientConfigService;

@RestController
@RequestMapping(value = "/superadmin/clientconfig")
public class ClientConfigController extends AbstractController {

	private static final long serialVersionUID = 2394833065851281843L;
	
	@Autowired
	private SuperAdminClientConfigService clientConfigService;
	
    // SAVE
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@RequestBody @Valid ClientConfigDto dto) {
        String id = clientConfigService.save(getUserLogin(), dto);
        return new Envelope(id).toResponseEntity(HttpStatus.OK); 
    }

    // DETAIL
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> detail() {
        ClientConfigDto dto = clientConfigService.getClientConfig(getUserLogin());
    	return new Envelope(dto).toResponseEntity(HttpStatus.OK); 
    }
    
}
