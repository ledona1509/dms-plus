package com.viettel.backend.message.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.core.MessageSendingOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class QuoteService {

	@Autowired
	private MessageSendingOperations<String> messagingTemplate;

	@Scheduled(fixedDelay = 1000)
	public void sendQuotes() {
		String destination = "/topic/price.stock.";
		this.messagingTemplate.convertAndSend(destination, "a");
	}
}

