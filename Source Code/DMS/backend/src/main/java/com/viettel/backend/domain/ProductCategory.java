package com.viettel.backend.domain;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ProductCategory")
public class ProductCategory extends NameCategory {

    private static final long serialVersionUID = 5845401220841748404L;

    public ProductCategory() {
        super();
    }

}
