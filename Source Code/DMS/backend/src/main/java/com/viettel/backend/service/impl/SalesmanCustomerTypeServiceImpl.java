package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.CustomerType;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerTypeRepository;
import com.viettel.backend.service.SalesmanCustomerTypeService;

@Service
public class SalesmanCustomerTypeServiceImpl extends AbstractSalesmanService implements SalesmanCustomerTypeService {

    private static final long serialVersionUID = -5771173067596328856L;

    @Autowired
    private CustomerTypeRepository repository;

    @Override
    public ListJson<NameCategoryDto> getAll(UserLogin userLogin) {
        checkIsSalesman(userLogin);

        List<CustomerType> domains = repository.getAll(userLogin.getClientId());
        if (domains == null || domains.isEmpty()) {
            return ListJson.<NameCategoryDto> emptyList();
        }

        List<NameCategoryDto> dtos = new ArrayList<NameCategoryDto>();
        for (CustomerType domain : domains) {
            dtos.add(new NameCategoryDto(domain));
        }

        return new ListJson<NameCategoryDto>(dtos, Long.valueOf(dtos.size()));
    }

}
