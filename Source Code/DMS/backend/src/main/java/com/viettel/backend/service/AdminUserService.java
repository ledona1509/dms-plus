package com.viettel.backend.service;

import com.viettel.backend.domain.User;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.UserDto;
import com.viettel.backend.dto.UserSimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface AdminUserService extends BasicCategoryService<User, UserSimpleDto, UserDto, UserDto> {

    public void resetPassword(UserLogin userLogin, String id);
    
    public ListJson<UserSimpleDto> getSalesmen(UserLogin userLogin, String _distributorId);
    
    public ListJson<UserSimpleDto> getSupervisors(UserLogin userLogin);
    
    public ListJson<UserSimpleDto> getSalesmanByDistributorWithAvailable(UserLogin userLogin, String distributorId);
    
}
