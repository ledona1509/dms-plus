package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Target;
import com.viettel.backend.domain.User;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.UserSimpleDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.TargetRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SupervisorUserService;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class SupervisorUserServiceImpl extends AbstractSupervisorService implements SupervisorUserService {

    private static final long serialVersionUID = -7521814129330891905L;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private TargetRepository targetRepository;

    @Override
    public ListJson<UserSimpleDto> getSalesmen(UserLogin userLogin, String _distributorId) {
        checkIsSupervisor(userLogin);

        Collection<ObjectId> distributorIds = null;
        if (_distributorId != null) {
            ObjectId distributorId = ObjectIdUtils.getObjectId(_distributorId, null);
            if (distributorId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            distributorIds = Arrays.asList(distributorId);
        } else {
            distributorIds = distributorRepository.getDistributorIdsBySupervisors(userLogin.getClientId(),
                    Arrays.asList(userLogin.getUserId()));
        }

        List<User> salesmen = userRepository.getSalesmenByDistributors(userLogin.getClientId(), distributorIds);
        if (salesmen == null || salesmen.isEmpty()) {
            return ListJson.emptyList();
        }

        List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(salesmen.size());
        for (User salesman : salesmen) {
            dtos.add(new UserSimpleDto(salesman));
        }

        return new ListJson<UserSimpleDto>(dtos, Long.valueOf(dtos.size()));
    }

    @Override
    public ListJson<UserSimpleDto> getSalesmenNoTarget(UserLogin userLogin, String _distributorId, int month, int year) {
        checkIsSupervisor(userLogin);

        // Get all distributors of supervisor or use selected distributor
        Collection<ObjectId> distributorIds = null;
        if (_distributorId != null) {
            ObjectId distributorId = ObjectIdUtils.getObjectId(_distributorId, null);
            if (distributorId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            distributorIds = Arrays.asList(distributorId);
        } else {
            distributorIds = distributorRepository.getDistributorIdsBySupervisors(userLogin.getClientId(),
                    Arrays.asList(userLogin.getUserId()));
        }

        // Get all id of salesmen of distributors
        Set<ObjectId> salesmanIds = userRepository
                .getSalesmanIdsByDistributors(userLogin.getClientId(), distributorIds);
        if (salesmanIds == null || salesmanIds.isEmpty()) {
            return ListJson.emptyList();
        }

        List<Target> targets = targetRepository.getTargetsBySalesmen(userLogin.getClientId(), salesmanIds, month, year,
                null, null);
        // Remove salesman have target
        if (targets != null && !targets.isEmpty()) {
            int targetsSize = targets.size();
            for (int i = 0; i < targetsSize; i++) {
                Target target = targets.get(i);
                salesmanIds.remove(target.getSalesman().getId());
            }
        }

        // Get list of salesmen remain
        List<User> salesmen = userRepository.getListByIds(userLogin.getClientId(), salesmanIds);

        List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(salesmen.size());
        for (User salesman : salesmen) {
            dtos.add(new UserSimpleDto(salesman));
        }

        return new ListJson<UserSimpleDto>(dtos, Long.valueOf(dtos.size()));
    }

}
