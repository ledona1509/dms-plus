package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SalesmanCustomerTypeService extends Serializable {

    public ListJson<NameCategoryDto> getAll(UserLogin userLogin);
    
}
