package com.viettel.backend.oauth2.core;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class RevokeNotifyDefaultTokenService extends DefaultTokenServices implements ApplicationEventPublisherAware {
    
    private static final Log logger = LogFactory.getLog(RevokeNotifyDefaultTokenService.class);
    
    private ApplicationEventPublisher eventPublisher;

    @Override
    public boolean revokeToken(String tokenValue) {
        boolean removed = super.revokeToken(tokenValue);
        if (removed) {
            publishEvent(new TokenRevokeEvent(this, tokenValue));
        }
        return removed;
    }
    
    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.eventPublisher = applicationEventPublisher;
    }
    
    private void publishEvent(ApplicationEvent event) {
        if (this.eventPublisher != null) {
            try {
                this.eventPublisher.publishEvent(event);
            }
            catch (Throwable ex) {
                logger.error("Error publishing " + event + ".", ex);
            }
        }
    }
}
