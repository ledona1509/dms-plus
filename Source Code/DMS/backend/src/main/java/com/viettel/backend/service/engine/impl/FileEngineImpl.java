package com.viettel.backend.service.engine.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.viettel.backend.file.DbFile;
import com.viettel.backend.file.DbFileMeta;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.FileRepository;
import com.viettel.backend.service.engine.FileEngine;
import com.viettel.backend.util.ImageUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Component
public class FileEngineImpl implements FileEngine {

    private static final long serialVersionUID = -4196457639452792913L;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${url.backend.file}")
    private String backendUrl;

    @Autowired
    private FileRepository fileRepository;
    
    @PostConstruct
    public void init() {
    	Assert.notNull(backendUrl, "Parameter \"url.backend.file\" cannot be null");
    	if (!backendUrl.endsWith("/")) {
    		backendUrl = backendUrl + "/";
    	}
    }
    
    @Override
    public DbFile getImage(String fileId) {
        return fileRepository.getImageById(ObjectIdUtils.getObjectId(fileId));
    }

    @Override
    public String storeImage(InputStream inputStream, String originalFileName, String contentType,
            DbFileMeta meta, String sizeType) {
        try {
            // TODO Check Extension
            byte[] bytes = IOUtils.toByteArray(inputStream);

            bytes = ImageUtils.resizeImage(bytes, sizeType);

            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);

            return fileRepository.storeImage(bis, originalFileName, contentType, meta);
        } catch (IOException e) {
            logger.error("InputStream is not correct", e);
            throw new UnsupportedOperationException("error when write photo");
        }
    }
    
    @Override
    public String store(Object context, InputStream inputStream, String originalFileName, String contentType,
            DbFileMeta meta) {
        if (context == null || !(context instanceof UserLogin)) {
            throw new UnsupportedOperationException("context invalid");
        }

        ObjectId clientId = ((UserLogin) context).getClientId();
        
        return fileRepository.store(clientId, inputStream, originalFileName, contentType, meta);
    }

    @Override
    public DbFile get(Object context, String fileId) {
        if (context == null || !(context instanceof UserLogin)) {
            throw new UnsupportedOperationException("context invalid");
        }

        ObjectId clientId = ((UserLogin) context).getClientId();
        
        return fileRepository.getFileById(clientId, ObjectIdUtils.getObjectId(fileId));
    }

    @Override
    public String getLink(String fileId) {
        return backendUrl + fileId;
    }

    @Override
    public void delete(Object context, String fileId) {
        if (context == null || !(context instanceof UserLogin)) {
            throw new UnsupportedOperationException("context invalid");
        }

        ObjectId clientId = ((UserLogin) context).getClientId();
        
        fileRepository.delete(clientId, ObjectIdUtils.getObjectId(fileId));
    }

    @Override
    public void delete(Object context, Collection<String> fileIds) {
        if (context == null || !(context instanceof UserLogin)) {
            throw new UnsupportedOperationException("context invalid");
        }

        ObjectId clientId = ((UserLogin) context).getClientId();
        
        fileRepository.delete(clientId, ObjectIdUtils.getObjectIds(fileIds));
    }

    @Override
    public void markAsUsed(Object context, String fileId) {
        if (context == null || !(context instanceof UserLogin)) {
            throw new UnsupportedOperationException("context invalid");
        }

        ObjectId clientId = ((UserLogin) context).getClientId();
        
        fileRepository.markAsUsed(clientId, ObjectIdUtils.getObjectId(fileId));
    }

    @Override
    public void markAsUsed(Object context, Collection<String> fileIds) {
        if (context == null || !(context instanceof UserLogin)) {
            throw new UnsupportedOperationException("context invalid");
        }

        ObjectId clientId = ((UserLogin) context).getClientId();
        
        fileRepository.markAsUsed(clientId, ObjectIdUtils.getObjectIds(fileIds));
    }

    @Override
    public boolean exists(Object context, String fileId) {
        if (context == null || !(context instanceof UserLogin)) {
            throw new UnsupportedOperationException("context invalid");
        }

        ObjectId clientId = ((UserLogin) context).getClientId();

        return fileRepository.exists(clientId, ObjectIdUtils.getObjectId(fileId));
    }

    @Override
    public boolean exists(Object context, Collection<String> fileIds) {
        if (context == null || !(context instanceof UserLogin)) {
            throw new UnsupportedOperationException("context invalid");
        }

        ObjectId clientId = ((UserLogin) context).getClientId();
        
        return fileRepository.existsAll(clientId, ObjectIdUtils.getObjectIds(fileIds));
    }

}
