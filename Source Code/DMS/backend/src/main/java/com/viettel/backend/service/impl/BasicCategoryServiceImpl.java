package com.viettel.backend.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.POSearchable;
import com.viettel.backend.dto.DTO;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.BasicCategoryService;
import com.viettel.backend.util.ObjectIdUtils;

public abstract class BasicCategoryServiceImpl<DOMAIN extends POSearchable, SIMPLEDTO extends DTO, DETAILDTO extends SIMPLEDTO, CREATEDTO extends Serializable>
        extends AbstractService implements BasicCategoryService<DOMAIN, SIMPLEDTO, DETAILDTO, CREATEDTO> {

    private static final long serialVersionUID = -3379246152761225425L;

    public abstract BasicCategoryRepository<DOMAIN> getRepository();

    public abstract boolean checkRole(UserLogin userLogin);

    public abstract DOMAIN createDomain(UserLogin userLogin, CREATEDTO createdto);

    public abstract void updateDomain(UserLogin userLogin, DOMAIN domain, CREATEDTO createdto);

    public abstract SIMPLEDTO createSimpleDto(UserLogin userLogin, DOMAIN domain);

    public abstract DETAILDTO createDetailDto(UserLogin userLogin, DOMAIN domain);
    
    public abstract String noPermissionExceptionCode();
    
    protected void checkBeforeDelete(UserLogin userLogin, ObjectId id) {
        //DO NOTHING
    }

    protected Sort getSort() {
        return null;
    }

    /** Get all active PO */
    @Override
    public ListJson<SIMPLEDTO> getAll(UserLogin userLogin) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        List<DOMAIN> domains = getRepository().getAll(userLogin.getClientId());
        if (domains == null || domains.isEmpty()) {
            return ListJson.<SIMPLEDTO> emptyList();
        }

        List<SIMPLEDTO> dtos = new ArrayList<SIMPLEDTO>();
        for (DOMAIN domain : domains) {
            dtos.add(createSimpleDto(userLogin, domain));
        }

        return new ListJson<SIMPLEDTO>(dtos, Long.valueOf(dtos.size()));
    }

    @Override
    public ListJson<SIMPLEDTO> getList(UserLogin userLogin, String search, Boolean draft, Pageable pageable) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        List<DOMAIN> domains = getRepository().getListWithDraft(userLogin.getClientId(), search, draft, pageable, getSort());
        if (domains == null || domains.isEmpty()) {
            return ListJson.<SIMPLEDTO> emptyList();
        }

        List<SIMPLEDTO> dtos = new ArrayList<SIMPLEDTO>();
        for (DOMAIN domain : domains) {
            dtos.add(createSimpleDto(userLogin, domain));
        }

        long size = Long.valueOf(dtos.size());
        if (pageable != null) {
            if (pageable.getPageNumber() > 0 || size == pageable.getPageSize()) {
                size = getRepository().countWithDraft(userLogin.getClientId(), search, draft);
            }
        }

        return new ListJson<SIMPLEDTO>(dtos, size);
    }

    @Override
    public DETAILDTO getById(UserLogin userLogin, String _id) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        DOMAIN domain = getRepository().getByIdWithDraft(userLogin.getClientId(), id);
        if (domain == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }

        return createDetailDto(userLogin, domain);
    }

    @Override
    public ObjectId create(UserLogin userLogin, CREATEDTO dto) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        DOMAIN domain = createDomain(userLogin, dto);

        domain = getRepository().save(userLogin.getClientId(), domain);

        return domain.getId();
    }

    @Override
    public ObjectId update(UserLogin userLogin, String _id, CREATEDTO createdto) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        DOMAIN domain = getRepository().getByIdWithDraft(userLogin.getClientId(), id);
        if (domain == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }

        updateDomain(userLogin, domain, createdto);

        domain = getRepository().save(userLogin.getClientId(), domain);

        return domain.getId();
    }

    @Override
    public boolean enable(UserLogin userLogin, String _id) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        getRepository().enable(userLogin.getClientId(), id);

        return true;
    }

    @Override
    public boolean delete(UserLogin userLogin, String _id) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }
        
        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        
        checkBeforeDelete(userLogin, id);

        return this.getRepository().delete(userLogin.getClientId(), id);
    }

}
