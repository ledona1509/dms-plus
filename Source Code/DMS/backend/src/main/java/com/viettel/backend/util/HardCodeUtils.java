package com.viettel.backend.util;

public class HardCodeUtils {

    public static final String ROLE_SUPER_ADMIN = "SUPER";
    public static final String ROLE_ADMIN = "AD";
    public static final String ROLE_SUPERVISOR = "SUP";
    public static final String ROLE_SALESMAN = "SM";
    public static final String ROLE_DISTRIBUTOR_ADMIN = "DISAD";
    
}
