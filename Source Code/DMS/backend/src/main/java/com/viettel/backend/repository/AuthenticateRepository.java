package com.viettel.backend.repository;

import java.io.Serializable;

import com.viettel.backend.domain.User;

public interface AuthenticateRepository extends Serializable {
	
	public User findByUserName(String username);
	
}
