package com.viettel.backend.service;

import com.viettel.backend.domain.UOM;
import com.viettel.backend.dto.UOMDto;

public interface AdminUOMService extends BasicCategoryService<UOM, UOMDto, UOMDto, UOMDto> {

}
