package com.viettel.backend.dto;

import java.util.ArrayList;
import java.util.List;

import com.viettel.backend.dto.embed.SurveyQuestionResultDto;

public class SurveyResultDto extends SurveySimpleDto {

    private static final long serialVersionUID = -880147501301988482L;

    private List<SurveyQuestionResultDto> questions;

    public List<SurveyQuestionResultDto> getQuestions() {
        return questions;
    }

    public void setQuestions(List<SurveyQuestionResultDto> questions) {
        this.questions = questions;
    }
    
    public void addQuestion(SurveyQuestionResultDto question) {
        if (this.questions == null) {
            this.questions = new ArrayList<SurveyQuestionResultDto>();
        }
        
        this.questions.add(question);
    }

}
