package com.viettel.backend.dto;

import com.viettel.backend.domain.SalesConfig;
import com.viettel.backend.oauth2.core.UserLogin;

public class SalesConfigDto extends DTO {

    private static final long serialVersionUID = 4618363876249230856L;

    private long visitDurationKPI;
    private double visitDistanceKPI;
    private double meetReqRate;

    public SalesConfigDto() {
        super();
    }

    public SalesConfigDto(UserLogin userLogin, SalesConfig salesConfig) {
        super(salesConfig);

        this.visitDurationKPI = salesConfig.getVisitDurationKPI();
        this.visitDistanceKPI = salesConfig.getVisitDistanceKPI();
        this.meetReqRate = salesConfig.getMeetReqRate();
    }

    public long getVisitDurationKPI() {
        return visitDurationKPI;
    }

    public void setVisitDurationKPI(long visitDurationKPI) {
        this.visitDurationKPI = visitDurationKPI;
    }

    public double getVisitDistanceKPI() {
        return visitDistanceKPI;
    }

    public void setVisitDistanceKPI(double visitDistanceKPI) {
        this.visitDistanceKPI = visitDistanceKPI;
    }

    public double getMeetReqRate() {
        return meetReqRate;
    }

    public void setMeetReqRate(double meetReqRate) {
        this.meetReqRate = meetReqRate;
    }

}
