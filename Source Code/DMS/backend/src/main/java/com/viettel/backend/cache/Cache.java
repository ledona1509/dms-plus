package com.viettel.backend.cache;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

public class Cache<K, V extends I_Cachable<V>> implements I_Cache<K, V>, Serializable {

    private static final long serialVersionUID = 1L;
    
    private ConcurrentHashMap<K, V> map = new ConcurrentHashMap<K, V>();

    public Cache() {
        this.map = new ConcurrentHashMap<K, V>();
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public V remove(K key) {
        return map.remove(key);
    }

    @Override
    public V put(K key, V value) {
        return map.put(key, value.cloneForCache());
    }
    
    @Override
    public V get(K key) {
        V value = map.get(key);
        if (value != null) {
            value = value.cloneForCache();
        }
        return value;
    }
    
}
