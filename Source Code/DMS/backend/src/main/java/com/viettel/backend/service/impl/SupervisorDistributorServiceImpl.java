package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Distributor;
import com.viettel.backend.dto.DistributorSimpleDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.service.SupervisorDistributorService;

@Service
public class SupervisorDistributorServiceImpl extends AbstractSupervisorService implements SupervisorDistributorService {

    private static final long serialVersionUID = 4822917139294672054L;

    @Autowired
    private DistributorRepository distributorRepository;

    @Override
    public ListJson<DistributorSimpleDto> getDistbutors(UserLogin userLogin) {
        checkIsSupervisor(userLogin);

        List<Distributor> distributors = distributorRepository.getDistributorsBySupervisors(userLogin.getClientId(),
                Arrays.asList(userLogin.getUserId()));

        if (distributors == null || distributors.isEmpty()) {
            return ListJson.emptyList();
        }

        List<DistributorSimpleDto> dtos = new ArrayList<DistributorSimpleDto>(distributors.size());
        for (Distributor distributor : distributors) {
            dtos.add(new DistributorSimpleDto(distributor));
        }

        return new ListJson<DistributorSimpleDto>(dtos, (long) dtos.size());
    }

}
