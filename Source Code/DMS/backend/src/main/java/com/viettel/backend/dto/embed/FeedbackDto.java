package com.viettel.backend.dto.embed;

import java.io.Serializable;

public class FeedbackDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5051588225542322295L;

	private String createdDate;
    private String message;
    
    public String getCreatedDate() {
		return createdDate;
	}
    
    public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
    
    public String getMessage() {
		return message;
	}
    
    public void setMessage(String message) {
		this.message = message;
	}
    
}
