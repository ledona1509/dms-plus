package com.viettel.backend.domain.embed;

import java.io.Serializable;
import java.util.List;

public class CustomerScheduleItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5890415709083121260L;
	
	public static final String COLUMNNAME_DAYS = "days";
	public static final String COLUMNNAME_WEEKS = "weeks";

	private List<Integer> days;
	
	private List<Integer> weeks;

	public List<Integer> getDays() {
		return days;
	}

	public void setDays(List<Integer> days) {
		this.days = days;
	}

	public List<Integer> getWeeks() {
		return weeks;
	}

	public void setWeeks(List<Integer> weeks) {
		this.weeks = weeks;
	}
	
}
