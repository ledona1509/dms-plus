package com.viettel.backend.service.impl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.UOM;
import com.viettel.backend.dto.UOMDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.repository.UOMRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminUOMService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class AdminUOMServiceImpl extends BasicCategoryServiceImpl<UOM, UOMDto, UOMDto, UOMDto> implements
        AdminUOMService {

    private static final long serialVersionUID = -5771173067596328856L;

    @Autowired
    private UOMRepository uomRepository;
    
    @Autowired
    private ProductRepository productRepository;

    @Override
    public BasicCategoryRepository<UOM> getRepository() {
        return uomRepository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN);
    }
    
    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.ADMIN_ONLY;
    }
    
    @Override
    protected void checkBeforeDelete(UserLogin userLogin, ObjectId id) {
        if (productRepository.checkUOMUsed(userLogin.getClientId(), id)) {
            throw new BusinessException(ExceptionCode.UOM_LINK_TO_PRODUCT);
        }
    }
    
    @Override
    public UOM createDomain(UserLogin userLogin, UOMDto createdto) {
        checkMandatoryParams(createdto, createdto.getCode(), createdto.getName());
        
        if (uomRepository.existsByName(userLogin.getClientId(), null, createdto.getName())) {
            throw new BusinessException(ExceptionCode.SAME_NAME_EXIST);
        }
        
        if (uomRepository.existsByCode(userLogin.getClientId(), null, createdto.getCode())) {
            throw new BusinessException(ExceptionCode.SAME_CODE_EXIST);
        }

        UOM uom = new UOM();
        uom.setDraft(true);

        initPOWhenCreate(UOM.class, userLogin, uom);

        uom.setCode(createdto.getCode());
        uom.setName(createdto.getName());

        return uom;
    }

    @Override
    public void updateDomain(UserLogin userLogin, UOM domain, UOMDto createdto) {
        if (!domain.isDraft()) {
            throw new BusinessException(ExceptionCode.CANNOT_UPDATE_NO_DRAFT_DATA);
        }
        
        if (uomRepository.existsByName(userLogin.getClientId(), domain.getId(), createdto.getName())) {
            throw new BusinessException(ExceptionCode.SAME_NAME_EXIST);
        }
        
        if (uomRepository.existsByCode(userLogin.getClientId(), domain.getId(), createdto.getCode())) {
            throw new BusinessException(ExceptionCode.SAME_CODE_EXIST);
        }
        
        checkMandatoryParams(createdto, createdto.getCode(), createdto.getName());

        domain.setCode(createdto.getCode());
        domain.setName(createdto.getName());
    }

    @Override
    public UOMDto createSimpleDto(UserLogin userLogin, UOM domain) {
        return new UOMDto(domain);
    }

    @Override
    public UOMDto createDetailDto(UserLogin userLogin, UOM domain) {
        return createSimpleDto(userLogin, domain);
    }

}
