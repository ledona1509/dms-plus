package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Promotion;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.PromotionSimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.PromotionRepository;
import com.viettel.backend.service.SalesmanPromotionService;

@Service
public class SalesmanPromotionServiceImpl extends AbstractSalesmanService implements SalesmanPromotionService {

    private static final long serialVersionUID = 2812222944238656195L;

    @Autowired
    private PromotionRepository promotionRepository;

    @Override
    public ListJson<PromotionSimpleDto> getPromtionsAvailableToday(UserLogin userLogin) {
        checkIsSalesman(userLogin);

        ObjectId clientId = userLogin.getClientId();

        List<Promotion> promotions = promotionRepository.getPromotionsAvailableToday(clientId);
        if (promotions == null || promotions.isEmpty()) {
            return ListJson.<PromotionSimpleDto> emptyList();
        }
        
        List<PromotionSimpleDto> dtos = new ArrayList<PromotionSimpleDto>(promotions.size());
        for (Promotion promotion : promotions) {
            PromotionSimpleDto dto = new PromotionSimpleDto(promotion);
            dtos.add(dto);
        }
        
        return new ListJson<PromotionSimpleDto>(dtos, (long) dtos.size());
    }

}
