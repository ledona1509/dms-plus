package com.viettel.backend.websocket.queue;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public abstract class DelayObject implements Delayed {
	
	public static final long DEFAULT_DELAY_IN_MILLIS = 2000l;
	
	private long startTime;
	
	public abstract void excute();
	
	public DelayObject(long delay) {
		this.startTime = System.currentTimeMillis() + delay;
	}
	
	@Override
    public int compareTo(Delayed o) {
		if (this.startTime < ((DelayObject) o).startTime) {
            return -1;
        }
		
        if (this.startTime > ((DelayObject) o).startTime) {
            return 1;
        }
        
        return 0;
    }

	@Override
    public long getDelay(TimeUnit unit) {
		long diff = startTime - System.currentTimeMillis();
        return unit.convert(diff, TimeUnit.MILLISECONDS);
    }
	
}
