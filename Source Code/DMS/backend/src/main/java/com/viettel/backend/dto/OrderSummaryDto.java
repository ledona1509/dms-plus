package com.viettel.backend.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class OrderSummaryDto implements Serializable {

    private static final long serialVersionUID = 7584629448715597869L;

    private String id;

    private String code;
    private String createdTime;

    private BigDecimal productivity;
    private BigDecimal grandTotal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

}
