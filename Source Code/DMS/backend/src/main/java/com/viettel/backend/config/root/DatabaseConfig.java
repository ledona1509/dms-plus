package com.viettel.backend.config.root;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoFactoryBean;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

import com.mongodb.Mongo;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoOptions;
import com.viettel.backend.domain.PO;

@SuppressWarnings("deprecation")
@Configuration
public class DatabaseConfig extends AbstractMongoConfiguration {

	public DatabaseConfig(){
		super();
	}
	
	@Value("${database.host}")
	private String host;
	
	@Value("${database.port}")
	private int port;
	
	@Value("${database.databasename}")
	private String dbName;
	
	@Value("${database.username}")
	private String username;
	
	@Value("${database.password}")
	private String password;
	
	@Value("${database.connection.per.host:100}")
	private int connectionPerHost;
	
	@Value("${database.max.waiting.thead:1500}")
	private int maxWaitingThread;
	
    public MongoFactoryBean mongoFactoryBean() throws Exception {
	    MongoClientOptions.Builder builder = new MongoClientOptions.Builder();
	    builder.connectionsPerHost(connectionPerHost);
	    builder.threadsAllowedToBlockForConnectionMultiplier(maxWaitingThread);
	    
		MongoFactoryBean mongoFactoryBean = new MongoFactoryBean();
		mongoFactoryBean.setHost(host);
		mongoFactoryBean.setPort(port);
		mongoFactoryBean.setMongoOptions(new MongoOptions(builder.build()));
		mongoFactoryBean.afterPropertiesSet();
		return mongoFactoryBean;
	}
	
	@Override
	protected UserCredentials getUserCredentials (){
		UserCredentials userCredentials = null;
		
		if (!StringUtils.isEmpty(username)
				&& !StringUtils.isEmpty(password)) {
			userCredentials = new UserCredentials(username, password);
		}
		
		return userCredentials;
	}
	
//	@Override
//	public CustomConversions customConversions() {
//	    return new CustomConversions(Arrays.asList(
//	            doubleToBigDecimalConverter(), 
//	            bigDecimalToDoubleConverter()));
//	}

	@Override
	protected String getDatabaseName() {
		return dbName;
	}

	@Bean
	@Override
	public Mongo mongo() throws Exception {
		return mongoFactoryBean().getObject();
	}
	
	@Override
	protected String getMappingBasePackage() {
		String domainPackageName = ClassUtils.getPackageName(PO.class);
	    return domainPackageName;
	}
	
	@Bean
	public GridFsTemplate gridFsTemplate() throws Exception {
		return new GridFsTemplate(mongoDbFactory(), mappingMongoConverter());
	}
	
//	@Bean
//	public DoubleToBigDecimalConverter doubleToBigDecimalConverter() {
//	    return new DoubleToBigDecimalConverter();
//	}
//	
//	@Bean
//	public BigDecimalToDoubleConverter bigDecimalToDoubleConverter() {
//	    return new BigDecimalToDoubleConverter();
//	}
//	
//	public static class DoubleToBigDecimalConverter implements Converter<Double, BigDecimal> {
//
//	    @Override
//	    public BigDecimal convert(Double source) {
//	        return new BigDecimal(source);
//	    }
//	}
//
//	public static class BigDecimalToDoubleConverter implements Converter<BigDecimal, Double> {
//
//	    @Override
//	    public Double convert(BigDecimal source) {
//	        return source.doubleValue();
//	    }
//	}
}
