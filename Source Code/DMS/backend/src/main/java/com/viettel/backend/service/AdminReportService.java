package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.report.DaySalesReportDto;
import com.viettel.backend.dto.report.DistributorSalesReportDto;
import com.viettel.backend.dto.report.ProductSalesReportDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface AdminReportService extends Serializable {

    public ListJson<DaySalesReportDto> getDaySalesReport(UserLogin userLogin, int month, int year);

    public ListJson<DistributorSalesReportDto> getDistributorSalesReport(UserLogin userLogin, int month, int year);

    public ListJson<ProductSalesReportDto> getProductSalesReport(UserLogin userLogin, String productCategoryId,
            int month, int year);
    
}
