package com.viettel.backend.repository.impl;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.Counter;
import com.viettel.backend.repository.CodeGeneratorRepository;

@Repository
public class CodeGeneratorRepositoryImpl implements CodeGeneratorRepository {

    private static final long serialVersionUID = 2110219098776321841L;

    @Autowired
    protected MongoTemplate dataTemplate;

    private int getNextSequence(String keyValue, int startValue, int step) {
        Counter counter = dataTemplate.findAndModify(query(where("_id").is(keyValue)), new Update().inc("seq", step),
                options().returnNew(true), Counter.class);

        if (counter == null) {
            counter = new Counter();
            counter.setId(keyValue);
            counter.setSeq(startValue);
            dataTemplate.insert(counter);
        }

        return counter.getSeq();
    }

    @Override
    public String getDistributorCode(String clientId) {
        String keyValue = "distributor_" + clientId;
        return "DIS" + String.format("%04d", getNextSequence(keyValue, 1, 1));
    }

    @Override
    public String getOrderCode(String clientId) {
        String keyValue = "order_" + clientId;
        return "ORDER" + String.format("%09d", getNextSequence(keyValue, 1, 1));
    }

    @Override
    public String getCustomerCode(String clientId) {
        String keyValue = "customer_" + clientId;
        return "CUS" + String.format("%06d", getNextSequence(keyValue, 1, 1));
    }

}
