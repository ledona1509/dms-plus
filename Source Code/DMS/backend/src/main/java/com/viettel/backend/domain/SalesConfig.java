package com.viettel.backend.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.cache.I_Cachable;

@Document(collection = "SalesConfig")
public class SalesConfig extends PO implements I_Cachable<SalesConfig> {

    private static final long serialVersionUID = -4995404568124100927L;

    private long visitDurationKPI;
    private double visitDistanceKPI;
    private double meetReqRate;

    public long getVisitDurationKPI() {
        return visitDurationKPI;
    }

    public void setVisitDurationKPI(long visitDurationKPI) {
        this.visitDurationKPI = visitDurationKPI;
    }

    public double getVisitDistanceKPI() {
        return visitDistanceKPI;
    }

    public void setVisitDistanceKPI(double visitDistanceKPI) {
        this.visitDistanceKPI = visitDistanceKPI;
    }

    public double getMeetReqRate() {
        return meetReqRate;
    }

    public void setMeetReqRate(double meetReqRate) {
        this.meetReqRate = meetReqRate;
    }

    @Override
    public SalesConfig cloneForCache() {
        SalesConfig salesConfig = new SalesConfig();
        PO.copy(this, salesConfig);
        salesConfig.setVisitDurationKPI(visitDurationKPI);
        salesConfig.setVisitDistanceKPI(visitDistanceKPI);
        salesConfig.setMeetReqRate(meetReqRate);
        return salesConfig;
    }

}
