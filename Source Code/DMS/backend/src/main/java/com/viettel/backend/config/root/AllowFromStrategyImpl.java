package com.viettel.backend.config.root;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.header.writers.frameoptions.AllowFromStrategy;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

public class AllowFromStrategyImpl implements AllowFromStrategy{
	
	private static final String HEADER_ATR_REFERER = "Referer";
	private static final String DEFAULT_ORIGIN_REQUEST_PARAMETER = "rframe";
	

	private String allowFromParameterName = DEFAULT_ORIGIN_REQUEST_PARAMETER;
	
	private Collection<String> allowed = new ArrayList<>();

    /**
     * Creates a new instance
     * @param allowed the origins that are allowed.
     */
    public AllowFromStrategyImpl(Collection<String> alloweds) {
    	super();
        Assert.notEmpty(alloweds, "Allowed origins cannot be empty.");
        for(String allowed : alloweds) {
        	this.allowed.add(getOrigin(allowed));
        }
        
    }
    
    protected boolean allowed(String allowFromOrigin) {
        return allowed.contains(allowFromOrigin);
    }
	
    @Override
	public String getAllowFromValue(HttpServletRequest request) {
		String allowFromOrigin = request.getParameter(allowFromParameterName);
		
		if (allowFromOrigin == null) {
			return null;
		}
		
		String referer = request.getHeader(HEADER_ATR_REFERER);

		if (referer == null) {
			return null;
		}
		
		String origin = getOrigin(referer);

		if (StringUtils.hasText(origin) && allowed(origin)) {
			return origin;
		}

		return null;
	}
	
	private String getOrigin(String url) {
		if (!url.startsWith("http:") && !url.startsWith("https:")) {
			return url;
		}

		String origin = null;

		try {
			URI uri = new URI(url);
			origin = uri.getScheme() + "://" + uri.getAuthority();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		return origin;
	}
    
    
}
