package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.RoleDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface AdminRoleService extends Serializable {
	
	public ListJson<RoleDto> getRoles(UserLogin userLogin);
	
}
