package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.embed.CustomerSchedule;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.entity.SimpleDate;

/**
 * Repository for customer approved
 * 
 * @author trungkh
 *
 */
public interface CustomerRepository extends BasicCategoryRepository<Customer> {

    public List<Customer> getCustomers(ObjectId clientId, String search, Pageable pageable, Sort sort);

    public Set<ObjectId> getCustomerIds(ObjectId clientId, String search);

    public long count(ObjectId clientId, String search);

    //** BY DISTRIBUTOR **
    public List<Customer> getCustomersByDistributor(ObjectId clientId, ObjectId distributorId, boolean withAvailable,
            String search, Pageable pageable, Sort sort);

    public Set<ObjectId> getCustomerIdsByDistributor(ObjectId clientId, ObjectId distributorId, boolean withAvailable,
            String search);

    public long countCustomersByDistributor(ObjectId clientId, ObjectId distributorId, boolean withAvailable,
            String search);

    //** AVAILABLE **
    public List<Customer> getCustomersAvailable(ObjectId clientId, String search, Pageable pageable, Sort sort);

    public long countCustomersAvailable(ObjectId clientId, String search);

    //** UNSCHEDULED **
    public List<Customer> getCustomersByDistributorUnscheduled(ObjectId clientId, ObjectId distributorId,
            String search, Pageable pageable, Sort sort);

    public Set<ObjectId> getCustomerIdsByDistributorUnscheduled(ObjectId clientId, ObjectId distributorId, String search);

    public long countCustomersByDistributorUnscheduled(ObjectId clientId, ObjectId distributorId, String search);

    /** UPDATE DISTRIBUTOR OF CUSTOMER BY IDs AND CLEAR ALL SCHEDULE IN OPTION */
    public void updateDistributorForCustomer(ObjectId clientId, Collection<ObjectId> customerIds,
            DistributorEmbed distributor, boolean clearSchedule);

    //** EDIT SCHEDULE **
    public void clearCustomerScheduleBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds);
    
    public void clearCustomerSchedule(ObjectId clientId, Collection<ObjectId> customerIds);

    public void updateCustomerSchedule(ObjectId clientId, ObjectId customerId, CustomerSchedule customerSchedule);

    //** BY SUPERVISOR **
    public List<Customer> getCustomersBySupervisor(ObjectId clientId, ObjectId supervisorId, Pageable pageable, Sort sort);

    public Set<ObjectId> getCustomerIdsBySupervisor(ObjectId clientId, ObjectId supervisorId);

    public long countCustomerBySupervisor(ObjectId clientId, ObjectId supervisorId);
    
    //** BY SALESMAN **
    public List<Customer> getCustomersBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, String search,
            Pageable pageable, Sort sort);

    public Set<ObjectId> getCustomerIdsBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, String search);

    public long countCustomersBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, String search);

    public List<Customer> getCustomersBySalesmenDate(ObjectId clientId, Collection<ObjectId> salesmanIds,
            String search, SimpleDate date, Pageable pageable, Sort sort);

    public Set<ObjectId> getCustomerIdsBySalesmenDate(ObjectId clientId, Collection<ObjectId> salesmanIds,
            String search, SimpleDate date);

    public long countCustomersBySalesmenDate(ObjectId clientId, Collection<ObjectId> salesmanIds, String search,
            SimpleDate date);
    
    public boolean checkCustomerTypeUsed(ObjectId clientId, ObjectId customerTypeId);
    
    public boolean checkDistributorUsed(ObjectId clientId, ObjectId distributorId);
}
