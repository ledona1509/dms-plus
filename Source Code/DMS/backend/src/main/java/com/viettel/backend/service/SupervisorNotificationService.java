package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.SupervisorNotificationDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SupervisorNotificationService extends Serializable {

    public SupervisorNotificationDto getNotification(UserLogin userLogin);

}
