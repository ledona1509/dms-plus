package com.viettel.backend.restful.supervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ExhibitionSimpleDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SupervisorExhibitionService;

@RestController(value="supervisorExhibitionController")
@RequestMapping(value = "/supervisor/exhibition")
public class ExhibitionController extends AbstractController {

    private static final long serialVersionUID = -3492046947585355471L;

    @Autowired
    private SupervisorExhibitionService supervisorExhibitionService;

    /** Lấy danh sách các chương trình trưng bày */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getExhibitons(@RequestParam(value = "q", required = false) String search,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        ListJson<ExhibitionSimpleDto> results = supervisorExhibitionService.getExhibitions(getUserLogin(), search,
                getPageRequest(page, size));
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

}
