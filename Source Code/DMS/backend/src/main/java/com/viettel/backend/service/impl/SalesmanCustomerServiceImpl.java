package com.viettel.backend.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.CustomerType;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.CustomerCreateDto;
import com.viettel.backend.dto.CustomerForVisitDto;
import com.viettel.backend.dto.CustomerSimpleDto;
import com.viettel.backend.dto.CustomerSummaryDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.LocationDto;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CodeGeneratorRepository;
import com.viettel.backend.repository.CustomerPendingRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.CustomerTypeRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SalesmanCustomerService;
import com.viettel.backend.service.engine.FileEngine;
import com.viettel.backend.service.engine.WebNotificationEngine;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.ObjectIdUtils;
import com.viettel.backend.util.StringUtils;

@Service
public class SalesmanCustomerServiceImpl extends AbstractSalesmanService implements SalesmanCustomerService {

    private static final long serialVersionUID = 2812222944238656195L;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerPendingRepository customerPendingRepository;

    @Autowired
    private CustomerTypeRepository customerTypeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private FileEngine fileEngine;

    @Autowired
    private WebNotificationEngine webNotificationEngine;
    
    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Override
    public CustomerSummaryDto getCustomerSummary(UserLogin userLogin, String id) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, id);

        ObjectId clientId = userLogin.getClientId();

        CustomerSummaryDto dto = new CustomerSummaryDto(customer);

        // THIS MONTH
        List<Order> poOfThisMonths = orderRepository.getOrderByCustomers(clientId, Arrays.asList(customer.getId()),
                DateTimeUtils.getPeriodThisMonth(), null, null, null);
        if (poOfThisMonths != null && !poOfThisMonths.isEmpty()) {
            BigDecimal output = BigDecimal.ZERO;
            for (Order order : poOfThisMonths) {
                if (order.getDetails() != null && !order.getDetails().isEmpty()) {
                    for (OrderDetail detail : order.getDetails()) {
                        output = output.add(detail.getOutput());
                    }
                }
            }

            dto.setOutputThisMonth(output);
            dto.setOrdersThisMonth(poOfThisMonths.size());
        } else {
            dto.setOutputThisMonth(BigDecimal.ZERO);
            dto.setOrdersThisMonth(0);
        }

        List<Map<String, Object>> revenueLastThreeMonth = new ArrayList<Map<String, Object>>(3);
        SimpleDate lastMonth = DateTimeUtils.getFirstOfLastMonth();
        // LAST MONTH
        BigDecimal revenue = BigDecimal.ZERO;
        List<Order> poOfLastMonths = orderRepository.getOrderByCustomers(clientId, Arrays.asList(customer.getId()),
                DateTimeUtils.getPeriodLastMonth(), null, null, null);
        if (poOfLastMonths != null && !poOfLastMonths.isEmpty()) {
            BigDecimal output = BigDecimal.ZERO;
            for (Order order : poOfLastMonths) {
                if (order.getDetails() != null && !order.getDetails().isEmpty()) {
                    for (OrderDetail detail : order.getDetails()) {
                        output = output.add(detail.getOutput());
                    }
                }
                if (order.getGrandTotal() != null) {
                    revenue = revenue.add(order.getGrandTotal());
                }
            }

            dto.setOutputLastMonth(output);
        } else {
            dto.setOutputLastMonth(BigDecimal.ZERO);
        }
        Map<String, Object> revenueDto = new HashMap<String, Object>();
        revenueDto.put("month", lastMonth.format("MM/yyyy"));
        revenueDto.put("revenue", revenue);
        revenueLastThreeMonth.add(revenueDto);

        // 2 MONTH AGO
        revenue = BigDecimal.ZERO;
        List<Order> poOf2MonthAgo = orderRepository.getOrderByCustomers(clientId, Arrays.asList(customer.getId()),
                DateTimeUtils.getPeriodLastsMonth(2), null, null, null);
        if (poOf2MonthAgo != null && !poOf2MonthAgo.isEmpty()) {
            for (Order order : poOf2MonthAgo) {
                if (order.getGrandTotal() != null) {
                    revenue = revenue.add(order.getGrandTotal());
                }
            }

        }
        revenueDto = new HashMap<String, Object>();
        revenueDto.put("month", DateTimeUtils.addMonths(lastMonth, -1).format("MM/yyyy"));
        revenueDto.put("revenue", revenue);
        revenueLastThreeMonth.add(revenueDto);

        // 3 MONTH AGO
        revenue = BigDecimal.ZERO;
        List<Order> poOf3MonthAgo = orderRepository.getOrderByCustomers(clientId, Arrays.asList(customer.getId()),
                DateTimeUtils.getPeriodLastsMonth(3), null, null, null);
        if (poOf3MonthAgo != null && !poOf3MonthAgo.isEmpty()) {
            for (Order order : poOf3MonthAgo) {
                if (order.getGrandTotal() != null) {
                    revenue = revenue.add(order.getGrandTotal());
                }
            }

        }
        revenueDto = new HashMap<String, Object>();
        revenueDto.put("month", DateTimeUtils.addMonths(lastMonth, -2).format("MM/yyyy"));
        revenueDto.put("revenue", revenue);
        revenueLastThreeMonth.add(revenueDto);

        dto.setRevenueLastThreeMonth(revenueLastThreeMonth);

        List<Order> lastFiveOrders = orderRepository.getLastOrderByCustomer(clientId, customer.getId(), 5);

        if (lastFiveOrders == null || lastFiveOrders.isEmpty()) {
            dto.setLastFiveOrders(Collections.<Map<String, Object>> emptyList());
        } else {
            List<Map<String, Object>> lastFiveOrderDtos = new ArrayList<Map<String, Object>>(5);
            for (Order order : lastFiveOrders) {
                Map<String, Object> orderDto = new HashMap<String, Object>();
                orderDto.put("date", order.getCreatedTime().getIsoTime());
                orderDto.put("skuNumber", order.getDetails() == null ? 0 : order.getDetails().size());
                orderDto.put("total", order.getGrandTotal());
                lastFiveOrderDtos.add(orderDto);
            }

            dto.setLastFiveOrders(lastFiveOrderDtos);
        }

        return dto;
    }

    @Override
    public ListJson<CustomerForVisitDto> getCustomersForVisitToday(UserLogin userLogin) {
        checkIsSalesman(userLogin);

        ObjectId clientId = userLogin.getClientId();
        ObjectId salesmanId = userLogin.getUserId();

        Collection<Customer> todayCustomers = customerRepository.getCustomersBySalesmenDate(clientId,
                Arrays.asList(salesmanId), null, DateTimeUtils.getToday(), null, null);
        if (todayCustomers == null || todayCustomers.isEmpty()) {
            return ListJson.<CustomerForVisitDto> emptyList();
        }

        List<ObjectId> todayCustomerIds = new ArrayList<ObjectId>(todayCustomers.size());
        for (Customer customer : todayCustomers) {
            todayCustomerIds.add(customer.getId());
        }

        Map<ObjectId, Visit> visitTodayByCustomerId = visitRepository.getMapVisitByCustomerIdsToday(clientId,
                todayCustomerIds);

        List<ObjectId> customerIdsOrderByVisitLastWeek = getCustomerIdsOrderByVisitLastWeek(clientId, salesmanId);

        List<CustomerForVisitDto> customerDtos = new ArrayList<CustomerForVisitDto>(todayCustomers.size());
        for (Customer customer : todayCustomers) {
            // STATUS
            int status = CustomerForVisitDto.STATUS_UNVISITED;
            Visit visit = visitTodayByCustomerId.get(customer.getId());
            if (visit != null) {
                if (visit.getVisitStatus() == Visit.VISIT_STATUS_VISITING) {
                    status = CustomerForVisitDto.STATUS_VISITING;
                } else {
                    status = CustomerForVisitDto.STATUS_VISITED;
                }
            }

            // ORDER
            int seqNo = customerIdsOrderByVisitLastWeek.indexOf(customer.getId());
            if (seqNo == -1) {
                seqNo = customerIdsOrderByVisitLastWeek.size();
            }

            CustomerForVisitDto customerDto = new CustomerForVisitDto(customer, true, status, seqNo);
            customerDtos.add(customerDto);
        }

        return new ListJson<>(customerDtos, Long.valueOf(customerDtos.size()));
    }

    @Override
    public ListJson<CustomerForVisitDto> getCustomersForVisitAll(UserLogin userLogin) {
        checkIsSalesman(userLogin);

        ObjectId clientId = userLogin.getClientId();
        ObjectId salesmanId = userLogin.getUserId();

        Collection<Customer> allCustomers = customerRepository.getCustomersBySalesmen(clientId,
                Arrays.asList(salesmanId), null, null, null);
        if (allCustomers == null || allCustomers.isEmpty()) {
            return ListJson.<CustomerForVisitDto> emptyList();
        }
        Set<ObjectId> allCustomerIds = new HashSet<ObjectId>();
        for (Customer customer : allCustomers) {
            allCustomerIds.add(customer.getId());
        }

        Collection<Customer> todayCustomers = customerRepository.getCustomersBySalesmenDate(clientId,
                Arrays.asList(salesmanId), null, DateTimeUtils.getToday(), null, null);
        Set<ObjectId> todayCustomerIds = new HashSet<ObjectId>();
        if (todayCustomers != null && !todayCustomers.isEmpty()) {
            for (Customer customer : todayCustomers) {
                todayCustomerIds.add(customer.getId());
            }
        }

        Map<ObjectId, Visit> visitTodayByCustomerId = visitRepository.getMapVisitByCustomerIdsToday(clientId,
                todayCustomerIds);

        List<ObjectId> customerIdsOrderByVisitLastWeek = getCustomerIdsOrderByVisitLastWeek(clientId, salesmanId);

        List<CustomerForVisitDto> customerDtos = new ArrayList<CustomerForVisitDto>(todayCustomers.size());
        for (Customer customer : allCustomers) {
            if (!todayCustomerIds.contains(customer.getId())) {
                CustomerForVisitDto customerDto = new CustomerForVisitDto(customer, false,
                        CustomerForVisitDto.STATUS_UNVISITED, 0);
                customerDtos.add(customerDto);
            } else {
                // TRUONG HOP TRONG TUYEN
                // STATUS
                int status = CustomerForVisitDto.STATUS_UNVISITED;
                Visit visit = visitTodayByCustomerId.get(customer.getId());
                if (visit != null) {
                    if (visit.getVisitStatus() == Visit.VISIT_STATUS_VISITING) {
                        status = CustomerForVisitDto.STATUS_VISITING;
                    } else {
                        status = CustomerForVisitDto.STATUS_VISITED;
                    }
                }

                // ORDER
                int seqNo = customerIdsOrderByVisitLastWeek.indexOf(customer.getId());
                if (seqNo == -1) {
                    seqNo = customerIdsOrderByVisitLastWeek.size();
                }

                CustomerForVisitDto customerDto = new CustomerForVisitDto(customer, true, status, seqNo);
                customerDtos.add(customerDto);
            }
        }

        return new ListJson<>(customerDtos, Long.valueOf(customerDtos.size()));
    }

    @Override
    public void updatePhone(UserLogin userLogin, String id, String phone) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, id);

        ObjectId clientId = userLogin.getClientId();

        if (StringUtils.isNullOrEmpty(phone)) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        customer.setPhone(phone);

        customerRepository.save(clientId, customer);
    }

    @Override
    public void updateMobile(UserLogin userLogin, String id, String mobile) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, id);

        ObjectId clientId = userLogin.getClientId();

        if (StringUtils.isNullOrEmpty(mobile)) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        customer.setMobile(mobile);

        customerRepository.save(clientId, customer);
    }

    @Override
    public void updateLocation(UserLogin userLogin, String id, LocationDto locationDto) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, id);

        ObjectId clientId = userLogin.getClientId();

        if (locationDto == null) {
            throw new BusinessException(ExceptionCode.INVALID_LOCATION_PARAM);
        }

        double[] location = new double[] { locationDto.getLongitude(), locationDto.getLatitude() };

        customer.setLocation(location);

        customerRepository.save(clientId, customer);
    }

    @Override
    public String register(UserLogin userLogin, CustomerCreateDto dto) {
        checkIsSalesman(userLogin);

        ObjectId clientId = userLogin.getClientId();
        ObjectId salesmanId = userLogin.getUserId();

        Customer customer = new Customer();

        initPOWhenCreate(Customer.class, userLogin, customer);

        customer.setName(dto.getName());
        customer.setCode(codeGeneratorRepository.getCustomerCode(userLogin.getClientId().toString()));
        customer.setMobile(dto.getMobile());
        customer.setPhone(dto.getPhone());
        customer.setContact(dto.getContact());
        customer.setIdNumber(dto.getIdNumber());
        customer.setEmail(dto.getEmail());
        customer.setAddress(dto.getAddress());
        customer.setDescription(dto.getDescription());

        ObjectId customerTypeId = ObjectIdUtils.getObjectId(dto.getCustomerTypeId(), null);
        if (customerTypeId != null) {
            CustomerType customerType = customerTypeRepository.getById(userLogin.getClientId(), customerTypeId);
            if (customerType == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            customer.setCustomerType(customerType);
        }

        // XXX check not link photo here
        if (dto.getPhotos() != null && dto.getPhotos().size() > 0) {
            if (!fileEngine.exists(userLogin, dto.getPhotos())) {
                throw new BusinessException(ExceptionCode.INVALID_PHOTO_PARAM);
            }
            customer.setPhotos(dto.getPhotos());
        }

        if (dto.getLatitude() > 0 & dto.getLongitude() > 0) {
            customer.setLocation(new double[] { dto.getLongitude(), dto.getLatitude() });
        }

        User salesman = userRepository.getById(clientId, salesmanId);
        if (salesman.getDistributor() == null) {
            throw new BusinessException(ExceptionCode.SALESMAN_NO_DISTRIBUTOR);
        }
        customer.setDistributor(salesman.getDistributor());

        customer.setApproveStatus(Customer.APPROVE_STATUS_PENDING);
        customer.setCreatedBy(new UserEmbed(salesman));
        customer.setCreatedTime(DateTimeUtils.getCurrentTime());

        customer = customerPendingRepository.save(userLogin.getClientId(), customer);

        Distributor distributor = distributorRepository.getById(clientId, salesman.getDistributor().getId());
        if (distributor.getSupervisor() != null && distributor.getSupervisor().getId() != null) {
            webNotificationEngine.notifyNewCustomerForSupervisor(userLogin, distributor.getSupervisor().getId());
        }

        return customer.getId().toString();
    }

    @Override
    public ListJson<CustomerSimpleDto> getCustomersRegistered(UserLogin userLogin, String search, Pageable pageable) {
        checkIsSalesman(userLogin);

        ObjectId clientId = userLogin.getClientId();
        ObjectId salesmanId = userLogin.getUserId();

        Sort sort = new Sort(Direction.DESC, Customer.COLUMNNAME_CREATED_TIME_VALUE);
        Collection<Customer> customers = customerPendingRepository.getCustomersByCreatedUsers(clientId,
                Arrays.asList(salesmanId), null, search, pageable, sort);
        if (customers == null || customers.isEmpty()) {
            return ListJson.<CustomerSimpleDto> emptyList();
        }

        List<CustomerSimpleDto> dtos = new ArrayList<CustomerSimpleDto>();
        for (Customer customer : customers) {
            CustomerSimpleDto dto = new CustomerSimpleDto(customer);
            dtos.add(dto);
        }

        long size = customers.size();
        if (pageable != null) {
            if (pageable.getPageNumber() > 0 || pageable.getPageSize() == size) {
                size = customerPendingRepository.countCustomersByCreatedUsers(clientId, Arrays.asList(salesmanId),
                        null, null);
            }
        }

        return new ListJson<CustomerSimpleDto>(dtos, size);
    }

    // ** PRIVATE **
    private List<ObjectId> getCustomerIdsOrderByVisitLastWeek(ObjectId clientId, ObjectId salesmanId) {
        // SMART ORDER
        Sort sort = new Sort(Visit.COLUMNNAME_START_TIME_VALUE);
        Period sameDayLastWeek = DateTimeUtils.getPeriodOneDay(DateTimeUtils.addWeeks(DateTimeUtils.getToday(), -1));
        List<Visit> visitsLastWeek = visitRepository.getVisitedsBySalesmen(clientId, Arrays.asList(salesmanId),
                sameDayLastWeek, sort);
        List<ObjectId> customerIdsLastWeek = new ArrayList<ObjectId>();
        if (visitsLastWeek != null && !visitsLastWeek.isEmpty()) {
            for (Visit visit : visitsLastWeek) {
                customerIdsLastWeek.add(visit.getCustomer().getId());
            }
        }

        return customerIdsLastWeek;
    }

}
