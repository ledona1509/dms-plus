package com.viettel.backend.service.engine.impl;

import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Order;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerPendingRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.FeedbackRepository;
import com.viettel.backend.repository.OrderPendingRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.service.engine.WebNotificationEngine;
import com.viettel.backend.websocket.core.Websocket;
import com.viettel.backend.websocket.queue.DelayObject;
import com.viettel.backend.websocket.queue.DelayQueueConsumer;

@Component
public class WebNotificationEngineImpl implements WebNotificationEngine {

    private static final long serialVersionUID = -4843458026392503128L;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String TOPIC_NEW_ORDER = "/topic/new/order";
    private static final String TOPIC_NEW_CUSTOMER = "/topic/new/customer";
    private static final String TOPIC_NEW_FEEDBACK = "/topic/new/feedback";

    private static final String TOPIC_CHANGED_ORDER = "/topic/changed/order";
    private static final String TOPIC_CHANGED_CUSTOMER = "/topic/changed/customer";
    private static final String TOPIC_CHANGED_FEEDBACK = "/topic/changed/feedback";

    // Creates an instance of blocking queue using the DelayQueue.
    private BlockingQueue<DelayObject> queue;
    private DelayQueueConsumer consumer;

    @Autowired
    private OrderPendingRepository orderPendingRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerPendingRepository customerPendingRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Autowired
    private UserRepository userRepository;

    public WebNotificationEngineImpl() {
        this.queue = new DelayQueue<DelayObject>();
        this.consumer = new DelayQueueConsumer(queue);
        this.consumer.start();
    }

    @Override
    public void notifyNewOrderForSupervisor(final UserLogin userLogin, final ObjectId supervisorId) {
        DelayObject delayObject = new DelayObject(DelayObject.DEFAULT_DELAY_IN_MILLIS) {
            @Override
            public void excute() {
                Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                        userLogin.getClientId(), Arrays.asList(supervisorId));
                long value = orderPendingRepository.countOrderByDistributors(userLogin.getClientId(), distributorIds,
                        Order.APPROVE_STATUS_PENDING, null);

                Websocket.sendToUser(supervisorId.toString(), TOPIC_NEW_ORDER, userLogin.getUserId().toString(), value);
            }
        };

        try {
            this.queue.put(delayObject);
        } catch (InterruptedException e) {
            logger.error("queue error", e);
        }
    }

    @Override
    public void notifyChangedOrderForSupervisor(final UserLogin userLogin, final ObjectId supervisorId) {
        DelayObject delayObject = new DelayObject(DelayObject.DEFAULT_DELAY_IN_MILLIS) {
            @Override
            public void excute() {
                Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                        userLogin.getClientId(), Arrays.asList(supervisorId));
                long value = orderPendingRepository.countOrderByDistributors(userLogin.getClientId(), distributorIds,
                        Order.APPROVE_STATUS_PENDING, null);

                Websocket.sendToUser(supervisorId.toString(), TOPIC_CHANGED_ORDER, userLogin.getUserId().toString(),
                        value);
            }
        };

        try {
            this.queue.put(delayObject);
        } catch (InterruptedException e) {
            logger.error("queue error", e);
        }
    }

    @Override
    public void notifyNewFeedbackForSupervisor(final UserLogin userLogin, final ObjectId supervisorId) {
        DelayObject delayObject = new DelayObject(DelayObject.DEFAULT_DELAY_IN_MILLIS) {
            @Override
            public void excute() {
                Set<ObjectId> customerIds = customerRepository.getCustomerIdsBySupervisor(userLogin.getClientId(), supervisorId);
                long value = feedbackRepository.countFeedbackByCustomersUnread(userLogin.getClientId(), customerIds);

                Websocket.sendToUser(supervisorId.toString(), TOPIC_NEW_FEEDBACK, userLogin.getUserId().toString(),
                        value);
            }
        };

        try {
            this.queue.put(delayObject);
        } catch (InterruptedException e) {
            logger.error("queue error", e);
        }
    }

    @Override
    public void notifyChangedFeedbackForSupervisor(final UserLogin userLogin, final ObjectId supervisorId) {
        DelayObject delayObject = new DelayObject(DelayObject.DEFAULT_DELAY_IN_MILLIS) {
            @Override
            public void excute() {
                Set<ObjectId> customerIds = customerRepository.getCustomerIdsBySupervisor(userLogin.getClientId(), supervisorId);
                long value = feedbackRepository.countFeedbackByCustomersUnread(userLogin.getClientId(), customerIds);

                Websocket.sendToUser(supervisorId.toString(), TOPIC_CHANGED_FEEDBACK, userLogin.getUserId().toString(),
                        value);
            }
        };

        try {
            this.queue.put(delayObject);
        } catch (InterruptedException e) {
            logger.error("queue error", e);
        }
    }

    @Override
    public void notifyNewCustomerForSupervisor(final UserLogin userLogin, final ObjectId supervisorId) {
        DelayObject delayObject = new DelayObject(DelayObject.DEFAULT_DELAY_IN_MILLIS) {
            @Override
            public void excute() {
                Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                        userLogin.getClientId(), Arrays.asList(supervisorId));
                long value = customerPendingRepository.countCustomersByDistributors(userLogin.getClientId(),
                        distributorIds, Customer.APPROVE_STATUS_PENDING, null);

                Websocket.sendToUser(supervisorId.toString(), TOPIC_NEW_CUSTOMER, userLogin.getUserId().toString(),
                        value);
            }
        };

        try {
            this.queue.put(delayObject);
        } catch (InterruptedException e) {
            logger.error("queue error", e);
        }
    }

    @Override
    public void notifyChangedCustomerForSupervisor(final UserLogin userLogin, final ObjectId supervisorId) {
        DelayObject delayObject = new DelayObject(DelayObject.DEFAULT_DELAY_IN_MILLIS) {
            @Override
            public void excute() {
                Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                        userLogin.getClientId(), Arrays.asList(supervisorId));
                long value = customerPendingRepository.countCustomersByDistributors(userLogin.getClientId(),
                        distributorIds, Customer.APPROVE_STATUS_PENDING, null);

                Websocket.sendToUser(supervisorId.toString(), TOPIC_CHANGED_CUSTOMER, userLogin.getUserId().toString(),
                        value);
            }
        };

        try {
            this.queue.put(delayObject);
        } catch (InterruptedException e) {
            logger.error("queue error", e);
        }
    }

}
