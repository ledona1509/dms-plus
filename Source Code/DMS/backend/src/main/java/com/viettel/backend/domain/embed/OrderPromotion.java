package com.viettel.backend.domain.embed;

import java.util.LinkedList;
import java.util.List;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.NameCategory;
import com.viettel.backend.engine.promotion.I_PromotionResult;

public class OrderPromotion extends NameCategory implements I_PromotionResult<ObjectId, ProductEmbed> {

    private static final long serialVersionUID = 6620807032420284594L;

    private List<OrderPromotionDetail> details;
    
    @SuppressWarnings("unchecked")
    public List<OrderPromotionDetail> getDetails() {
        return details;
    }
    
    public void setDetails(List<OrderPromotionDetail> details) {
        this.details = details;
    }
    
    public void addDetail(OrderPromotionDetail detail) {
        if (this.details == null) {
            this.details = new LinkedList<OrderPromotionDetail>();
        }
        
        this.details.add(detail);
    }

}
