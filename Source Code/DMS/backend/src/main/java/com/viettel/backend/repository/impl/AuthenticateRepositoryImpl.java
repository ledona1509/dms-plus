package com.viettel.backend.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.User;
import com.viettel.backend.domain.PO;
import com.viettel.backend.repository.AuthenticateRepository;

@Repository
public class AuthenticateRepositoryImpl implements AuthenticateRepository {

    private static final long serialVersionUID = 5331643104671190941L;

    @Autowired
    private MongoTemplate dataTemplate;

    @Override
    public User findByUserName(String username) {
        Query query = new Query().addCriteria(Criteria.where(PO.COLUMNNAME_ACTIVE).is(true)
                .and(User.COLUMNNAME_USERNAME).is(username).and(PO.COLUMNNAME_DRAFT).is(false));

        return dataTemplate.findOne(query, User.class);
    }

}
