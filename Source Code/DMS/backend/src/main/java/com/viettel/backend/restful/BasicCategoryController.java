package com.viettel.backend.restful;

import java.io.Serializable;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.viettel.backend.domain.POSearchable;
import com.viettel.backend.dto.DTO;
import com.viettel.backend.dto.IdDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.service.BasicCategoryService;

public abstract class BasicCategoryController<DOMAIN extends POSearchable, SIMPLEDTO extends DTO, DETAILDTO extends SIMPLEDTO, CREATEDTO extends Serializable>
        extends AbstractController implements Serializable {

    private static final long serialVersionUID = -3792697128018949936L;

    protected abstract BasicCategoryService<DOMAIN, SIMPLEDTO, DETAILDTO, CREATEDTO> getService();

    @RequestMapping(value = "", method = RequestMethod.POST)
    public final ResponseEntity<?> create(@RequestBody @Valid CREATEDTO dto) {
        return _create(dto);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public final ResponseEntity<?> update(@PathVariable String id, @RequestBody @Valid CREATEDTO dto) {
        return _update(id, dto);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public final ResponseEntity<?> detail(@PathVariable String id) {
        return _detail(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public final ResponseEntity<?> delete(@PathVariable String id) {
        return _delete(id);
    }

    // LIST
    @RequestMapping(value = "", method = RequestMethod.GET)
    public final ResponseEntity<?> getList(@RequestParam(value = "q", required = false) String search,
            @RequestParam(required = false) Boolean draft, @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size) {
        return _getList(search, draft, getPageRequest(page, size));
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public final ResponseEntity<?> getAll() {
        return _getAll();
    }
    
    @RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
    public final ResponseEntity<?> enable(@PathVariable String id) {
        return _enable(id);
    }

    // NEW
    protected ResponseEntity<?> _create(CREATEDTO dto) {
        ObjectId domainId = getService().create(getUserLogin(), dto);
        if (domainId == null) {
            throw new UnsupportedOperationException("error when create");
        }

        return new Envelope(new IdDto(domainId.toString())).toResponseEntity(HttpStatus.OK);
    }

    // UPDATE
    protected ResponseEntity<?> _update(String id, CREATEDTO dto) {
        ObjectId domainId = getService().update(getUserLogin(), id, dto);
        if (domainId == null) {
            throw new UnsupportedOperationException("error when update");
        }

        return new Envelope(new IdDto(domainId.toString())).toResponseEntity(HttpStatus.OK);
    }

    // DETAIL
    protected ResponseEntity<?> _detail(String id) {
        DETAILDTO dto = getService().getById(getUserLogin(), id);

        if (dto == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }

        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    // DELETE
    protected ResponseEntity<?> _delete(String id) {
        boolean result = getService().delete(getUserLogin(), id);
        if (!result) {
            throw new UnsupportedOperationException("error when delete");
        }

        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

    // LIST
    protected ResponseEntity<?> _getList(String search, Boolean draft, Pageable pageable) {
        ListJson<SIMPLEDTO> dtos = getService().getList(getUserLogin(), search, draft, pageable);
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }

    // GET ALL
    protected ResponseEntity<?> _getAll() {
        ListJson<SIMPLEDTO> dtos = getService().getAll(getUserLogin());
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }
    
    // ENABLE
    protected ResponseEntity<?> _enable(String id) {
        getService().enable(getUserLogin(), id);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

}
