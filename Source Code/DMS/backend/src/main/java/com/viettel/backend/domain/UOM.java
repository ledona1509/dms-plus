package com.viettel.backend.domain;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "UOM")
public class UOM extends NameCategory {

    private static final long serialVersionUID = 863511075009914692L;

    public static final String COLUMNNAME_CODE = "code";

    private String code;

    public UOM() {
        super();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String[] getSearchValues() {
        return new String[] { getCode(), getName() };
    }
}
