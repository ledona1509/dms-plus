package com.viettel.backend.oauth2.core;

import javax.sql.DataSource;

import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;

public class MamJdbcAuthorizationCodeServices extends JdbcAuthorizationCodeServices {

	private static final String SELECT_STATEMENT = "select code, authentication from o_oauth_code where code = ?";
	private static final String INSERT_STATEMENT = "insert into o_oauth_code (code, authentication) values (?, ?)";
	private static final String DELETE_STATEMENT = "delete from o_oauth_code where code = ?";

	public MamJdbcAuthorizationCodeServices(DataSource dataSource) {
		super(dataSource);
		
		setDeleteAuthenticationSql(DELETE_STATEMENT);
		setInsertAuthenticationSql(INSERT_STATEMENT);
		setSelectAuthenticationSql(SELECT_STATEMENT);
	}
}

