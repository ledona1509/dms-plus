package com.viettel.backend.util;

import java.util.Collection;
import java.util.HashMap;

/**
 *
 * @author kimhatrung
 */
public class HashMapDoubleKey<K1,K2,V>{
    
    private HashMap<HashMapDoubleKey<K1,K2,V>.Key,V> map;

    public HashMapDoubleKey() {
        this.map = new HashMap<Key, V>();
    }
    
    public V get(K1 k1,K2 k2){
        return this.map.get(new Key(k1, k2));
    }
    
    public V put (K1 k1, K2 k2, V value){
        return this.map.put(new Key(k1, k2), value);
    }
    
    public boolean containsKey(K1 k1, K2 k2){
        return map.containsKey(new Key(k1, k2));
    }
    
    public boolean containsValue(V value){
        return map.containsValue(value);
    }
    
    public void clear(){
        this.map.clear();
    }
     public Collection<V> values(){
         return this.values();
     }
    
    
    class Key{
        private K1 k1;
        private K2 k2;

        public Key(K1 k1, K2 k2) {
            this.k1 = k1;
            this.k2 = k2;
        }
        
        

        @SuppressWarnings("rawtypes")
        @Override
        public boolean equals(Object o) {
            if(!(o instanceof HashMapDoubleKey.Key)){
                return false;
            }else{
                HashMapDoubleKey.Key other = (HashMapDoubleKey.Key) o;
                return (k1.equals(other.k1) && k2.equals(other.k2));
            }
        }

        @Override
        public int hashCode() {
            return k1.hashCode()+k2.hashCode();
        }
    }
}
