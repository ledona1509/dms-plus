package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.User;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.UserDto;
import com.viettel.backend.dto.UserSimpleDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminUserService;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;
import com.viettel.backend.util.PasswordUtils;

@Service
public class AdminUserServiceImpl extends BasicCategoryServiceImpl<User, UserSimpleDto, UserDto, UserDto> implements
        AdminUserService {

    private static final long serialVersionUID = -5771173067596328856L;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Override
    public BasicCategoryRepository<User> getRepository() {
        return userRepository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN);
    }
    
    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.ADMIN_ONLY;
    }
    
    @Override
    protected void checkBeforeDelete(UserLogin userLogin, ObjectId id) {
        User user = userRepository.getByIdWithDraft(userLogin.getClientId(), id);
        if (user == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }
        
        if (user.getId().equals(userLogin.getUserId())) {
            throw new BusinessException(ExceptionCode.CANNOT_DELETE_CURRENT_USER);
        }
        
        if (user.getRoles().contains(HardCodeUtils.ROLE_SUPERVISOR)) {
            if (distributorRepository.checkSupervisorUsed(userLogin.getClientId(), id)) {
                throw new BusinessException(ExceptionCode.USER_LINK_TO_DISTRIBUTOR);
            }
        } else if (user.getRoles().contains(HardCodeUtils.ROLE_SALESMAN)) {
            if (user.getDistributor() != null) {
                throw new BusinessException(ExceptionCode.USER_LINK_TO_DISTRIBUTOR);
            }
        }
    }

    @Override
    public User createDomain(UserLogin userLogin, UserDto createdto) {
        checkMandatoryParams(createdto, createdto.getUsername(), createdto.getFullname(), createdto.getRole());

        User user = new User();
        user.setDraft(true);
        user.setPassword(PasswordUtils.getDefaultPassword());

        initPOWhenCreate(User.class, userLogin, user);

        user.setUsername(createdto.getUsername());
        user.setFullname(createdto.getFullname());
        user.setRoles(Arrays.asList(createdto.getRole()));

        return user;
    }

    @Override
    public void updateDomain(UserLogin userLogin, User domain, UserDto createdto) {
        if (!domain.isDraft()) {
            throw new BusinessException(ExceptionCode.CANNOT_UPDATE_NO_DRAFT_DATA);
        }

        checkMandatoryParams(createdto, createdto.getUsername(), createdto.getFullname(), createdto.getRole());

        domain.setUsername(createdto.getUsername());
        domain.setFullname(createdto.getFullname());
        domain.setRoles(Arrays.asList(createdto.getRole()));
    }

    @Override
    public UserSimpleDto createSimpleDto(UserLogin userLogin, User domain) {
        return new UserSimpleDto(domain);
    }

    @Override
    public UserDto createDetailDto(UserLogin userLogin, User domain) {
        return new UserDto(domain);
    }

    @Override
    public void resetPassword(UserLogin userLogin, String id) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        ObjectId userId = ObjectIdUtils.getObjectId(id, null);
        if (userId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        User user = userRepository.getById(userLogin.getClientId(), userId);
        if (user == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        user.setPassword(PasswordUtils.getDefaultPassword());

        userRepository.save(userLogin.getClientId(), user);
    }
    
    @Override
    public ListJson<UserSimpleDto> getSalesmen(UserLogin userLogin, String _distributorId) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        List<User> users = null;
        if (_distributorId == null) {
            users = userRepository.getUsersByRole(userLogin.getClientId(), HardCodeUtils.ROLE_SALESMAN);
            if (users == null || users.isEmpty()) {
                return ListJson.<UserSimpleDto> emptyList();
            }
        } else  {
            ObjectId distributorId = ObjectIdUtils.getObjectId(_distributorId, null);
            if (distributorId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            if (!distributorRepository.exists(userLogin.getClientId(), distributorId)) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            
            users = userRepository.getSalesmenByDistributors(userLogin.getClientId(),
                    Arrays.asList(distributorId));
            if (users == null || users.isEmpty()) {
                return ListJson.<UserSimpleDto> emptyList();
            }
        }

        List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(users.size());
        for (User user : users) {
            dtos.add(new UserSimpleDto(user));
        }

        return new ListJson<UserSimpleDto>(dtos, (long) dtos.size());
    }

    @Override
    public ListJson<UserSimpleDto> getSupervisors(UserLogin userLogin) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        List<User> users = userRepository.getUsersByRole(userLogin.getClientId(), HardCodeUtils.ROLE_SUPERVISOR);
        if (users == null || users.isEmpty()) {
            return ListJson.<UserSimpleDto> emptyList();
        }

        List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(users.size());
        for (User user : users) {
            dtos.add(new UserSimpleDto(user));
        }

        return new ListJson<UserSimpleDto>(dtos, (long) dtos.size());
    }

    @Override
    public ListJson<UserSimpleDto> getSalesmanByDistributorWithAvailable(UserLogin userLogin, String _distributorId) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        ObjectId distributorId = ObjectIdUtils.getObjectId(_distributorId, null);
        if (distributorId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        if (!distributorRepository.exists(userLogin.getClientId(), distributorId)) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        
        List<User> users = userRepository.getSalesmenByDistributors(userLogin.getClientId(),
                Arrays.asList(distributorId, null));
        if (users == null || users.isEmpty()) {
            return ListJson.<UserSimpleDto> emptyList();
        }

        List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(users.size());
        for (User user : users) {
            dtos.add(new UserSimpleDto(user));
        }

        return new ListJson<UserSimpleDto>(dtos, (long) dtos.size());
    }
    
}
