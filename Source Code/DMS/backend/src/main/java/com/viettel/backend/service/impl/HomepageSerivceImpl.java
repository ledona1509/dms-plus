package com.viettel.backend.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.cache.Cache;
import com.viettel.backend.cache.CacheManager;
import com.viettel.backend.cache.I_Cachable;
import com.viettel.backend.cache.I_Cache;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.Target;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.dto.HomepageDto;
import com.viettel.backend.dto.embed.Result;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CalendarConfigRepository;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.TargetRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.HomepageService;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.Quicksort;

@Service
public class HomepageSerivceImpl extends AbstractService implements HomepageService {

    private static final long serialVersionUID = 5686134592376322356L;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CalendarConfigRepository calendarConfigRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Autowired
    private TargetRepository targetRepository;

    @Autowired
    private CacheManager cacheManager;

    @Override
    public HomepageDto getHomepage(UserLogin userLogin) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_ADMIN) && !userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        ObjectId clientId = userLogin.getClientId();

        HomepageDto dto = new HomepageDto();

        SimpleDate today = DateTimeUtils.getToday();
        dto.setToday(today.getIsoDate());

        Set<ObjectId> salesmanIds;
        if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            salesmanIds = userRepository.getSalesmanIdsBySupervisors(clientId, Arrays.asList(userLogin.getUserId()));
            if (salesmanIds == null || salesmanIds.isEmpty()) {
                return dto;
            }
        } else {
            List<User> salesmen = userRepository.getUsersByRole(clientId, HardCodeUtils.ROLE_SALESMAN);
            salesmanIds = new HashSet<ObjectId>();
            if (salesmen == null || salesmen.isEmpty()) {
                return dto;
            }
            for (User salesman : salesmen) {
                salesmanIds.add(salesman.getId());
            }
        }

        // LAST MONTH
        calculateLastMonth(clientId, userLogin.getUserId(), dto, salesmanIds);

        // THIS MONTH WITHOUT TODAY
        calculateThisMonthWithoutToday(clientId, userLogin.getUserId(), dto, salesmanIds);

        // TODAY REVENUE
        Period todayPeriod = DateTimeUtils.getPeriodToday();
        List<Order> ordersToday = orderRepository.getOrderBySalesmen(clientId, salesmanIds, todayPeriod, null, null,
                null);
        List<ObjectId> orderIdsToday = new LinkedList<ObjectId>();

        if (ordersToday != null && !ordersToday.isEmpty()) {
            BigDecimal revenueToday = BigDecimal.ZERO;
            BigDecimal outputToday = BigDecimal.ZERO;
            int numberOrderNoVisit = 0;

            for (Order order : ordersToday) {
                orderIdsToday.add(order.getId());
                if (!order.isVisit()) {
                    numberOrderNoVisit++;
                }

                revenueToday = revenueToday.add(order.getGrandTotal());
                if (order.getDetails() != null) {
                    for (OrderDetail orderDetail : order.getDetails()) {
                        // TODO use output of product
                        outputToday = outputToday.add(orderDetail.getOutput());
                    }
                }
            }
            dto.setRevenueToday(revenueToday);
            dto.setOutputToday(outputToday);
            dto.setNumberOrderNoVisit(numberOrderNoVisit);
        }

        // TODAY VISIT
        List<Visit> visitsToday = visitRepository.getVisitedsBySalesmen(clientId, salesmanIds, todayPeriod, null);
        if (visitsToday != null && !visitsToday.isEmpty()) {
            int numberVisitHasOrder = 0;
            int numberVisitErrorTime = 0;

            if (visitsToday != null) {
                for (Visit visit : visitsToday) {
                    if (visit.isOrder()) {
                        numberVisitHasOrder++;
                    }

                    long duration = SimpleDate.getDuration(visit.getStartTime(), visit.getEndTime());
                    if (duration < getSalesConfig(userLogin).getVisitDurationKPI()) {
                        numberVisitErrorTime++;
                    }
                }
            }
            dto.setNumberVisit(visitsToday.size());
            dto.setNumberVisitHasOrder(numberVisitHasOrder);
            dto.setNumberVisitErrorTime(numberVisitErrorTime);
        }

        return dto;
    }

    @SuppressWarnings("unchecked")
    private void calculateThisMonthWithoutToday(ObjectId clientId, ObjectId userId, HomepageDto dto,
            Set<ObjectId> salesmanIds) {
        SimpleDate today = DateTimeUtils.getToday();
        Period period = new Period(DateTimeUtils.getFirstOfThisMonth(), today);

        String cacheNameSummary = "homepage.this.month.summary.by.period.of.user:";

        // TIM XEM DA CO CACHE CUA SUPERVISOR HOAC ADMIN CHUA
        I_Cache<String, ThisMonthSummary> cacheSummary = cacheManager.getCache(cacheNameSummary + userId.toString());

        if (cacheSummary == null) {
            cacheSummary = new Cache<String, ThisMonthSummary>();
            cacheManager.putCache(cacheNameSummary + userId.toString(), cacheSummary);
        }

        ThisMonthSummary thisMonthSummary = cacheSummary.get(period.toString());

        // KHONG CO CACHE SUMMARY
        if (thisMonthSummary == null) {
            thisMonthSummary = new ThisMonthSummary();

            List<Order> ordersThisMonth = orderRepository.getOrderBySalesmen(clientId, salesmanIds, period, null, null,
                    null);
            HashMap<ObjectId, BigDecimal> revenueBySalesman = new HashMap<ObjectId, BigDecimal>();

            if (ordersThisMonth != null && !ordersThisMonth.isEmpty()) {
                int orderByDayThisMonth = 0;
                BigDecimal revenueByOrderThisMonth = BigDecimal.ZERO;
                BigDecimal outputByOrderThisMonth = BigDecimal.ZERO;
                BigDecimal revenueThisMonth = BigDecimal.ZERO;
                BigDecimal outputThisMonth = BigDecimal.ZERO;

                HashMap<ObjectId, HashMap<String, Object>> outputByProduct = new HashMap<ObjectId, HashMap<String, Object>>();

                for (Order order : ordersThisMonth) {
                    revenueThisMonth = revenueThisMonth.add(order.getGrandTotal());
                    if (order.getDetails() != null) {
                        for (OrderDetail orderDetail : order.getDetails()) {
                            // TODO use output of product
                            outputThisMonth = outputThisMonth.add(orderDetail.getOutput());

                            HashMap<String, Object> productAndOutput = outputByProduct.get(orderDetail.getProduct()
                                    .getId());
                            if (productAndOutput == null) {
                                productAndOutput = new HashMap<String, Object>();
                                productAndOutput.put("product", orderDetail.getProduct().getName());
                                productAndOutput.put("output", orderDetail.getOutput());

                                outputByProduct.put(orderDetail.getProduct().getId(), productAndOutput);
                            } else {
                                BigDecimal old = (BigDecimal) productAndOutput.get("output");
                                productAndOutput.put("output", old.add(orderDetail.getOutput()));
                            }
                        }
                    }

                    BigDecimal revenueOfSalesman = revenueBySalesman.get(order.getSalesman().getId());
                    if (revenueOfSalesman == null) {
                        revenueOfSalesman = BigDecimal.ZERO;
                    }
                    revenueOfSalesman = revenueOfSalesman.add(order.getGrandTotal());
                    revenueBySalesman.put(order.getSalesman().getId(), revenueOfSalesman);
                }

                // TOP 5 PRODUCT
                HashMap<String, Object>[] productAndOutputs = (HashMap<String, Object>[]) new HashMap[outputByProduct
                        .values().size()];
                outputByProduct.values().toArray(productAndOutputs);
                Quicksort.sort(productAndOutputs, new Quicksort.Comparator<Map<String, Object>>() {
                    public int compare(Map<String, Object> t1, Map<String, Object> t2) {
                        BigDecimal t1Output = (BigDecimal) t1.get("output");
                        BigDecimal t2Output = (BigDecimal) t2.get("output");

                        return t1Output.compareTo(t2Output);
                    };
                });
                int size = productAndOutputs.length < 5 ? productAndOutputs.length : 5;
                List<HashMap<String, Object>> top5products = new ArrayList<HashMap<String, Object>>(size);
                for (int i = 0; i < size; i++) {
                    top5products.add(productAndOutputs[productAndOutputs.length - i - 1]);
                }
                thisMonthSummary.top5products = top5products;

                List<SimpleDate> workingDays = calendarConfigRepository.getWorkingDays(clientId,
                        DateTimeUtils.getPeriodThisMonth());
                if (workingDays == null || workingDays.isEmpty()) {
                    throw new UnsupportedOperationException("working days not found");
                }
                int numberWorkingDayPassed = 0;
                for (SimpleDate date : workingDays) {
                    if (date.compareTo(today) >= 0) {
                        break;
                    }
                    numberWorkingDayPassed++;
                }

                orderByDayThisMonth = ordersThisMonth.size() / numberWorkingDayPassed;
                if (ordersThisMonth.size() != 0) {
                    revenueByOrderThisMonth = revenueThisMonth.divide(new BigDecimal(ordersThisMonth.size()), 0,
                            BigDecimal.ROUND_HALF_UP);
                    outputByOrderThisMonth = outputThisMonth.divide(new BigDecimal(ordersThisMonth.size()), 0,
                            BigDecimal.ROUND_HALF_UP);
                }

                thisMonthSummary.orderByDayThisMonth = orderByDayThisMonth;
                thisMonthSummary.revenueByOrderThisMonth = revenueByOrderThisMonth;
                thisMonthSummary.outputByOrderThisMonth = outputByOrderThisMonth;
                thisMonthSummary.revenueThisMonth = revenueThisMonth;
                thisMonthSummary.outputThisMonth = outputThisMonth;
            }

            // TARGET
            List<Target> targets = targetRepository.getTargetsBySalesmen(clientId, salesmanIds, today.getMonth(),
                    today.getYear(), null, null);
            if (targets != null && !targets.isEmpty()) {
                BigDecimal revenueTargetThisMonth = BigDecimal.ZERO;
                BigDecimal outputTargetThisMonth = BigDecimal.ZERO;

                List<HashMap<String, Object>> worst5salesmen = new LinkedList<HashMap<String, Object>>();

                for (Target target : targets) {
                    if (target.getRevenue() != null) {
                        revenueTargetThisMonth = revenueTargetThisMonth.add(target.getRevenue());

                        HashMap<String, Object> temp = new HashMap<String, Object>();
                        temp.put("salesmanId", target.getSalesman().getId().toString());
                        temp.put("salesman", target.getSalesman());
                        BigDecimal revenueOfSalesman = revenueBySalesman.get(target.getSalesman().getId());
                        Result tempResult = new Result(target.getRevenue(), revenueOfSalesman);
                        temp.put("result", tempResult);

                        if (worst5salesmen.size() == 5) {
                            Result higherResult = (Result) worst5salesmen.get(worst5salesmen.size() - 1).get("result");
                            if (tempResult.getPercentage() < higherResult.getPercentage()) {
                                worst5salesmen.remove(worst5salesmen.size() - 1);
                            }
                        }

                        if (worst5salesmen.size() < 5) {
                            boolean added = false;
                            for (int i = 0; i < worst5salesmen.size(); i++) {
                                Result oldResult = (Result) worst5salesmen.get(i).get("result");
                                if (tempResult.getPercentage() <= oldResult.getPercentage()) {
                                    worst5salesmen.add(i, temp);
                                    added = true;
                                    break;
                                }
                            }

                            if (!added) {
                                worst5salesmen.add(worst5salesmen.size(), temp);
                            }
                        }
                    }

                    if (target.getOutput() != null) {
                        outputTargetThisMonth = outputTargetThisMonth.add(target.getOutput());
                    }
                }

                thisMonthSummary.revenueTargetThisMonth = revenueTargetThisMonth;
                thisMonthSummary.outputTargetThisMonth = outputTargetThisMonth;
                thisMonthSummary.worst5salesmen = worst5salesmen;
            }
            
            cacheSummary.put(period.toString(), thisMonthSummary);
        }
        
        dto.setTop5products(thisMonthSummary.top5products);

        dto.setOrderByDayThisMonth(thisMonthSummary.orderByDayThisMonth);
        dto.setRevenueByOrderThisMonth(thisMonthSummary.revenueByOrderThisMonth);
        dto.setOutputByOrderThisMonth(thisMonthSummary.outputByOrderThisMonth);
        dto.setRevenueThisMonth(thisMonthSummary.revenueThisMonth);
        dto.setOutputThisMonth(thisMonthSummary.outputThisMonth);

        dto.setRevenueTargetThisMonth(thisMonthSummary.revenueTargetThisMonth);
        dto.setOutputTargetThisMonth(thisMonthSummary.outputTargetThisMonth);
        dto.setWorst5salesmen(thisMonthSummary.worst5salesmen);
    }

    @SuppressWarnings("unchecked")
    private void calculateLastMonth(ObjectId clientId, ObjectId userId, HomepageDto dto, Set<ObjectId> salesmanIds) {
        String cacheNameSummary = "homepage.last.month.summary.by.period.of.user:";
        String cacheNameDetail = "homepage.last.month.summary.by.salesman.and.period";

        Period lastMonthPeriod = DateTimeUtils.getPeriodLastMonth();

        // TIM XEM DA CO CACHE CUA SUPERVISOR HOAC ADMIN CHUA
        I_Cache<String, LastMonthSummary> cacheSummary = cacheManager.getCache(cacheNameSummary + userId.toString());

        if (cacheSummary == null) {
            cacheSummary = new Cache<String, LastMonthSummary>();
            cacheManager.putCache(cacheNameSummary + userId.toString(), cacheSummary);
        }

        LastMonthSummary lastMonthSummary = cacheSummary.get(lastMonthPeriod.toString());

        // KHONG CO CACHE SUMMARY
        if (lastMonthSummary == null) {
            int nbOrder = 0;
            BigDecimal outputLastMonth = BigDecimal.ZERO;
            BigDecimal revenueLastMonth = BigDecimal.ZERO;

            if (salesmanIds != null && !salesmanIds.isEmpty()) {
                // TIM CACHE CUA TUNG SALESMAN
                Set<ObjectId> salesmanNoCacheIds;
                I_Cache<String, LastMonthSummary> cacheDetail = cacheManager.getCache(cacheNameDetail);
                if (cacheDetail == null) {
                    cacheDetail = new Cache<String, LastMonthSummary>();
                    cacheManager.putCache(cacheNameDetail, cacheDetail);
                    salesmanNoCacheIds = salesmanIds;
                } else {
                    salesmanNoCacheIds = new HashSet<ObjectId>();
                    for (ObjectId salesmanId : salesmanIds) {
                        LastMonthSummary monthSummary = cacheDetail.get(salesmanId.toString()
                                + lastMonthPeriod.toString());
                        if (monthSummary != null) {
                            nbOrder = nbOrder + monthSummary.nbOrder;
                            revenueLastMonth = revenueLastMonth.add(monthSummary.revenue);
                            outputLastMonth = outputLastMonth.add(monthSummary.output);
                        } else {
                            salesmanNoCacheIds.add(salesmanId);
                        }
                    }
                }

                if (salesmanNoCacheIds != null && !salesmanNoCacheIds.isEmpty()) {
                    HashMap<ObjectId, LastMonthSummary> monthSummaryBySalesman = new HashMap<ObjectId, HomepageSerivceImpl.LastMonthSummary>();

                    List<Order> ordersLastMonth = orderRepository.getOrderBySalesmen(clientId, salesmanNoCacheIds,
                            lastMonthPeriod, null, null, null);
                    if (ordersLastMonth != null && !ordersLastMonth.isEmpty()) {
                        nbOrder = nbOrder + ordersLastMonth.size();
                        for (Order order : ordersLastMonth) {
                            revenueLastMonth = revenueLastMonth.add(order.getGrandTotal());
                            LastMonthSummary monthSummary = monthSummaryBySalesman.get(order.getSalesman().getId());
                            if (monthSummary == null) {
                                monthSummary = new LastMonthSummary(0, BigDecimal.ZERO, BigDecimal.ZERO);
                            }
                            monthSummary.nbOrder = monthSummary.nbOrder + 1;
                            monthSummary.revenue = monthSummary.revenue.add(order.getGrandTotal());

                            if (order.getDetails() != null) {
                                for (OrderDetail orderDetail : order.getDetails()) {
                                    // TODO use output of product
                                    outputLastMonth = outputLastMonth.add(orderDetail.getOutput());
                                    monthSummary.output = monthSummary.output.add(orderDetail.getOutput());
                                }
                            }
                            monthSummaryBySalesman.put(order.getSalesman().getId(), monthSummary);
                        }
                    }

                    // SET GIA TRI VAO CACHE CUA TUNG NHAN VIEN
                    for (ObjectId salesmanId : salesmanNoCacheIds) {
                        LastMonthSummary monthSummary = monthSummaryBySalesman.get(salesmanId);
                        if (monthSummary == null) {
                            monthSummary = new LastMonthSummary(0, BigDecimal.ZERO, BigDecimal.ZERO);
                        }
                        cacheDetail.put(salesmanId.toString() + lastMonthPeriod.toString(), monthSummary);
                    }
                }
            }

            lastMonthSummary = new LastMonthSummary(nbOrder, revenueLastMonth, outputLastMonth);
            cacheSummary.put(lastMonthPeriod.toString(), lastMonthSummary);
        }

        List<SimpleDate> workingDaysOfLastMonth = calendarConfigRepository.getWorkingDays(clientId, lastMonthPeriod);
        if (workingDaysOfLastMonth != null && !workingDaysOfLastMonth.isEmpty()) {
            dto.setOrderByDayLastMonth(lastMonthSummary.nbOrder / workingDaysOfLastMonth.size());
        }

        if (lastMonthSummary.nbOrder != 0) {
            dto.setRevenueByOrderLastMonth(lastMonthSummary.revenue.divide(new BigDecimal(lastMonthSummary.nbOrder), 0,
                    BigDecimal.ROUND_HALF_UP));

            dto.setOutputByOrderLastMonth(lastMonthSummary.output.divide(new BigDecimal(lastMonthSummary.nbOrder), 0,
                    BigDecimal.ROUND_HALF_UP));
        } else {
            dto.setRevenueByOrderLastMonth(BigDecimal.ZERO);

            dto.setOutputByOrderLastMonth(BigDecimal.ZERO);
        }
    }

    private static class LastMonthSummary implements Serializable, I_Cachable<LastMonthSummary> {

        private static final long serialVersionUID = 1L;

        protected int nbOrder;
        protected BigDecimal revenue;
        protected BigDecimal output;

        public LastMonthSummary(int nbOrder, BigDecimal revenue, BigDecimal output) {
            super();

            this.nbOrder = nbOrder;
            this.revenue = revenue;
            this.output = output;
        }

        @Override
        public LastMonthSummary cloneForCache() {
            return new LastMonthSummary(nbOrder, revenue, output);
        }

    }

    private static class ThisMonthSummary implements Serializable, I_Cachable<ThisMonthSummary> {

        private static final long serialVersionUID = 1L;

        protected int orderByDayThisMonth;
        protected BigDecimal revenueByOrderThisMonth;
        protected BigDecimal outputByOrderThisMonth;
        protected BigDecimal revenueThisMonth;
        protected BigDecimal revenueTargetThisMonth;
        protected BigDecimal outputThisMonth;
        protected BigDecimal outputTargetThisMonth;
        protected List<HashMap<String, Object>> top5products;
        protected List<HashMap<String, Object>> worst5salesmen;

        public ThisMonthSummary() {
            super();
        }

        public ThisMonthSummary(int orderByDayThisMonth, BigDecimal revenueByOrderThisMonth,
                BigDecimal outputByOrderThisMonth, BigDecimal revenueThisMonth, BigDecimal revenueTargetThisMonth,
                BigDecimal outputThisMonth, BigDecimal outputTargetThisMonth,
                List<HashMap<String, Object>> top5products, List<HashMap<String, Object>> worst5salesmen) {
            super();

            this.orderByDayThisMonth = orderByDayThisMonth;
            this.revenueByOrderThisMonth = revenueByOrderThisMonth;
            this.outputByOrderThisMonth = outputByOrderThisMonth;
            this.revenueThisMonth = revenueThisMonth;
            this.revenueTargetThisMonth = revenueTargetThisMonth;
            this.outputThisMonth = outputThisMonth;
            this.outputTargetThisMonth = outputTargetThisMonth;
            this.top5products = top5products;
            this.worst5salesmen = worst5salesmen;
        }

        @Override
        public ThisMonthSummary cloneForCache() {
            return new ThisMonthSummary(orderByDayThisMonth, revenueByOrderThisMonth, outputByOrderThisMonth,
                    revenueThisMonth, revenueTargetThisMonth, outputThisMonth, outputTargetThisMonth, top5products,
                    worst5salesmen);

        }
    }

}
