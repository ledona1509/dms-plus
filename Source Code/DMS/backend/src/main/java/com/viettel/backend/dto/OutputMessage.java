package com.viettel.backend.dto;

import java.util.Date;

public class OutputMessage extends Message {
	
	private String sender;
	private Date serverSentTime;
	
	public OutputMessage(){
		super();
		
		setId(0);
		serverSentTime = new Date();
	}
	
	public OutputMessage(Message original, String sender, Date time) {
		super(original.getId(), original.getData());
		this.sender = sender;
		this.serverSentTime = time;
	}

	public Date getServerSentTime() {
		return serverSentTime;
	}

	public void setServerSentTime(Date serverSentTime) {
		this.serverSentTime = serverSentTime;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}
	
}
