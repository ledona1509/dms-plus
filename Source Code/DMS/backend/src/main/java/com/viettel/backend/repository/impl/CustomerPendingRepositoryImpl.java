package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.repository.CustomerPendingRepository;
import com.viettel.backend.util.CriteriaUtils;

@Repository
public class CustomerPendingRepositoryImpl extends BasicRepositoryImpl<Customer> implements CustomerPendingRepository {

    private static final long serialVersionUID = -818511222864781714L;

    @Override
    public List<Customer> getCustomersByCreatedUsers(ObjectId clientId, Collection<ObjectId> userIds, Integer status,
            String search, Pageable pageable, Sort sort) {
        Criteria statusCriteria = null;
        if (status != null) {
            statusCriteria = Criteria.where(Customer.COLUMNNAME_APPROVE_STATUS).is(status);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Customer.COLUMNNAME_SEARCH, search);

        Criteria createdByCriteria = Criteria.where(Customer.COLUMNNAME_CREATED_BY_ID).in(userIds);

        return super._getList(clientId, true,
                CriteriaUtils.andOperator(statusCriteria, searchCriteria, createdByCriteria), pageable, sort);
    }

    @Override
    public long countCustomersByCreatedUsers(ObjectId clientId, Collection<ObjectId> userIds, Integer status,
            String search) {
        Criteria statusCriteria = null;
        if (status != null) {
            statusCriteria = Criteria.where(Customer.COLUMNNAME_APPROVE_STATUS).is(status);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Customer.COLUMNNAME_SEARCH, search);

        Criteria createdByCriteria = Criteria.where(Customer.COLUMNNAME_CREATED_BY_ID).in(userIds);

        return super._count(clientId, true,
                CriteriaUtils.andOperator(statusCriteria, searchCriteria, createdByCriteria));
    }

    @Override
    public List<Customer> getCustomersByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds,
            Integer status, String search, Pageable pageable, Sort sort) {
        Criteria statusCriteria = null;
        if (status != null) {
            statusCriteria = Criteria.where(Customer.COLUMNNAME_APPROVE_STATUS).is(status);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Customer.COLUMNNAME_SEARCH, search);

        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);

        return super._getList(clientId, true,
                CriteriaUtils.andOperator(statusCriteria, searchCriteria, distributorCriteria), pageable, sort);
    }

    @Override
    public long countCustomersByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, Integer status,
            String search) {
        Criteria statusCriteria = null;
        if (status != null) {
            statusCriteria = Criteria.where(Customer.COLUMNNAME_APPROVE_STATUS).is(status);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Customer.COLUMNNAME_SEARCH, search);

        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);

        return super._count(clientId, true,
                CriteriaUtils.andOperator(statusCriteria, searchCriteria, distributorCriteria));
    }

}
