package com.viettel.backend.config.root;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.InMemoryApprovalStore;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.InMemoryAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

import com.viettel.backend.oauth2.core.AccessTokenEnhancer;
import com.viettel.backend.oauth2.core.ExtraSignedRequestConverter;
import com.viettel.backend.oauth2.core.HardCodeClientDetailsService;
import com.viettel.backend.oauth2.core.RevokeNotifyDefaultTokenService;
import com.viettel.backend.oauth2.core.SignedRequestConverter;
import com.viettel.backend.oauth2.core.SimpleUserApprovalHandler;
import com.viettel.backend.oauth2.core.UniqueAuthenticationKeyGenerator;

@Configuration
public class OAuth2ServerConfig {
	
	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
		
		@Value("${oauth2.realm}")
		private String realm;
		
	    @Bean
	    public SignedRequestConverter signedRequestConverter() {
	        return new ExtraSignedRequestConverter();
	    }

		@Override
		public void configure(ResourceServerSecurityConfigurer resources) {
			resources.resourceId(realm);
		}

		@Override
		public void configure(HttpSecurity http) throws Exception {
			// @formatter:off
			http
			.requestMatchers().antMatchers(
					"/oauth/users/**", 
					// "/oauth/clients/**",
					"/oauth/userinfo", 
					"/api/**", 
					"/websocket/**")
			.and()
				.authorizeRequests()
					// Revoke token endpoint
					.regexMatchers(HttpMethod.DELETE, "/oauth/users/([^/].*?)/tokens/.*")
						.access("#oauth2.isUser() and hasRole('ROLE_USER')")
						
					// Get user's info endpoint
					.regexMatchers(HttpMethod.GET, "/oauth/userinfo")
						.access("#oauth2.isUser() and hasRole('ROLE_USER')")
						
					// List token for a user
					//.regexMatchers(HttpMethod.GET, "/oauth/clients/([^/].*?)/users/.*")
					//	.access("#oauth2.clientHasRole('ROLE_CLIENT') and (hasRole('ROLE_USER') or #oauth2.isClient()) and #oauth2.hasScope('read')")
					
					// List token for a client
					//.regexMatchers(HttpMethod.GET, "/oauth/clients/.*")
					//	.access("#oauth2.clientHasRole('ROLE_CLIENT') and #oauth2.isClient() and #oauth2.hasScope('read')")
					
					// Endpoints for role admin
					.regexMatchers(HttpMethod.GET, "/api/admin/.*")
						.access("#oauth2.isUser() and hasRole('ROLE_AD')")
						
					// Endpoints for role salesman
					.regexMatchers(HttpMethod.GET, "/api/salesman/.*")
						.access("#oauth2.isUser() and hasRole('ROLE_SM')")
						
					// Endpoints for role admin
					.regexMatchers(HttpMethod.GET, "/api/superadmin/.*")
						.access("#oauth2.isUser() and hasRole('ROLE_SUPER')")
						
					// Endpoints for role supervisor
					.regexMatchers(HttpMethod.GET, "/api/superadmin/.*")
						.access("#oauth2.isUser() and hasRole('ROLE_SUP')")
						
					// All other endpoint
					.regexMatchers("/.*")
						.access("#oauth2.isUser() and hasRole('ROLE_USER')")
						;
			

			// @formatter:on
		}

	}

	@Configuration
	@EnableAuthorizationServer
	protected static class AuthorizationServerConfiguration extends
			AuthorizationServerConfigurerAdapter {

		@Autowired
		private UserApprovalHandler userApprovalHandler;

		@Autowired
		@Qualifier("authenticationManagerBean")
		private AuthenticationManager authenticationManager;
		
		@Value("${oauth2.realm}")
		private String realm;
		
		@Value("${oauth2.client.id}")
		private String clientId;
		
		@Value("${oauth2.client.secret}")
		private String clientSecret;
		
		@Value("#{'${oauth2.grant.type}'.split(',')}")
		private List<String> granType;
		
		@Value("#{'${oauth2.scope}'.split(',')}")
		private List<String> scope;
		
		@Value("${oauth2.refresh.token.validity}")
		private int refreshTokenValidity;
		
		@Value("${oauth2.access.token.validity}")
		private int accessTokenValidity;
		
		@Value("#{'${oauth2.redirect.uri}'.split(',')}")
		private List<String> redirectUri;

		@Override
		public void configure(ClientDetailsServiceConfigurer clients)
				throws Exception {
			clients.withClientDetails(clientDetailsService());
		}

		@Bean
		public TokenStore tokenStore() {
			InMemoryTokenStore tokenStore = new InMemoryTokenStore();
			tokenStore.setAuthenticationKeyGenerator(new UniqueAuthenticationKeyGenerator());
			
			return tokenStore;
		}

		@Override
		public void configure(AuthorizationServerEndpointsConfigurer endpoints)
				throws Exception {
			endpoints.tokenStore(tokenStore())
					.userApprovalHandler(userApprovalHandler)
					.tokenEnhancer(tokenEnchancer())
					.authorizationCodeServices(authorizationCodeServices())
					.authenticationManager(authenticationManager)
					.tokenServices(tokenServices());
		}

		@Bean
		@Primary
		public AuthorizationServerTokenServices tokenServices() {
            RevokeNotifyDefaultTokenService tokenServices = new RevokeNotifyDefaultTokenService();
            tokenServices.setTokenStore(tokenStore());
            tokenServices.setSupportRefreshToken(true);
            tokenServices.setClientDetailsService(clientDetailsService());
            tokenServices.setTokenEnhancer(tokenEnchancer());
            return tokenServices;
        }

        @Bean
		public AuthorizationCodeServices authorizationCodeServices() {
			return new InMemoryAuthorizationCodeServices();
		}

		@Override
		public void configure(AuthorizationServerSecurityConfigurer oauthServer)
				throws Exception {
			oauthServer
					.realm(realm)
					.allowFormAuthenticationForClients()
			;
		}

		@Bean
		public ClientDetailsService clientDetailsService() {
			BaseClientDetails clientDetails = new BaseClientDetails();
	        clientDetails.setClientId(clientId);
	        clientDetails.setClientSecret(clientSecret);
	        clientDetails.setAuthorizedGrantTypes(granType);
	        clientDetails.setScope(scope);
	        clientDetails.setRefreshTokenValiditySeconds(refreshTokenValidity);
	        clientDetails.setAccessTokenValiditySeconds(accessTokenValidity);
	        clientDetails.setRegisteredRedirectUri(new HashSet<String>(redirectUri));
	        // clientDetails.setAuthorities(extractAuthorities(app.getAuthorities()));
	        // clientDetails.setAdditionalInformation(extractAdditionalInformation(app.getAdditionalInformation()));
	        
	        clientDetails.setAutoApproveScopes(clientDetails.getScope());
	        
	        return new HardCodeClientDetailsService(clientDetails);
		}
		
		@Bean
		public TokenEnhancer tokenEnchancer() {
		    return new AccessTokenEnhancer();
		}
		
	}
	
	protected static class Stuff {

		@Autowired
		private ClientDetailsService clientDetailsService;

		@Autowired
		private TokenStore tokenStore;

		@Bean
		public ApprovalStore approvalStore() throws Exception {
			return new InMemoryApprovalStore();
		}

		@Bean
		@Lazy
		@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
		public SimpleUserApprovalHandler userApprovalHandler() throws Exception {
			SimpleUserApprovalHandler handler = new SimpleUserApprovalHandler();
			handler.setApprovalStore(approvalStore());
			handler.setRequestFactory(new DefaultOAuth2RequestFactory(
					clientDetailsService));
			handler.setClientDetailsService(clientDetailsService);
			handler.setUseApprovalStore(true);
			return handler;
		}
	}
}
