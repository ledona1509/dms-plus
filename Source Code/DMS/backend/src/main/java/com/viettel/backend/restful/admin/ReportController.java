package com.viettel.backend.restful.admin;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ExhibitionReportDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.SalesmanSalesReportMonthlyDto;
import com.viettel.backend.dto.SurveyResultDto;
import com.viettel.backend.dto.VisitReportDailyDto;
import com.viettel.backend.dto.VisitReportDto;
import com.viettel.backend.dto.report.DaySalesReportDto;
import com.viettel.backend.dto.report.DistributorSalesReportDto;
import com.viettel.backend.dto.report.ProductSalesReportDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.AdminReportService;
import com.viettel.backend.service.CommonReportService;

@RestController(value = "adminReportController")
@RequestMapping(value = "/admin/report")
public class ReportController extends AbstractController {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private AdminReportService adminReportService;

    @Autowired
    private CommonReportService commonReportService;

    @RequestMapping(value = "/sales/daily", method = RequestMethod.GET)
    public ResponseEntity<?> getDaySalesReport(@RequestParam(value = "month") int month,
            @RequestParam(value = "year") int year) {
        ListJson<DaySalesReportDto> results = adminReportService.getDaySalesReport(getUserLogin(), month, year);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/sales/distributor", method = RequestMethod.GET)
    public ResponseEntity<?> getDistributorSalesReport(@RequestParam(value = "month") int month,
            @RequestParam(value = "year") int year) {
        ListJson<DistributorSalesReportDto> results = adminReportService.getDistributorSalesReport(getUserLogin(),
                month, year);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/sales/product", method = RequestMethod.GET)
    public ResponseEntity<?> getProductSalesReport(@RequestParam(value = "month") int month,
            @RequestParam(value = "year") int year, @RequestParam(required = true) String productCategoryId) {
        ListJson<ProductSalesReportDto> results = adminReportService.getProductSalesReport(getUserLogin(),
                productCategoryId, month, year);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/sales/salesman", method = RequestMethod.GET)
    public ResponseEntity<?> getSalesmanSalesReportMonthly(@RequestParam(value = "month") int month,
            @RequestParam(value = "year") int year, @RequestParam(required = false) String salesmanId) {
        ListJson<SalesmanSalesReportMonthlyDto> listJson = commonReportService.getSalesmanSalesReportMonthly(
                getUserLogin(), salesmanId, month, year);
        return new Envelope(listJson).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/visit/daily", method = RequestMethod.GET)
    public ResponseEntity<?> getVisitReportDaily(@RequestParam(value = "salesmanId", required = false) String salesmanId) {
        VisitReportDailyDto dto = commonReportService.getVisitReportDaily(getUserLogin(), salesmanId);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    /**
     * Báo cáo ghé thăm theo một khoảng thời gian - lấy theo salesman
     */
    @RequestMapping(value = "/visit", method = RequestMethod.GET, params = { "salesmanId" })
    public ResponseEntity<?> getSalesmanVisitReport(@RequestParam String fromDate, @RequestParam String toDate,
            @RequestParam String salesmanId) {
        ListJson<VisitReportDto> listJson = commonReportService.getSalesmanVisitReport(getUserLogin(), salesmanId,
                fromDate, toDate);
        return new Envelope(listJson).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/survey/{surveyId}", method = RequestMethod.GET)
    public ResponseEntity<?> getSurveyReport(@PathVariable String surveyId) {
        SurveyResultDto dto = commonReportService.getSurveyReport(getUserLogin(), surveyId);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/survey/{surveyId}/export", method = RequestMethod.GET)
    public ResponseEntity<?> exportSurveyResult(@PathVariable String surveyId) throws IOException {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"Survey.xlsx\"");
        ResponseEntity<byte[]> result = new ResponseEntity<byte[]>(commonReportService.exportSurveyReport(
                getUserLogin(), surveyId), header, HttpStatus.OK);
        return result;
    }

    @RequestMapping(value = "/exhibition/{exhibitionId}", method = RequestMethod.GET)
    public ResponseEntity<?> getExhibitionReport(@PathVariable String exhibitionId) {
        ExhibitionReportDto dto = commonReportService.getExhibitionReport(getUserLogin(), exhibitionId);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/exhibition/{exhibitionId}/export", method = RequestMethod.GET)
    public ResponseEntity<?> exportExhibitionReport(@PathVariable String exhibitionId) throws IOException {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"Exhibition.xlsx\"");
        ResponseEntity<byte[]> result = new ResponseEntity<byte[]>(commonReportService.exportExhibitionReport(
                getUserLogin(), exhibitionId), header, HttpStatus.OK);
        return result;
    }

}
