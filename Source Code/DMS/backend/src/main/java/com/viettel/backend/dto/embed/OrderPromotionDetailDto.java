package com.viettel.backend.dto.embed;

import com.viettel.backend.domain.embed.OrderPromotionDetail;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.engine.promotion.I_PromotionDetailResult;
import com.viettel.backend.engine.promotion.PromotionCondition;
import com.viettel.backend.engine.promotion.PromotionRewardResult;

public class OrderPromotionDetailDto extends NameCategoryDto implements
        I_PromotionDetailResult<String, ProductEmbedDto> {

    private static final long serialVersionUID = -1299505378953469850L;

    private PromotionCondition<String, ProductEmbedDto> condition;
    private PromotionRewardResult<String, ProductEmbedDto> rewardResult;

    public OrderPromotionDetailDto() {
        super();
    }
    
    public OrderPromotionDetailDto(OrderPromotionDetail orderPromotionDetail, String defaultProductPhoto) {
        super(orderPromotionDetail);

        if (orderPromotionDetail.getRewardResult() != null) {
            this.rewardResult = new PromotionRewardResult<String, ProductEmbedDto>();
            if (orderPromotionDetail.getRewardResult().getProduct() != null) {
                this.rewardResult.setProduct(new ProductEmbedDto(orderPromotionDetail.getRewardResult()
                        .getProduct(), defaultProductPhoto));
            }
            this.rewardResult.setAmount(orderPromotionDetail.getRewardResult().getAmount());
            this.rewardResult.setQuantity(orderPromotionDetail.getRewardResult().getQuantity());
        }
    }
    
    public PromotionCondition<String, ProductEmbedDto> getCondition() {
        return condition;
    }
    
    public void setCondition(PromotionCondition<String, ProductEmbedDto> condition) {
        this.condition = condition;
    }

    public PromotionRewardResult<String, ProductEmbedDto> getRewardResult() {
        return rewardResult;
    }

    public void setRewardResult(PromotionRewardResult<String, ProductEmbedDto> rewardResult) {
        this.rewardResult = rewardResult;
    }

}
