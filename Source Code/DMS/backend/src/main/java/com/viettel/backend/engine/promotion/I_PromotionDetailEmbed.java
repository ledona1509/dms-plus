package com.viettel.backend.engine.promotion;

import java.io.Serializable;

public interface I_PromotionDetailEmbed<ID extends Serializable, P extends I_Product<ID>> extends Serializable {

    public ID getId();

    public int getType();

    public PromotionCondition<ID, P> getCondition();
    
    public PromotionReward<ID, P> getReward();

}
