package com.viettel.backend.service.engine;

import java.io.Serializable;
import java.util.List;

import com.viettel.backend.domain.Order;
import com.viettel.backend.dto.OrderCreateDto;
import com.viettel.backend.dto.embed.OrderPromotionDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface PromotionEngine extends Serializable {

    public void calculatePromotion(UserLogin userLogin, OrderCreateDto dto, Order order);
    
    public List<OrderPromotionDto> getPromotions(UserLogin userLogin, OrderCreateDto dto);
    
}
