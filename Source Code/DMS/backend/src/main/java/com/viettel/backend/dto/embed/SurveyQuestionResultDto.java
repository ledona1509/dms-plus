package com.viettel.backend.dto.embed;

import java.util.ArrayList;
import java.util.List;

import com.viettel.backend.dto.NameCategoryDto;

public class SurveyQuestionResultDto extends NameCategoryDto {

    /**
     * 
     */
    private static final long serialVersionUID = 5496584668724710642L;

    private boolean multipleChoice;
    private boolean required;

//    private int totalAnswer;
    private List<SurveyOptionResultDto> options;
//
//    public SurveyQuestionResultDto() {
//        super();
//        this.totalAnswer = 0;
//    }

    public boolean isMultipleChoice() {
        return multipleChoice;
    }

    public void setMultipleChoice(boolean multipleChoice) {
        this.multipleChoice = multipleChoice;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public List<SurveyOptionResultDto> getOptions() {
        return options;
    }

    public void setOptions(List<SurveyOptionResultDto> options) {
        this.options = options;
//        totalAnswer = 0;
//        for (SurveyOptionResultDto option : options) {
//            totalAnswer = totalAnswer + option.getResult();
//        }
    }

    public void addOption(SurveyOptionResultDto option) {
        if (this.options == null) {
            this.options = new ArrayList<SurveyOptionResultDto>();
        }

        this.options.add(option);

//        totalAnswer = totalAnswer + option.getResult();
    }
//
//    public int getTotalAnswer() {
//        return totalAnswer;
//    }
//
//    public void setTotalAnswer(int totalAnswer) {
//        this.totalAnswer = totalAnswer;
//    }
//
//    public HashMap<String, Integer> getPercentage() {
//        HashMap<String, Integer> percentageMap = new HashMap<String, Integer>();
//        if (!multipleChoice) {
//            if (options != null && totalAnswer > 0) {
//                int index = 1;
//                int totalPercentage = 0;
//                for (SurveyOptionResultDto option : options) {
//                    if (totalAnswer != 0) {
//                        percentageMap.put(option.getId().toString(), (100 - totalPercentage));
//                    }
//                    // LAST ONE
//                    if (index == options.size()) {
//                        percentageMap.put(option.getId().toString(), (100 - totalPercentage));
//                    } else {
//                        BigDecimal percentage = new BigDecimal(option.getResult()).divide(new BigDecimal(totalAnswer),
//                                2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
//                        totalPercentage = totalPercentage + percentage.intValue();
//                        
//                        percentageMap.put(option.getId().toString(), percentage.intValue());
//                    }
//
//                    index++;
//                }
//            }
//        }
//        return percentageMap;
//    }
}
