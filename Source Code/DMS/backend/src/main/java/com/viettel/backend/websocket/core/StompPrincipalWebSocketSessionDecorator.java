package com.viettel.backend.websocket.core;

import java.io.IOException;
import java.security.Principal;
import java.util.Map;

import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.WebSocketSessionDecorator;

/**
 * @author thanh
 */
public class StompPrincipalWebSocketSessionDecorator extends WebSocketSessionDecorator {

    public StompPrincipalWebSocketSessionDecorator(WebSocketSession session) {
        super(session);
    }
    
    @Override
    public Principal getPrincipal() {
        Map<String, Object> attrs = getAttributes();
        if (attrs.containsKey(StompLoginSubProtocolWebSocketHandler.SESSION_KEY_VERYFIED)
                && attrs.containsKey(StompLoginSubProtocolWebSocketHandler.SESSION_KEY_PRINCIPAL)) {
            UserPrincipal principal = (UserPrincipal) attrs.get(StompLoginSubProtocolWebSocketHandler.SESSION_KEY_PRINCIPAL);
            return principal;
        }
        return null;
    }
    
    @Override
    public void sendMessage(WebSocketMessage<?> message) throws IOException {
        getDelegate().sendMessage(message);
    }
}
