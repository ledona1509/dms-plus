package com.viettel.backend.dto;

import java.util.ArrayList;
import java.util.List;

import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.domain.embed.OrderPromotion;
import com.viettel.backend.dto.embed.OrderDetailDto;
import com.viettel.backend.dto.embed.OrderPromotionDto;
import com.viettel.backend.dto.embed.ProductEmbedDto;
import com.viettel.backend.engine.promotion.I_Order;

public class OrderDto extends OrderSimpleDto implements I_Order<String, ProductEmbedDto> {

    private static final long serialVersionUID = 4372092863401688032L;

    private List<OrderDetailDto> details;
    private List<OrderPromotionDto> promotionResults;

    public OrderDto(Order order, String defaultProductPhoto) {
        super(order);

        if (order.getDetails() != null) {
            this.details = new ArrayList<OrderDetailDto>(order.getDetails().size());
            for (OrderDetail detail : order.getDetails()) {
                this.details.add(new OrderDetailDto(detail, defaultProductPhoto));
            }
        }

        if (order.getPromotionResults() != null) {
            this.promotionResults = new ArrayList<OrderPromotionDto>(order.getDetails().size());
            for (OrderPromotion promotionResult : order.getPromotionResults()) {
                this.promotionResults.add(new OrderPromotionDto(promotionResult,
                        defaultProductPhoto));
            }
        }
    }

    @SuppressWarnings("unchecked")
    public List<OrderDetailDto> getDetails() {
        return details;
    }

    public void setDetails(List<OrderDetailDto> details) {
        this.details = details;
    }

    @SuppressWarnings("unchecked")
    public List<OrderPromotionDto> getPromotionResults() {
        return promotionResults;
    }

    public void setPromotionResults(List<OrderPromotionDto> promotionResults) {
        this.promotionResults = promotionResults;
    }

}
