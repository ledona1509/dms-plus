package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.Order;
import com.viettel.backend.entity.SimpleDate.Period;

public interface OrderPendingRepository extends BasicRepository<Order> {

    public List<Order> getOrderBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Integer approveStatus,
            String searchCode, Pageable pageable, Sort sort);

    public long countOrderBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Integer approveStatus,
            String searchCode);

    public List<Order> getOrderByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Integer approveStatus,
            Period period, String searchCode, Pageable pageable, Sort sort);

    public long countOrderByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Integer approveStatus,
            Period period, String searchCode);

    public List<Order> getOrderByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds,
            Integer approveStatus, String searchCode, Pageable pageable, Sort sort);

    public long countOrderByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, Integer approveStatus,
            String searchCode);

}
