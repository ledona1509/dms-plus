package com.viettel.backend.repository.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.mongodb.WriteResult;
import com.viettel.backend.domain.NameCategory;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.POSearchable;
import com.viettel.backend.domain.annotation.ClientRootInclude;
import com.viettel.backend.util.CriteriaUtils;
import com.viettel.backend.util.StringUtils;

public abstract class AbstractRepository<D extends PO> implements Serializable {

    private static final long serialVersionUID = 1264740210875227683L;

    @Autowired
    private MongoTemplate dataTemplate;

    private Class<D> domainClazz;

    @SuppressWarnings("unchecked")
    public AbstractRepository() {
        Class<?> superClazz = getClass();
        Type superType = superClazz.getGenericSuperclass();
        while (!(superType instanceof ParameterizedType)) {
            superClazz = superClazz.getSuperclass();
            superType = superClazz.getGenericSuperclass();
        }

        int paraIndex = 0;
        ParameterizedType genericSuperclass = (ParameterizedType) superType;
        this.domainClazz = (Class<D>) genericSuperclass.getActualTypeArguments()[paraIndex++];
    }

    protected final List<D> _getList(ObjectId clientId, Boolean active, Criteria criteria, Pageable pageable, Sort sort) {
        Query mainQuery = initQuery(clientId, active, getDefaultDraftFilter(), criteria, pageable, sort);
        return dataTemplate.find(mainQuery, domainClazz);
    }

    protected final List<D> _getListByIds(ObjectId clientId, Boolean active, Collection<ObjectId> ids,
            Pageable pageable, Sort sort) {
        Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).in(ids);
        return this._getList(clientId, active, criteria, pageable, sort);
    }

    protected final D _getFirst(ObjectId clientId, Boolean active, Criteria criteria, Sort sort) {
        Query mainQuery = initQuery(clientId, active, getDefaultDraftFilter(), criteria, null, sort);
        return dataTemplate.findOne(mainQuery, domainClazz);
    }

    protected final D _getById(ObjectId clientId, Boolean active, ObjectId id) {
        Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).is(id);
        Query mainQuery = initQuery(clientId, active, getDefaultDraftFilter(), criteria, null, null);
        return dataTemplate.findOne(mainQuery, domainClazz);
    }

    protected final Long _count(ObjectId clientId, Boolean active, Criteria criteria) {
        Query mainQuery = initQuery(clientId, active, getDefaultDraftFilter(), criteria, null, null);
        return dataTemplate.count(mainQuery, domainClazz);
    }

    protected final boolean _exists(ObjectId clientId, Boolean active, ObjectId id) {
        Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).is(id);
        return this._exists(clientId, active, criteria);
    }

    protected final boolean _exists(ObjectId clientId, Boolean active, Criteria criteria) {
        Query mainQuery = initQuery(clientId, active, getDefaultDraftFilter(), criteria, null, null);
        return dataTemplate.exists(mainQuery, domainClazz);
    }

    protected final D _save(ObjectId clientId, D domain) {
        if (domain.getClientId() == null || clientId == null || !domain.getClientId().equals(clientId)) {
            throw new IllegalArgumentException("client id not valid");
        }

        if (domain instanceof POSearchable) {
            POSearchable searchableDomain = (POSearchable) domain;
            String searchValue = "";

            String[] sources = searchableDomain.getSearchValues();
            if (sources != null && sources.length > 0) {
                String value = StringUtils.arrayToDelimitedString(sources, " ");
                searchValue = StringUtils.getSearchableString(value);
            }

            searchableDomain.setSearch(searchValue);
        }

        dataTemplate.save(domain);

        return domain;
    }

    protected final boolean _delete(ObjectId clientId, ObjectId id) {
        if (id == null) {
            throw new IllegalArgumentException("id not valid");
        }

        Update update = new Update();
        update.set(PO.COLUMNNAME_ACTIVE, false);

        Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).is(id);
        return this._updateMulti(clientId, true, criteria, update) == 1;
    }

    protected final boolean _delete(ObjectId clientId, Collection<ObjectId> ids) {
        if (ids == null || ids.isEmpty()) {
            throw new IllegalArgumentException("ids is not valid");
        }

        Update update = new Update();
        update.set(PO.COLUMNNAME_ACTIVE, false);

        return this._updateMulti(clientId, true, ids, update) == ids.size();
    }

    protected final int _updateMulti(ObjectId clientId, Boolean active, Collection<ObjectId> ids, Update update) {
        if (ids == null || ids.isEmpty()) {
            throw new IllegalArgumentException("ids is not valid");
        }

        Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).in(ids);
        return this._updateMulti(clientId, active, criteria, update);
    }

    protected final int _updateMulti(ObjectId clientId, Boolean active, Criteria criteria, Update update) {
        Query mainQuery = initQuery(clientId, active, null, criteria, null, null);
        WriteResult result = dataTemplate.updateMulti(mainQuery, update, domainClazz);
        return result.getN();
    }

    protected final Set<ObjectId> _getIdSet(List<D> pos) {
        if (pos == null || pos.isEmpty()) {
            return Collections.<ObjectId> emptySet();
        }

        Set<ObjectId> poIds = new HashSet<ObjectId>(pos.size());
        for (PO po : pos) {
            poIds.add(po.getId());
        }

        return poIds;
    }

    protected final Map<ObjectId, D> _getMap(List<D> pos) {
        if (pos == null || pos.isEmpty()) {
            return Collections.<ObjectId, D> emptyMap();
        }

        Map<ObjectId, D> results = new HashMap<ObjectId, D>();
        for (D po : pos) {
            results.put(po.getId(), po);
        }

        return results;
    }

    protected Criteria getDefaultCriteria() {
        return null;
    }

    /************************************************************************/
    /***************************** DRAFT METHODS ****************************/
    /************************************************************************/

    protected Boolean getDefaultDraftFilter() {
        return null;
    }

    protected final List<D> _getListWithDraft(ObjectId clientId, Boolean active, Boolean draft, Criteria criteria,
            Pageable pageable, Sort sort) {
        Query mainQuery = initQuery(clientId, active, draft, criteria, pageable, sort);
        return dataTemplate.find(mainQuery, domainClazz);
    }

    protected final Long _countWithDraft(ObjectId clientId, Boolean active, Boolean draft, Criteria criteria) {
        Query mainQuery = initQuery(clientId, active, draft, criteria, null, null);
        return dataTemplate.count(mainQuery, domainClazz);
    }

    protected final D _getByIdWithDraft(ObjectId clientId, Boolean active, ObjectId id) {
        Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).is(id);
        Query mainQuery = initQuery(clientId, active, null, criteria, null, null);
        return dataTemplate.findOne(mainQuery, domainClazz);
    }

    protected final boolean _enableDraft(ObjectId clientId, ObjectId id) {
        if (id == null) {
            throw new IllegalArgumentException("id not valid");
        }

        Update update = new Update();
        update.set(PO.COLUMNNAME_DRAFT, false);

        return _updateMulti(clientId, true, Arrays.asList(id), update) == 1;
    }

    /************************************************************************/
    /**************************** PRIVATE METHODS ***************************/
    /************************************************************************/
    private Query initQuery(ObjectId clientId, Boolean active, Boolean draft, Criteria criteria, Pageable pageable,
            Sort sort) {

        if (clientId == null) {
            throw new IllegalArgumentException("client id not valid");
        }

        Criteria clientFilterCriteria = null;
        if (isIncludeClientRoot()) {
            clientFilterCriteria = CriteriaUtils.orOperator(Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(clientId),
                    Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.CLIENT_ROOT_ID));
        } else {
            clientFilterCriteria = Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(clientId);
        }

        Criteria activeCriteria = null;
        if (active != null) {
            activeCriteria = Criteria.where(PO.COLUMNNAME_ACTIVE).is(active);
        }

        Criteria draftCriteria = null;
        if (draft != null) {
            draftCriteria = Criteria.where(PO.COLUMNNAME_DRAFT).is(draft);
        }

        Criteria mainCriteria = CriteriaUtils.andOperator(clientFilterCriteria, activeCriteria, draftCriteria,
                getDefaultCriteria(), criteria);

        Query mainQuery = new Query();
        mainQuery.addCriteria(mainCriteria);

        if (sort == null) {
            if (NameCategory.class.isAssignableFrom(domainClazz)) {
                sort = new Sort(NameCategory.COLUMNNAME_NAME);
            }
        }

        if (pageable != null) {
            if (sort != null) {
                pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), sort);
            }
            mainQuery.with(pageable);
        } else if (sort != null) {
            mainQuery.with(sort);
        }

        return mainQuery;
    }

    private boolean isIncludeClientRoot() {
        return (domainClazz.isAnnotationPresent(ClientRootInclude.class));
    }
}
