package com.viettel.backend.repository;

import java.io.Serializable;
import java.util.List;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.CalendarConfig;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;

public interface CalendarConfigRepository extends Serializable {

    public CalendarConfig getCalendarConfig(ObjectId clientId);

    public CalendarConfig save(ObjectId clientId, CalendarConfig calendarConfig);

    /**
     * Return list of working day in this period
     */
    public List<SimpleDate> getWorkingDays(ObjectId clientId, Period period);

}
