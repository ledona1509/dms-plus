package com.viettel.backend.dto;

import com.viettel.backend.domain.Exhibition;

/**
 * @author thanh
 */
public class ExhibitionSimpleDto extends NameCategoryDto {

    private static final long serialVersionUID = 2264346176267776793L;

    private String startDate;
    private String endDate;
    private String description;

    public ExhibitionSimpleDto() {
        super();
    }

    public ExhibitionSimpleDto(Exhibition exhibition) {
        super(exhibition);

        this.startDate = exhibition.getStartDate() != null ? exhibition.getStartDate().getIsoDate() : null;
        this.endDate = exhibition.getEndDate() != null ? exhibition.getEndDate().getIsoDate() : null;
        this.description = exhibition.getDescription();
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
