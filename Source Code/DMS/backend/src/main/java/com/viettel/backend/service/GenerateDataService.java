package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.oauth2.core.UserLogin;

public interface GenerateDataService extends Serializable {

    public void generateCustomerByDistributor(UserLogin userLogin, String distributorId, double longitude, double latitude, int numberCustomer);
    
    public void generateVisitsAndOrders(UserLogin userLogin, String fromDate, String toDate);
    
}
