package com.viettel.backend.restful;

public class ExceptionCode {

    public static final String NOT_FOUND = "not.found";
    public static final String INVALID_PARAM = "invalid.param";
    public static final String INVALID_DATE_PARAM = "invalid.date.param";
    public static final String INVALID_PHOTO_PARAM = "invalid.photo.param";
    public static final String INVALID_LOCATION_PARAM = "invalid.location.param";
    public static final String INVALID_SCHEDULE_PARAM = "invalid.customer.param";
//  public static final String INVALID_CUSTOMER_PARAM = "invalid.customer.param";
//  public static final String INVALID_DISTRIBUTOR_PARAM = "invalid.distributor.param";
//  public static final String INVALID_SALESMAN_PARAM = "invalid.salesman.param";
//  public static final String INVALID_PRODUCT_PARAM = "invalid.salesman.param";
//  public static final String INVALID_SUPERVISOR_PARAM = "invalid.supervisor.param";
    
    public static final String PERMISSION_DENIED = "permission.denied";
    public static final String SUPERADMIN_ONLY = "superadmin.only";
    public static final String ADMIN_ONLY = "admin.only";
    public static final String SUPERVISOR_ONLY = "supervisor.only";
    public static final String SALESMAN_ONLY = "salesman.only";
    
    public static final String SALESMAN_NO_DISTRIBUTOR = "salesman.no.distributor";
    public static final String SUPERVISOR_NO_DISTRIBUTOR = "supervisor.no.distributor";
    
    public static final String SALESMAN_HAS_TARGET = "salesman.has.target";
    
    public static final String CUSTOMER_HAS_PENDING_ORDER = "customer.has.pending.order";
    
    public static final String CANNOT_UPDATE_NO_DRAFT_DATA = "cannot.update.no.draft.data";
    
    public static final String USERNAME_NOT_AVAILABLE = "username.not.available";
    public static final String INVALID_NEW_PASSWORD = "invalid.new.password";
    public static final String INVALID_OLD_PASSWORD = "invalid.old.password";
    
    public static final String VISIT_ENDED = "visit.ended";
    public static final String VISIT_NOT_ENDED = "visit.not.ended";
    public static final String CUSTOMER_VISITED = "customer.visited";
    
    public static final String CANNOT_DELETE_CURRENT_USER = "cannot.delete.current.user";
    
    public static final String USER_LINK_TO_DISTRIBUTOR = "user.link.to.distributor";
    public static final String UOM_LINK_TO_PRODUCT = "uom.link.to.product";
    public static final String PRODUCT_CATEGORY_LINK_TO_PRODUCT = "product.category.link.to.product";
    public static final String CUSTOMER_TYPE_TO_CUSTOMER = "customer.type.link.to.customer";
    public static final String DISTRIBUTOR_LINK_TO_SALESMAN = "distributor.link.to.salesman";
    public static final String DISTRIBUTOR_LINK_TO_CUSTOMER = "distributor.link.to.customer";
    
    public static final String SAME_NAME_EXIST = "same.name.exist";
    public static final String SAME_CODE_EXIST = "same.name.exist";
}
