package com.viettel.backend.service;

import com.viettel.backend.domain.Distributor;
import com.viettel.backend.dto.DistributorCreateDto;
import com.viettel.backend.dto.DistributorDto;
import com.viettel.backend.dto.DistributorSimpleDto;

public interface AdminDistributorService extends
        BasicCategoryService<Distributor, DistributorSimpleDto, DistributorDto, DistributorCreateDto> {

}
