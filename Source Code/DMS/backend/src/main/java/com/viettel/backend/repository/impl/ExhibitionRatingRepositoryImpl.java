package com.viettel.backend.repository.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.ExhibitionRating;
import com.viettel.backend.repository.ExhibitionRatingRepository;
import com.viettel.backend.util.CriteriaUtils;

/**
 * @author thanh
 */
@Repository
public class ExhibitionRatingRepositoryImpl extends AbstractRepository<Visit> implements ExhibitionRatingRepository {

    private static final long serialVersionUID = -621341622021413043L;

    @Override
    protected Criteria getDefaultCriteria() {
        Criteria isVisitCriteria = Criteria.where(Visit.COLUMNNAME_IS_VISIT).is(true);
        Criteria visitStatusCriteria = Criteria.where(Visit.COLUMNNAME_VISIT_STATUS).is(Visit.VISIT_STATUS_VISITED);

        return CriteriaUtils.andOperator(isVisitCriteria, visitStatusCriteria);
    }

    @Override
    public List<ExhibitionRating> getExhibitionRatingsByCustomers(ObjectId clientId, ObjectId exhibitionId,
            Collection<ObjectId> customerIds) {
        Criteria exhibitionCriteria = Criteria.where(Visit.COLUMNNAME_EXHIBITION_RATINGS).elemMatch(
                Criteria.where(ExhibitionRating.COLUMNNAME_EXHIBITION_ID).is(exhibitionId));
        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).in(customerIds);

        List<Visit> visits = _getList(clientId, true, CriteriaUtils.andOperator(exhibitionCriteria, customerCriteria),
                null, null);

        if (visits == null || visits.isEmpty()) {
            return Collections.<ExhibitionRating> emptyList();
        }

        List<ExhibitionRating> exhibitionRatings = new ArrayList<ExhibitionRating>();
        for (Visit visit : visits) {
            if (visit.getExhibitionRatings() != null && !visit.getExhibitionRatings().isEmpty()) {
                for (ExhibitionRating exhibitionRating : visit.getExhibitionRatings()) {
                    exhibitionRating.setVisit(visit);
                    exhibitionRatings.add(exhibitionRating);
                }
            }
        }

        return exhibitionRatings;
    }

    @Override
    public List<ExhibitionRating> getExhibitionRatings(ObjectId clientId, ObjectId exhibitionId) {
        Criteria exhibitionCriteria = Criteria.where(Visit.COLUMNNAME_EXHIBITION_RATINGS).elemMatch(
                Criteria.where(ExhibitionRating.COLUMNNAME_EXHIBITION_ID).is(exhibitionId));

        List<Visit> visits = _getList(clientId, true, exhibitionCriteria, null, null);

        if (visits == null || visits.isEmpty()) {
            return Collections.<ExhibitionRating> emptyList();
        }

        List<ExhibitionRating> exhibitionRatings = new ArrayList<ExhibitionRating>();
        for (Visit visit : visits) {
            if (visit.getExhibitionRatings() != null && !visit.getExhibitionRatings().isEmpty()) {
                for (ExhibitionRating exhibitionRating : visit.getExhibitionRatings()) {
                    if (!exhibitionRating.getExhibitionId().equals(exhibitionId)) {
                        continue;
                    }
                    
                    exhibitionRating.setVisit(visit);
                    exhibitionRatings.add(exhibitionRating);
                }
            }
        }

        return exhibitionRatings;
    }

}
