package com.viettel.backend.cache;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.SalesConfig;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;

public class CacheManager implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private ConcurrentHashMap<String, I_Cache<?, ?>> cacheMap;
    
    private I_Cache<ObjectId, SalesConfig> salesConfigCache;
    private I_Cache<ObjectId, ClientConfig> clientConfigCache;
    private I_Cache<Period, ListCachable<SimpleDate>> workingDaysCache;
    
    public CacheManager() {
        super();
        
        this.cacheMap = new ConcurrentHashMap<String, I_Cache<?,?>>();
        this.salesConfigCache = new Cache<ObjectId, SalesConfig>();
        this.clientConfigCache = new Cache<ObjectId, ClientConfig>();
        this.workingDaysCache = new Cache<SimpleDate.Period, ListCachable<SimpleDate>>();
    }
    
    public I_Cache<ObjectId, SalesConfig> getSalesConfigCache() {
        return salesConfigCache;
    }
    
    /**
     * Client config cache by client id
     */
    public I_Cache<ObjectId, ClientConfig> getClientConfigCache() {
        return clientConfigCache;
    }
    
    /**
     * list workings days cache by period
     */
    public I_Cache<Period, ListCachable<SimpleDate>> getWorkingDaysCache() {
        return workingDaysCache;
    }
    
    @SuppressWarnings("rawtypes")
    public I_Cache getCache(String cacheName) {
        return cacheMap.get(cacheName);
    }
    
    @SuppressWarnings("rawtypes")
    public I_Cache putCache(String cacheName, I_Cache cache) {
        return cacheMap.put(cacheName, cache);
    }
    
    @SuppressWarnings("rawtypes")
    public I_Cache removeCache(String cacheName) {
        return cacheMap.remove(cacheName);
    }
    
}
