package com.viettel.backend.dto;

import java.io.Serializable;

import com.viettel.backend.domain.PO;

/**
 * @author thanh
 */
public abstract class DTO implements Serializable {

    private static final long serialVersionUID = -862151765440311950L;

    private String id;
    private boolean draft;

    public DTO() {
        super();

        this.id = null;
        this.draft = false;
    }

    public DTO(PO po) {
        super();

        if (po != null && po.getId() != null) {
            this.id = po.getId().toString();
            this.draft = po.isDraft();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }

}
