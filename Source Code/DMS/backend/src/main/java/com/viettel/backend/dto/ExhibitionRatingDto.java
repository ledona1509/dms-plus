package com.viettel.backend.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.viettel.backend.domain.embed.ExhibitionRating;
import com.viettel.backend.domain.embed.ExhibitionRatingItem;

/**
 * @author thanh
 */
public class ExhibitionRatingDto implements Serializable {

    private static final long serialVersionUID = 3227127774937212395L;

    private String exhibitionId;
    private List<ExhibitionRatingItemDto> items;

    public ExhibitionRatingDto() {
        super();
    }

    public ExhibitionRatingDto(ExhibitionRating exhibitionRating) {
        super();

        this.exhibitionId = exhibitionRating.getExhibitionId().toString();
        if (exhibitionRating.getItems() != null) {
            this.items = new ArrayList<ExhibitionRatingItemDto>(exhibitionRating.getItems().size());
            for (ExhibitionRatingItem item : exhibitionRating.getItems()) {
                this.items.add(new ExhibitionRatingItemDto(item));
            }
        }
    }

    public String getExhibitionId() {
        return exhibitionId;
    }

    public void setExhibitionId(String exhibitionId) {
        this.exhibitionId = exhibitionId;
    }

    public List<ExhibitionRatingItemDto> getItems() {
        return items;
    }

    public void setItems(List<ExhibitionRatingItemDto> items) {
        this.items = items;
    }

    /**
     * @author thanh
     */
    public static class ExhibitionRatingItemDto implements Serializable {

        private static final long serialVersionUID = 5980956405684245124L;

        private String exhibitionItemId;
        private float rate;

        public ExhibitionRatingItemDto() {
            super();
        }

        public ExhibitionRatingItemDto(ExhibitionRatingItem exhibitionRatingItem) {
            super();

            this.exhibitionItemId = exhibitionRatingItem.getExhibitionItemId().toString();
            this.rate = exhibitionRatingItem.getRate();
        }

        public String getExhibitionItemId() {
            return exhibitionItemId;
        }

        public void setExhibitionItemId(String exhibitionItemId) {
            this.exhibitionItemId = exhibitionItemId;
        }

        public float getRate() {
            return rate;
        }

        public void setRate(float rate) {
            this.rate = rate;
        }

    }

}
