package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.RoleDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.AdminRoleService;

@RestController
@RequestMapping(value = "/admin/role")
public class RoleController extends AbstractController {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private AdminRoleService adminRoleService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        ListJson<RoleDto> results = adminRoleService.getRoles(getUserLogin());
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

}
