package com.viettel.backend.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.domain.embed.CustomerEmbed;
import com.viettel.backend.domain.embed.ExhibitionCategory;
import com.viettel.backend.entity.SimpleDate;

@Document(collection = "Exhibition")
public class Exhibition extends NameCategory {

    private static final long serialVersionUID = 3227127774937212395L;

    public static final String COLUMNNAME_DESCRIPTION = "description";
    public static final String COLUMNNAME_START_DATE_VALUE = "startDate.value";
    public static final String COLUMNNAME_END_DATE_VALUE = "endDate.value";
    public static final String COLUMNNAME_CATEGORY = "category";
    public static final String COLUMNNAME_PARTICIPANTS = "participants";

    private String description;
    private SimpleDate startDate;
    private SimpleDate endDate;
    private List<ExhibitionCategory> categories;
    private List<CustomerEmbed> participants;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SimpleDate getStartDate() {
        return startDate;
    }

    public void setStartDate(SimpleDate startDate) {
        this.startDate = startDate;
    }

    public SimpleDate getEndDate() {
        return endDate;
    }

    public void setEndDate(SimpleDate endDate) {
        this.endDate = endDate;
    }

    public List<ExhibitionCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<ExhibitionCategory> categories) {
        this.categories = categories;
    }

    public List<CustomerEmbed> getParticipants() {
        return participants;
    }

    public void setParticipants(List<CustomerEmbed> participants) {
        this.participants = participants;
    }

}
