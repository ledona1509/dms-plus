package com.viettel.backend.restful.supervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.UserSimpleDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SupervisorUserService;

@RestController(value = "supervisorSalesmanController")
@RequestMapping(value = "/supervisor/salesman")
public class SalesmanController extends AbstractController {

    private static final long serialVersionUID = -2083390468606881411L;

    @Autowired
    private SupervisorUserService supervisorUserService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getSalesmanList(@RequestParam(required = false) String distributorId) {

        ListJson<UserSimpleDto> results = supervisorUserService.getSalesmen(
                getUserLogin(), distributorId);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/notarget", method = RequestMethod.GET)
    public ResponseEntity<?> getSalesmanListNotHaveTarget(
            @RequestParam(required = false) String distributionId,
            @RequestParam int month, @RequestParam int year) {

        ListJson<UserSimpleDto> results = supervisorUserService.getSalesmenNoTarget(
                getUserLogin(), distributionId, month, year);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

}
