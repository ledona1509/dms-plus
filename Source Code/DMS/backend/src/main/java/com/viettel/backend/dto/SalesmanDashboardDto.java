package com.viettel.backend.dto;

import java.io.Serializable;

import com.viettel.backend.dto.embed.Result;

public class SalesmanDashboardDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6931337321980872133L;
    
    private Result revenue;
    private Result output;
    private Result nbSaleDays;

    public Result getRevenue() {
        return revenue;
    }

    public void setRevenue(Result revenue) {
        this.revenue = revenue;
    }

    public Result getOutput() {
        return output;
    }

    public void setOutput(Result output) {
        this.output = output;
    }

    public Result getNbSaleDays() {
        return nbSaleDays;
    }

    public void setNbSaleDays(Result nbSaleDays) {
        this.nbSaleDays = nbSaleDays;
    }
    
 

}
