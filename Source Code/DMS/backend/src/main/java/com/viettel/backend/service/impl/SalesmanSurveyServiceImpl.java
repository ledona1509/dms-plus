package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Survey;
import com.viettel.backend.domain.User;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.SurveyDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.SurveyAnswerRepository;
import com.viettel.backend.repository.SurveyRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.service.SalesmanSurveyService;

@Service
public class SalesmanSurveyServiceImpl extends AbstractSalesmanService implements SalesmanSurveyService {

    private static final long serialVersionUID = -5402109549735065233L;

    @Autowired
    private SurveyRepository surveyRepository;

    @Autowired
    private SurveyAnswerRepository surveyAnswerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public ListJson<SurveyDto> getSurveysByCustomer(UserLogin userLogin, String _customerId) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, _customerId);

        Collection<Survey> surveys = surveyRepository.getSurveyAvailableByCustomer(userLogin.getClientId(),
                customer.getId());
        if (surveys == null || surveys.isEmpty()) {
            return ListJson.<SurveyDto> emptyList();
        }

        List<SurveyDto> dtos = new ArrayList<SurveyDto>(surveys.size());
        for (Survey survey : surveys) {
            SurveyDto dto = new SurveyDto(survey);
            dtos.add(dto);
        }

        return new ListJson<SurveyDto>(dtos, Long.valueOf(dtos.size()));
    }

}
