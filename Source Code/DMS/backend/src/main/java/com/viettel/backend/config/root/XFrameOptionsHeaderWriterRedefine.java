package com.viettel.backend.config.root;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.header.HeaderWriter;
import org.springframework.security.web.header.writers.frameoptions.AllowFromStrategy;
import org.springframework.util.Assert;

public class XFrameOptionsHeaderWriterRedefine implements HeaderWriter {

	public static final String XFRAME_OPTIONS_HEADER = "X-Frame-Options";
	
    private final AllowFromStrategy allowFromStrategy;
    
    public XFrameOptionsHeaderWriterRedefine(AllowFromStrategy allowFromStrategy) {
    	Assert.notNull(allowFromStrategy, "allowFromStrategy cannot be null");
    	this.allowFromStrategy = allowFromStrategy;
    }
    
	@Override
	public void writeHeaders(HttpServletRequest request,
			HttpServletResponse response) {
		boolean allowed = false;
		
        String allowFromValue = allowFromStrategy.getAllowFromValue(request);
        if(allowFromValue != null) {
        	allowed = true;
        	response.addHeader(XFRAME_OPTIONS_HEADER, XFrameOptionsMode.ALLOW_FROM.getMode() + " " + allowFromValue);
        }
		
		if(!allowed) {
			response.addHeader(XFRAME_OPTIONS_HEADER, XFrameOptionsMode.DENY.getMode());
		}
	}
	
	 public enum XFrameOptionsMode {
        DENY("DENY"),
        SAMEORIGIN("SAMEORIGIN"),
        ALLOW_FROM("ALLOW-FROM");
        
        private String mode;

        private XFrameOptionsMode(String mode) {
            this.mode = mode;
        }

        private String getMode() {
            return mode;
        }
	 }

}
