package com.viettel.backend.dto;

import java.math.BigDecimal;

import com.viettel.backend.domain.Product;
import com.viettel.backend.engine.promotion.I_Product;

public class ProductDto extends NameCategoryDto implements I_Product<String> {

    private static final long serialVersionUID = -3795654109422932796L;

    private String code;
    private String photo;
    private UOMDto uom;
    private BigDecimal price;
    private BigDecimal output;

    private NameCategoryDto productCategory;
    private String description;

    public ProductDto(Product product, String defaultPhoto) {
        super(product);

        this.code = product.getCode();
        this.price = product.getPrice();
        this.output = product.getOutput();

        if (product.getUom() != null) {
            this.uom = new UOMDto(product.getUom());
        }

        if (product.getPhoto() != null) {
            this.photo = product.getPhoto();
        } else {
            this.photo = defaultPhoto;
        }
        
        this.description = product.getDescription();

        if (product.getProductCategory() != null) {
            this.productCategory = new NameCategoryDto(product.getProductCategory());
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public UOMDto getUom() {
        return uom;
    }

    public void setUom(UOMDto uom) {
        this.uom = uom;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getOutput() {
        return output;
    }

    public void setOutput(BigDecimal output) {
        this.output = output;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public NameCategoryDto getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(NameCategoryDto productCategory) {
        this.productCategory = productCategory;
    }

}
