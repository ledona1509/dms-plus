package com.viettel.backend.restful;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.service.GenerateDataService;

@RestController
@RequestMapping(value = "/generate")
public class GenerateDataController extends AbstractController {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private GenerateDataService generateDataService;

    @RequestMapping(value = "/customer", method = RequestMethod.POST)
    public ResponseEntity<?> generateCustomerByDistributor(@RequestParam String distributorId,
            @RequestParam double longitude, @RequestParam double latitude, @RequestParam int numberCustomer) {
        generateDataService.generateCustomerByDistributor(getUserLogin(), distributorId, longitude, latitude,
                numberCustomer);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/visit", method = RequestMethod.POST)
    public ResponseEntity<?> generateCustomerByDistributor(@RequestParam String fromDate, @RequestParam String toDate) {
        generateDataService.generateVisitsAndOrders(getUserLogin(), fromDate, toDate);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

}
