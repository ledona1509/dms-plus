package com.viettel.backend.dto.embed;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CustomerScheduleDto implements Serializable {
    /**
	 * 
	 */
    private static final long serialVersionUID = 6025241488260901007L;

    private String customerId;
    private String code;
    private String name;
    private String address;
    private String salesmanId;

    private List<CustomerScheduleItemDto> items;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<CustomerScheduleItemDto> getItems() {
        return items;
    }

    public void setItems(List<CustomerScheduleItemDto> items) {
        this.items = items;
    }

    public void addItem(CustomerScheduleItemDto item) {
        if (this.items == null) {
            this.items = new ArrayList<CustomerScheduleItemDto>();
        }

        this.items.add(item);
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }
}
