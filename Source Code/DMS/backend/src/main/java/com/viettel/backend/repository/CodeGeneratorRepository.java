package com.viettel.backend.repository;

import java.io.Serializable;

public interface CodeGeneratorRepository extends Serializable {

    public String getDistributorCode(String clientId);

    public String getOrderCode(String clientId);

    public String getCustomerCode(String clientId);
}
