package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;

import com.viettel.backend.domain.PO;
import com.viettel.backend.repository.BasicRepository;

public abstract class BasicRepositoryImpl<D extends PO> extends AbstractRepository<D> implements BasicRepository<D> {

    private static final long serialVersionUID = -8958421384263600937L;

    // ** ACTIVE = TRUE **
    @Override
    public D getById(ObjectId clientId, ObjectId id) {
        return super._getById(clientId, true, id);
    }

    @Override
    public List<D> getListByIds(ObjectId clientId, Collection<ObjectId> ids) {
        return super._getListByIds(clientId, true, ids, null, null);
    }

    @Override
    public boolean exists(ObjectId clientId, ObjectId id) {
        return super._exists(clientId, true, id);
    }

    @Override
    public boolean exists(ObjectId clientId, Collection<ObjectId> ids) {
        Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).in(ids);
        long count = super._count(clientId, true, criteria);
        return (count != ids.size());
    }

    @Override
    public D save(ObjectId clientId, D domain) {
        return super._save(clientId, domain);
    }

    @Override
    public boolean delete(ObjectId clientId, ObjectId id) {
        return super._delete(clientId, id);
    }

    @Override
    public Map<ObjectId, D> getMapByIds(ObjectId clientId, Collection<ObjectId> ids) {
        List<D> domains = super._getListByIds(clientId, true, ids, null, null);
        return _getMap(domains);
    }

    // //** ACTIVE = BOTH **
    // @Override
    // public D getByIdWithDeleted(ObjectId clientId, ObjectId id) {
    // return super._getById(clientId, null, id);
    // }
    //
    // @Override
    // public List<D> getListByIdsWithDeleted(ObjectId clientId,
    // Collection<ObjectId> ids) {
    // return super._getListByIds(clientId, null, ids, null, null);
    // }
    //
    // @Override
    // public Map<ObjectId, D> getMapByIdsWithDeleted(ObjectId clientId,
    // Collection<ObjectId> ids) {
    // List<D> domains = super._getListByIds(clientId, null, ids,
    // null, null);
    // return _getMap(domains);
    // }

}
