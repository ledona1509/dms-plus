package com.viettel.backend.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Exhibition;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.SalesConfig;
import com.viettel.backend.domain.Survey;
import com.viettel.backend.domain.Target;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.CustomerSchedule;
import com.viettel.backend.domain.embed.CustomerScheduleItem;
import com.viettel.backend.domain.embed.ExhibitionCategory;
import com.viettel.backend.domain.embed.ExhibitionItem;
import com.viettel.backend.domain.embed.ExhibitionRating;
import com.viettel.backend.domain.embed.ExhibitionRatingItem;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.domain.embed.SurveyAnswer;
import com.viettel.backend.domain.embed.SurveyOption;
import com.viettel.backend.domain.embed.SurveyQuestion;
import com.viettel.backend.dto.ExhibitionReportDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.SalesmanSalesReportMonthlyDto;
import com.viettel.backend.dto.SurveyResultDto;
import com.viettel.backend.dto.VisitReportDailyDto;
import com.viettel.backend.dto.VisitReportDto;
import com.viettel.backend.dto.embed.Result;
import com.viettel.backend.dto.embed.SurveyOptionResultDto;
import com.viettel.backend.dto.embed.SurveyQuestionResultDto;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.ExhibitionRatingRepository;
import com.viettel.backend.repository.ExhibitionRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.SurveyAnswerRepository;
import com.viettel.backend.repository.SurveyRepository;
import com.viettel.backend.repository.TargetRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SupervisorReportService;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.HashMapDoubleKey;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class SupervisorReportSerivceImpl extends AbstractSupervisorService implements SupervisorReportService {

    private static final long serialVersionUID = 5686134592376322356L;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private TargetRepository targetRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Autowired
    private SurveyRepository surveyRepository;

    @Autowired
    private SurveyAnswerRepository surveyAnswerRepository;

    @Autowired
    private ExhibitionRepository exhibitionRepository;

    @Autowired
    private ExhibitionRatingRepository exhibitionRatingRepository;

    @Override
    public ListJson<VisitReportDto> getVisitReportBySalesman(UserLogin userLogin, String salesmanId,
            String fromDateIsoDate, String toDateIsoDate) {
        checkIsSupervisor(userLogin);

        ObjectId clientId = userLogin.getClientId();

        SimpleDate fromDate = SimpleDate.createByIsoDate(fromDateIsoDate, null);
        SimpleDate toDate = SimpleDate.createByIsoDate(toDateIsoDate, null);
        if (fromDate == null || toDate == null) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        if (fromDate.compareTo(toDate) >= 0) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }
        Period period = new Period(fromDate, toDate);

        ClientConfig clientConfig = getClientCondig(userLogin);

        User salesman = checkSupervisorSalesman(userLogin, salesmanId);

        List<Customer> customers = customerRepository.getCustomersBySalesmen(clientId, Arrays.asList(salesman.getId()),
                null, null, null);
        if (customers == null || customers.isEmpty()) {
            return ListJson.emptyList();
        }

        // DANH SACH TUYEN TRONG KHOANG THOI GIAN
        HashMapDoubleKey<Integer, Integer, Integer> numberVisitBySchedule = new HashMapDoubleKey<Integer, Integer, Integer>();
        SimpleDate tmp = new SimpleDate(fromDate);
        while (tmp.compareTo(toDate) <= 0) {
            int firstDayOfWeek = clientConfig.getFirstDayOfWeek();
            int minimalDaysInFirstWeek = clientConfig.getMinimalDaysInFirstWeek();

            // Tuan lam viec cua ngay hom nay tinh tu dau nam
            int weekOfToday = DateTimeUtils.getWeekOfYear(tmp, firstDayOfWeek, minimalDaysInFirstWeek);
            int weekDuration = clientConfig.getNumberWeekOfFrequency();
            // Thu tu cua tuan theo week duration
            int weekIndex = weekOfToday % weekDuration + 1;
            int dayOfTmp = tmp.getDayOfWeek();

            Integer numberVisit = numberVisitBySchedule.get(weekIndex, dayOfTmp);
            if (numberVisit == null) {
                numberVisit = 1;
            } else {
                numberVisit = numberVisit + 1;
            }

            numberVisitBySchedule.put(weekIndex, dayOfTmp, numberVisit);

            tmp = DateTimeUtils.addDays(tmp, 1);
        }

        List<ObjectId> customerIds = new ArrayList<ObjectId>(customers.size());
        HashMap<ObjectId, VisitReportDto> reportVisitByCustomer = new HashMap<ObjectId, VisitReportDto>();
        List<VisitReportDto> reportVisitMonthlys = new ArrayList<VisitReportDto>(customers.size());
        for (Customer customer : customers) {
            VisitReportDto dto = new VisitReportDto();
            dto.setCustomer(customer.getName());
            dto.setCustomerId(customer.getId().toString());

            CustomerSchedule customerSchedule = customer.getSchedule();
            if (customerSchedule != null) {
                List<CustomerScheduleItem> items = customerSchedule.getItems();
                if (items != null && !items.isEmpty()) {
                    for (CustomerScheduleItem item : items) {
                        List<Integer> weeks = item.getWeeks();
                        List<Integer> days = item.getDays();

                        if (weeks != null && !weeks.isEmpty() && days != null && !days.isEmpty()) {
                            int numberVisitPlan = 0;
                            for (int week : weeks) {
                                for (int day : days) {
                                    Integer tmpNumberVisitPlan = numberVisitBySchedule.get(week, day);
                                    tmpNumberVisitPlan = tmpNumberVisitPlan == null ? 0 : tmpNumberVisitPlan;

                                    numberVisitPlan = numberVisitPlan + tmpNumberVisitPlan;
                                }
                            }

                            dto.setNumberVisitPlan(numberVisitPlan);
                        }
                    }
                }
            }

            customerIds.add(customer.getId());
            reportVisitMonthlys.add(dto);
            reportVisitByCustomer.put(customer.getId(), dto);
        }

        Sort sort = new Sort(Direction.DESC, Visit.COLUMNNAME_END_TIME_VALUE);
        List<Visit> visits = visitRepository.getVisitedsBySalesmen(clientId, Arrays.asList(salesman.getId()), period,
                sort);
        if (visits != null && !visits.isEmpty()) {
            for (Visit visit : visits) {
                VisitReportDto dto = reportVisitByCustomer.get(visit.getCustomer().getId());
                if (dto == null) {
                    continue;
                }

                dto.setNumberVisited(dto.getNumberVisited() + 1);

                if (visit.isClosed()) {
                    dto.setNumberVisitClosed(dto.getNumberVisitClosed() + 1);
                } else {
                    if (visit.isErrorDuration()) {
                        dto.setNumberVisitErrorTime(dto.getNumberVisitErrorTime() + 1);
                    } else {
                        dto.setNumberVisitNoErrorTime(dto.getNumberVisitNoErrorTime() + 1);
                    }
                }

                if (visit.getDistance() == null) {
                    dto.setNumberVisitUnlocated(dto.getNumberVisitUnlocated() + 1);
                } else {
                    if (visit.getLocationStatus() == Visit.LOCATION_STATUS_LOCATED) {
                        dto.setNumberVisitInRange(dto.getNumberVisitInRange() + 1);
                    } else {
                        dto.setNumberVisitOutRange(dto.getNumberVisitOutRange() + 1);
                    }
                }
            }
        }

        return new ListJson<VisitReportDto>(reportVisitMonthlys, (long) reportVisitMonthlys.size());
    }

    public ListJson<VisitReportDto> getVisitReportByCustomer(UserLogin userLogin, String customerId,
            String fromDateIsoDate, String toDateIsoDate) {
        checkIsSupervisor(userLogin);
        ObjectId clientId = userLogin.getClientId();

        SimpleDate fromDate = SimpleDate.createByIsoDate(fromDateIsoDate, null);
        SimpleDate toDate = SimpleDate.createByIsoDate(toDateIsoDate, null);

        if (fromDate == null || toDate == null) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        if (fromDate.compareTo(toDate) >= 0) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }
        Period period = new Period(fromDate, toDate);

        ClientConfig clientConfig = getClientCondig(userLogin);

        Customer customer = checkSupervisorCustomer(userLogin, customerId);

        // DANH SACH TUYEN TRONG KHOANG THOI GIAN
        HashMapDoubleKey<Integer, Integer, Integer> numberVisitBySchedule = new HashMapDoubleKey<Integer, Integer, Integer>();
        SimpleDate tmp = new SimpleDate(fromDate);
        while (tmp.compareTo(toDate) <= 0) {
            // TODO: Get client configuration
            int firstDayOfWeek = DateTimeUtils.DEFAULT_FIRST_DAY_OF_WEEK;
            int minimalDaysInFirstWeek = DateTimeUtils.DEFAULT_MINIMAL_DAYS_IN_FIRST_WEEK;

            // Tuan lam viec cua ngay hom nay tinh tu dau nam
            int weekOfToday = DateTimeUtils.getWeekOfYear(tmp, firstDayOfWeek, minimalDaysInFirstWeek);
            int weekDuration = clientConfig.getNumberWeekOfFrequency();
            // Thu tu cua tuan theo week duration
            int weekIndex = weekOfToday % weekDuration + 1;
            int dayOfTmp = tmp.getDayOfWeek();

            Integer numberVisit = numberVisitBySchedule.get(weekIndex, dayOfTmp);
            if (numberVisit == null) {
                numberVisit = 1;
            } else {
                numberVisit = numberVisit + 1;
            }

            numberVisitBySchedule.put(weekIndex, dayOfTmp, numberVisit);

            tmp = DateTimeUtils.addDays(tmp, 1);
        }

        List<VisitReportDto> reportVisitMonthlys = new ArrayList<VisitReportDto>(1);
        VisitReportDto dto = new VisitReportDto();
        dto.setCustomer(customer.getName());
        dto.setCustomerId(customer.getId().toString());

        CustomerSchedule customerSchedule = customer.getSchedule();
        if (customerSchedule != null) {
            List<CustomerScheduleItem> items = customerSchedule.getItems();
            if (items != null && !items.isEmpty()) {
                for (CustomerScheduleItem item : items) {
                    List<Integer> weeks = item.getWeeks();
                    List<Integer> days = item.getDays();

                    if (weeks != null && !weeks.isEmpty() && days != null && !days.isEmpty()) {
                        int numberVisitPlan = 0;
                        for (int week : weeks) {
                            for (int day : days) {
                                Integer tmpNumberVisitPlan = numberVisitBySchedule.get(week, day);
                                tmpNumberVisitPlan = tmpNumberVisitPlan == null ? 0 : tmpNumberVisitPlan;

                                numberVisitPlan = numberVisitPlan + tmpNumberVisitPlan;
                            }
                        }

                        dto.setNumberVisitPlan(numberVisitPlan);
                    }
                }
            }
        }
        
        Sort sort = new Sort(Direction.DESC, Visit.COLUMNNAME_END_TIME_VALUE);
        List<Visit> visits = visitRepository.getVisitedsByCustomers(clientId, Arrays.asList(customer.getId()), period,
                sort);
        if (visits != null && !visits.isEmpty()) {
            for (Visit visit : visits) {
                dto.setNumberVisited(dto.getNumberVisited() + 1);

                if (visit.isClosed()) {
                    dto.setNumberVisitClosed(dto.getNumberVisitClosed() + 1);
                } else {
                    long duration = SimpleDate.getDuration(visit.getStartTime(), visit.getEndTime());
                    if (duration < getSalesConfig(userLogin).getVisitDurationKPI()) {
                        dto.setNumberVisitErrorTime(dto.getNumberVisitErrorTime() + 1);
                    } else {
                        dto.setNumberVisitNoErrorTime(dto.getNumberVisitNoErrorTime() + 1);
                    }
                }

                if (visit.getDistance() == null) {
                    dto.setNumberVisitUnlocated(dto.getNumberVisitUnlocated() + 1);
                } else {
                    if (visit.getLocationStatus() == Visit.LOCATION_STATUS_LOCATED) {
                        dto.setNumberVisitInRange(dto.getNumberVisitInRange() + 1);
                    } else {
                        dto.setNumberVisitOutRange(dto.getNumberVisitOutRange() + 1);
                    }
                }
            }
        }

        reportVisitMonthlys.add(dto);

        return new ListJson<VisitReportDto>(reportVisitMonthlys, (long) reportVisitMonthlys.size());
    }

    @Override
    public ListJson<SalesmanSalesReportMonthlyDto> getSalesmanSalesReportMonthly(UserLogin userLogin,
            String salesmanId, int month, int year) {
        checkIsSupervisor(userLogin);
        ObjectId clientId = userLogin.getClientId();
        ObjectId supervisorId = userLogin.getUserId();

        List<User> salesmen = null;
        Collection<ObjectId> salesmanIds = null;
        if (salesmanId == null) {
            salesmen = userRepository.getSalesmenBySupervisors(clientId, Arrays.asList(supervisorId));
            if (salesmen == null || salesmen.isEmpty()) {
                return ListJson.<SalesmanSalesReportMonthlyDto> emptyList();
            }

            salesmanIds = new HashSet<ObjectId>(salesmen.size());
            for (User salesman : salesmen) {
                salesmanIds.add(salesman.getId());
            }

        } else {
            User saleman = checkSupervisorSalesman(userLogin, salesmanId);

            salesmen = Arrays.asList(saleman);
            salesmanIds = Arrays.asList(saleman.getId());
        }

        HashMap<ObjectId, Target> targetBySalesman = new HashMap<ObjectId, Target>();
        List<Target> targets = targetRepository.getTargetsBySalesmen(clientId, salesmanIds, month, year, null, null);
        if (targets != null && !targets.isEmpty()) {
            for (Target target : targets) {
                targetBySalesman.put(target.getSalesman().getId(), target);
            }
        }

        HashMap<ObjectId, SalesmanSalesReportMonthlyDto> reportBySalesman = new HashMap<ObjectId, SalesmanSalesReportMonthlyDto>();

        List<Order> orders = orderRepository.getOrderBySalesmen(clientId, salesmanIds,
                DateTimeUtils.getPeriodByMonth(month, year), null, null, null);
        if (orders != null && !orders.isEmpty()) {
            HashMap<ObjectId, HashSet<ObjectId>> customersBySalesman = new HashMap<ObjectId, HashSet<ObjectId>>();
            for (Order order : orders) {
                SalesmanSalesReportMonthlyDto report = reportBySalesman.get(order.getSalesman().getId());
                HashSet<ObjectId> customerIds = customersBySalesman.get(order.getSalesman().getId());
                if (report == null) {
                    report = new SalesmanSalesReportMonthlyDto();
                    report.setSalesmanId(order.getSalesman().getId().toString());
                    report.setSalesman(order.getSalesman().getFullname());

                    Target target = targetBySalesman.get(order.getSalesman().getId());
                    if (target == null) {
                        report.setRevenue(new Result(0, 0));
                        report.setOutput(new Result(0, 0));
                    } else {
                        report.setRevenue(new Result(target.getRevenue(), BigDecimal.ZERO));
                        report.setOutput(new Result(target.getOutput(), BigDecimal.ZERO));
                    }

                    reportBySalesman.put(order.getSalesman().getId(), report);

                    customerIds = new HashSet<ObjectId>();
                    customersBySalesman.put(order.getSalesman().getId(), customerIds);
                }

                report.getRevenue().setActual(report.getRevenue().getActual().add(order.getGrandTotal()));

                BigDecimal output = BigDecimal.ZERO;
                for (OrderDetail poDetail : order.getDetails()) {
                    output = output.add(poDetail.getOutput());
                }
                report.getOutput().setActual(report.getOutput().getActual().add(output));

                customerIds.add(order.getCustomer().getId());
                report.setNumberCustomer(customerIds.size());
            }
        }

        List<SalesmanSalesReportMonthlyDto> reports = new ArrayList<SalesmanSalesReportMonthlyDto>(salesmen.size());
        for (User salesman : salesmen) {
            SalesmanSalesReportMonthlyDto report = reportBySalesman.get(salesman.getId());
            if (report == null) {
                report = new SalesmanSalesReportMonthlyDto();
                report.setSalesmanId(salesman.getId().toString());
                report.setSalesman(salesman.getFullname());

                Target target = targetBySalesman.get(salesman.getId());
                if (target == null) {
                    report.setRevenue(new Result(0, 0));
                    report.setOutput(new Result(0, 0));
                } else {
                    report.setRevenue(new Result(target.getRevenue(), BigDecimal.ZERO));
                    report.setOutput(new Result(target.getOutput(), BigDecimal.ZERO));
                }

                report.setNumberCustomer(0);
            }

            reports.add(report);
        }

        return new ListJson<SalesmanSalesReportMonthlyDto>(reports, (long) reports.size());
    }

    @Override
    public VisitReportDailyDto getVisitReportDaily(UserLogin userLogin, String salesmanId) {
        checkIsSupervisor(userLogin);

        Collection<ObjectId> salesmanIds = null;
        if (salesmanId == null) {
            salesmanIds = userRepository.getSalesmanIdsBySupervisors(userLogin.getClientId(),
                    Arrays.asList(userLogin.getUserId()));
        } else {
            User salesman = checkSupervisorSalesman(userLogin, salesmanId);
            salesmanIds = Arrays.asList(salesman.getId());
        }

        Sort sort = new Sort(Direction.DESC, Visit.COLUMNNAME_END_TIME_VALUE);
        Collection<Visit> visits = visitRepository.getVisitedsBySalesmen(userLogin.getClientId(), salesmanIds,
                DateTimeUtils.getPeriodToday(), sort);

        long numberCustomerForVisitToday = customerRepository.countCustomersBySalesmenDate(userLogin.getClientId(),
                salesmanIds, null, DateTimeUtils.getToday());

        VisitReportDailyDto dto = new VisitReportDailyDto(userLogin, visits, getClientCondig(userLogin).getDefaultProductPhoto(),
                (int) numberCustomerForVisitToday);

        return dto;
    }

    @Override
    public SurveyResultDto getSurveyReport(UserLogin userLogin, String _surveyId) {
        checkIsSupervisor(userLogin);

        ObjectId surveyId = ObjectIdUtils.getObjectId(_surveyId, null);
        if (surveyId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Survey survey = surveyRepository.getById(userLogin.getClientId(), surveyId);
        if (survey == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Set<ObjectId> customerIds = customerRepository.getCustomerIdsBySupervisor(userLogin.getClientId(),
                userLogin.getUserId());

        List<SurveyAnswer> answers = surveyAnswerRepository.getSurveyAnswersByCustomers(userLogin.getClientId(),
                surveyId, customerIds);
        HashMap<ObjectId, Integer> resultByOptionId = new HashMap<ObjectId, Integer>();
        if (answers != null) {
            for (SurveyAnswer answer : answers) {
                if (answer.getOptions() != null) {
                    for (ObjectId optionId : answer.getOptions()) {
                        Integer result = resultByOptionId.get(optionId);
                        if (result == null) {
                            result = 0;
                        }
                        result = result + 1;
                        resultByOptionId.put(optionId, result);
                    }
                }
            }
        }

        SurveyResultDto surveyResultDto = new SurveyResultDto();
        surveyResultDto.setId(survey.getId().toString());
        surveyResultDto.setName(survey.getName());
        surveyResultDto.setStartDate(survey.getStartDate().getIsoDate());
        surveyResultDto.setEndDate(survey.getEndDate().getIsoDate());
        surveyResultDto.setRequired(survey.isRequired());

        if (survey.getQuestions() != null) {
            for (SurveyQuestion question : survey.getQuestions()) {
                SurveyQuestionResultDto questionResultDto = new SurveyQuestionResultDto();
                questionResultDto.setId(question.getId().toString());
                questionResultDto.setName(question.getName());
                questionResultDto.setMultipleChoice(question.isMultipleChoice());
                questionResultDto.setRequired(question.isRequired());

                if (question.getOptions() != null) {
                    for (SurveyOption option : question.getOptions()) {
                        SurveyOptionResultDto optionResultDto = new SurveyOptionResultDto();
                        optionResultDto.setId(option.getId().toString());
                        optionResultDto.setName(option.getName());
                        Integer result = resultByOptionId.get(option.getId());
                        if (result != null) {
                            optionResultDto.setResult(result);
                        }

                        questionResultDto.addOption(optionResultDto);
                    }
                }

                surveyResultDto.addQuestion(questionResultDto);
            }
        }

        return surveyResultDto;
    }

    @Override
    public byte[] exportSurveyReport(UserLogin userLogin, String _surveyId) {
        checkIsSupervisor(userLogin);

        ObjectId surveyId = ObjectIdUtils.getObjectId(_surveyId, null);
        if (surveyId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Survey survey = surveyRepository.getById(userLogin.getClientId(), surveyId);
        if (survey == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Set<ObjectId> customerIds = customerRepository.getCustomerIdsBySupervisor(userLogin.getClientId(),
                userLogin.getUserId());

        List<SurveyAnswer> answers = surveyAnswerRepository.getSurveyAnswersByCustomers(userLogin.getClientId(),
                surveyId, customerIds);

        // Finds the workbook instance for XLSX file
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Survey");

        if (answers == null) {
            XSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue("Empty");
        } else {
            XSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue("Time");
            row.createCell(1).setCellValue("Customer");
            row.createCell(2).setCellValue("Salesman");
            if (survey.getQuestions() != null) {
                int index = 3;

                for (SurveyQuestion question : survey.getQuestions()) {
                    row.createCell(index).setCellValue(question.getName());
                    index++;
                }
            }

            int answerIndex = 1;
            for (SurveyAnswer answer : answers) {
                row = sheet.createRow(answerIndex);
                row.createCell(0).setCellValue(answer.getVisit().getEndTime().format("dd/MM/yyyy HH:mm"));
                row.createCell(1).setCellValue(answer.getVisit().getCustomer().getName());
                row.createCell(2).setCellValue(answer.getVisit().getSalesman().getFullname());

                if (survey.getQuestions() != null) {
                    int index = 3;

                    for (SurveyQuestion question : survey.getQuestions()) {
                        StringBuilder optionsDisplay = new StringBuilder();
                        boolean isFirst = true;
                        if (question.getOptions() != null) {
                            for (SurveyOption option : question.getOptions()) {
                                if (answer.getOptions() != null && answer.getOptions().contains(option.getId())) {
                                    if (!isFirst) {
                                        optionsDisplay.append(", ");
                                    }
                                    isFirst = false;

                                    optionsDisplay.append(option.getName());
                                }
                            }
                        }

                        row.createCell(index).setCellValue(optionsDisplay.toString());

                        index++;
                    }
                }

                answerIndex++;
            }
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);
            workbook.close();

            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public ExhibitionReportDto getExhibitionReport(UserLogin userLogin, String _exhibitionId) {
        checkIsSupervisor(userLogin);

        ObjectId exhibitionId = ObjectIdUtils.getObjectId(_exhibitionId, null);
        if (exhibitionId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Exhibition exhibition = exhibitionRepository.getById(userLogin.getClientId(), exhibitionId);
        if (exhibition == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }

        // Fetch all customers
        Set<ObjectId> customerIds = customerRepository.getCustomerIdsBySupervisor(userLogin.getClientId(),
                userLogin.getUserId());
        if (customerIds == null || customerIds.isEmpty()) {
            return new ExhibitionReportDto(exhibition, 0, 0);
        }

        int numberOfCustomers = customerIds.size();
        List<ExhibitionRating> ratings = exhibitionRatingRepository.getExhibitionRatingsByCustomers(
                userLogin.getClientId(), exhibitionId, customerIds);
        if (ratings == null) {
            ratings = Collections.emptyList();
        }

        Map<ObjectId, List<Float>> rateByCustomer = new HashMap<ObjectId, List<Float>>();
        for (ExhibitionRating rating : ratings) {
            // If no item, skip it
            if (rating.getItems() == null || rating.getItems().isEmpty()) {
                continue;
            }

            List<Float> customerPoints = rateByCustomer.get(rating.getVisit().getCustomer().getId());
            if (customerPoints == null) {
                customerPoints = new LinkedList<Float>();
                rateByCustomer.put(rating.getVisit().getCustomer().getId(), customerPoints);
            }

            float rate = 0;
            for (ExhibitionRatingItem rateingItem : rating.getItems()) {
                rate += rateingItem.getRate();
            }
            rate = rate / rating.getItems().size();
            customerPoints.add(rate);
        }

        SalesConfig salesConfig = getSalesConfig(userLogin);
        
        long numberOfMeetReqCustomers = 0;
        for (Map.Entry<ObjectId, List<Float>> entry : rateByCustomer.entrySet()) {
            float totalPoint = 0;
            for (Float point : entry.getValue()) {
                totalPoint += point;
            }
            float avgPoint = totalPoint / entry.getValue().size();
            if (avgPoint >= salesConfig.getMeetReqRate()) {
                numberOfMeetReqCustomers += 1;
            }
        }

        // XXX verify!!!
        float meetReqRate = Math.round(numberOfMeetReqCustomers / numberOfCustomers * 100);
        float paticipatedRate = Math.round(rateByCustomer.size() / (float) numberOfCustomers * 100);

        return new ExhibitionReportDto(exhibition, paticipatedRate, meetReqRate);
    }

    @Override
    public byte[] exportExhibitionReport(UserLogin userLogin, String _exhibitionId) {
        checkIsSupervisor(userLogin);

        ObjectId exhibitionId = ObjectIdUtils.getObjectId(_exhibitionId, null);
        if (exhibitionId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Exhibition exhibition = exhibitionRepository.getById(userLogin.getClientId(), exhibitionId);
        if (exhibition == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }

        // Fetch all customers
        Set<ObjectId> customerIds = customerRepository.getCustomerIdsBySupervisor(userLogin.getClientId(),
                userLogin.getUserId());

        List<ExhibitionRating> ratings;
        if (customerIds == null || customerIds.isEmpty()) {
            ratings = Collections.emptyList();
        } else {
            ratings = exhibitionRatingRepository.getExhibitionRatingsByCustomers(userLogin.getClientId(), exhibitionId,
                    customerIds);
            if (ratings == null) {
                ratings = Collections.emptyList();
            }
        }

        // Finds the workbook instance for XLSX file
        XSSFWorkbook workbook = new XSSFWorkbook();
        // Create the sheet
        XSSFSheet sheet = workbook.createSheet("Exhibition");

        // Create header row
        XSSFRow row = sheet.createRow(0);
        row.createCell(0).setCellValue("Time");
        row.createCell(1).setCellValue("Customer");
        row.createCell(2).setCellValue("Salesman");

        List<ObjectId> itemIds = new ArrayList<ObjectId>(10);
        if (exhibition.getCategories() != null) {
            int index = 3;

            for (ExhibitionCategory category : exhibition.getCategories()) {
                if (category.getItems() != null) {
                    for (ExhibitionItem item : category.getItems()) {
                        row.createCell(index).setCellValue(item.getName());
                        itemIds.add(item.getId());
                        index++;
                    }
                }
            }
        }

        int rowIndex = 1;
        int size = ratings.size();
        int itemsSize = itemIds.size();

        Map<ObjectId, Float> mapPoints = new HashMap<ObjectId, Float>(20);

        for (int i = 0; i < size; i++) {
            ExhibitionRating rating = ratings.get(i);
            row = sheet.createRow(rowIndex);
            row.createCell(0).setCellValue(rating.getVisit().getEndTime().format("dd/MM/yyyy HH:mm"));
            row.createCell(1).setCellValue(rating.getVisit().getCustomer().getName());
            row.createCell(2).setCellValue(rating.getVisit().getSalesman().getFullname());

            mapPoints.clear();

            List<ExhibitionRatingItem> ratingItems = rating.getItems();
            if (ratingItems != null) {
                int index = 3;
                int ratedCount = ratingItems.size();

                for (int j = 0; j < ratedCount; j++) {
                    ExhibitionRatingItem ratingItem = ratingItems.get(j);
                    mapPoints.put(ratingItem.getExhibitionItemId(), ratingItem.getRate());
                }

                for (int j = 0; j < itemsSize; j++) {
                    ObjectId itemId = itemIds.get(j);
                    XSSFCell cell = row.createCell(index);
                    Float point = mapPoints.get(itemId);
                    if (point != null) {
                        cell.setCellValue(point);
                    }

                    index++;
                }
            }

            rowIndex++;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);
            workbook.close();

            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
