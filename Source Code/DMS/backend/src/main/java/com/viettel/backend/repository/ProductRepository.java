package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.Product;

public interface ProductRepository extends BasicNameCategoryRepository<Product> {
    
    public List<Product> getProductsByCategory(ObjectId clientId, ObjectId productCategoryId);
    
    public List<Product> getProducts(ObjectId clientId, String search, Pageable pageable, Sort sort);
    
    public long count(ObjectId clientId, String search);

    public List<Product> getProductsNotInIds(ObjectId clientId, Collection<ObjectId> ids, Pageable pageable, Sort sort);
    
    public long countProductNotInIds(ObjectId clientId, Collection<ObjectId> ids);
    
    public boolean existsByCode(ObjectId clientId, ObjectId id, String code);
    
    public boolean checkUOMUsed(ObjectId clientId, ObjectId uomId);
    
    public boolean checkProductCategoryUsed(ObjectId clientId, ObjectId productCategoryId);

}
