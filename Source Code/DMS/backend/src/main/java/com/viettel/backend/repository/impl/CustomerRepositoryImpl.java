package com.viettel.backend.repository.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.POSearchable;
import com.viettel.backend.domain.embed.CustomerSchedule;
import com.viettel.backend.domain.embed.CustomerScheduleItem;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.util.CriteriaUtils;
import com.viettel.backend.util.DateTimeUtils;

@Repository
public class CustomerRepositoryImpl extends BasicCategoryRepositoryImpl<Customer> implements CustomerRepository {

    private static final long serialVersionUID = 2100546745554205377L;

    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Override
    protected Criteria getDefaultCriteria() {
        Criteria approvedCriteria = Criteria.where(Customer.COLUMNNAME_APPROVE_STATUS).is(
                Customer.APPROVE_STATUS_APPROVED);
        return approvedCriteria;
    }

    @Override
    public List<Customer> getCustomers(ObjectId clientId, String search, Pageable pageable, Sort sort) {
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Customer.COLUMNNAME_SEARCH, search);
        return super._getList(clientId, true, searchCriteria, pageable, sort);
    }

    @Override
    public Set<ObjectId> getCustomerIds(ObjectId clientId, String search) {
        List<Customer> customers = getCustomers(clientId, search, null, null);
        return _getIdSet(customers);
    }

    @Override
    public long count(ObjectId clientId, String search) {
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);

        return super._count(clientId, true, searchCriteria);
    }

    // ** BY DISTRIBUTOR **
    @Override
    public List<Customer> getCustomersByDistributor(ObjectId clientId, ObjectId distributorId, boolean withAvailable,
            String search, Pageable pageable, Sort sort) {
        if (distributorId == null) {
            throw new IllegalArgumentException("distributorId is null");
        }

        Set<ObjectId> distributorIds = new HashSet<ObjectId>();
        distributorIds.add(distributorId);
        if (withAvailable) {
            distributorIds.add(null);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);

        return super._getList(clientId, true, CriteriaUtils.andOperator(distributorCriteria, searchCriteria), pageable,
                sort);
    }

    @Override
    public Set<ObjectId> getCustomerIdsByDistributor(ObjectId clientId, ObjectId distributorId, boolean withAvailable,
            String search) {
        List<Customer> customers = getCustomersByDistributor(clientId, distributorId, withAvailable, search, null, null);
        return _getIdSet(customers);
    }

    @Override
    public long countCustomersByDistributor(ObjectId clientId, ObjectId distributorId, boolean withAvailable,
            String search) {
        if (distributorId == null) {
            throw new IllegalArgumentException("distributorId is null");
        }

        Set<ObjectId> distributorIds = new HashSet<ObjectId>();
        distributorIds.add(distributorId);
        if (withAvailable) {
            distributorIds.add(null);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);

        return super._count(clientId, true, CriteriaUtils.andOperator(distributorCriteria, searchCriteria));
    }

    // ** AVAILABLE **
    @Override
    public List<Customer> getCustomersAvailable(ObjectId clientId, String search, Pageable pageable, Sort sort) {
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).is(null);

        return super._getList(clientId, true, CriteriaUtils.andOperator(distributorCriteria, searchCriteria), pageable,
                sort);
    }

    @Override
    public long countCustomersAvailable(ObjectId clientId, String search) {
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).is(null);

        return super._count(clientId, true, CriteriaUtils.andOperator(distributorCriteria, searchCriteria));
    }

    // ** UNSCHEDULED **
    @Override
    public List<Customer> getCustomersByDistributorUnscheduled(ObjectId clientId, ObjectId distributorId,
            String search, Pageable pageable, Sort sort) {
        if (distributorId == null) {
            throw new IllegalArgumentException("distributorId is null");
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).is(distributorId);
        Criteria scheduleCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE).is(null);

        return super._getList(clientId, true,
                CriteriaUtils.andOperator(distributorCriteria, scheduleCriteria, searchCriteria), pageable, sort);
    }

    @Override
    public Set<ObjectId> getCustomerIdsByDistributorUnscheduled(ObjectId clientId, ObjectId distributorId, String search) {
        List<Customer> customers = getCustomersByDistributorUnscheduled(clientId, distributorId, search, null, null);
        return _getIdSet(customers);
    }

    @Override
    public long countCustomersByDistributorUnscheduled(ObjectId clientId, ObjectId distributorId, String search) {
        if (distributorId == null) {
            throw new IllegalArgumentException("distributorId is null");
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).is(distributorId);
        Criteria scheduleCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE).is(null);

        return super._count(clientId, true,
                CriteriaUtils.andOperator(distributorCriteria, scheduleCriteria, searchCriteria));
    }

    /** UPDATE DISTRIBUTOR OF CUSTOMER BY IDs AND CLEAR ALL SCHEDULE IN OPTION */
    @Override
    public void updateDistributorForCustomer(ObjectId clientId, Collection<ObjectId> customerIds,
            DistributorEmbed distributor, boolean clearSchedule) {
        Update update = new Update();
        update.set(Customer.COLUMNNAME_DISTRIBUTOR, distributor);
        if (clearSchedule) {
            update.set(Customer.COLUMNNAME_SCHEDULE, null);
        }

        Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).in(customerIds);
        super._updateMulti(clientId, true, criteria, update);
    }

    // ** EDIT SCHEDULE *
    @Override
    public void clearCustomerScheduleBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds) {
        if (salesmanIds != null && !salesmanIds.isEmpty()) {
            Update update = new Update();
            update.set(Customer.COLUMNNAME_SCHEDULE, null);

            Criteria criteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE_SALESMAN_ID).in(salesmanIds);
            super._updateMulti(clientId, true, criteria, update);
        }
    }

    @Override
    public void clearCustomerSchedule(ObjectId clientId, Collection<ObjectId> customerIds) {
        if (customerIds != null && !customerIds.isEmpty()) {
            Update update = new Update();
            update.set(Customer.COLUMNNAME_SCHEDULE, null);

            Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).in(customerIds);
            super._updateMulti(clientId, true, criteria, update);
        }
    }

    @Override
    public void updateCustomerSchedule(ObjectId clientId, ObjectId customerId, CustomerSchedule customerSchedule) {
        if (customerId == null) {
            throw new IllegalArgumentException("customerId is null");
        }

        Update update = new Update();
        update.set(Customer.COLUMNNAME_SCHEDULE, customerSchedule);

        Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).is(customerId);
        super._updateMulti(clientId, true, criteria, update);
    }

    // ** BY SUPERVISOR **
    @Override
    public List<Customer> getCustomersBySupervisor(ObjectId clientId, ObjectId supervisorId, Pageable pageable,
            Sort sort) {
        if (supervisorId == null) {
            throw new IllegalArgumentException("supervisorId is null");
        }

        Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(clientId,
                Arrays.asList(supervisorId));

        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);

        return super._getList(clientId, true, distributorCriteria, pageable, sort);
    }

    @Override
    public Set<ObjectId> getCustomerIdsBySupervisor(ObjectId clientId, ObjectId supervisorId) {
        List<Customer> customers = getCustomersBySupervisor(clientId, supervisorId, null, null);
        return _getIdSet(customers);
    }

    @Override
    public long countCustomerBySupervisor(ObjectId clientId, ObjectId supervisorId) {
        if (supervisorId == null) {
            throw new IllegalArgumentException("supervisorId is null");
        }

        Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(clientId,
                Arrays.asList(supervisorId));

        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);

        return super._count(clientId, true, distributorCriteria);
    }

    // ** BY SALESMAN **
    @Override
    public List<Customer> getCustomersBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, String search,
            Pageable pageable, Sort sort) {
        if (salesmanIds == null) {
            throw new IllegalArgumentException("salesmanId is null");
        }

        if (salesmanIds.isEmpty()) {
            return Collections.<Customer> emptyList();
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria salesmanCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE_SALESMAN_ID).in(salesmanIds);

        return _getList(clientId, true, CriteriaUtils.andOperator(salesmanCriteria, searchCriteria), pageable, sort);
    }

    @Override
    public Set<ObjectId> getCustomerIdsBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, String search) {
        List<Customer> customers = getCustomersBySalesmen(clientId, salesmanIds, search, null, null);
        return _getIdSet(customers);
    }

    @Override
    public long countCustomersBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, String search) {
        if (salesmanIds == null) {
            throw new IllegalArgumentException("salesmanId is null");
        }

        if (salesmanIds.isEmpty()) {
            return 0l;
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria salesmanCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE_SALESMAN_ID).in(salesmanIds);

        return _count(clientId, true, CriteriaUtils.andOperator(salesmanCriteria, searchCriteria));
    }

    @Override
    public List<Customer> getCustomersBySalesmenDate(ObjectId clientId, Collection<ObjectId> salesmanIds,
            String search, SimpleDate date, Pageable pageable, Sort sort) {
        if (salesmanIds == null) {
            throw new IllegalArgumentException("salesmanId is null");
        }

        if (salesmanIds.isEmpty()) {
            return Collections.<Customer> emptyList();
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria scheduleCriteria = getScheduleCriteria(clientId, salesmanIds, date);

        return _getList(clientId, true, CriteriaUtils.andOperator(scheduleCriteria, searchCriteria), pageable, sort);
    }

    @Override
    public Set<ObjectId> getCustomerIdsBySalesmenDate(ObjectId clientId, Collection<ObjectId> salesmanIds,
            String search, SimpleDate date) {
        List<Customer> customers = getCustomersBySalesmenDate(clientId, salesmanIds, search, date, null, null);
        return _getIdSet(customers);
    }

    @Override
    public long countCustomersBySalesmenDate(ObjectId clientId, Collection<ObjectId> salesmanIds, String search,
            SimpleDate date) {
        if (salesmanIds == null) {
            throw new IllegalArgumentException("salesmanId is null");
        }

        if (salesmanIds.isEmpty()) {
            return 0l;
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria scheduleCriteria = getScheduleCriteria(clientId, salesmanIds, date);

        return _count(clientId, true, CriteriaUtils.andOperator(scheduleCriteria, searchCriteria));
    }

    // ** PRIVATE **
    private Criteria getScheduleCriteria(ObjectId clientId, Collection<ObjectId> salesmanIds, SimpleDate date) {
        ClientConfig clientConfig = clientConfigRepository.getClientConfig(PO.CLIENT_ROOT_ID);
        if (clientConfig == null) {
            throw new UnsupportedOperationException("client config id null");
        }

        int firstDayOfWeek = clientConfig.getFirstDayOfWeek();
        int minimalDaysInFirstWeek = clientConfig.getMinimalDaysInFirstWeek();

        // Tuan lam viec cua ngay hom nay tinh tu dau nam
        int weekOfToday = DateTimeUtils.getWeekOfYear(date, firstDayOfWeek, minimalDaysInFirstWeek);
        int weeekOfFrequency = clientConfig.getNumberWeekOfFrequency();
        // Thu tu cua tuan theo week duration
        int weekIndex = (weekOfToday % weeekOfFrequency) + 1;

        int dayOfToday = date.getDayOfWeek();

        Criteria scheduleCriteria = CriteriaUtils.andOperator(
                Criteria.where(Customer.COLUMNNAME_SCHEDULE_SALESMAN_ID).in(salesmanIds),
                Criteria.where(Customer.COLUMNNAME_SCHEDULE_ITEMS).elemMatch(
                        CriteriaUtils.andOperator(Criteria.where(CustomerScheduleItem.COLUMNNAME_DAYS).all(dayOfToday),
                                Criteria.where(CustomerScheduleItem.COLUMNNAME_WEEKS).all(weekIndex))));

        return scheduleCriteria;
    }

    @Override
    public boolean checkCustomerTypeUsed(ObjectId clientId, ObjectId customerTypeId) {
        if (customerTypeId == null) {
            throw new IllegalArgumentException("customerTypeId is null");
        }

        Criteria criteria = Criteria.where(Customer.COLUMNNAME_CUSTOMER_TYPE_ID).is(customerTypeId);

        return super._exists(clientId, true, criteria);
    }

    @Override
    public boolean checkDistributorUsed(ObjectId clientId, ObjectId distributorId) {
        if (distributorId == null) {
            throw new IllegalArgumentException("distributorId is null");
        }

        Criteria criteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).is(distributorId);

        return super._exists(clientId, true, criteria);
    }

}
