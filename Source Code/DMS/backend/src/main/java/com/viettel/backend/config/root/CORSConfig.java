package com.viettel.backend.config.root;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

@Configuration
public class CORSConfig {

	@Bean
	public SimpleCORSFilter corsFilter() {
		return new SimpleCORSFilter();
	}
	
	public static class SimpleCORSFilter implements Filter {
		
		@Value("#{'${cors.allowed.origins}'.split(',')}")
		private List<String> allowedOrigins;

		public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
			HttpServletResponse httpResponse = (HttpServletResponse) res;
			HttpServletRequest httpRequest = ((HttpServletRequest) req);
			String path = httpRequest.getRequestURI().substring(httpRequest.getContextPath().length());
			if (isAllowedForCorsPath(path)) {
				String origin = httpRequest.getHeader("Origin");
				if (isOriginAllowed(origin)) {
					httpResponse.setHeader("Access-Control-Allow-Origin", origin);
				}
				httpResponse.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
				httpResponse.setHeader("Access-Control-Max-Age", "3600");
				httpResponse.setHeader("Access-Control-Allow-Headers", "x-requested-with, accept, authorization, content-type");
				httpResponse.setHeader("Access-Control-Allow-Credentials", "true");
			}
			if (httpRequest.getMethod().equals("OPTIONS")) {
	            try {
	                httpResponse.getWriter().print("OK");
	                httpResponse.getWriter().flush();
	            } catch (IOException e) {
	            	;
	            }
	        } else{
	    		chain.doFilter(req, res);
	        }
		}

		public void init(FilterConfig filterConfig) {}

		public void destroy() {}
		
		private boolean isOriginAllowed(String origin) {
			if (StringUtils.isEmpty(origin)) {
				return false;
			}
			
			for (String allowedOrigin : allowedOrigins) {
				if (allowedOrigin.equalsIgnoreCase(origin)) {
					return true;
				}
			}
			return false;
		}
		
		private boolean isAllowedForCorsPath(String path) {
			if (path.startsWith("/account")) {
				// Allow cross-domain ping
				if (path.startsWith("/account/ping")) {
					return true;
				}
				return false;
			}
			return true;
		}

	}
}
