package com.viettel.backend.restful.salesman;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.CustomerSaleResultDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.OrderSummaryDto;
import com.viettel.backend.dto.SalesResultDaily;
import com.viettel.backend.dto.SalesmanDashboardDto;
import com.viettel.backend.oauth2.core.SecurityContextHelper;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SalesmanDashboardService;

@RestController
@RequestMapping(value = "/salesman/dashboard")
public class DashboardController {

    @Autowired
    private SalesmanDashboardService salesmanDashboardService;

    // GET SALESMAN DASHBOARD
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getSalesmanDashboard() {
        SalesmanDashboardDto dto = salesmanDashboardService.getSalesmanDashboard(SecurityContextHelper.getCurrentUser());
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/bycustomer", method = RequestMethod.GET)
    public ResponseEntity<?> getSalesmanDashboardByCustomer() {
        ListJson<CustomerSaleResultDto> results = salesmanDashboardService.getSalesmanDashboardByCustomer(SecurityContextHelper
                .getCurrentUser());
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/bycustomer/detail", method = RequestMethod.GET)
    public ResponseEntity<?> getSalesmanDashboardByCustomerDetail(
            @RequestParam(value = "customerId", required = true) String customerId) {
        ListJson<OrderSummaryDto> results = salesmanDashboardService.getSalesmanDashboardByCustomerDetail(
                SecurityContextHelper.getCurrentUser(), customerId);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/byday", method = RequestMethod.GET)
    public ResponseEntity<?> getSalesmanDashboardByDay() {
        ListJson<SalesResultDaily> results = salesmanDashboardService.getSalesmanDashboardByDay(SecurityContextHelper
                .getCurrentUser());
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/byday/detail", method = RequestMethod.GET)
    public ResponseEntity<?> getSalesmanDashboardByDayDetail(@RequestParam(value = "date", required = true) String date) {
        ListJson<CustomerSaleResultDto> results = salesmanDashboardService.getSalesmanDashboardByDayDetail(
                SecurityContextHelper.getCurrentUser(), date);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

}
