package com.viettel.backend.dto;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.oauth2.core.UserLogin;

public class ClientConfigDto extends DTO {

    private static final long serialVersionUID = 4618363876249230856L;

    // DEFAULT VALUE
    private String defaultProductPhoto;
    private String defaultCustomerPhoto;
    private String defaultDateFormat;

    // CALENDAR INDEX
    private int firstDayOfWeek;
    private int minimalDaysInFirstWeek;
    private int numberWeekOfFrequency;

    public ClientConfigDto() {
        super();
    }

    public ClientConfigDto(UserLogin userLogin, ClientConfig clientConfig) {
        super(clientConfig);
        
        this.setDefaultProductPhoto(clientConfig.getDefaultProductPhoto());
        this.setDefaultCustomerPhoto(clientConfig.getDefaultCustomerPhoto());
        this.setDefaultDateFormat(clientConfig.getDefaultDateFormat());
        
        this.setFirstDayOfWeek(clientConfig.getFirstDayOfWeek());
        this.setMinimalDaysInFirstWeek(clientConfig.getMinimalDaysInFirstWeek());
        this.setNumberWeekOfFrequency(clientConfig.getNumberWeekOfFrequency());
    }

    public String getDefaultProductPhoto() {
        return defaultProductPhoto;
    }

    public void setDefaultProductPhoto(String defaultProductPhoto) {
        this.defaultProductPhoto = defaultProductPhoto;
    }

    public String getDefaultCustomerPhoto() {
        return defaultCustomerPhoto;
    }

    public void setDefaultCustomerPhoto(String defaultCustomerPhoto) {
        this.defaultCustomerPhoto = defaultCustomerPhoto;
    }

    public String getDefaultDateFormat() {
        return defaultDateFormat;
    }

    public void setDefaultDateFormat(String defaultDateFormat) {
        this.defaultDateFormat = defaultDateFormat;
    }

    public int getFirstDayOfWeek() {
        return firstDayOfWeek;
    }

    public void setFirstDayOfWeek(int firstDayOfWeek) {
        this.firstDayOfWeek = firstDayOfWeek;
    }

    public int getMinimalDaysInFirstWeek() {
        return minimalDaysInFirstWeek;
    }

    public void setMinimalDaysInFirstWeek(int minimalDaysInFirstWeek) {
        this.minimalDaysInFirstWeek = minimalDaysInFirstWeek;
    }

    public int getNumberWeekOfFrequency() {
        return numberWeekOfFrequency;
    }

    public void setNumberWeekOfFrequency(int numberWeekOfFrequency) {
        this.numberWeekOfFrequency = numberWeekOfFrequency;
    }

}
