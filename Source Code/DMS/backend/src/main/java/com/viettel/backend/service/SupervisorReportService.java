package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.ExhibitionReportDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.SalesmanSalesReportMonthlyDto;
import com.viettel.backend.dto.SurveyResultDto;
import com.viettel.backend.dto.VisitReportDailyDto;
import com.viettel.backend.dto.VisitReportDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SupervisorReportService extends Serializable {

    public ListJson<VisitReportDto> getVisitReportBySalesman(UserLogin userLogin, String salesmanId, String fromDate,
            String toDate);

    /**
     * Chỉ trả về một bản ghi
     */
    public ListJson<VisitReportDto> getVisitReportByCustomer(UserLogin userLogin, String customerId, String fromDate,
            String toDate);

    /**
     * Trả về báo cáo bán hàng của nhân viên bán hàng theo tháng
     * 
     * @param salesmanId
     *            Nếu = null thì trả về báo cáo của tất cả NVBH của GSBH
     */
    public ListJson<SalesmanSalesReportMonthlyDto> getSalesmanSalesReportMonthly(UserLogin userLogin,
            String salesmanId, int month, int year);

    public VisitReportDailyDto getVisitReportDaily(UserLogin userLogin, String salesmanId);

    public SurveyResultDto getSurveyReport(UserLogin userLogin, String surveyId);

    public byte[] exportSurveyReport(UserLogin userLogin, String surveyId);

    public ExhibitionReportDto getExhibitionReport(UserLogin userLogin, String exhibitionId);

    public byte[] exportExhibitionReport(UserLogin userLogin, String exhibitionId);

}
