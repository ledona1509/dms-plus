package com.viettel.backend.service.impl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.User;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;

public abstract class AbstractSalesmanService extends AbstractService {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private CustomerRepository customerRepository;
    
    public void checkIsSalesman(UserLogin userLogin) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_SALESMAN)) {
            throw new BusinessException(ExceptionCode.SALESMAN_ONLY);
        }
    }
    
    public User getCurrentSalesman(UserLogin userLogin) {
        User salesman = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());
        if (salesman == null) {
            throw new UnsupportedOperationException("current user not found");
        }
        
        return salesman;
    }

    public Distributor checkSalesmanDistributor(UserLogin userLogin, User salesman) {
        if (salesman.getDistributor() == null || salesman.getDistributor().getId() == null) {
            throw new BusinessException(ExceptionCode.SALESMAN_NO_DISTRIBUTOR);
        }
        Distributor distributor = distributorRepository.getById(userLogin.getClientId(), salesman.getDistributor().getId());
        if (distributor == null) {
            throw new BusinessException(ExceptionCode.SALESMAN_NO_DISTRIBUTOR);
        }

        return distributor;
    }

    public Customer checkSalesmanCustomer(UserLogin userLogin, User salesman, String _customerId) {
        ObjectId customerId = ObjectIdUtils.getObjectId(_customerId, null);
        if (customerId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Customer customer = customerRepository.getById(userLogin.getClientId(), customerId);
        if (customer == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        if (customer.getSchedule() == null || customer.getSchedule().getSalesmanId() == null
                || !customer.getSchedule().getSalesmanId().equals(salesman.getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        return customer;
    }

}
