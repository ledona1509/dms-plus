package com.viettel.backend.dto;

import com.viettel.backend.domain.Survey;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.util.DateTimeUtils;

public class SurveySimpleDto extends NameCategoryDto {

    private static final long serialVersionUID = 2264346176267776793L;

    private static final int STATUS_BEFORE_PROCESS = 0;
    private static final int STATUS_PROCESSING = 1;
    private static final int STATUS_PROCESSED = 2;

    private String startDate;
    private String endDate;
    private boolean required;
    
    public SurveySimpleDto() {
        super();
    }
    
    public SurveySimpleDto(Survey survey) {
        super(survey);
        
        this.startDate = survey.getStartDate() != null ? survey.getStartDate().getIsoDate() : null;
        this.endDate = survey.getEndDate() != null ? survey.getEndDate().getIsoDate() : null;
        this.required = survey.isRequired();
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public int getDateStatus() {
        SimpleDate startDate = SimpleDate.createByIsoDate(this.startDate, null);
        SimpleDate endDate = SimpleDate.createByIsoDate(this.endDate, null);

        SimpleDate today = DateTimeUtils.getToday();

        if (startDate.compareTo(today) > 0) {
            return STATUS_BEFORE_PROCESS;
        } else {
            if (endDate.compareTo(today) < 0) {
                return STATUS_PROCESSED;
            } else {
                return STATUS_PROCESSING;
            }
        }
    }

}
