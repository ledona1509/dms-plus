package com.viettel.backend.restful.salesman;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.PromotionSimpleDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SalesmanPromotionService;

@RestController("salesmanPromotionController")
@RequestMapping(value = "salesman/promotion")
public class PromotionController extends AbstractController {

    private static final long serialVersionUID = -3681331724934086989L;
    
    @Autowired
    private SalesmanPromotionService salesmanPromotionService;

    // list
    @RequestMapping(value = "/available", method = RequestMethod.GET)
    public ResponseEntity<?> available() {
        ListJson<PromotionSimpleDto> results = salesmanPromotionService.getPromtionsAvailableToday(getUserLogin());
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

}
