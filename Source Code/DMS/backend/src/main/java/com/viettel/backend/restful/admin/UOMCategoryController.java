package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.domain.UOM;
import com.viettel.backend.dto.UOMDto;
import com.viettel.backend.restful.BasicCategoryController;
import com.viettel.backend.service.AdminUOMService;
import com.viettel.backend.service.BasicCategoryService;

@RestController
@RequestMapping(value = "/admin/uom")
public class UOMCategoryController extends BasicCategoryController<UOM, UOMDto, UOMDto, UOMDto> {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private AdminUOMService adminUOMService;

    @Override
    protected BasicCategoryService<UOM, UOMDto, UOMDto, UOMDto> getService() {
        return adminUOMService;
    }

}
