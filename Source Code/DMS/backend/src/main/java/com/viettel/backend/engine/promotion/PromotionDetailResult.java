package com.viettel.backend.engine.promotion;

import java.io.Serializable;

public class PromotionDetailResult<ID extends Serializable, P extends I_Product<ID>> implements Serializable, I_PromotionDetailResult<ID, P> {

    private static final long serialVersionUID = 7124067067799670041L;

    private ID id;
    private PromotionRewardResult<ID, P> rewardResult;

    public PromotionDetailResult() {
        super();
    }

    public PromotionDetailResult(ID id, PromotionRewardResult<ID, P> rewardResult) {
        super();
        this.id = id;
        this.rewardResult = rewardResult;
    }

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    public PromotionRewardResult<ID, P> getRewardResult() {
        return rewardResult;
    }

    public void setRewardResult(PromotionRewardResult<ID, P> rewardResult) {
        this.rewardResult = rewardResult;
    }

}
