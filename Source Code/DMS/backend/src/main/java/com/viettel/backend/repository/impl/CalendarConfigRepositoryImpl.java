package com.viettel.backend.repository.impl;

import java.util.LinkedList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.viettel.backend.cache.CacheManager;
import com.viettel.backend.cache.ListCachable;
import com.viettel.backend.domain.CalendarConfig;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.repository.CalendarConfigRepository;
import com.viettel.backend.util.DateTimeUtils;

@Repository
public class CalendarConfigRepositoryImpl extends BasicRepositoryImpl<CalendarConfig> implements
        CalendarConfigRepository {

    private static final long serialVersionUID = -8401381924226354121L;
    
    @Autowired
    private CacheManager cacheManager;

    @Override
    public CalendarConfig getCalendarConfig(ObjectId clientId) {
        return super._getFirst(clientId, true, null, null);
    }

    @Override
    public CalendarConfig save(ObjectId clientId, CalendarConfig domain) {
        cacheManager.getWorkingDaysCache().clear();
        return super._save(clientId, domain);
    }

    @Override
    public List<SimpleDate> getWorkingDays(ObjectId clientId, Period period) {
        ListCachable<SimpleDate> list = cacheManager.getWorkingDaysCache().get(period);
        if (list != null) {
            return list;
        }
        
        CalendarConfig calendarConfig = getCalendarConfig(clientId);
        if (calendarConfig == null) {
            throw new UnsupportedOperationException("calendar config not found");
        }

        List<SimpleDate> dates = new LinkedList<SimpleDate>();
        SimpleDate temp = period.getFromDate();
        while (temp.compareTo(period.getToDate()) < 0) {
            if (calendarConfig != null) {
                int day = temp.getDayOfWeek();
                int date = temp.getDate();
                int month = temp.getMonth();
                int year = temp.getYear();

                // NGAY LAM BU
                if (calendarConfig.getExceptionWorkingDays() != null) {
                    boolean added = false;
                    for (SimpleDate simpleDate : calendarConfig.getExceptionWorkingDays()) {
                        if (simpleDate.getDate() == date && simpleDate.getMonth() == month
                                && simpleDate.getYear() == year) {
                            dates.add(temp);
                            temp = DateTimeUtils.addDays(temp, 1);
                            added = true;
                            break;
                        }
                    }
                    if (added) {
                        continue;
                    }
                }

                // NGAY NGHI DAC BIET
                if (calendarConfig.getExceptionHolidays() != null) {
                    boolean added = false;
                    for (SimpleDate simpleDate : calendarConfig.getExceptionHolidays()) {
                        if (simpleDate.getDate() == date && simpleDate.getMonth() == month
                                && simpleDate.getYear() == year) {
                            temp = DateTimeUtils.addDays(temp, 1);
                            break;
                        }
                    }
                    if (added) {
                        continue;
                    }
                }

                // NGAY NGHI HANG NAM
                if (calendarConfig.getEveryYearHolidays() != null) {
                    boolean added = false;
                    for (SimpleDate simpleDate : calendarConfig.getEveryYearHolidays()) {
                        if (simpleDate.getDate() == date && simpleDate.getMonth() == month) {
                            temp = DateTimeUtils.addDays(temp, 1);
                            break;
                        }
                    }
                    if (added) {
                        continue;
                    }
                }

                // BO WEEKEND
                if (calendarConfig.getWorkingDays() != null && calendarConfig.getWorkingDays().contains(day)) {
                    dates.add(temp);
                    temp = DateTimeUtils.addDays(temp, 1);
                    continue;
                }

            } else {
                dates.add(temp);
            }

            temp = DateTimeUtils.addDays(temp, 1);
        }

        list = new ListCachable<SimpleDate>(dates);
        cacheManager.getWorkingDaysCache().put(period, list);
        
        return list;
    }

}