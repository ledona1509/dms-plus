package com.viettel.backend.restful.salesman;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.ProductForOrderDto;
import com.viettel.backend.dto.ProductDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SalesmanProductService;

@RestController("salesmanProductController")
@RequestMapping(value = "salesman/product")
public class ProductController extends AbstractController {

    private static final long serialVersionUID = 8172833297326015220L;
    
    @Autowired
    private SalesmanProductService salesmanProductService;

    // LIST PRODUCT
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> list(@RequestParam(value = "q", required = false, defaultValue = "") String search,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        ListJson<ProductDto> results = salesmanProductService.getProducts(getUserLogin(), search,
                getPageRequest(page, size));
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    // PRODUCT FOR ORDER
    @RequestMapping(value = "/fororder", method = RequestMethod.GET)
    public ResponseEntity<?> getProductsForOrder(@RequestParam(value = "customerId", required = true) String customerId) {
        ListJson<ProductForOrderDto> results = salesmanProductService.getProductsForOrder(getUserLogin(), customerId);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

}
