package com.viettel.backend.config.root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;

import com.viettel.backend.websocket.core.OAuth2TokenWebSocketAuthenticationHandler;
import com.viettel.backend.websocket.core.Websocket;
import com.viettel.backend.websocket.core.StompLoginSubProtocolWebSocketHandlerDecoratorFactory;
import com.viettel.backend.websocket.core.WebSocketAuthenticationHandler;

@Configuration
@EnableWebSocketMessageBroker
@EnableScheduling
@ComponentScan(basePackages = { "com.viettel.backend.message.controller" })
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    @Autowired
    TokenStore tokenStore;

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
    	config.enableSimpleBroker("/topic", "/user", "/public");
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/websocket").withSockJS();
    }
    
    @Override
    public void configureWebSocketTransport(WebSocketTransportRegistration registration) {
        registration.addDecoratorFactory(loginSubProtocolWebSocketHandlerDecoratorFactory());
    }
    
    @Bean
    public StompLoginSubProtocolWebSocketHandlerDecoratorFactory loginSubProtocolWebSocketHandlerDecoratorFactory() {
        return new StompLoginSubProtocolWebSocketHandlerDecoratorFactory(webSocketAuthenticationProvider());
    }
    
    @Bean
    public WebSocketAuthenticationHandler webSocketAuthenticationProvider() {
        return new OAuth2TokenWebSocketAuthenticationHandler(tokenStore);
    }
    
    @Bean
    public Websocket websocket() {
        return new Websocket();
    }

}
