package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.embed.FeedbackDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SalesmanFeedbackService extends Serializable {

    /** feedbacks of 5 lastest visit **/
    public ListJson<FeedbackDto> getLastCustomerFeedbacks(UserLogin userLogin, String customerId);

}
