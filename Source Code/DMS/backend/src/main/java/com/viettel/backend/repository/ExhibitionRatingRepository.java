package com.viettel.backend.repository;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.embed.ExhibitionRating;

public interface ExhibitionRatingRepository extends Serializable {

    public List<ExhibitionRating> getExhibitionRatingsByCustomers(ObjectId clientId, ObjectId exhibitionId,
            Collection<ObjectId> customerIds);
    
    public List<ExhibitionRating> getExhibitionRatings(ObjectId clientId, ObjectId exhibitionId);
    
}
