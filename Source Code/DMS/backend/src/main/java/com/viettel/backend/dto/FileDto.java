package com.viettel.backend.dto;

import java.io.Serializable;

import com.viettel.backend.domain.embed.FileEmbed;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.engine.FileEngine;

public class FileDto implements Serializable {

    private static final long serialVersionUID = -2675338244024212422L;

    private String id;
    private String name;
    private String link;

    public FileDto() {
        super();
    }

    public FileDto(UserLogin userLogin, String fileId, FileEngine fileEngine) {
        super();
        if (fileId != null) {
            this.id = fileId;

            generateLink(userLogin, fileEngine);
        }
    }

    public FileDto(UserLogin userLogin, FileEmbed fileEmbed, FileEngine fileEngine) {
        super();
        if (fileEmbed != null && fileEmbed.getId() != null) {
            this.id = fileEmbed.getId();
            this.name = fileEmbed.getName();

            generateLink(userLogin, fileEngine);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    private void generateLink(UserLogin userLogin, FileEngine fileEngine) {
        String url = fileEngine.getLink(getId());
        this.setLink(url);
    }

}
