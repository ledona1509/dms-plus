package com.viettel.backend.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.cache.I_Cachable;

@Document(collection = "ClientConfig")
public class ClientConfig extends PO implements I_Cachable<ClientConfig> {

    private static final long serialVersionUID = -4995404568124100927L;

    // DEFAULT VALUE
    private String defaultProductPhoto;
    private String defaultCustomerPhoto;
    private String defaultDateFormat;

    // CALENDAR INDEX
    private int firstDayOfWeek;
    private int minimalDaysInFirstWeek;
    private int numberWeekOfFrequency;

    public String getDefaultProductPhoto() {
        return defaultProductPhoto;
    }

    public void setDefaultProductPhoto(String defaultProductPhoto) {
        this.defaultProductPhoto = defaultProductPhoto;
    }

    public String getDefaultCustomerPhoto() {
        return defaultCustomerPhoto;
    }

    public void setDefaultCustomerPhoto(String defaultCustomerPhoto) {
        this.defaultCustomerPhoto = defaultCustomerPhoto;
    }

    public String getDefaultDateFormat() {
        return defaultDateFormat;
    }

    public void setDefaultDateFormat(String defaultDateFormat) {
        this.defaultDateFormat = defaultDateFormat;
    }

    public int getFirstDayOfWeek() {
        return firstDayOfWeek;
    }

    public void setFirstDayOfWeek(int firstDayOfWeek) {
        this.firstDayOfWeek = firstDayOfWeek;
    }

    public int getMinimalDaysInFirstWeek() {
        return minimalDaysInFirstWeek;
    }

    public void setMinimalDaysInFirstWeek(int minimalDaysInFirstWeek) {
        this.minimalDaysInFirstWeek = minimalDaysInFirstWeek;
    }

    public int getNumberWeekOfFrequency() {
        return numberWeekOfFrequency;
    }

    public void setNumberWeekOfFrequency(int numberWeekOfFrequency) {
        this.numberWeekOfFrequency = numberWeekOfFrequency;
    }
    
    @Override
    public ClientConfig cloneForCache() {
        ClientConfig clientConfig = new ClientConfig();
        PO.copy(this, clientConfig);
        
        clientConfig.setDefaultProductPhoto(getDefaultProductPhoto());
        clientConfig.setDefaultCustomerPhoto(getDefaultCustomerPhoto());
        clientConfig.setDefaultDateFormat(getDefaultDateFormat());
        
        clientConfig.setFirstDayOfWeek(getFirstDayOfWeek());
        clientConfig.setMinimalDaysInFirstWeek(getMinimalDaysInFirstWeek());
        clientConfig.setNumberWeekOfFrequency(getNumberWeekOfFrequency());
        
        return clientConfig;
    }

}
