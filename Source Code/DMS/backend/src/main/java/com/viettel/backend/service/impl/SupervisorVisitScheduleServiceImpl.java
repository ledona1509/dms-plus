package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.CustomerSchedule;
import com.viettel.backend.domain.embed.CustomerScheduleItem;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.embed.CustomerScheduleDto;
import com.viettel.backend.dto.embed.CustomerScheduleItemDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SupervisorVisitScheduleService;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class SupervisorVisitScheduleServiceImpl extends AbstractSupervisorService implements
        SupervisorVisitScheduleService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private DistributorRepository distributorRepository;
    
    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Override
    public ListJson<CustomerScheduleDto> getCustomerSchedulesByDistributor(UserLogin userLogin, String customerSearch,
            String distributorId, String salesmanId, boolean all, Pageable pageable) {
        checkIsSupervisor(userLogin);

        Distributor distributor = checkSupervisorDistributor(userLogin, distributorId);

        Collection<Customer> customers = null;
        long count;

        if (all) {
            // CUSTOMER SCHEDULED OF ALL SALESMAN
            customers = customerRepository.getCustomersByDistributor(userLogin.getClientId(), distributor.getId(),
                    false, customerSearch, pageable, null);
            count = customerRepository.countCustomersByDistributor(userLogin.getClientId(), distributor.getId(),
                    false, customerSearch);
        } else {
            if (salesmanId == null) {
                // CUSTOMER UNSCHEDULED
                customers = customerRepository.getCustomersByDistributorUnscheduled(userLogin.getClientId(),
                        distributor.getId(), customerSearch, pageable, null);
                count = customerRepository.countCustomersByDistributorUnscheduled(userLogin.getClientId(),
                        distributor.getId(), customerSearch);
            }
            // CUSTOMER SCHEDULED OF ONE SALESMAN
            else {
                User saleman = checkSupervisorSalesman(userLogin, salesmanId, distributor.getId());

                customers = customerRepository.getCustomersBySalesmen(userLogin.getClientId(),
                        Arrays.asList(saleman.getId()), customerSearch, pageable, null);
                count = customerRepository.countCustomersBySalesmen(userLogin.getClientId(),
                        Arrays.asList(saleman.getId()), customerSearch);
            }
        }

        if (customers == null || customers.isEmpty()) {
            return ListJson.<CustomerScheduleDto> emptyList();
        }

        List<CustomerScheduleDto> customerScheduleDtos = new ArrayList<CustomerScheduleDto>(customers.size());
        for (Customer customer : customers) {
            CustomerScheduleDto customerScheduleDto = new CustomerScheduleDto();
            if (customer.getSchedule() != null) {
                customerScheduleDto.setCustomerId(customer.getId().toString());
                customerScheduleDto.setSalesmanId(customer.getSchedule().getSalesmanId().toString());

                if (customer.getSchedule().getItems() != null) {
                    for (CustomerScheduleItem item : customer.getSchedule().getItems()) {
                        CustomerScheduleItemDto itemDto = new CustomerScheduleItemDto();
                        itemDto.setDays(item.getDays());
                        itemDto.setWeeks(item.getWeeks());

                        customerScheduleDto.addItem(itemDto);
                    }
                }

            } else {
                customerScheduleDto.setCustomerId(customer.getId().toString());

                CustomerScheduleItemDto customerScheduleItemDto = new CustomerScheduleItemDto();
                customerScheduleItemDto.setDays(Collections.<Integer> emptyList());
                customerScheduleItemDto.setWeeks(getDefaultWeeks(userLogin));

                customerScheduleDto.addItem(customerScheduleItemDto);
            }

            customerScheduleDto.setName(customer.getName());
            customerScheduleDto.setCode(customer.getCode());
            customerScheduleDto.setAddress(customer.getAddress());

            customerScheduleDtos.add(customerScheduleDto);
        }

        return new ListJson<CustomerScheduleDto>(customerScheduleDtos, count);

    }

    @Override
    public void saveByDistributor(UserLogin userLogin, String distributorId, List<CustomerScheduleDto> dtos) {
        if (dtos == null || dtos.isEmpty()) {
            return;
        }

        checkIsSupervisor(userLogin);

        Distributor distributor = checkSupervisorDistributor(userLogin, distributorId);

        Set<ObjectId> salesmanOfDistributorIds = userRepository.getSalesmanIdsByDistributors(userLogin.getClientId(),
                Arrays.asList(distributor.getId()));

        Set<ObjectId> customerOfDistributorIds = customerRepository.getCustomerIdsByDistributor(
                userLogin.getClientId(), distributor.getId(), false, null);

        Map<ObjectId, CustomerSchedule> customerSchedules = new HashMap<ObjectId, CustomerSchedule>();
        List<ObjectId> customerHaveScheduleIds = new LinkedList<ObjectId>();
        List<ObjectId> customerNotHaveScheduleIds = new LinkedList<ObjectId>();

        for (CustomerScheduleDto dto : dtos) {
            ObjectId customerId = ObjectIdUtils.getObjectId(dto.getCustomerId(), null);
            if (customerId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            if (!customerOfDistributorIds.contains(customerId)) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            CustomerSchedule customerSchedule = new CustomerSchedule();
            ObjectId salesmanId = ObjectIdUtils.getObjectId(dto.getSalesmanId(), null);
            if (salesmanId != null) {
                if (!salesmanOfDistributorIds.contains(salesmanId)) {
                    throw new BusinessException(ExceptionCode.INVALID_PARAM);
                }

                customerSchedule.setSalesmanId(salesmanId);

                List<CustomerScheduleItemDto> itemDtos = dto.getItems();
                List<CustomerScheduleItem> items = new ArrayList<CustomerScheduleItem>(itemDtos.size());
                if (itemDtos != null && itemDtos.size() > 0) {
                    for (CustomerScheduleItemDto itemDto : itemDtos) {
                        if (itemDto.getDays() != null && !itemDto.getDays().isEmpty() && itemDto.getWeeks() != null
                                && !itemDto.getWeeks().isEmpty()) {
                            CustomerScheduleItem item = new CustomerScheduleItem();

                            item.setDays(itemDto.getDays());
                            item.setWeeks(itemDto.getWeeks());

                            items.add(item);
                        }
                    }
                }

                if (!items.isEmpty()) {
                    customerSchedule.setItems(items);
                    customerSchedules.put(customerId, customerSchedule);
                }

                customerHaveScheduleIds.add(customerId);
            } else {
                customerNotHaveScheduleIds.add(customerId);
            }
        }

        // XOA TUYEN CHO NHUNG KHACH HANG KO CON TUYEN
        customerRepository.clearCustomerSchedule(userLogin.getClientId(), customerNotHaveScheduleIds);

        // UPDATE LAI TUYEN CHO CAC KHACH HANG CO DOI TUYEN
        for (ObjectId customerId : customerHaveScheduleIds) {
            customerRepository.updateCustomerSchedule(userLogin.getClientId(), customerId,
                    customerSchedules.get(customerId));
        }

    }

    @Override
    public void saveByCustomer(UserLogin userLogin, String customerId, CustomerScheduleDto dto) {
        // Validate schudle
        if (dto == null || dto.getItems() == null || dto.getItems().size() == 0) {
            throw new BusinessException(ExceptionCode.INVALID_SCHEDULE_PARAM);
        }

        for (CustomerScheduleItemDto itemDto : dto.getItems()) {
            if (itemDto.getDays() == null || itemDto.getDays().isEmpty() || itemDto.getWeeks() == null
                    || itemDto.getWeeks().isEmpty()) {
                throw new BusinessException(ExceptionCode.INVALID_SCHEDULE_PARAM);
            }
        }

        // Skip customerId in DTO
        if (customerId == null || dto.getSalesmanId() == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        checkIsSupervisor(userLogin);
        Customer customer = checkSupervisorCustomer(userLogin, customerId);

        Collection<ObjectId> salesmanOfDistributorIds = userRepository.getSalesmanIdsByDistributors(
                userLogin.getClientId(), Arrays.asList(customer.getDistributor().getId()));

        ObjectId salesmanId = ObjectIdUtils.getObjectId(dto.getSalesmanId(), null);
        if (salesmanId == null || !salesmanOfDistributorIds.contains(salesmanId)) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        // Create schedule
        CustomerSchedule customerSchedule = new CustomerSchedule();
        customerSchedule.setSalesmanId(salesmanId);

        List<CustomerScheduleItemDto> itemDtos = dto.getItems();
        List<CustomerScheduleItem> items = new ArrayList<CustomerScheduleItem>(itemDtos.size());
        for (CustomerScheduleItemDto itemDto : itemDtos) {
            CustomerScheduleItem item = new CustomerScheduleItem();

            item.setDays(itemDto.getDays());
            item.setWeeks(itemDto.getWeeks());

            items.add(item);
        }
        customerSchedule.setItems(items);

        // Save to database
        customerRepository.updateCustomerSchedule(userLogin.getClientId(), customer.getId(), customerSchedule);
    }

    private List<Integer> getDefaultWeeks(UserLogin userLogin) {
        ClientConfig clientConfig = getClientCondig(userLogin);
        List<Integer> weeks = new ArrayList<Integer>(clientConfig.getNumberWeekOfFrequency());
        for (int i = 0; i < clientConfig.getNumberWeekOfFrequency(); i++) {
            weeks.add(i + 1);
        }

        return weeks;
    }

}
