package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.Survey;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.repository.SurveyAnswerRepository;
import com.viettel.backend.repository.SurveyRepository;
import com.viettel.backend.util.CriteriaUtils;
import com.viettel.backend.util.DateTimeUtils;

@Repository
public class SurveyRepositoryImpl extends BasicCategoryRepositoryImpl<Survey> implements SurveyRepository {

    private static final long serialVersionUID = -9182004570746524625L;

    @Autowired
    private SurveyAnswerRepository surveyAnswerRepository;

    @Override
    public List<Survey> getSurveys(ObjectId clientId, String search, Pageable pageable, Sort sort) {
        return getListWithDraft(clientId, search, false, pageable, sort);
    }

    @Override
    public long count(ObjectId clientId, String search) {
        return count(clientId, search);
    }

    @Override
    public List<Survey> getSurveyAvailableByCustomer(ObjectId clientId, ObjectId customerId) {
        SimpleDate today = DateTimeUtils.getToday();

        Criteria dateCriteria = CriteriaUtils.andOperator(
                Criteria.where(Survey.COLUMNNAME_START_DATE_VALUE).lte(today.getValue()),
                Criteria.where(Survey.COLUMNNAME_END_DATE_VALUE).gte(today.getValue()));

        Sort sort = new Sort(Direction.DESC, Survey.COLUMNNAME_START_DATE_VALUE);

        List<Survey> surveys = super._getList(clientId, true, dateCriteria, null, sort);

        if (surveys == null || surveys.isEmpty()) {
            return Collections.<Survey> emptyList();
        }

        HashSet<ObjectId> surveyIds = new HashSet<ObjectId>();
        for (Survey survey : surveys) {
            surveyIds.add(survey.getId());
        }

        // LAY SURVEY DA DUOC THUC HIEN
        Collection<ObjectId> surveyDoneIds = surveyAnswerRepository.getSurveyIdsDoneByCustomer(clientId, customerId,
                surveyIds);

        if (surveyDoneIds == null || surveyDoneIds.isEmpty()) {
            return surveys;
        }

        List<Survey> surveyAvailables = new LinkedList<Survey>();
        for (Survey survey : surveys) {
            if (!surveyDoneIds.contains(survey.getId())) {
                surveyAvailables.add(survey);
            }
        }

        return surveyAvailables;
    }

}
