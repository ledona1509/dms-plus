package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.domain.Product;
import com.viettel.backend.dto.ProductCreateDto;
import com.viettel.backend.dto.ProductDto;
import com.viettel.backend.restful.BasicCategoryController;
import com.viettel.backend.service.AdminProductService;
import com.viettel.backend.service.BasicCategoryService;

@RestController("adminProductController")
@RequestMapping(value = "/admin/product")
public class ProductController extends
        BasicCategoryController<Product, ProductDto, ProductDto, ProductCreateDto> {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private AdminProductService adminProductService;

    @Override
    protected BasicCategoryService<Product, ProductDto, ProductDto, ProductCreateDto> getService() {
        return adminProductService;
    }

}
