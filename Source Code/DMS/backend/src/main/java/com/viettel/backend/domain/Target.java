package com.viettel.backend.domain;

import java.math.BigDecimal;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.domain.embed.UserEmbed;

@Document(collection = "Target")
public class Target extends PO {

    private static final long serialVersionUID = -2535716202731672260L;

    public static final String COLUMNNAME_SALESMAN_ID = "salesman.id";
    public static final String COLUMNNAME_MONTH = "month";
    public static final String COLUMNNAME_YEAR = "year";

    private UserEmbed salesman;
    private int month;
    private int year;
    private BigDecimal revenue;
    private BigDecimal output;

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    public BigDecimal getOutput() {
        return output;
    }

    public void setOutput(BigDecimal output) {
        this.output = output;
    }

    public UserEmbed getSalesman() {
        return salesman;
    }

    public void setSalesman(UserEmbed salesman) {
        this.salesman = salesman;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

}
