package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.viettel.backend.domain.Order;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.util.CriteriaUtils;

@Repository
public class OrderRepositoryImpl extends BasicRepositoryImpl<Order> implements OrderRepository {

    private static final long serialVersionUID = -8401381924226354121L;

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    protected Criteria getDefaultCriteria() {
        Criteria isOrderCriteria = Criteria.where(Order.COLUMNNAME_IS_ORDER).is(true);
        Criteria approveStatusCriteria = Criteria.where(Order.COLUMNNAME_APPROVE_STATUS).is(
                Order.APPROVE_STATUS_APPROVED);
        return CriteriaUtils.andOperator(isOrderCriteria, approveStatusCriteria);
    }

    @Override
    public Order getOrderBySalesmanAndId(ObjectId clientId, ObjectId salesmanId, ObjectId orderId) {
        Assert.notNull(salesmanId);
        Assert.notNull(orderId);

        Criteria salesmanCriteria = Criteria.where(Order.COLUMNNAME_SALESMAN_ID).is(salesmanId);

        Criteria orderCriteria = Criteria.where(Order.COLUMNNAME_ID).is(orderId);

        Criteria criteria = CriteriaUtils.andOperator(salesmanCriteria, orderCriteria);

        return super._getFirst(clientId, true, criteria, null);
    }

    @Override
    public List<Order> getOrderBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Period period,
            String searchCode, Pageable pageable, Sort sort) {
        if (salesmanIds == null || salesmanIds.isEmpty()) {
            return Collections.<Order> emptyList();
        }
        
        if (period == null) {
            throw new IllegalArgumentException("period is null");
        }

        Criteria salesmanCriteria = Criteria.where(Order.COLUMNNAME_SALESMAN_ID).in(salesmanIds);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils.andOperator(dateCriteria, salesmanCriteria, searchCriteria);

        return super._getList(clientId, true, criteria, pageable, sort);
    }

    @Override
    public long countOrderBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Period period,
            String searchCode) {
        if (salesmanIds == null || salesmanIds.isEmpty()) {
            return 0l;
        }
        
        if (period == null) {
            throw new IllegalArgumentException("period is null");
        }

        Criteria salesmanCriteria = Criteria.where(Order.COLUMNNAME_SALESMAN_ID).in(salesmanIds);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils.andOperator(dateCriteria, salesmanCriteria, searchCriteria);

        return super._count(clientId, true, criteria);
    }

    @Override
    public List<Order> getOrderBySalesmanAndCustomers(ObjectId clientId, ObjectId salesmanId,
            Collection<ObjectId> customerIds, Period period, String searchCode, Pageable pageable, Sort sort) {
        Assert.notNull(salesmanId);
        if (customerIds.isEmpty()) {
            return Collections.<Order> emptyList();
        }

        if (period == null) {
            throw new IllegalArgumentException("period is null");
        }

        Criteria salesmanCriteria = Criteria.where(Order.COLUMNNAME_SALESMAN_ID).is(salesmanId);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria searchCriteria = null;
        if (!StringUtils.isEmpty(searchCode)) {
            searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);
        }

        Criteria customerCriteria = null;
        if (customerIds != null) {
            customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).in(customerIds);
        }

        Criteria criteria = CriteriaUtils.andOperator(dateCriteria, salesmanCriteria, customerCriteria, searchCriteria);

        return super._getList(clientId, true, criteria, pageable, sort);
    }

    @Override
    public long countOrderBySalesmanAndCustomers(ObjectId clientId, ObjectId salesmanId,
            Collection<ObjectId> customerIds, Period period, String searchCode) {
        Assert.notNull(salesmanId);
        if (customerIds.isEmpty()) {
            return 0l;
        }

        if (period == null) {
            throw new IllegalArgumentException("period is null");
        }

        Criteria salesmanCriteria = Criteria.where(Order.COLUMNNAME_SALESMAN_ID).is(salesmanId);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria searchCriteria = null;
        if (!StringUtils.isEmpty(searchCode)) {
            searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);
        }

        Criteria customerCriteria = null;
        if (customerIds != null) {
            customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).in(customerIds);
        }

        Criteria criteria = CriteriaUtils.andOperator(dateCriteria, salesmanCriteria, customerCriteria, searchCriteria);

        return super._count(clientId, true, criteria);
    }

    @Override
    public List<Order> getOrderByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, Period period,
            String searchCode, Pageable pageable, Sort sort) {
        Assert.notNull(distributorIds);
        if (distributorIds.isEmpty()) {
            return Collections.<Order> emptyList();
        }

        if (period == null) {
            throw new IllegalArgumentException("period is null");
        }

        Criteria distributorCriteria = null;
        if (distributorIds != null) {
            distributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);
        }

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria searchCriteria = null;
        if (!StringUtils.isEmpty(searchCode)) {
            searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);
        }

        Criteria criteria = CriteriaUtils.andOperator(dateCriteria, distributorCriteria, searchCriteria);

        return super._getList(clientId, true, criteria, pageable, sort);
    }

    @Override
    public long countOrderByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, Period period,
            String searchCode) {
        Assert.notNull(distributorIds);
        if (distributorIds.isEmpty()) {
            return 0l;
        }

        if (period == null) {
            throw new IllegalArgumentException("period is null");
        }

        Criteria distributorCriteria = null;
        if (distributorIds != null) {
            distributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);
        }

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria searchCriteria = null;
        if (!StringUtils.isEmpty(searchCode)) {
            searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);
        }

        Criteria criteria = CriteriaUtils.andOperator(dateCriteria, distributorCriteria, searchCriteria);

        return super._count(clientId, true, criteria);
    }

    @Override
    public List<Order> getOrderByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Period period,
            String searchCode, Pageable pageable, Sort sort) {
        if (customerIds == null || customerIds.isEmpty()) {
            return Collections.<Order> emptyList();
        }

        if (period == null) {
            throw new IllegalArgumentException("period is null");
        }

        Criteria customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).in(customerIds);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils.andOperator(dateCriteria, customerCriteria, searchCriteria);

        return super._getList(clientId, true, criteria, pageable, sort);
    }

    @Override
    public long countOrderByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Period period,
            String searchCode) {
        if (customerIds == null || customerIds.isEmpty()) {
            return 0l;
        }

        if (period == null) {
            throw new IllegalArgumentException("period is null");
        }

        Criteria customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).in(customerIds);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils.andOperator(dateCriteria, customerCriteria, searchCriteria);

        return super._count(clientId, true, criteria);
    }

    @Override
    public List<Order> getLastOrderByCustomer(ObjectId clientId, ObjectId customerId, int size) {
        if (customerId == null) {
            return Collections.<Order> emptyList();
        }

        Criteria customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).is(customerId);

        PageRequest pageable = new PageRequest(0, size, Direction.DESC, Order.COLUMNNAME_APPROVE_TIME_VALUE);

        return super._getList(clientId, true, customerCriteria, pageable, null);
    }

    @Override
    public List<Order> getOrders(ObjectId clientId, Period period, ObjectId distributorId, ObjectId customerId,
            ObjectId salesmanId) {
        if (period == null) {
            throw new IllegalArgumentException("period is null");
        }

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria distributorCriteria = null;
        if (distributorId != null) {
            distributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).is(distributorId);
        }

        Criteria customerCriteria = null;
        if (customerId != null) {
            customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).is(customerId);
        }

        Criteria salesmanCriteria = null;
        if (salesmanId != null) {
            salesmanCriteria = Criteria.where(Order.COLUMNNAME_SALESMAN_ID).is(salesmanId);
        }

        Criteria criteria = CriteriaUtils.andOperator(dateCriteria, distributorCriteria, customerCriteria,
                salesmanCriteria);

        return _getList(clientId, true, criteria, null, null);
    }

}
