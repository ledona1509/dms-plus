package com.viettel.backend.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.Target;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.dto.CustomerSaleResultDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.OrderSummaryDto;
import com.viettel.backend.dto.SalesResultDaily;
import com.viettel.backend.dto.SalesmanDashboardDto;
import com.viettel.backend.dto.embed.Result;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CalendarConfigRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.repository.TargetRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SalesmanDashboardService;
import com.viettel.backend.util.DateTimeUtils;

@Service
public class SalesmanDashboardServiceImpl extends AbstractSalesmanService implements SalesmanDashboardService {

    private static final long serialVersionUID = -9137978270275195070L;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CalendarConfigRepository calendarConfigRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private TargetRepository targetRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Override
    public SalesmanDashboardDto getSalesmanDashboard(UserLogin userLogin) {
        checkIsSalesman(userLogin);

        // GET TARGET
        SimpleDate today = DateTimeUtils.getToday();
        Target target = targetRepository.getTargetBySalesman(userLogin.getClientId(), userLogin.getUserId(),
                today.getMonth(), today.getYear());

        BigDecimal revenue = BigDecimal.ZERO;
        BigDecimal output = BigDecimal.ZERO;

        // ORDER
        List<Order> orders = orderRepository.getOrderBySalesmen(userLogin.getClientId(),
                Arrays.asList(userLogin.getUserId()), DateTimeUtils.getPeriodThisMonth(), null, null, null);

        if (orders != null && !orders.isEmpty()) {
            for (Order order : orders) {
                if (order.getGrandTotal() != null) {
                    revenue = revenue.add(order.getGrandTotal());
                }

                if (order.getDetails() != null && !order.getDetails().isEmpty()) {
                    for (OrderDetail detail : order.getDetails()) {
                        output = output.add(detail.getOutput());
                    }
                }
            }
        }

        SalesmanDashboardDto dashboardDto = new SalesmanDashboardDto();
        if (target != null) {
            dashboardDto.setRevenue(new Result(target.getRevenue(), revenue));
            dashboardDto.setOutput(new Result(target.getOutput(), output));
        } else {
            dashboardDto.setRevenue(new Result(null, revenue));
            dashboardDto.setOutput(new Result(null, output));
        }

        // GET WORKING DAY OF THIS MONTH
        List<SimpleDate> workingDays = calendarConfigRepository.getWorkingDays(userLogin.getClientId(),
                DateTimeUtils.getPeriodByMonth(today.getMonth(), today.getYear()));
        if (workingDays == null || workingDays.isEmpty()) {
            dashboardDto.setNbSaleDays(new Result(0, 0));
        } else {
            int todayIndexOfMonth = 0;
            for (SimpleDate date : workingDays) {
                if (date.compareTo(today) > 0) {
                    break;
                }
                todayIndexOfMonth++;
            }

            dashboardDto.setNbSaleDays(new Result(workingDays.size(), todayIndexOfMonth));
        }

        return dashboardDto;
    }

    @Override
    public ListJson<CustomerSaleResultDto> getSalesmanDashboardByCustomer(UserLogin userLogin) {
        checkIsSalesman(userLogin);

        Set<ObjectId> customerIds = new HashSet<ObjectId>();
        Map<ObjectId, BigDecimal[]> customerMaps = new HashMap<ObjectId, BigDecimal[]>();

        // Kiem tra tat ca cac don hang da phat sinh do salesman dat duoc trong
        // thang
        List<Order> orders = orderRepository.getOrderBySalesmen(userLogin.getClientId(),
                Arrays.asList(userLogin.getUserId()), DateTimeUtils.getPeriodThisMonth(), null, null, null);
        if (orders != null && !orders.isEmpty()) {
            for (Order order : orders) {
                if (order.getCustomer() == null || order.getCustomer().getId() == null) {
                    continue;
                }

                List<OrderDetail> details = order.getDetails();
                if (details == null || details.isEmpty()) {
                    continue;
                }

                ObjectId customerId = order.getCustomer().getId();
                BigDecimal[] values = customerMaps.get(customerId);
                if (values == null) {
                    customerIds.add(customerId);

                    // san luong / doanh thu
                    values = new BigDecimal[] { BigDecimal.ZERO, BigDecimal.ZERO };
                }

                for (OrderDetail detail : details) {
                    values[0] = values[0].add(detail.getOutput());
                }

                if (order.getGrandTotal() != null && order.getGrandTotal().signum() > 0) {
                    values[1] = values[1].add(order.getGrandTotal());
                }

                customerMaps.put(customerId, values);
            }
        }

        // Tat ca danh sach khach hang do salesman dang quan ly
        Set<ObjectId> allCustomerIds = customerRepository.getCustomerIdsBySalesmen(userLogin.getClientId(),
                Arrays.asList(userLogin.getUserId()), null);
        if (allCustomerIds == null || allCustomerIds.isEmpty()) {
            if (customerIds.isEmpty()) {
                return ListJson.<CustomerSaleResultDto> emptyList();
            }
        } else {
            // Bo sung vao danh sach khach hang da phat sinh don hang
            for (ObjectId customerId : allCustomerIds) {
                customerIds.add(customerId);
            }
        }

        // Danh sach khach hang do salesman quan ly va khong quan ly nhung co
        // phat sinh don hang trong thang
        List<Customer> customers = customerRepository.getListByIds(userLogin.getClientId(), customerIds);
        if (customers == null || customers.isEmpty()) {
            return ListJson.<CustomerSaleResultDto> emptyList();
        }

        List<CustomerSaleResultDto> results = new ArrayList<CustomerSaleResultDto>();
        for (Customer customer : customers) {
            CustomerSaleResultDto dto = new CustomerSaleResultDto(customer, BigDecimal.ZERO, BigDecimal.ZERO);

            BigDecimal[] values = customerMaps.get(customer.getId());
            if (values != null) {
                dto.setProductivity(values[0]);
                dto.setRevenue(values[1]);
            }

            results.add(dto);
        }

        return new ListJson<CustomerSaleResultDto>(results, Long.valueOf(results.size()));
    }

    @Override
    public ListJson<OrderSummaryDto> getSalesmanDashboardByCustomerDetail(UserLogin userLogin, String customerId) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, customerId);

        List<Order> orders = orderRepository.getOrderBySalesmanAndCustomers(userLogin.getClientId(),
                userLogin.getUserId(), Arrays.asList(customer.getId()), DateTimeUtils.getPeriodThisMonth(), null, null,
                null);
        if (orders == null || orders.isEmpty()) {
            return ListJson.<OrderSummaryDto> emptyList();
        }

        List<OrderSummaryDto> results = new ArrayList<OrderSummaryDto>();
        for (Order order : orders) {
            List<OrderDetail> details = order.getDetails();
            if (details == null || details.isEmpty()) {
                continue;
            }

            BigDecimal productivity = BigDecimal.ZERO;
            for (OrderDetail detail : details) {
                productivity = productivity.add(detail.getOutput());
            }

            OrderSummaryDto dto = new OrderSummaryDto();
            dto.setId(order.getId().toString());
            dto.setCode(order.getCode());
            dto.setCreatedTime(order.getCreatedTime().getIsoTime());
            dto.setGrandTotal(order.getGrandTotal());
            dto.setProductivity(productivity);

            results.add(dto);
        }

        return new ListJson<OrderSummaryDto>(results, Long.valueOf(results.size()));
    }

    @Override
    public ListJson<SalesResultDaily> getSalesmanDashboardByDay(UserLogin userLogin) {
        checkIsSalesman(userLogin);

        List<SimpleDate> workingDays = calendarConfigRepository.getWorkingDays(userLogin.getClientId(),
                DateTimeUtils.getPeriodThisMonth());
        if (workingDays == null || workingDays.isEmpty()) {
            return ListJson.<SalesResultDaily> emptyList();
        }

        Map<Long, BigDecimal[]> orderDateMaps = new HashMap<Long, BigDecimal[]>();
        List<Order> orders = orderRepository.getOrderBySalesmen(userLogin.getClientId(),
                Arrays.asList(userLogin.getUserId()), DateTimeUtils.getPeriodThisMonth(), null, null, null);
        if (orders != null && !orders.isEmpty()) {
            for (Order order : orders) {
                List<OrderDetail> details = order.getDetails();
                if (details == null || details.isEmpty()) {
                    continue;
                }

                Long createdDateValue = SimpleDate.truncDate(order.getCreatedTime()).getValue();
                BigDecimal[] values = orderDateMaps.get(createdDateValue);
                if (values == null) {
                    values = new BigDecimal[] { BigDecimal.ZERO, BigDecimal.ZERO };
                }

                for (OrderDetail detail : details) {
                    values[0] = values[0].add(detail.getOutput());
                }

                values[1] = values[1].add(order.getGrandTotal());
                orderDateMaps.put(createdDateValue, values);
            }
        }

        long tomorrowValue = DateTimeUtils.getTomorrow().getValue();
        List<SalesResultDaily> results = new ArrayList<SalesResultDaily>();
        for (SimpleDate workingDay : workingDays) {
            BigDecimal[] values = orderDateMaps.get(workingDay.getValue());
            if (values != null || workingDay.getValue() < tomorrowValue) {
                SalesResultDaily dto = new SalesResultDaily();
                dto.setDate(workingDay.getIsoDate());
                dto.setProductivity(BigDecimal.ZERO);
                dto.setRevenue(BigDecimal.ZERO);

                if (values != null) {
                    dto.setProductivity(values[0]);
                    dto.setRevenue(values[1]);
                }

                results.add(dto);
            }
        }

        return new ListJson<SalesResultDaily>(results, Long.valueOf(results.size()));
    }

    @Override
    public ListJson<CustomerSaleResultDto> getSalesmanDashboardByDayDetail(UserLogin userLogin, String isoDate) {
        checkIsSalesman(userLogin);

        SimpleDate date = SimpleDate.createByIsoDate(isoDate, null);
        if (date == null) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        List<Order> orders = orderRepository.getOrderBySalesmen(userLogin.getClientId(),
                Arrays.asList(userLogin.getUserId()), DateTimeUtils.getPeriodOneDay(date), null, null, null);
        if (orders == null || orders.isEmpty()) {
            return ListJson.<CustomerSaleResultDto> emptyList();
        }

        Map<ObjectId, BigDecimal[]> customerMaps = new HashMap<ObjectId, BigDecimal[]>();
        for (Order order : orders) {
            if (order.getCustomer() == null || order.getCustomer().getId() == null) {
                continue;
            }

            List<OrderDetail> details = order.getDetails();
            if (details == null || details.isEmpty()) {
                continue;
            }

            ObjectId customerId = order.getCustomer().getId();
            BigDecimal[] values = customerMaps.get(customerId);
            if (values == null) {
                // san luong / doanh thu
                values = new BigDecimal[] { BigDecimal.ZERO, BigDecimal.ZERO };
            }

            for (OrderDetail detail : details) {
                values[0] = values[0].add(detail.getOutput());
            }

            if (order.getGrandTotal() != null && order.getGrandTotal().signum() > 0) {
                values[1] = values[1].add(order.getGrandTotal());
            }

            customerMaps.put(customerId, values);
        }

        if (customerMaps.isEmpty()) {
            return ListJson.<CustomerSaleResultDto> emptyList();
        }

        Collection<Customer> customers = customerRepository
                .getListByIds(userLogin.getClientId(), customerMaps.keySet());
        if (customers == null || customers.isEmpty() || customers.size() != customerMaps.size()) {
            return ListJson.<CustomerSaleResultDto> emptyList();
        }

        List<CustomerSaleResultDto> results = new ArrayList<CustomerSaleResultDto>();
        for (Customer customer : customers) {
            CustomerSaleResultDto dto = new CustomerSaleResultDto(customer, BigDecimal.ZERO, BigDecimal.ZERO);

            BigDecimal[] values = customerMaps.get(customer.getId());
            if (values != null) {
                dto.setProductivity(values[0]);
                dto.setRevenue(values[1]);
            }

            results.add(dto);
        }

        return new ListJson<CustomerSaleResultDto>(results, Long.valueOf(results.size()));
    }

}