package com.viettel.backend.dto.embed;

import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.dto.NameCategoryDto;

public class DistributorEmbedDto extends NameCategoryDto {
    /**
	 * 
	 */
    private static final long serialVersionUID = -7283608181950420990L;

    public DistributorEmbedDto() {
        super();
    }

    public DistributorEmbedDto(DistributorEmbed distributor) {
        super(distributor);

        this.code = distributor.getCode();
    }

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
