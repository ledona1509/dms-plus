package com.viettel.backend.engine.promotion;

import java.io.Serializable;
import java.util.List;

public class PromotionResult<ID extends Serializable, P extends I_Product<ID>> implements Serializable, I_PromotionResult<ID, P> {

    private static final long serialVersionUID = -1246230730041320249L;

    private ID id;
    private List<PromotionDetailResult<ID, P>> details;

    public PromotionResult() {
        super();
    }

    public PromotionResult(ID id, List<PromotionDetailResult<ID, P>> details) {
        super();

        this.id = id;
        this.details = details;
    }

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    @SuppressWarnings("unchecked")
    public List<PromotionDetailResult<ID, P>> getDetails() {
        return details;
    }

    public void setDetails(List<PromotionDetailResult<ID, P>> details) {
        this.details = details;
    }

}
