package com.viettel.backend.dto.embed;

import com.viettel.backend.domain.embed.CustomerEmbed;
import com.viettel.backend.dto.NameCategoryDto;

public class CustomerEmbedDto extends NameCategoryDto {

    private static final long serialVersionUID = 1652073192406165389L;
    
    private String code;
    private String address;
    
    public CustomerEmbedDto() {
        super();
    }
    
    public CustomerEmbedDto(CustomerEmbed customer) {
        super(customer);
        
        this.code = customer.getCode();
        this.address = customer.getAddress();
    }
    
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}
