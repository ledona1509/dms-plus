package com.viettel.backend.restful.salesman;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SalesmanCustomerTypeService;

@RestController(value="salesmanCustomerTypeController")
@RequestMapping(value = "/salesman/customertype")
public class CustomerTypeController extends AbstractController {

    private static final long serialVersionUID = 2800307633195967878L;

    @Autowired
    private SalesmanCustomerTypeService service;

    // LIST ALL
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<?> listAvailable() {
        ListJson<NameCategoryDto> dtos = service.getAll(getUserLogin());
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }

}
