package com.viettel.backend.oauth2.mvc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    
    @Value("${url.webview}")
    private String webviewUrl;

    @RequestMapping(value = "/")
    public String index() {
        return "redirect:" + webviewUrl;
    }
    
}
