package com.viettel.backend.engine.promotion;

import java.io.Serializable;
import java.util.List;

public interface I_PromotionResult<ID extends Serializable, P extends I_Product<ID>> extends Serializable {

    public ID getId();
    
    public <DETAIL extends I_PromotionDetailResult<ID, P>> List<DETAIL> getDetails();
    
}
