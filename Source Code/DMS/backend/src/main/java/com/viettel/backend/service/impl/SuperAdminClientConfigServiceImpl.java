package com.viettel.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.dto.ClientConfigDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SuperAdminClientConfigService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class SuperAdminClientConfigServiceImpl extends AbstractService implements SuperAdminClientConfigService {

    private static final long serialVersionUID = -2527783300704110407L;

    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Override
    public ClientConfigDto getClientConfig(UserLogin userLogin) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_SUPER_ADMIN)) {
            throw new BusinessException(ExceptionCode.SUPERADMIN_ONLY);
        }

        ClientConfig clientConfig = clientConfigRepository.getClientConfig(userLogin.getClientId());
        if (clientConfig == null) {
            return null;
        }

        ClientConfigDto dto = new ClientConfigDto(userLogin, clientConfig);

        return dto;
    }

    @Override
    public String save(UserLogin userLogin, ClientConfigDto dto) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_SUPER_ADMIN)) {
            throw new BusinessException(ExceptionCode.SUPERADMIN_ONLY);
        }

        checkMandatoryParams(dto, dto.getDefaultProductPhoto(), dto.getDefaultCustomerPhoto(),
                dto.getDefaultDateFormat(), dto.getFirstDayOfWeek(), dto.getMinimalDaysInFirstWeek(),
                dto.getNumberWeekOfFrequency());

        ClientConfig clientConfig = clientConfigRepository.getClientConfig(userLogin.getClientId());
        if (clientConfig == null) {
            clientConfig = new ClientConfig();
            initPOWhenCreate(ClientConfig.class, userLogin, clientConfig);
        }

        clientConfig.setDefaultProductPhoto(dto.getDefaultProductPhoto());
        clientConfig.setDefaultCustomerPhoto(dto.getDefaultCustomerPhoto());
        clientConfig.setDefaultDateFormat(dto.getDefaultDateFormat());

        clientConfig.setFirstDayOfWeek(dto.getFirstDayOfWeek());
        clientConfig.setMinimalDaysInFirstWeek(dto.getMinimalDaysInFirstWeek());
        clientConfig.setNumberWeekOfFrequency(dto.getNumberWeekOfFrequency());

        clientConfig = clientConfigRepository.save(userLogin.getClientId(), clientConfig);

        return clientConfig.getId().toString();
    }

}
