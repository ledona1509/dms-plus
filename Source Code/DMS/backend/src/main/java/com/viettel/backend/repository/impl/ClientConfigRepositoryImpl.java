package com.viettel.backend.repository.impl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.viettel.backend.cache.CacheManager;
import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.repository.ClientConfigRepository;

@Repository
public class ClientConfigRepositoryImpl extends AbstractRepository<ClientConfig> implements ClientConfigRepository {

    private static final long serialVersionUID = -1702535078849812226L;
    
    @Autowired
    private CacheManager cacheManager;

    @Override
    public ClientConfig getClientConfig(ObjectId clientId) {
        ClientConfig clientConfig = cacheManager.getClientConfigCache().get(clientId);
        if (clientConfig == null) {
            clientConfig = _getFirst(clientId, true, null, null);
            cacheManager.getClientConfigCache().put(clientId, clientConfig);
        }
        return clientConfig;
    }

    @Override
    public ClientConfig save(ObjectId clientId, ClientConfig domain) {
        ClientConfig clientConfig = _save(clientId, domain);
        cacheManager.getClientConfigCache().put(clientId, clientConfig);
        return clientConfig;
    }

}
