package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.OrderDto;
import com.viettel.backend.dto.OrderSimpleDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.CodeGeneratorRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.OrderPendingRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.repository.PromotionRepository;
import com.viettel.backend.repository.UOMRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SupervisorOrderService;
import com.viettel.backend.service.engine.PromotionEngine;
import com.viettel.backend.service.engine.WebNotificationEngine;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class SupervisorOrderServiceImpl extends AbstractSupervisorService implements SupervisorOrderService {

    private static final long serialVersionUID = 4840219204026219311L;

    public static final int NUMBER_PO_FOR_SMART_ORDER_PRODUCT = 1;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderPendingRepository orderPendingRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private PromotionRepository promotionRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private UOMRepository uomRepository;

    @Autowired
    private CodeGeneratorRepository counterRepository;

    @Autowired
    private PromotionEngine promotionEngine;

    @Autowired
    private WebNotificationEngine webNotificationEngine;

    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Override
    public OrderDto getOrderById(UserLogin userLogin, String _orderId) {
        ObjectId clientId = userLogin.getClientId();
        ObjectId currentUserId = userLogin.getUserId();
        ObjectId orderId = ObjectIdUtils.getObjectId(_orderId, null);

        if (orderId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Order order = orderRepository.getById(clientId, orderId);
        if (order == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        // Kiem tra nha phan phoi
        Collection<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(clientId,
                Arrays.asList(currentUserId));
        if (distributorIds == null || distributorIds.isEmpty()) {
            throw new BusinessException(ExceptionCode.SUPERVISOR_NO_DISTRIBUTOR);
        }
        if (!distributorIds.contains(order.getDistributor().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        return new OrderDto(order, getClientCondig(userLogin).getDefaultProductPhoto());
    }

    @Override
    public ListJson<OrderSimpleDto> getPendingOrders(UserLogin userLogin, String _distributorId, Pageable pageable) {

        ObjectId clientId = userLogin.getClientId();
        ObjectId supervisorId = userLogin.getUserId();
        ObjectId distributorId = ObjectIdUtils.getObjectId(_distributorId, null);

        // Kiem tra nha phan phoi
        Collection<ObjectId> distributorIdsForQuery = null;
        Collection<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(clientId,
                Arrays.asList(supervisorId));
        if (distributorIds == null || distributorIds.isEmpty()) {
            return ListJson.emptyList();
        }
        // ID nha phan phoi khong duoc quan ly
        if (distributorId != null) {
            if (!distributorIds.contains(distributorId)) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            distributorIdsForQuery = Arrays.asList(distributorId);
        } else {
            distributorIdsForQuery = distributorIds;
        }

        Collection<Order> orders = orderPendingRepository.getOrderByDistributors(clientId, distributorIdsForQuery,
                Order.APPROVE_STATUS_PENDING, null, pageable, new Sort(Direction.DESC, Order.COLUMNNAME_CREATED_TIME_VALUE));

        if (orders == null || orders.isEmpty()) {
            return ListJson.<OrderSimpleDto> emptyList();
        }

        List<OrderSimpleDto> dtos = new ArrayList<OrderSimpleDto>(orders.size());
        for (Order order : orders) {
            dtos.add(new OrderSimpleDto(order));
        }

        long size = Long.valueOf(dtos.size());
        if (pageable != null) {
            if (pageable.getPageNumber() > 0 || pageable.getPageSize() == size) {
                size = orderPendingRepository.countOrderByDistributors(clientId, distributorIdsForQuery,
                        Order.APPROVE_STATUS_PENDING, null);
            }
        }

        return new ListJson<OrderSimpleDto>(dtos, size);
    }

    @Override
    public OrderDto getPendingOrderById(UserLogin userLogin, String _orderId) {
        ObjectId clientId = userLogin.getClientId();
        ObjectId currentUserId = userLogin.getUserId();
        ObjectId orderId = ObjectIdUtils.getObjectId(_orderId, null);

        if (orderId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Order order = orderPendingRepository.getById(clientId, orderId);
        if (order == null || order.getApproveStatus() != Order.APPROVE_STATUS_PENDING) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        // Kiem tra nha phan phoi
        Collection<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(clientId,
                Arrays.asList(currentUserId));
        if (distributorIds == null || distributorIds.isEmpty()) {
            throw new BusinessException(ExceptionCode.SUPERVISOR_NO_DISTRIBUTOR);
        }
        if (!distributorIds.contains(order.getDistributor().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        return new OrderDto(order, getClientCondig(userLogin).getDefaultProductPhoto());
    }

    @Override
    public void approvePendingOrder(UserLogin userLogin, String _orderId) {
        ObjectId clientId = userLogin.getClientId();
        ObjectId supervisorId = userLogin.getUserId();
        ObjectId orderId = ObjectIdUtils.getObjectId(_orderId, null);

        if (orderId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Order order = orderPendingRepository.getById(clientId, orderId);
        if (order == null || order.getApproveStatus() != Order.APPROVE_STATUS_PENDING) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        // Kiem tra nha phan phoi
        Collection<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(clientId,
                Arrays.asList(supervisorId));
        if (distributorIds == null || distributorIds.isEmpty()) {
            throw new BusinessException(ExceptionCode.SALESMAN_NO_DISTRIBUTOR);
        }
        if (!distributorIds.contains(order.getDistributor().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        
        User supervisor = userRepository.getById(clientId, supervisorId);

        order.setApproveStatus(Order.APPROVE_STATUS_APPROVED);
        order.setApproveTime(DateTimeUtils.getCurrentTime());
        order.setApproveUser(new UserEmbed(supervisor));
        order = orderPendingRepository.save(clientId, order);

        webNotificationEngine.notifyChangedOrderForSupervisor(userLogin, userLogin.getUserId());
    }

    @Override
    public void rejectPendingOrder(UserLogin userLogin, String _orderId) {
        ObjectId clientId = userLogin.getClientId();
        ObjectId currentUserId = userLogin.getUserId();
        ObjectId orderId = ObjectIdUtils.getObjectId(_orderId, null);

        if (orderId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Order order = orderPendingRepository.getById(clientId, orderId);
        if (order == null || order.getApproveStatus() != Order.APPROVE_STATUS_PENDING) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        // Kiem tra nha phan phoi
        Collection<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(clientId,
                Arrays.asList(currentUserId));
        if (distributorIds == null || distributorIds.isEmpty()) {
            throw new BusinessException(ExceptionCode.SUPERVISOR_NO_DISTRIBUTOR);
        }
        if (!distributorIds.contains(order.getDistributor().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        order.setApproveStatus(Order.APPROVE_STATUS_REJECTED);
        order = orderPendingRepository.save(clientId, order);

        webNotificationEngine.notifyChangedOrderForSupervisor(userLogin, userLogin.getUserId());
    }

}
