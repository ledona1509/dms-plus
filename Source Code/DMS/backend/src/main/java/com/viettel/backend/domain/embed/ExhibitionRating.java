package com.viettel.backend.domain.embed;

import java.io.Serializable;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Transient;

import com.viettel.backend.domain.Visit;

public class ExhibitionRating implements Serializable {

    private static final long serialVersionUID = 7632969965465389576L;

    public static final String COLUMNNAME_EXHIBITION_ID = "exhibitionId";
    public static final String COLUMNNAME_ITEMS = "items";

    private ObjectId exhibitionId;
    private List<ExhibitionRatingItem> items;
    
    @Transient
    private Visit visit;

    public ObjectId getExhibitionId() {
        return exhibitionId;
    }

    public void setExhibitionId(ObjectId exhibitionId) {
        this.exhibitionId = exhibitionId;
    }

    public List<ExhibitionRatingItem> getItems() {
        return items;
    }

    public void setItems(List<ExhibitionRatingItem> items) {
        this.items = items;
    }
    
    public Visit getVisit() {
        return visit;
    }
    
    public void setVisit(Visit visit) {
        this.visit = visit;
    }

}
