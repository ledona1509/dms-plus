package com.viettel.backend.service.engine;

import java.io.Serializable;

import org.bson.types.ObjectId;

import com.viettel.backend.oauth2.core.UserLogin;

public interface WebNotificationEngine extends Serializable {

    public void notifyNewOrderForSupervisor(UserLogin userLogin, ObjectId supervisorId);

    public void notifyNewFeedbackForSupervisor(UserLogin userLogin, ObjectId supervisorId);

    public void notifyNewCustomerForSupervisor(UserLogin userLogin, ObjectId supervisorId);

    public void notifyChangedOrderForSupervisor(UserLogin userLogin, ObjectId supervisorId);

    public void notifyChangedFeedbackForSupervisor(UserLogin userLogin, ObjectId supervisorId);

    public void notifyChangedCustomerForSupervisor(UserLogin userLogin, ObjectId supervisorId);
}
