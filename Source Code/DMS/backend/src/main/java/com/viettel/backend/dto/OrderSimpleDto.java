package com.viettel.backend.dto;

import java.math.BigDecimal;

import com.viettel.backend.domain.Order;
import com.viettel.backend.dto.embed.CustomerEmbedDto;
import com.viettel.backend.dto.embed.DistributorEmbedDto;
import com.viettel.backend.dto.embed.UserEmbedDto;

public class OrderSimpleDto extends DTO {

    private static final long serialVersionUID = -25582112767713146L;

    private DistributorEmbedDto distributor;
    private CustomerEmbedDto customer;
    private UserEmbedDto salesman;

    private String createdTime;

    private String code;
    private int approveStatus;

    private int deliveryType;
    private String deliveryTime;

    private String comment;

    private BigDecimal subTotal;
    private BigDecimal promotionAmt;

    private BigDecimal discountPercentage;
    private BigDecimal discountAmt;

    private BigDecimal grandTotal;
    
    public OrderSimpleDto(Order order) {
        super(order);
        this.createdTime = order.getCreatedTime() != null ? order.getCreatedTime().getIsoTime() : null;
        this.code = order.getCode();
        this.approveStatus = order.getApproveStatus();
        this.deliveryType = order.getDeliveryType();
        this.deliveryTime = order.getDeliveryTime() != null ? order.getDeliveryTime().getIsoTime() : null;
        this.comment = order.getComment();
        this.subTotal = order.getSubTotal();
        this.promotionAmt = order.getPromotionAmt();
        this.discountPercentage = order.getDiscountPercentage();
        this.discountAmt = order.getDiscountAmt();
        this.grandTotal = order.getGrandTotal();
        
        if (order.getDistributor() != null) {
            this.distributor = new DistributorEmbedDto(order.getDistributor());
        }
        
        if (order.getCustomer() != null) {
            this.customer = new CustomerEmbedDto(order.getCustomer());
        }
        
        if (order.getSalesman() != null) {
            this.salesman = new UserEmbedDto(order.getSalesman());
        }
    }

    public DistributorEmbedDto getDistributor() {
        return distributor;
    }

    public void setDistributor(DistributorEmbedDto distributor) {
        this.distributor = distributor;
    }

    public CustomerEmbedDto getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEmbedDto customer) {
        this.customer = customer;
    }

    public UserEmbedDto getSalesman() {
        return salesman;
    }

    public void setSalesman(UserEmbedDto salesman) {
        this.salesman = salesman;
    }

    public String getCreatedTime() {
        return createdTime;
    }
    
    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(int approveStatus) {
        this.approveStatus = approveStatus;
    }

    public int getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(int deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }
    
    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getPromotionAmt() {
        return promotionAmt;
    }

    public void setPromotionAmt(BigDecimal promotionAmt) {
        this.promotionAmt = promotionAmt;
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(BigDecimal discountAmt) {
        this.discountAmt = discountAmt;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

}
