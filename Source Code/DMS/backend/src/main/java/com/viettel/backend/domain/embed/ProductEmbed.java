package com.viettel.backend.domain.embed;

import java.math.BigDecimal;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.NameCategory;
import com.viettel.backend.domain.Product;
import com.viettel.backend.domain.UOM;
import com.viettel.backend.engine.promotion.I_Product;

public class ProductEmbed extends NameCategory implements I_Product<ObjectId> {

    private static final long serialVersionUID = -5848974644014161066L;

    private String code;
    private String photo;
    private UOM uom;
    private BigDecimal price;
    private BigDecimal output;

    public ProductEmbed() {
        super();
    }

    public ProductEmbed(Product product) {
        super();
        setId(product.getId());
        setName(product.getName());

        this.code = product.getCode();
        this.photo = product.getPhoto();
        this.uom = product.getUom();
        this.price = product.getPrice();
        this.output = product.getOutput();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public UOM getUom() {
        return uom;
    }

    public void setUom(UOM uom) {
        this.uom = uom;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getOutput() {
        return output;
    }

    public void setOutput(BigDecimal output) {
        this.output = output;
    }

}
