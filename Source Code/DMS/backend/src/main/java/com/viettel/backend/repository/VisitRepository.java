package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.Visit;
import com.viettel.backend.entity.SimpleDate.Period;

public interface VisitRepository extends BasicRepository<Visit> {

    // ** ANY VISIT STATUS **
    public Visit getVisitByCustomerToday(ObjectId clientId, ObjectId customerId);

    /** Map by customerId */
    public Map<ObjectId, Visit> getMapVisitByCustomerIdsToday(ObjectId clientId, Collection<ObjectId> customerIds);

    // ** VISITED ONLY **
    public List<Visit> getVisitedsBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Period period, Sort sort);

    public List<Visit> getVisitedsByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Period period, Sort sort);
    
    public List<Visit> getLastVisitedsByCustomer(ObjectId clientId, ObjectId customerId, int size);

}
