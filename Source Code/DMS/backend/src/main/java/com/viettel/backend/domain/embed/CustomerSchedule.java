package com.viettel.backend.domain.embed;

import java.io.Serializable;
import java.util.List;

import org.bson.types.ObjectId;

public class CustomerSchedule implements Serializable {

    /**
	 * 
	 */
    private static final long serialVersionUID = -5890415709083121260L;
    
    private ObjectId salesmanId;

    private List<CustomerScheduleItem> items;

    public ObjectId getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(ObjectId salesmanId) {
        this.salesmanId = salesmanId;
    }
    
    public List<CustomerScheduleItem> getItems() {
        return items;
    }

    public void setItems(List<CustomerScheduleItem> items) {
        this.items = items;
    }

}
