package com.viettel.backend.engine.promotion;

import java.io.Serializable;
import java.util.List;

public interface I_Promotion<ID extends Serializable, P extends I_Product<ID>> extends Serializable {

    public ID getId();

    public <DETAIL extends I_PromotionDetail<ID, P>> List<DETAIL> getDetails();

}
