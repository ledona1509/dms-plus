(function(angular, undefined) {'use strict';
    var module = angular.module('vt.report', []);
    
    module.provider('$report', function () {
        var defaultConfigs = {
            REPORT_URL : ""
        };
        
        this.set =
        this.Set = function ()
        {
            if (arguments.length !== 1 && typeof arguments[0] === 'object') {
                return this;
            }
            
            if(typeof arguments[0] !== 'string'){
                return this;
            }
            
            var value = typeof arguments[1] === 'undefined' ? null : arguments[1];
            var key = arguments[0];
            defaultConfigs[key] = value;
            
            return this;
        };
        
        this.$get = function ($log) {
            var configs = {};
            
            configs.get = function() {
                var returnValue = null;
                if(arguments.length === 0) {
                    $log.error('arguments null');
                    throw 'arguments error';
                }
                
                var key = arguments[0];
                
                if (angular.isString(arguments[0])) {
                    returnValue = defaultConfigs[key];
                }
                return returnValue;
            };
            
            configs.getReportUrl = function(){
                return configs.get('REPORT_URL');  
            };
            
            var objectToQueryString = function (obj) {
                var str = [];
                angular.forEach(obj, function (value, key) {
                    str.push(encodeURIComponent(key) + "=" + encodeURIComponent(value));
                });
                return str.join("&");
            };

            configs.getRequestUrl = function(params){
                var url = configs.getReportUrl();
                if(angular.isString(params)) {
                    if(params.indexOf(0) !== "?") {
                        url += "?";
                    }
                    url += params +"&rframe=true";
                } else if(angular.isObject(params)) {
                    var defaultParams = {
                        rframe: true
                    };
                    defaultParams = angular.extend(defaultParams, params);
                    url += "?" + objectToQueryString(defaultParams);
                } else {
                    throw new Error("Invalid arguments");
                }

                return url;
            };
            
            return configs;
        };
        
    });

    module.directive('vtReport', function ($report, $log, $http, $sce, $compile, $rootScope) {
        return {
            restrict: 'E',
            scope: {
                params: '@'
            },
            transclude: true,
            link: function(scope, element, attrs) {
                var params = attrs.params || {};
                var url = $report.getRequestUrl(params);
                
                $http.get(url).then(function(response){
                    element.text("");
                    var scope = $rootScope.$new(true);
                    var reportUrl = response.data;
                    reportUrl = $sce.trustAsResourceUrl(reportUrl);
                    var iframe = angular.element('<iframe style="width: 100%; height: 100%"></iframe>');
                    iframe.attr('ng-src', reportUrl);
                    var backdropDomEl = $compile(iframe)(scope);
                    element.append(backdropDomEl);
                }, function(error){
                    $log.log("error: " + error);
                });
            },
            template: '<span style="width: 100%; height: 100%">waiting ...</span>'
        };
    });
})(window.angular);

