(function (window, angular, undefined) {
    'use strict';
    var module = angular.module('vt.ui.file-manager', []);

    module.value('vtFileManagerConfig', {
        uploadUrl: '/',
        getDownloadUrl: function(fileId) {
            return '/' + fileId;
        },
        getDeleteUrl: function(fileId) {
            return '/' + fileId;
        },
        getHeaders: function() {
            return {};
        }
    });

    module.controller('vtFileMultipleController', ['$scope', '$attrs', 'FileUploader', 'vtFileManagerConfig', '$http',
                function ($scope, $attrs, FileUploader, vtFileManagerConfig, $http) {

        var uploader = $scope.uploader = new FileUploader({
            url: vtFileManagerConfig.uploadUrl,
            getHeaders: vtFileManagerConfig.getHeaders
        });

        uploader.onAfterAddingAll = function(addedFileItems) {
            uploader.uploadAll();
        };

        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            fileItem.id = response;
            if ($scope.files != null) {
                fileItem.isNew = true;
                $scope.files.push(fileItem);
            }
        };

        $scope.removeOldFile = function(oldFile) {
            $scope.files.splice($scope.files.indexOf(oldFile),1);
        };

        $scope.removeFile = function(file) {
            if (!angular.isNull(file.id)) {
                $http.delete(vtFileManagerConfig.getDeleteUrl(file.id)).
                    success(function(data, status, headers, config) {

                    }).
                    error(function(data, status, headers, config) {

                    })
            }

            file.remove();

            var index = $scope.files.indexOf(file);
            if (index != null || index >= 0) {
                $scope.files.splice(index, 1);
            }
        };

        $scope.getDownloadUrl = function (file) {
            return vtFileManagerConfig.getDownloadUrl(file.id);
        };
    }]);

    module.directive('vtFileMultiple', function () {
        return {
            restrict: 'AE',
            scope: {
                files: '=',
                vtTitle: '@',
                vtUploadUrl: '@'
            },
            replace: true,
            templateUrl: "directive/template/vt-file-manager/vt-file-multiple.html",
            controller: "vtFileManagerController"
        };
    });

    module.controller('vtImageSingleThumbnailController', ['$scope', '$element', '$attrs', 'FileUploader', 'vtFileManagerConfig', '$http',
        function ($scope, $element, $attrs, FileUploader, vtFileManagerConfig, $http) {
            imageController(this, $scope, $element, $attrs, FileUploader, vtFileManagerConfig, $http, 'thumb')
        }]);

    module.directive('vtImageSingleThumbnail', function () {
        return {
            restrict: 'AE',
            scope: {
                vtTitle: '@',
                vtUploadUrl: '@'
            },
            require: ['vtImageSingleThumbnail', '?ngModel'],
            replace: true,
            templateUrl: "directive/template/vt-file-manager/vt-image-single-thumbnail.html",
            controller: "vtImageSingleThumbnailController",
            link: function(scope, element, attrs, ctrls) {
                var vtImageSingleThumbnailController = ctrls[0], ngModelCtrl = ctrls[1];

                if (!ngModelCtrl) {
                    return; // do nothing if no ng-model
                }

                vtImageSingleThumbnailController.init(ngModelCtrl);
            }
        };
    });

    module.controller('vtImageSingleMediumController', ['$scope', '$element', '$attrs', 'FileUploader', 'vtFileManagerConfig', '$http',
        function ($scope, $element, $attrs, FileUploader, vtFileManagerConfig, $http) {
            imageController(this, $scope, $element, $attrs, FileUploader, vtFileManagerConfig, $http, 'medium')
        }]);

    module.directive('vtImageSingleMedium', function () {
        return {
            restrict: 'AE',
            scope: {
                vtTitle: '@',
                vtUploadUrl: '@'
            },
            require: ['vtImageSingleMedium', '?ngModel'],
            replace: true,
            templateUrl: "directive/template/vt-file-manager/vt-image-single-medium.html",
            controller: "vtImageSingleMediumController",
            link: function(scope, element, attrs, ctrls) {
                var vtImageSingleMediumController = ctrls[0], ngModelCtrl = ctrls[1];

                if (!ngModelCtrl) {
                    return; // do nothing if no ng-model
                }

                vtImageSingleMediumController.init(ngModelCtrl);
            }
        };
    });

    module.controller('vtImageSingleStandardController', ['$scope', '$element', '$attrs', 'FileUploader', 'vtFileManagerConfig', '$http',
        function ($scope, $element, $attrs, FileUploader, vtFileManagerConfig, $http) {
            imageController(this, $scope, $element, $attrs, FileUploader, vtFileManagerConfig, $http, 'standard')
        }]);

    module.directive('vtImageSingleStandard', function () {
        return {
            restrict: 'AE',
            scope: {
                file: '=',
                vtTitle: '@',
                vtUploadUrl: '@'
            },
            require: ['vtImageSingleStandard', '?ngModel'],
            replace: true,
            templateUrl: "directive/template/vt-file-manager/vt-image-single-standard.html",
            controller: "vtImageSingleStandardController",
            link: function(scope, element, attrs, ctrls) {
                var vtImageSingleStandardController = ctrls[0], ngModelCtrl = ctrls[1];

                if (!ngModelCtrl) {
                    return; // do nothing if no ng-model
                }

                vtImageSingleStandardController.init(ngModelCtrl);
            }
        };
    });

    function imageController(ctrl, $scope, $element, $attrs, FileUploader, vtFileManagerConfig, $http, sizeType) {
        var ngModelCtrl = { $setViewValue: angular.noop } ;// nullModelCtrl

        ctrl.init = function(ngModelCtrl_) {
            ngModelCtrl = ngModelCtrl_;
        };

        $scope.getFile = function() {
            return ngModelCtrl.$viewValue;
        };

        $scope.getLink  = function() {
            var id = $scope.getFile();
            if (id != null) {
                return vtFileManagerConfig.getDownloadUrl(id);
            }
            return null;
        };

        if ($attrs.sizeType != null) {
            sizeType = $attrs.sizeType;
        }

        var url = vtFileManagerConfig.uploadUrl;
        if (sizeType != null && ('thumb' === sizeType || 'medium' === sizeType || 'standard' === sizeType)) {
            url = url + "?sizetype=" + sizeType;
        } else {
            url = url + "?sizetype=" + 'standard';
        }

        var inputs = $element.find('input');
        var input = null;
        if (inputs.length > 0) {
            input = $(inputs[0]);
        }

        var tempFileItem = null;

        var uploader = $scope.uploader = new FileUploader({
            url: url,
            getHeaders: vtFileManagerConfig.getHeaders
        });

        uploader.onAfterAddingAll = function() {
            uploader.uploadAll();
        };

        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            fileItem.id = response;
            if (ngModelCtrl.$viewValue != null) {
                $scope.removeFile(ngModelCtrl.$viewValue);
            }
            ngModelCtrl.$setViewValue(fileItem.id);
            ngModelCtrl.$render();

            tempFileItem = fileItem;
        };

        $scope.removeFile = function() {
            if (!angular.isUndefinedOrNull(ngModelCtrl.$viewValue)) {
                $http.delete(vtFileManagerConfig.getDeleteUrl(ngModelCtrl.$viewValue)).
                    success(function(data, status, headers, config) {

                    }).
                    error(function(data, status, headers, config) {

                    });

                if (tempFileItem != null) {
                    tempFileItem.remove();
                }

                ngModelCtrl.$setViewValue(null);
                ngModelCtrl.$render();

                if (input != null) {
                    input.val("");
                }
            }
        };
    }

})(window, window.angular);