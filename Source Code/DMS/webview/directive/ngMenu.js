app.directive('ngMenu', function () {
    function createMenuItem(itemModel) {
        var menuItem = $(document.createElement('li'));
        var link = $(document.createElement('a'));

        if (itemModel.children != null && itemModel.children.length > 0) {
            menuItem.addClass('has-sub');

            link.attr('href', '#');
            link.text( itemModel.name );

            link.bind("click", function (event) {
                if (menuItem.hasClass('opened')) {
                    menuItem.removeClass('opened');
                } else {
                    menuItem.addClass('opened');
                }

                var siblings = menuItem.siblings();
                if (siblings != null) {
                    for (var i = 0; i < siblings.length; i++) {
                        var sibling = $(siblings[i]);
                        sibling.removeClass('opened');
                    }
                }

                event.preventDefault();
            });
            menuItem.append(link);

            var children = $(document.createElement('ul'));
            children.addClass('nav');
            for (var i = 0; i < itemModel.children.length; i++) {
                var child = createMenuItem(itemModel.children[i]);
                children.append(child);
            }
            menuItem.append(children);
        } else {
            link.attr('href', itemModel.uri);
            link.text( itemModel.name );
            menuItem.append(link);
        }

        return menuItem;
    };

    return function (scope, element, attrs) {
        element.addClass('nav');

        scope.$watch(attrs.ngMenu, function ngMenuWatchAction(value) {
            element.empty();

            if (value != null) {
                for (var i = 0; i < value.length; i++) {
                    var child = createMenuItem(value[i]);
                    element.append(child);
                }
            }
        });
    };
});