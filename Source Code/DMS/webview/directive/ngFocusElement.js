app.directive('ngFocusElement', function () {
    return function (scope, element) {
        element.focus();
    };
});