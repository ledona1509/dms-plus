app.factory('CustomerFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'customer/:action/:subAction/:id',
            null,
            {
                forapprove: {
                    method: 'GET',
                    params: {
                        action: 'forapprove'
                    },
                    isArray: false
                },
                countforapprove: {
                    method: 'GET',
                    headers: {'Content-Type':'application/json'},
                    params: {
                        action: 'countforapprove'
                    },
                    isArray: false
                },
                updateforapprove: {
                    method: 'PUT',
                    params: {
                        action: 'forapprove'
                    },
                    headers: {'Content-Type':'application/json'}
                },
                approve: {
                    method: 'PUT',
                    params: {
                        action: 'approve'
                    },
                    headers: {'Content-Type':'application/json'}
                },
                reject: {
                    method: 'PUT',
                    params: {
                        action: 'reject'
                    },
                    headers: {'Content-Type':'application/json'}
                },
                detail: {
                    method: 'GET',
                    isArray: false
                },
                forsalesmanToday: {
                    method: 'GET',
                    params: {
                        action: 'forsalesman',
                        subAction: 'today'
                    },
                    isArray: false
                },
                forsalesmanAll: {
                    method: 'GET',
                    params: {
                        action: 'forsalesman',
                        subAction: 'all'
                    },
                    isArray: false
                }
            }
        );
    }
);
