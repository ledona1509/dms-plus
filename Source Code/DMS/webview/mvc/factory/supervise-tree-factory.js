app.factory('SuperviseTreeFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'supervisetree/:action/:subaction/:id',
            null,
            {
                getTree: {
                    method: 'GET',
                    params: {
                        action: 'gettree'
                    },
                    isArray: false
                },
                supervisorAvailable: {
                    method: 'GET',
                    params: {
                        action: 'supervisor',
                        subaction: 'available'
                    },
                    isArray: false
                },
                create: {
                    method: 'POST',
                    headers: {'Content-Type':'application/json'}
                },
                update: {
                    method: 'PUT',
                    headers: {'Content-Type':'application/json'}
                }
            }
        );
    }
);
