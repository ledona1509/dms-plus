app.factory('SimpleCategoryFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + ':category/:action/:id',
            null,
            {
                create: {
                    method: 'POST',
                    headers: {'Content-Type':'application/json'}
                },
                update: {
                    method: 'PUT',
                    headers: {'Content-Type':'application/json'}
                },
                detail: {
                    method: 'GET',
                    isArray: false
                },
                list: {
                    method: 'GET',
                    isArray: false
                },
                all: {
                    method: 'GET',
                    params:  {
                        action: 'all'
                    },
                    isArray: false
                },
                delete: {
                    method: 'DELETE'
                }
            }
        );
    }
);
