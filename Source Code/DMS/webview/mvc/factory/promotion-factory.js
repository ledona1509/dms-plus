app.factory('PromotionFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'promotion/:action/:subAction/:id',
            null,
            {
                approve: {
                    method: 'PUT',
                    headers: {'Content-Type':'application/json'},
                    params: {
                        action: 'approve'
                    },
                    isArray: false
                },
                reject: {
                    method: 'PUT',
                    headers: {'Content-Type':'application/json'},
                    params: {
                        action: 'reject'
                    },
                    isArray: false
                },
                products: {
                    method: 'GET',
                    headers: {'Content-Type':'application/json'},
                    params: {
                        action: 'product',
                        subAction: 'available'
                    },
                    isArray: false
                },
                allproducts: {
                    method: 'GET',
                    headers: {'Content-Type':'application/json'},
                    params: {
                        action: 'product',
                        subAction: 'all'
                    },
                    isArray: false
                }
            }
        );
    }
);
