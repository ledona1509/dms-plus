app.factory('ClientConfigFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'clientconfig/:action/:subAction/:id',
            null,
            {
                getConfig: {
                    method: 'GET',
                    isArray: false
                },
                save: {
                    method: 'PUT'
                }
            }
        );
    }
);
