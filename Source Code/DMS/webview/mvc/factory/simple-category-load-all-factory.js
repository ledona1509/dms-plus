app.factory('SimpleCategoryLoadAllFactory',
    function($log, SimpleCategoryFactory) {
        var cache = window.localStorage || {};
        var prefix = "ALL_";
        
        function get(category, callback, errorback) {
            var tmData = cache[prefix + category];
            if (tmData && tmData.length > 0) {
                if (callback) {
                    callback(JSON.parse(tmData));
                }
                return;
            }
            
            SimpleCategoryFactory.list({'category': category , 'action': 'all'},
                function(response) {
                    var data = response.list;
                    cache[prefix + category] = JSON.stringify(data);;
                    if (callback) {
                        callback(data);
                    }
                }, function(error) {
                    $log.error(error);
                    if (errorback) {
                        errorback(error);
                    }
                }
            );
        }
        
        function clear(category) {
            cache.removeItem(prefix + category);
        }
        
        function reload(category) {
            clear(category);
            get(category);
        }
        
        return {
            'get': get,
            'clear' : clear,
            'reload' : reload
        };
    }
);


