app.factory('DashboardVisitFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'dashboardvisit/:action/:subAction/:id',
            null,
            {
                daily: {
                    method: 'GET',
                    params: {
                        action: 'daily'
                    },
                    isArray: false
                }
            }
        );
    }
);
