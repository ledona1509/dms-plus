app.factory('HomepageFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'homepage/:action/:subAction/:id',
            null,
            {
                supervisor: {
                    method: 'GET',
                    params: {
                        action: 'supervisor'
                    },
                    isArray: false
                }
            }
        );
    }
);
