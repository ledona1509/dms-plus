app.factory('PurchaseOrderFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'purchaseorder/:action/:id',
            null,
            {
                forapprove: {
                    method: 'GET',
                    headers: {'Content-Type':'application/json'},
                    params: {
                        action: 'forapprove'
                    },
                    isArray: false
                },
                countforapprove: {
                    method: 'GET',
                    headers: {'Content-Type':'application/json'},
                    params: {
                        action: 'countforapprove'
                    },
                    isArray: false
                },
                detailforapprove: {
                    method: 'GET',
                    params: {
                        action: 'forapprove'
                    },
                    isArray: false
                },
                approve: {
                    method: 'PUT',
                    headers: {'Content-Type':'application/json'},
                    params: {
                        action: 'approve'
                    },
                    isArray: false
                },
                reject: {
                    method: 'PUT',
                    headers: {'Content-Type':'application/json'},
                    params: {
                        action: 'reject'
                    },
                    isArray: false
                },
                approvedList: {
                    method: 'GET',
                    params: {
                        action: 'approved'
                    },
                    isArray: false
                },
                approvedDetail: {
                    method: 'GET',
                    params: {
                        action: 'approved'
                    },
                    isArray: false
                }
            }
        );
    }
);
