app.factory('VisitScheduleFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'visitschedule/:action/:subAction/:id',
            null,
            {
                bydistributor: {
                    method: 'GET',
                    params: {
                        action: 'bydistributor'
                    },
                    isArray: false
                },
                save: {
                    method: 'PUT',
                    params: {
                        action: 'save'
                    }
                },
                saveByCustomer: {
                    method: 'PUT',
                    params: {
                        action: 'bycustomer',
                        subAction: 'save'
                    }
                }
            }
        );
    }
);
