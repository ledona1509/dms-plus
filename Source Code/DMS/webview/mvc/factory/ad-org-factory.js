app.factory('ADOrgFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'adorg/:action/:id',
            null,
            {
                create: {
                    method: 'PUT',
                    headers: {'Content-Type':'application/json'}
                },
                update: {
                    method: 'PUT',
                    headers: {'Content-Type':'application/json'}
                },
                detail: {
                    method: 'GET',
                    isArray: false
                },
                list: {
                    method: 'GET',
                    isArray: false
                },
                delete: {
                    method: 'DELETE'
                },
                getTree: {
                    method: 'GET',
                    params: {
                        action: 'tree'
                    },
                    isArray: false
                },
                deleteNode: {
                    method: 'DELETE',
                    params: {
                        action: 'tree'
                    }
                },
                saveTree: {
                    method: 'PUT',
                    params: {
                        action: 'tree'
                    },
                    headers: {'Content-Type':'application/json'}
                }
            }
        );
    }
);
