app.factory('DashboardFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'dashboard/:role/:action/:subAction/:id',
            null,
            {
                supervisorMonthlyOrderSummary: {
                    method: 'GET',
                    params: {
                        role: 'supervisor',
                        action: 'monthly',
                        subAction: 'orders'
                    },
                    isArray: false
                }
            }
        );
    }
);
