app.factory('ADUserFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'aduser/:action/:subAction/:subType/:id',
            null,
            {
                resetpassword: {
                    method: 'PUT',
                    params: {
                        action: 'resetpassword'
                    }
                },
                supervisorLeaf: {
                    method: 'GET',
                    params: {
                        action: 'supervisor',
                        subAction: 'leaf'
                    }
                },
                salesmanBySupervisor: {
                    method: 'GET',
                    params: {
                        action: 'salesman',
                        subAction: 'bysupervisor'
                    }
                },
                salesmanBySupervisorNoTarget: {
                    method: 'GET',
                    params: {
                        action: 'salesman',
                        subAction: 'bysupervisor',
                        subType: 'notarget'
                    }
                }

            }
        );
    }
);
