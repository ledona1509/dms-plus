app.factory('ExhibitionFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'exhibition/:action/:subAction/:id/:childAction1/:childAction2',
            null,
            {
                list: {
                    method: 'GET',
                    isArray: false
                },
                detail: {
                    method: 'GET',
                    isArray: false
                },
                reportSummary: {
                    method: 'GET',
                    params: {
                        childAction1: 'report',
                        childAction2: 'summary'
                    },
                    isArray: false
                }
            }
        );
    }
);
