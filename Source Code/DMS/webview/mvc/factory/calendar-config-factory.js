app.factory('CalendarConfigFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'calendarconfig/:action/:subAction/:id',
            null,
            {
                getConfig: {
                    method: 'GET',
                    isArray: false
                },
                save: {
                    method: 'PUT'
                }
            }
        );
    }
);
