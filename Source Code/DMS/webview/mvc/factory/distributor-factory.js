app.factory('DistributorFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'distributor/:action/:subAction/:id',
            null,
            {
                salesmanAvailable: {
                    method: 'GET',
                    params: {
                        action: 'salesman',
                        subAction: 'available'
                    },
                    isArray: false
                },

                customerAvailable: {
                    method: 'GET',
                    params: {
                        action: 'customer',
                        subAction: 'available'
                    },
                    isArray: false
                },

                allSalesman: {
                    method: 'GET',
                    params: {
                        action: 'salesman',
                        subAction: 'bydistributor'
                    },
                    isArray: false
                },

                bySupervisor: {
                    method: 'GET',
                    params: {
                        action: 'bysupervisor'
                    },
                    isArray: false
                },

                customerBySupervisor: {
                    method: 'GET',
                    params: {
                        action: 'bysupervisor',
                        subAction: 'customers'
                    },
                    isArray: false
                },
                salesmanBySupervisor: {
                    method: 'GET',
                    params: {
                        action: 'bysupervisor',
                        subAction: 'salesmen'
                    },
                    isArray: false
                }

            }
        );
    }
);
