app.factory('ReportSupervisorFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'report/supervisor/:report/:reportType/:subType',
            null,
            {
                saleSalesmanMonthly: {
                    method: 'GET',
                    params: {
                        report: 'sale',
                        reportType: 'salesman',
                        subType: 'monthly'
                    },
                    isArray: false
                },
                visitMonthly: {
                    method: 'GET',
                    params: {
                        report: 'visit',
                        subType: 'monthly'
                    },
                    isArray: false
                },
                surveyResult: {
                    method: 'GET',
                    params: {
                        report: 'survey'
                    },
                    isArray: false
                }
            }
        );
    }
);
