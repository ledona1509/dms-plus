app.factory('FeedbackFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'feedback/:action/:subAction/:id',
            null,
            {
                listBySupervisor: {
                    method: 'GET',
                    params: {
                        action: 'bysupervisor'
                    },
                    isArray: false
                },
                readBySupervisor: {
                    method: 'PUT',
                    params: {
                        action: 'bysupervisor'
                    }
                },
                countBySupervisorUnread: {
                    method: 'GET',
                    params: {
                        action: 'bysupervisor',
                        subAction: 'countunread'
                    }
                }
            }
        );
    }
);
