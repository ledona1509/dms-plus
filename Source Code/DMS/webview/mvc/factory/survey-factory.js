app.factory('SurveyFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'survey/:action/:subAction/:id',
            null,
            {
                listSupervisor: {
                    method: 'GET',
                    params: {
                        action: 'supervisor'
                    },
                    isArray: false
                }
            }
        );
    }
);
