app.factory('VisitFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'visit/:action/:subAction/:id',
            null,
            {
                detail: {
                    method: 'GET',
                    params: {
                        action: 'detail'
                    },
                    isArray: false
                }
            }
        );
    }
);
