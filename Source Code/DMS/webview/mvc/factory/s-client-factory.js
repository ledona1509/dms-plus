app.factory('SClientFactory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + 'sclient/:action/:id',
            null,
            {
                create: {
                    method: 'PUT',
                    headers: {'Content-Type':'application/json'}
                },
                update: {
                    method: 'PUT',
                    headers: {'Content-Type':'application/json'}
                },
                detail: {
                    method: 'GET',
                    isArray: false
                },
                list: {
                    method: 'GET',
                    isArray: false
                },
                delete: {
                    method: 'DELETE'
                }
            }
        );
    }
);
