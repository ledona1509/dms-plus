var app = angular.module('ViettelApp', ['ngRoute', 'ngResource','pascalprecht.translate', 'vtToken', 'ui.bootstrap',
        'angular-loading-bar', 'vt.ui.search-box', 'vt.ui.checkbox', 'vt.ui.checkbox-tri-state', 'vt.ui.radio',
        'app.ui.services', 'ngPDFViewer', 'vt.ui.file-manager', 'angularFileUpload', 'ui.router', 'vt.push', 'ui.tree',
        'ngIdle', 'ui.select', 'ngSanitize', 'vt.report', 'uiGmapgoogle-maps', 'fcsa-number', 'dialogs.main',
        'scrollable-table', 'angularMoment', 'angular-carousel']);

//CONSTANT
//IP BACKEND
app.constant("ADDRESS_BACKEND", "http://localhost:7080/backend/api/");
app.constant("ADDRESS_OAUTH", "http://localhost:7080/backend/");
app.constant("ADDRESS_SOCKET", "http://localhost:7080/backend/websocket");
app.constant("ADDRESS_PING", "http://localhost:7080/backend/account/ping");
app.constant("REPORT_SERVER", "http://localhost:7080/backend/report");

app.constant("NO_AUTH_PATH", ['/404', '/500']);
app.constant("FULL_SCREEN_PATH", ['/404', '/500']);

// Time out: 30 mins
app.constant("NG_IDLE_MAX_IDLE_TIME", 60 * 30);
// Count down time before logout: 8 secs
app.constant("NG_IDLE_TIMEOUT", 8);
// Auto-ping interval: 10 mins
app.constant("NG_IDLE_PING_INTEVAL", 60 * 10);

//CONFIG FOR SIMPLE CATEGORY
app.constant("PATTERN", {
    bigdecimal_positive: '/^[0-9]*[\\.]?[0-9]*$/i',
    integer_positive: '/^[0-9]*$/i'
});

//STATE CONFIG
app.constant("STATE_CONFIG", {
    404: {
        isSpecial: true,
        template: 'mvc/view/404.html'
    },
    500: {
        isSpecial: true,
        template: 'mvc/view/500.html'
    },
    'no-function': {
        isSpecial: true,
        template: 'mvc/view/salesman/no-function-warning.html'
    },
    //SUPER ADMIN
    client: {
        isUsePopup: true,
        title: 'menu.client',
        who: 'superadmin'
    },
    clientconfig: {
        isSpecial: true,
        template: 'mvc/view/superadmin/client-config.html',
        ctrl: 'ClientConfigCtrl'
    },
    //ADMIN
    'homepage-admin': {
        isSpecial: true,
        url: '/homepage/admin',
        template: 'mvc/view/supervisor/homepage-supervisor.html',
        ctrl: 'ADHomepageCtrl'
    },
    user: {
        isUsePopup: true,
        title: 'menu.user',
        who: 'admin',
        listCtrl: 'UserCtrl',
        detailTemplate: 'mvc/view/admin/user-detail-popup.html',
        detailCtrl: 'UserDetailPopupCtrl'
    },
    salesconfig: {
        isSpecial: true,
        template: 'mvc/view/admin/sales-config.html',
        ctrl: 'SalesConfigCtrl'
    },
    calendarconfig: {
        isSpecial: true,
        template: 'mvc/view/admin/calendar-config.html',
        ctrl: 'CalendarConfigCtrl'
    },
    uom: {
        isUsePopup: true,
        title: 'menu.uom',
        who: 'admin',
        useCode: true
    },
    productcategory: {
        isUsePopup: true,
        title: 'menu.product.category',
        who: 'admin'
    },
    product: {
        isUsePopup: false,
        title: 'menu.product',
        who: 'admin',
        useCode: true,
        detailTemplate: 'mvc/view/admin/product-detail.html',
        detailCtrl: 'ProductDetailCtrl'
    },
    customertype: {
        isUsePopup: true,
        title: 'menu.customer.type',
        who: 'admin'
    },
    customer: {
        isUsePopup: false,
        title: 'menu.customer',
        who: 'admin',
        listCtrl: 'CustomerCtrl',
        detailTemplate: 'mvc/view/admin/customer-detail.html',
        detailCtrl: 'CustomerDetailCtrl'
    },
    distributor: {
        isUsePopup: false,
        title: 'menu.distributor',
        who: 'admin',
        useCode: true,
        detailTemplate: 'mvc/view/admin/distributor-detail.html',
        detailCtrl: 'DistributorDetailCtrl'
    },
    promotion: {
        isUsePopup: false,
        title: 'menu.promotion',
        who: 'admin',
        listCtrl: 'PromotionCtrl',
        detailTemplate: 'mvc/view/admin/promotion-detail.html',
        detailCtrl: 'PromotionDetailCtrl'
    },
    survey: {
        isUsePopup: false,
        title: 'menu.survey',
        who: 'admin',
        listCtrl: 'SurveyCtrl',
        detailTemplate: 'mvc/view/admin/survey-detail.html',
        detailCtrl: 'SurveyDetailCtrl'
    },
    exhibition: {
        isUsePopup: false,
        title: 'menu.exhibition',
        who: 'admin',
        listCtrl: 'SurveyCtrl',
        detailTemplate: 'mvc/view/admin/exhibition-detail.html',
        detailCtrl: 'ExhibitionDetailCtrl'
    },
    'admin-dashboard-visit': {
        isSpecial: true,
        url: '/admin/dashboard/visit?salesmanid',
        template: 'mvc/view/supervisor/dashboard-visit.html',
        ctrl: 'ADDashboardVisitCtrl'
    },
    'admin-dashboard-visit-detail': {
        isSpecial: true,
        url: '/admin/dashboard/visit/detail/:id?returnSalesmanId',
        template: 'mvc/view/supervisor/dashboard-visit-detail.html',
        ctrl: 'ADDashboardVisitDetailCtrl'
    },
    'admin-visit-tracking': {
        isSpecial: true,
        template: 'mvc/view/supervisor/visit-tracking.html',
        ctrl: 'ADVisitTrackingCtrl'
    },
    'admin-sales-report': {
        isSpecial: true,
        url: '/admin/sales/report',
        template: 'mvc/view/admin/admin-sales-report.html',
        ctrl: 'AdminSalesReportCtrl'
    },
    'admin-sales-report-daily': {
        isSpecial: true,
        url: '/admin/sales/report/daily?month&year',
        template: 'mvc/view/admin/admin-sales-report-daily.html',
        ctrl: 'AdminSalesReportDailyCtrl'
    },
    'admin-sales-report-distributor': {
        isSpecial: true,
        url: '/admin/sales/report/distributor?month&year',
        template: 'mvc/view/admin/admin-sales-report-distributor.html',
        ctrl: 'AdminSalesReportDistributorCtrl'
    },
    'admin-sales-report-product': {
        isSpecial: true,
        url: '/admin/sales/report/product?month&year&productCategoryId',
        template: 'mvc/view/admin/admin-sales-report-product.html',
        ctrl: 'AdminSalesReportProductCtrl'
    },
    'admin-sales-report-salesman': {
        isSpecial: true,
        url: '/admin/sales/report/salesman?month&year&salesmanId',
        template: 'mvc/view/supervisor/report-sale-salesman-monthly-detail.html',
        ctrl: 'AdminSalesReportSalesmanCtrl'
    },
    'admin-visit-report': {
        isSpecial: true,
        url: '/admin/report/visit',
        template: 'mvc/view/supervisor/report-visit-monthly.html',
        ctrl: 'AdminVisitReportCtrl'
    },
    'admin-visit-report-detail': {
        isSpecial: true,
        url: '/admin/report/visit/detail?fromDate&toDate&salesmanId',
        template: 'mvc/view/supervisor/report-visit-monthly-detail.html',
        ctrl: 'AdminVisitReportDetailCtrl'
    },
    'admin-report-survey': {
        isUsePopup: false,
        listTemplate: 'mvc/view/report/report-survey-list.html',
        listCtrl: 'ReportSurveyCtrl',
        detailTemplate: 'mvc/view/report/report-survey-detail.html',
        detailCtrl: 'ReportSurveyDetailCtrl'
    },
    'admin-report-exhibition': {
        isUsePopup: false,
        listTemplate: 'mvc/view/report/report-exhibition-list.html',
        listCtrl: 'ReportExhibitionCtrl',
        detailTemplate: 'mvc/view/report/report-exhibition-detail.html',
        detailCtrl: 'ReportExhibitionDetailCtrl'
    },
    'admin-order-list': {
        isSpecial: true,
        url: '/admin-order-list?fromDate&toDate&distributorId&page',
        template: 'mvc/view/report/order-list.html',
        ctrl: 'OrderListCtrl'
    },
    'admin-order-detail': {
        isSpecial: true,
        url: '/admin-order-detail/:id',
        template: 'mvc/view/report/order-detail.html',
        ctrl: 'OrderDetailCtrl'
    },
    // Supervisor
    'customer-approve': {
        title: 'menu.approve.customer',
        isUsePopup: false,
        listTemplate: 'mvc/view/supervisor/customer-approve.html',
        listCtrl: 'SUPCustomerApproveCtrl',
        detailTemplate: 'mvc/view/supervisor/customer-approve-detail.html',
        detailCtrl: 'SUPCustomerApproveDetailCtrl'
    },
    'customer-create': {
        isSpecial: true,
        template: 'mvc/view/supervisor/customer-create.html',
        ctrl: 'SUPCustomerCreateCtrl'
    },
    'dashboard-visit': {
        isSpecial: true,
        url: '/dashboard/visit?salesmanid',
        template: 'mvc/view/supervisor/dashboard-visit.html',
        ctrl: 'SUPDashboardVisitCtrl'
    },
    'dashboard-visit-detail': {
        isSpecial: true,
        url: '/dashboard/visit/detail/:id?returnSalesmanId',
        template: 'mvc/view/supervisor/dashboard-visit-detail.html',
        ctrl: 'SUPDashboardVisitDetailCtrl'
    },
    feedback: {
        title: 'menu.feedback',
        isUsePopup: false,
        listTemplate: 'mvc/view/supervisor/feedback-list.html',
        listCtrl: 'SUPFeedbackListCtrl',
        detailTemplate: 'mvc/view/supervisor/feedback-detail.html',
        detailCtrl: 'SUPFeedbackDetailCtrl'
    },
    'homepage-supervisor': {
        isSpecial: true,
        url: '/homepage/supervisor',
        template: 'mvc/view/supervisor/homepage-supervisor.html',
        ctrl: 'SUPHomepageCtrl'
    },
    'orders-approve': {
        title: 'purchase.order',
        isUsePopup: false,
        listTemplate: 'mvc/view/supervisor/order-approve.html',
        listCtrl: 'SUPOrderApproveCtrl',
        detailTemplate: 'mvc/view/supervisor/order-approve-detail.html',
        detailCtrl: 'SUPOrderApproveDetailCtrl'
    },
    'sup-report-sale-salesman-monthly': {
        isSpecial: true,
        url: '/report/sale/salesman/monthly',
        template: 'mvc/view/supervisor/report-sale-salesman-monthly.html',
        ctrl: 'SUPReportSaleSalesmanMonthly'
    },
    'sup-report-sale-salesman-monthly-detail': {
        isSpecial: true,
        url: '/report/sale/salesman/monthly/detail?salesmanId&month&year',
        template: 'mvc/view/supervisor/report-sale-salesman-monthly-detail.html',
        ctrl: 'SUPReportSaleSalesmanMonthlyDetail'
    },
    'sup-report-visit-monthly': {
        isSpecial: true,
        url: '/report/visit/monthly',
        template: 'mvc/view/supervisor/report-visit-monthly.html',
        ctrl: 'SUPReportVisitMonthly'
    },
    'sup-report-visit-monthly-detail': {
        isSpecial: true,
        url: '/report/visit/monthly/detail?fromDate&toDate&distributorId&salesmanId&customerId',
        template: 'mvc/view/supervisor/report-visit-monthly-detail.html',
        ctrl: 'SUPReportVisitMonthlyDetail'
    },
    'supervisor-report-survey': {
        isUsePopup: false,
        listTemplate: 'mvc/view/report/report-survey-list.html',
        listCtrl: 'ReportSurveyCtrl',
        detailTemplate: 'mvc/view/report/report-survey-detail.html',
        detailCtrl: 'ReportSurveyDetailCtrl'
    },
    'supervisor-report-exhibition': {
        isUsePopup: false,
        listTemplate: 'mvc/view/report/report-exhibition-list.html',
        listCtrl: 'ReportExhibitionCtrl',
        detailTemplate: 'mvc/view/report/report-exhibition-detail.html',
        detailCtrl: 'ReportExhibitionDetailCtrl'
    },
    'supervisor-order-list': {
        isSpecial: true,
        url: '/supervisor-order-list?fromDate&toDate&distributorId',
        template: 'mvc/view/report/order-list.html',
        ctrl: 'OrderListCtrl'
    },
    'supervisor-order-detail': {
        isSpecial: true,
        url: '/supervisor-order-detail/:id',
        template: 'mvc/view/report/order-detail.html',
        ctrl: 'OrderDetailCtrl'
    },
    target: {
        isSpecial: true,
        template: 'mvc/view/supervisor/target.html',
        ctrl: 'SUPTargetListCtrl'
    },
    visitschedule: {
        isSpecial: true,
        template: 'mvc/view/supervisor/visit-schedule.html',
        ctrl: 'SUPVisitScheduleCtrl'
    },
    'visit-tracking': {
        isSpecial: true,
        template: 'mvc/view/supervisor/visit-tracking.html',
        ctrl: 'SUPVisitTrackingCtrl'
    }
    // End supervisor
});

//STATE CONFIG
app.constant("STATE_BY_ROLE_CONFIG", {
    sm: [
        'no-function'
    ],
    super: [
        'client',
        'clientconfig'
    ],
    ad: [
        'homepage-admin',
        'user',
        'salesconfig',
        'calendarconfig',
        'uom',
        'productcategory',
        'product',
        'customertype',
        'customer',
        'feedbacktype',
        'supervisetree',
        'distributor',
        'purchaseorder',
        'promotion',
        'survey',
        'clientconfig',
        'exhibition',
        'admin-sales-report',
        'admin-sales-report-daily',
        'admin-sales-report-distributor',
        'admin-sales-report-product',
        'admin-sales-report-salesman',
        'admin-dashboard-visit',
        'admin-dashboard-visit-detail',
        'admin-visit-tracking',
        'admin-visit-report',
        'admin-visit-report-detail',
        'admin-order-list',
        'admin-order-detail',
        'admin-report-survey',
        'admin-report-exhibition'
    ],
    sup: [
        'homepage-supervisor',
        'visitschedule',
        'customer-approve',
        'customer-create',
        'orders',
        'orders-approve',
        'target',
        'dashboard-visit',
        'visit-tracking',
        'sup-report-sale-salesman-monthly',
        'sup-report-visit-monthly',
        'feedback',
        'supervisor-report-survey',
        'supervisor-report-exhibition',
        'supervisor-order-list',
        'supervisor-order-detail'
    ]
});

app.config(function($tokenProvider, $translateProvider, $provide, $urlRouterProvider, $stateProvider, $vtpushProvider, $reportProvider,
        ADDRESS_BACKEND, ADDRESS_OAUTH, ADDRESS_SOCKET, REPORT_SERVER, STATE_CONFIG) {
    
    // Configure $token service for login management
    var defaultParams = {
        client_id: 'vinacomin',
        authorizationEndpoint: (ADDRESS_OAUTH + "oauth/authorize"),
        scope: "read",
        revokeTokenEndpoint: (ADDRESS_OAUTH + "oauth"),
        verifyTokenEndpoint: ADDRESS_OAUTH + 'oauth/userinfo',
        logoutUrl: ADDRESS_OAUTH + 'account/logout'
    };
    $tokenProvider.extendConfig(defaultParams);

    // Configure WebSockets
    $vtpushProvider.set('SOCKET_URL', ADDRESS_SOCKET);

    // Configure file manager
    $provide.decorator('vtFileManagerConfig', function($token, ADDRESS_BACKEND) {
        return {
            uploadUrl: ADDRESS_BACKEND + 'image',

            getDownloadUrl: function(fileId) {
                return ADDRESS_BACKEND + 'image/' + fileId;
            },
            getDeleteUrl: function(fileId) {
                return ADDRESS_BACKEND + 'file?id=' + fileId + '&access_token=' + $token.getAccessToken();
            },
            getHeaders: function() {
                return {
                    Authorization: 'Bearer ' + $token.getAccessToken()
                };
            }
        };
    });
    
    // Configure multi-language support
    $translateProvider.preferredLanguage('vi');
    $translateProvider.useStaticFilesLoader({
        prefix: 'mvc/message/',
        suffix: '.json'
    });
    
    //Config report server
    $reportProvider.set('REPORT_URL', REPORT_SERVER);

    // Configure view states
    $urlRouterProvider.when("", "/");
    $urlRouterProvider.when("/", "/home");
    $urlRouterProvider.otherwise("/404");

    $stateProvider.state('home', { url: '/home' });

    //STATE
    for (var statename in STATE_CONFIG) {
        if (STATE_CONFIG.hasOwnProperty(statename)) {
            var config = STATE_CONFIG[statename];
            if (config.isSpecial) {
                var state = {
                    url: ("/" + statename)
                };

                state.templateUrl = config.template;

                if (config.ctrl != null) {
                    state.controller = config.ctrl;

                    if (config.url != null) {
                        state.url = config.url;
                    }
                }

                $stateProvider.state(statename, state);
            } else {
                var stateList = {
                    url: ('/' + statename),
                    templateUrl: 'mvc/view/common/category-list.html',
                    controller: 'CategoryListCtrl'
                };

                if (config.listCtrl != null) {
                    stateList.controller = config.listCtrl;
                }
                if (config.listTemplate != null) {
                    stateList.templateUrl = config.listTemplate;
                }

                if (config.isUsePopup) {
                    $stateProvider.state(statename, stateList);
                } else {
                    $stateProvider.state(statename + '-list', stateList);

                    $stateProvider.state(statename + '-detail', {
                        url: ("/" + statename + "/:id"),
                        templateUrl: config.detailTemplate,
                        controller: config.detailCtrl
                    });
                }
            }
        }
    }
});

// Configure Idle detector and Auto-ping service to keep session alive
app.config(['KeepaliveProvider', 'IdleProvider', 'NG_IDLE_MAX_IDLE_TIME', 'NG_IDLE_TIMEOUT', 'NG_IDLE_PING_INTEVAL',
        function(KeepaliveProvider, IdleProvider, NG_IDLE_MAX_IDLE_TIME, NG_IDLE_TIMEOUT, NG_IDLE_PING_INTEVAL) {
    // Time out
    IdleProvider.idle(NG_IDLE_MAX_IDLE_TIME);

    // Count down time before logout
    IdleProvider.timeout(NG_IDLE_TIMEOUT);

    // Auto-ping interval
    KeepaliveProvider.interval(NG_IDLE_PING_INTEVAL);
}]);

// GOOGLE MAPS
app.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        //day la key cua haizpt@gmail.com
        key: 'AIzaSyAQV_93Tla8E1wCabgPKk8TwAqg97_poJI', 
        v: '3.17',
        libraries: 'places,drawing,weather,geometry,visualization'
    });
});

app.run(function($rootScope, Idle, NO_AUTH_PATH, FULL_SCREEN_PATH, STATE_BY_ROLE_CONFIG, $location, $token, $translate) {
    // Start listening to user interaction to detect time-out event
    Idle.watch();

    // Notify some view when page ready
    // eg: Remove loading indicator
    $rootScope.loaded = true;

    // Load saved language
    var lang = $token.getUserLanguage();
    if (!angular.isUndefinedOrNull(lang)) {
        $translate.use(lang);
    }

    $rootScope.isNoAuthPage = function () {
        var path = $location.path();
        return _.contains(NO_AUTH_PATH, path) || /^[\/]?error=.*$/.test(path);
    };

    $rootScope.isFullScreenPage = function() {
        var path = $location.path();
        return _.contains(FULL_SCREEN_PATH, path);
    };

    $rootScope.checkStateByRole = function(roleCodes, statename) {
        if (angular.isUndefinedOrNull(roleCodes) || roleCodes.length <= 0 || angular.isUndefinedOrNull(statename)) {
            return false;
        }

        if (statename == '404') {
            return true;
        }

        for (var i = 0; i < roleCodes.length; i++) {
            var roleCode = roleCodes[i];
            if (roleCode != null && STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()] != null) {
                if (STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()].indexOf(statename) >= 0) {
                    return true;
                }

                if (angular.endsWith(statename, '-detail')) {
                    if ( STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()].indexOf(statename.substring(0, statename.length - 7)) >= 0) {
                        return true;
                    }
                }

                if (angular.endsWith(statename, '-list')) {
                    if ( STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()].indexOf(statename.substring(0, statename.length - 5)) >= 0) {
                        return true;
                    }
                }
            }
        }

        return false;
    };

    $rootScope.getHomeState = function(roleCodes) {
        if (angular.isUndefinedOrNull(roleCodes) || roleCodes.length <= 0) {
            return '404';
        }

        for (var i = 0; i < roleCodes.length; i++) {
            var roleCode = roleCodes[i];
            if (roleCode != null) {
                if (STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()] != null && STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()].length > 0) {
                    return STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()][0];
                }
            }
        }

        return '404';
    };

});

app.service('ImageService', function(ADDRESS_BACKEND) {
    this.getImageUrl = function(imageId) {
        if (angular.isUndefinedOrNull(imageId)) {
            return undefined;
        }
        return ADDRESS_BACKEND + 'image/' + imageId;
    }
});