app.controller('ReportExhibitionCtrl', function($scope, $log, $filter, $location, $state, $token, logger, Factory,
                                                ADDRESS_BACKEND) {

    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        $scope.who = path.split("-")[0];
    }

    function init() {
        loadConfig();

        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;

        $scope.tableCtrl = initTableCtrl(reloadData, null, null, null);

        $scope.tableCtrl.refresh();
    }

    function reloadData() {
        $scope.proccessing = true;
        $scope.error = false;

        var queryString = {
            'who': $scope.who,
            'category': 'exhibition',
            'page': $scope.currentPage,
            'size': $scope.itemsPerPage,
            'draft': false
        };

        Factory.doGet(
            queryString,
            function(data) {
                $scope.tableCtrl.records = [];
                angular.copy(data.list, $scope.tableCtrl.records);
                $scope.totalItems = data.count;

                if ($scope.tableCtrl.records == null) {
                    $scope.tableCtrl.records = [];
                    $scope.totalItems = 0;
                }

                // Init status
                for (var i = 0; i < $scope.tableCtrl.records.length; i++) {
                    var record = $scope.tableCtrl.records[i];
                    record.status = $scope.getStatus(record);
                }

                initSelectableArray($scope.tableCtrl.records, null);

                $scope.tableCtrl.updateSelectAllStatus();

                $scope.proccessing = false;
            },
            function(error) {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.getStatus = function(record) {
        var currentDate = new Date();
        if (moment(currentDate).isBefore(record.startDate)) {
            return 0;
        }
        if (moment(record.endDate).isBefore(currentDate)) {
            return 2;
        }
        return 1;
    }

    $scope.refresh = function () {
        init();
    };

    $scope.viewDetail = function (record) {
        $state.go($scope.who + '-report-exhibition-detail', {id: record.id});
    };

    $scope.export = function(record) {
        location.href = ADDRESS_BACKEND + $scope.who + '/report/exhibition/' + record.id + '/export?access_token=' + $token.getAccessToken();
    };

    init();

});