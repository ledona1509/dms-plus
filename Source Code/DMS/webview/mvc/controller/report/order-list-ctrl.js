app.controller('OrderListCtrl', function($scope, $log, $filter, $location, $state, $token, logger, Factory,
                                         ADDRESS_BACKEND, $stateParams) {

    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        $scope.who = path.split("-")[0];
    }

    function init() {
        loadConfig();

        $scope.dateOptions = {
            "year-format": "'yy'",
            "starting-day": 1
        };
        $scope.format = "dd/MM/yyyy";

        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;

        $scope.refresh();
    }

    $scope.refresh = function() {
        $scope.global = {};
        if ($stateParams.fromDate == null && $stateParams.toDate == null && $stateParams.distributorId == null) {
            $scope.global.fromDate = $scope.getFirstDayOfMonth();
            $scope.global.toDate = new Date();

            initDistributors();
        } else {
            $scope.global.fromDate = angular.parseDate($stateParams.fromDate);
            $scope.global.toDate = angular.parseDate($stateParams.toDate);
            if ($scope.global.fromDate == null || $scope.global.toDate == null
                || $scope.global.fromDate.getTime() > $scope.global.toDate.getTime()) {
                $state.go('404');
            } else {
                $scope.global.distributorId = $stateParams.distributorId;

                initDistributors();
            }

        }
    };

    function initDistributors() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.distributors = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'distributor'
            },
            function(data){
                $scope.distributors = data.list;

                if ($scope.distributors == null) {
                    $scope.distributors = [];
                }

                $scope.distributors.unshift({
                    id: null,
                    name: $filter('translate')('order.list.search.distributor.all')
                });

                if ($scope.global.distributorId != null) {
                    var validDistributor = false;
                    for (var i = 0; i < $scope.distributors.length; i++) {
                        if ($scope.distributors[i].id == $scope.global.distributorId) {
                            validDistributor = true;
                            break;
                        }
                    }

                    if (!validDistributor) {
                        $state.go('404');
                        $scope.proccessing = false;
                    } else {
                        $scope.proccessing = false;
                        reloadData();
                    }
                } else {
                    $scope.proccessing = false;
                    reloadData();
                }
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.search = function () {
        if ($scope.global.fromDate == null || $scope.global.toDate == null
            || $scope.global.fromDate.getTime() > $scope.global.toDate.getTime()) {
            logger.logError($filter('translate')('order.list.search.data.invalid'));
        } else {
            $state.go($scope.who + '-order-list', {
                fromDate: angular.formatDate($scope.global.fromDate),
                toDate: angular.formatDate($scope.global.toDate),
                distributorId: $scope.global.distributorId
            }, { reload: true });
        }
    };

    $scope.getDetailLink = function(record) {
        return '#/' + $scope.who + '-order-detail/' + record.id;
    };

    $scope.pageChanged = function() {
        if ($scope.global.fromDate == null || $scope.global.toDate == null
            || $scope.global.fromDate.getTime() > $scope.global.toDate.getTime()) {
            logger.logError($filter('translate')('order.list.search.data.invalid'));
        } else {
            reloadData();
        }
    };

    function reloadData() {
        $scope.proccessing = true;
        $scope.error = false;

        var queryString = {
            'who': $scope.who,
            'category': 'order',
            'page': $scope.currentPage,
            'size': $scope.itemsPerPage
        };

        if (!angular.isUndefinedOrNull($scope.global.distributorId)) {
            queryString.distributorId = $scope.global.distributorId;
        }

        queryString.fromDate = angular.formatDate($scope.global.fromDate);
        queryString.toDate = angular.formatDate($scope.global.toDate);

        Factory.doGet(
            queryString,
            function(data) {
                $scope.records = data.list;
                $scope.totalItems = data.count;

                if ($scope.records == null) {
                    $scope.records = [];
                    $scope.totalItems = 0;
                }

                $scope.proccessing = false;
            },
            function(error) {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.openStart = function($event) {
        return $event.preventDefault(), $event.stopPropagation(), $scope.fromDateOpened = !0
    };

    $scope.openEnd = function($event) {
        return $event.preventDefault(), $event.stopPropagation(), $scope.toDateOpened = !0
    };

    $scope.getFirstDayOfMonth = function() {
        var date = new Date();
        date.setDate(1);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        return date;
    };

    init();

});