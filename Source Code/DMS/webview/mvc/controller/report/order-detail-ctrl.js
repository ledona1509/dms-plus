app.controller('OrderDetailCtrl', function ($scope, $log, $filter, $location, $state, $token, logger, Factory,
                                                ADDRESS_BACKEND, $stateParams) {

    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        $scope.who = path.split("-")[0];
    }

    function init() {
        loadConfig();

        $scope.recordId = $stateParams.id;
        $scope.clientName = $token.getUserInfo().clientName;

        $scope.refresh();
    }

    $scope.refresh = function() {
        loadData($scope.recordId);
    };

    function loadData(id) {
        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'order',
                'id': id
            },
            function(data) {
                $scope.record = data;
                transformAfterLoad();
                $scope.proccessing = false;
            },
            function(error){
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.hasPromotion = function() {
        if (angular.isUndefinedOrNull($scope.record)
            || angular.isUndefinedOrNull($scope.record.promotionResults)
            || $scope.record.promotionResults.length == 0) {
            return false;
        }

        return true;
    };

    function transformAfterLoad() {
        if (!$scope.hasPromotion()) {
            $scope.rewardProducts = undefined;
            return;
        }
        var rewards = [];
        for (var i = 0; i < $scope.record.promotionResults.length; i++) {
            var promotion = $scope.record.promotionResults[i];
            if (promotion.details == undefined || promotion.details.length == 0) {
                continue;
            }
            for (var j = 0; j < promotion.details.length; j++) {
                var detail = promotion.details[j];
                var rewardResult = detail.rewardResult;
                if (rewardResult == undefined
                    || angular.isUndefinedOrNull(rewardResult.quantity)
                    || angular.isUndefinedOrNull(rewardResult.product)
                ) {
                    continue;
                }
                rewards.push(rewardResult);
            }
        }
        $scope.rewardProducts = rewards;
    }

    $scope.getDeliveryTypeName = function (deliveryType) {
        if (deliveryType == 1) {
            return $filter('translate')('purchase.order.delivery.type.in.day');
        } else if (deliveryType == 2) {
            return $filter('translate')('purchase.order.delivery.type.another.day');
        } else {
            return $filter('translate')('purchase.order.delivery.type.immediate');
        }
    };

    $scope.showDate = function(isoDateString) {
        var date =  angular.parseDate(isoDateString);

        if (date == null) {
            date =  angular.parseDateTime(isoDateString);
        }

        if (date == null) {
            return '';
        }

        return moment(date).format('DD/MM/YYYY');

    };

    $scope.printInvoice = function () {
        var popupWin, printContents;
        return printContents = document.getElementById("invoice").innerHTML,
            popupWin = window.open(),
            popupWin.document.open(),
            popupWin.document.write(
                '<html>' +
                    '<head><link rel="stylesheet" type="text/css" href="styles/main.css" /></head>' +
                    '<body onload="window.print()">' +
                    printContents +
                '</html>'),
            popupWin.document.close()
    };

    $scope.back = function () {
        $state.go($scope.who + '-order-list');
    };

    init();

});
