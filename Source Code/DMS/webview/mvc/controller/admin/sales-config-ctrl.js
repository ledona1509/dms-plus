app.controller('SalesConfigCtrl', function($scope, $filter, $token, logger, Factory, PATTERN) {

    function init() {
        $scope.title = $filter('translate')('menu.sales.config');
        $scope.who = 'admin';
        $scope.categoryName = 'salesconfig';

        $scope.pattern = PATTERN;

        $scope.refresh();
    }

    $scope.refresh = function() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.isChanged = false;

        Factory.doGet(
            {
                who: $scope.who,
                category: $scope.categoryName
            },
            function(data){
                $scope.record = data;

                if (angular.isUndefinedOrNull($scope.record)) {
                    $scope.record = {};
                } else {
                    $scope.record.visitDurationKPI = $scope.record.visitDurationKPI / (60 * 1000);
                }

                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    };

    $scope.save = function() {
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            $scope.proccessing = true;
            $scope.error = false;

            var data = angular.copy($scope.record);
            data.visitDurationKPI = $scope.record.visitDurationKPI * 60 * 1000;

            Factory.doPut(
                {
                    who: $scope.who,
                    category: $scope.categoryName
                },
                data,
                function(){
                    $scope.proccessing = false;
                    logger.logSuccess($filter('translate')('save.success'));
                    $scope.refresh();
                },
                function(error) {
                    console.log(error);

                    $scope.proccessing = false;
                    $scope.error = true;
                }
            );
        }
    };

    $scope.markAsChanged = function() {
        $scope.isChanged = true;
    };

    init();
});


