app.controller('ProductDetailCtrl', function ($scope, $filter, $log, logger, $stateParams, $state , Factory, PATTERN) {

    function init() {
        $scope.title = $filter('translate')('menu.product');
        $scope.categoryName = 'product';
        $scope.who = 'admin';

        $scope.recordId = $stateParams.id;
        if ($scope.recordId != 'new') {
            $scope.action = 'edit';
        } else {
            $scope.action = 'new';
        }

        $scope.pattern = PATTERN;

        loadAllProductCategory();
        loadAllUom();

        $scope.refresh();
    }

    $scope.refresh = function() {
        $scope.isChanged = false;

        if ($scope.action == 'edit') {
            loadData($scope.recordId);
        } else {
            $scope.record = {};
            $scope.record.output = 1;
        }
    };

    function loadData(id) {
        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: id
            },
            function(data) {
                $scope.record = data;

                if (!angular.isUndefinedOrNull($scope.record.uom)) {
                    $scope.record.uomId = $scope.record.uom.id;
                }

                if (!angular.isUndefinedOrNull($scope.record.productCategory)) {
                    $scope.record.productCategoryId = $scope.record.productCategory.id;
                }

                $scope.proccessing = false;
            },
            function(error){
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.markAsChanged = function() {
        $scope.isChanged = true;
    };

    $scope.ok = function (isEnable) {
        //Validate
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            $scope.proccessing = true;

            // Edit record
            if ($scope.action === 'edit') {
                Factory.doPut(
                    {
                        'who': $scope.who,
                        'category': $scope.categoryName,
                        'id': $scope.record.id
                    },
                    $scope.record,
                    function () {
                        $scope.proccessing = false;
                        if (isEnable) {
                            enable($scope.record.id);
                        } else {
                            logger.logSuccess($filter('translate')('edit.success'));
                            $scope.refresh();
                        }
                    },
                    function () {
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
            // Create new record
            else {
                Factory.doPost(
                    {
                        'who': $scope.who,
                        'category': $scope.categoryName
                    },
                    $scope.record,
                    function(data) {
                        $scope.proccessing = false;
                        if (isEnable) {
                            enable(data.id);
                        } else {
                            logger.logSuccess($filter('translate')('create.success'));
                            $scope.cancel();
                        }
                    },
                    function(){
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
        }
    };

    $scope.cancel = function () {
        $state.go($scope.categoryName + '-list');
    };

    function loadAllProductCategory() {
        $scope.productCategorys = [];

        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'productcategory',
                'subCategory': 'all'
            },
            function(data) {
                $scope.productCategories = data.list;
                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function loadAllUom() {
        $scope.uoms = [];

        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'uom',
                'subCategory': 'all'
            },
            function(data) {
                $scope.uoms = data.list;
                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.isDraft = function() {
        return $scope.record == null || $scope.record.draft == null ||  $scope.record.draft;
    };

    $scope.isEdit = function() {
        return $scope.action == 'edit';
    };

    $scope.isNew = function() {
        return $scope.action == 'new';
    };

    function enable(domainId) {
        $scope.proccessing = true;

        Factory.doPut(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: domainId,
                action: 'enable'
            },
            null,
            function() {
                $scope.proccessing = false;
                if ($scope.action === 'edit') {
                    $scope.refresh();
                } else {
                    $scope.cancel();
                }
                logger.logSuccess($filter('translate')('enable.success'));

            },
            function() {
                $scope.proccessing = false;
                logger.logError($filter('translate')('enable.error'));
            }
        );
    }

    init();

});



