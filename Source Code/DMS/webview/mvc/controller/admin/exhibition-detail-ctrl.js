app.controller('ExhibitionDetailCtrl', function ($scope, $log, $filter, $location, $state, logger, $modal, $stateParams, Factory, PATTERN) {

    function init() {
        $scope.title = $filter('translate')('menu.exhibition');
        $scope.categoryName = 'exhibition';
        $scope.who = 'admin';

        $scope.recordId = $stateParams.id;
        if ($scope.recordId != 'new') {
            $scope.action = 'edit';
        } else {
            $scope.action = 'new';
        }

        $scope.pattern = PATTERN;

        $scope.refresh();
    }

    $scope.refresh = function() {
        $scope.isChanged = false;

        if ($scope.action == 'edit') {
            loadData($scope.recordId);
        } else {
            $scope.record = {};
            $scope.record.startDate = { date: new Date(), opened: false };
            $scope.record.endDate = { date: new Date(), opened: false };
            $scope.record.categories = [];
        }
    };

    function loadData(id) {
        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id:id
            },
            function(data) {
                $scope.record = data;

                transformAfterLoad();

                $scope.proccessing = false;
            },
            function(error){
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.markAsChanged = function() {
        $scope.isChanged = true;
    };

    $scope.ok = function (isEnable) {
        //Validate
        if (!$scope.form.$valid || !checkValidBeforeSave()) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            $scope.proccessing = true;

            transformBeforeSave();

            // Edit record
            if ($scope.action === 'edit') {
                Factory.doPut(
                    {
                        who: $scope.who,
                        category: $scope.categoryName,
                        id: $scope.record.id
                    },
                    $scope.record,
                    function () {
                        $scope.proccessing = false;
                        if (isEnable) {
                            enable($scope.record.id);
                        } else {
                            logger.logSuccess($filter('translate')('edit.success'));
                            $scope.refresh();
                        }
                    },
                    function () {
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                        transformAfterLoad();
                    }
                );
            }
            // Create new record
            else {
                Factory.doPost(
                    {
                        who: $scope.who,
                        category: $scope.categoryName
                    },
                    $scope.record,
                    function(data) {
                        $scope.proccessing = false;
                        if (isEnable) {
                            enable(data.id);
                        } else {
                            logger.logSuccess($filter('translate')('create.success'));
                            $scope.cancel();
                        }
                    },
                    function(){
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                        transformAfterLoad();
                    }
                );
            }
        }
    };

    $scope.cancel = function () {
        $state.go($scope.categoryName + '-list');
    };

    $scope.isDraft = function() {
        return $scope.record == null || $scope.record.draft == null ||  $scope.record.draft;
    };

    $scope.isEdit = function() {
        return $scope.action == 'edit';
    };

    $scope.isNew = function() {
        return $scope.action == 'new';
    };

    function enable(domainId) {
        $scope.proccessing = true;

        Factory.doPut(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: domainId,
                action: 'enable'
            },
            null,
            function() {
                $scope.proccessing = false;
                if ($scope.action === 'edit') {
                    $scope.refresh();
                } else {
                    $scope.cancel();
                }
                logger.logSuccess($filter('translate')('enable.success'));

            },
            function() {
                $scope.proccessing = false;
                logger.logError($filter('translate')('enable.error'));
            }
        );
    }

    $scope.createCategory = function() {
        var modalInstance = $modal.open({
            templateUrl: 'mvc/view/admin/exhibition-category-detail-popup.html',
            controller: 'ExhibitionCategoryDetailPopupCtrl',
            resolve: {
                category: function () {
                    return null;
                }
            }
        });
        modalInstance.result.then(function (category) {
            $scope.record.categories.push(category);
        }, function () {
        });

        $scope.markAsChanged();
    };

    $scope.editCategory = function(category) {
        var modalInstance = $modal.open({
            templateUrl: 'mvc/view/admin/exhibition-category-detail-popup.html',
            controller: 'ExhibitionCategoryDetailPopupCtrl',
            resolve: {
                category: function () {
                    return category;
                }
            }
        });
        modalInstance.result.then(function () {
        }, function () {
        });

        $scope.markAsChanged();
    };

    $scope.deleteCategory = function(category) {
        $scope.record.categories.splice($scope.record.categories.indexOf(category), 1);
        $scope.markAsChanged();
    };

    $scope.open = function($event, data) {
        $event.preventDefault();
        $event.stopPropagation();
        data.opened = true;
        $scope.markAsChanged();
    };

    function transformAfterLoad() {
        if ($scope.record.startDate == null) {
            $scope.record.startDate = { date: new Date(), opened: false };
        } else {
            var startDateTxt = $scope.record.startDate;
            $scope.record.startDate = { date: new Date(startDateTxt), opened: false };
        }

        if ($scope.record.endDate == null) {
            $scope.record.endDate = { date: new Date(), opened: false };
        } else {
            var endDateTxt = $scope.record.endDate;
            $scope.record.endDate = { date: new Date(endDateTxt), opened: false };
        }

        if (angular.isUndefinedOrNull($scope.record.category)) {
            $scope.record.category = [];
        } else {
            for (var i = 0; i < $scope.record.category.length; i++) {
                var category = $scope.record.category[i];
                if (angular.isUndefinedOrNull(category.items)) {
                    category.items = [];
                }
            }
        }
    }

    function checkValidBeforeSave() {
        if (angular.isUndefinedOrNull($scope.record.categories) || $scope.record.categories.length == 0) {
            return false;
        } else {
            for (var i = 0; i < $scope.record.categories.length; i++) {
                var category = $scope.record.categories[i];
                if (angular.isUndefinedOrNull(category.items) || category.items.length == 0) {
                    return false;
                }
            }
        }

        return true;
    }

    function transformBeforeSave() {
        var date = $scope.record.startDate.date;
        $scope.record.startDate = getFormattedDate(date);

        date = $scope.record.endDate.date;
        $scope.record.endDate = getFormattedDate(date);
    }

    function getFormattedDate(date) {
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return year + '-' + month + '-' + day;
    }

    init();

});



