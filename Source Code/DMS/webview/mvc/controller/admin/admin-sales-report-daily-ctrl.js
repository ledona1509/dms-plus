app.controller('AdminSalesReportDailyCtrl', function ($scope, $log, $filter, $state, $stateParams, logger, Factory) {
    function init() {
        $scope.global = {};
        $scope.global.month = $stateParams.month;
        $scope.global.year = $stateParams.year;

        var date = new Date();
        date.setMonth($scope.global.month);
        date.setYear($scope.global.year);
        $scope.global.monthDisplay = moment(date).format('MM/YYYY');

        reloadData();
    }

    function reloadData() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.records = [];
        $scope.total = {
            revenue: 0,
            nbOrder: 0,
            nbDistributor: 0,
            nbCustomer: 0,
            nbSalesman: 0
        };

        var queryParams = {
            'who': 'admin',
            'category': 'report',
            'subCategory': 'sales',
            'param': 'daily',
            'month': $scope.global.month,
            'year': $scope.global.year
        };

        Factory.doGet(
            queryParams,
            function (data) {
                $scope.records = data.list;

                if (angular.isUndefinedOrNull($scope.records)) {
                    $scope.records = [];
                }

                loadChartData($scope.records);

                $scope.proccessing = false;
            },
            function () {
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.refresh = function () {
        reloadData();
    };

    $scope.displayIsoDate = function(isoDate) {
        return angular.displayIsoDate(isoDate);
    };

    function tooltip(label, xval, yval, flotItem) {
        return $filter('translate')('admin.sales.report.date') + ' ' + xval + ': ' + $filter('number')(yval, 0);
    }

    function loadChartData(data) {
        var revenueData = [];
        var nbOrderDate = [];
        if (data != null) {
            for (var i = 0; i < data.length; i++) {
                revenueData.push( [ i + 1, data[i].revenue ] );
                nbOrderDate.push( [ i + 1, data[i].nbOrder ] );
                $scope.total = {
                    revenue: $scope.total.revenue + data[i].revenue,
                    nbOrder: $scope.total.nbOrder + data[i].nbOrder
                };
            }
        }

        $scope.chart = {};
        $scope.chart.data = [
            {data: revenueData, label: $filter('translate')('admin.sales.report.revenue')},
            { data: nbOrderDate, label: $filter('translate')('admin.sales.report.number.order'), lines: {fill: !1}, yaxis: 2}
        ];
        $scope.chart.options = {
            series: {
                lines: {
                    show: !0,
                    fill: !0,
                    fillColor: {
                        colors: [{
                            opacity: 0
                        }, {
                            opacity: .3
                        }]
                    }
                },
                points: {
                    show: !0,
                    lineWidth: 2,
                    fill: !0,
                    fillColor: "#ffffff",
                    symbol: "circle",
                    radius: 5
                }
            },
            colors: ["#31C0BE", "#8170CA", "#E87352"],
            tooltip: {
                show: true,
                content: tooltip
            },
            tooltipOpts: {
                defaultTheme: !1
            },
            grid: {
                hoverable: !0,
                clickable: !0,
                tickColor: "#f9f9f9",
                borderWidth: 1,
                borderColor: "#eeeeee"
            },
            yaxes: [ {}, {
                position: "right"
            }]
        };
    }

    init();

});
