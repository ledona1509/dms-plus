app.controller('UserCtrl', function($scope, $log, $filter, $location, $state, logger, $modal, Factory, STATE_CONFIG) {

    function init() {
        var columns = [
            { header: 'username', property: 'username' },
            { header: 'fullname', property: 'fullname' }
        ];

        initScopeForCategoryList($scope, $log, $filter, $location, $state, logger, $modal, Factory, STATE_CONFIG, columns, null);

        $scope.init();
    }

    init();

});
