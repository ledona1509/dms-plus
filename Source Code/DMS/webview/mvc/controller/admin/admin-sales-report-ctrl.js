app.controller('AdminSalesReportCtrl', function($scope, $log, $filter, $token, $location, $state, $stateParams,
                                                        logger, Factory) {

    function init() {
        $scope.currentUserId = $token.getUserInfo().id;

        $scope.global = {};

        $scope.global.currentMonth = {};
        $scope.global.currentMonth.date = new Date();
        $scope.global.currentMonth.opened = false;

        $scope.global.productCategoryId = null;

        $scope.global.salesmanId = 'all';

        $scope.global.reportType = "daily";

        loadAllProductCategory();
        loadSalesmen();
    }

    $scope.open = function($event, data) {
        $event.preventDefault();
        $event.stopPropagation();
        data.opened = true;
    };

    $scope.refresh = function() {
        init();
    };

    $scope.report = function() {
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            if ($scope.global.reportType == "daily") {
                $state.go('admin-sales-report-daily',
                    {
                        month: $scope.global.currentMonth.date.getMonth(),
                        year: $scope.global.currentMonth.date.getFullYear()
                    });
            } else if ($scope.global.reportType == "distributor") {
                $state.go('admin-sales-report-distributor',
                    {
                        month: $scope.global.currentMonth.date.getMonth(),
                        year: $scope.global.currentMonth.date.getFullYear()
                    });
            } else if ($scope.global.reportType == "product") {
                $state.go('admin-sales-report-product',
                    {
                        month: $scope.global.currentMonth.date.getMonth(),
                        year: $scope.global.currentMonth.date.getFullYear(),
                        productCategoryId: $scope.global.productCategoryId
                    });
            } else if ($scope.global.reportType == "salesman") {
                $state.go('admin-sales-report-salesman',
                    {
                        month: $scope.global.currentMonth.date.getMonth(),
                        year: $scope.global.currentMonth.date.getFullYear(),
                        salesmanId: $scope.global.salesmanId
                    });
            }
        }
    };

    function loadAllProductCategory() {
        $scope.productCategorys = [];

        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                'who': 'admin',
                'category': 'productcategory',
                'subCategory': 'all'
            },
            function(data) {
                $scope.productCategories = data.list;
                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function loadSalesmen() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.salesmen = [];

        Factory.doGet(
            {
                'who': 'admin',
                'category': 'salesman'
            },
            function(data){
                $scope.salesmen = data.list;

                if (angular.isUndefinedOrNull($scope.salesmen)) {
                    $scope.salesmen = [];
                }

                $scope.salesmen.unshift({
                    id: 'all',
                    fullname: '-- ' + $filter('translate')('report.salesman.sale.monthly.all') + ' --'
                });

                $scope.global.currentSalesmanId = 'all';

                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    init();

});
