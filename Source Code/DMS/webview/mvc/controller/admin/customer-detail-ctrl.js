app.controller('CustomerDetailCtrl', function ($scope, $filter, $log, logger, $stateParams, $state , Factory) {

    function init() {
        $scope.title = $filter('translate')('menu.customer');
        $scope.categoryName = 'customer';
        $scope.who = 'admin';

        $scope.recordId = $stateParams.id;
        if ($scope.recordId != 'new') {
            $scope.action = 'edit';
        } else {
            $scope.action = 'new';
        }

        loadCustomerTypes();
        loadDistributor();

        $scope.refresh();
    }

    $scope.refresh = function() {
        $scope.isChanged = false;

        if ($scope.action == 'edit') {
            loadData($scope.recordId);
        } else {
            $scope.record = {};
            $scope.record.output = 1;

            initMap();
        }
    };

    function loadData(id) {
        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': $scope.categoryName,
                id: id
            },
            function(data) {
                $scope.record = data;

                if (!angular.isUndefinedOrNull($scope.record.customerType)) {
                    $scope.record.customerTypeId = $scope.record.customerType.id;
                }

                if (!angular.isUndefinedOrNull($scope.record.distributor)) {
                    $scope.record.distributorId = $scope.record.distributor.id;
                }

                initMap();

                $scope.proccessing = false;
            },
            function(error){
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.markAsChanged = function() {
        $scope.isChanged = true;
    };

    $scope.ok = function (isEnable) {
        //Validate
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            $scope.proccessing = true;

            // Edit record
            if ($scope.action === 'edit') {
                Factory.doPut(
                    {
                        'who': $scope.who,
                        'category': $scope.categoryName,
                        'id': $scope.record.id
                    },
                    $scope.record,
                    function () {
                        $scope.proccessing = false;
                        if (isEnable) {
                            enable($scope.record.id);
                        } else {
                            logger.logSuccess($filter('translate')('edit.success'));
                            $scope.refresh();
                        }
                    },
                    function (error) {
                        $scope.proccessing = false;
                        if (error.data.meta.error_message == 'customer.has.pending.order') {
                            logger.logError($filter('translate')('customer.has.order.pending'));
                        } else {
                            logger.logError($filter('translate')('save.error'));
                        }
                    }
                );
            }
            // Create new record
            else {
                Factory.doPost(
                    {
                        'who': $scope.who,
                        'category': $scope.categoryName
                    },
                    $scope.record,
                    function(data) {
                        $scope.proccessing = false;
                        if (isEnable) {
                            enable(data.id);
                        } else {
                            logger.logSuccess($filter('translate')('create.success'));
                            $scope.cancel();
                        }
                    },
                    function(){
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
        }
    };

    $scope.cancel = function () {
        $state.go($scope.categoryName + '-list');
    };

    $scope.isDraft = function() {
        return $scope.record == null || $scope.record.draft == null ||  $scope.record.draft;
    };

    $scope.isEdit = function() {
        return $scope.action == 'edit';
    };

    $scope.isNew = function() {
        return $scope.action == 'new';
    };

    function enable(domainId) {
        $scope.proccessing = true;

        Factory.doPut(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: domainId,
                action: 'enable'
            },
            null,
            function() {
                $scope.proccessing = false;
                if ($scope.action === 'edit') {
                    $scope.refresh();
                } else {
                    $scope.cancel();
                }
                logger.logSuccess($filter('translate')('enable.success'));

            },
            function() {
                $scope.proccessing = false;
                logger.logError($filter('translate')('enable.error'));
            }
        );
    }

    function loadCustomerTypes() {
        $scope.proccessing = true;
        $scope.customerTypes = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'customertype',
                'subCategory': 'all'
            },
            function(data) {
                $scope.customerTypes = data.list;
                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $scope.proccessing = false;
                $scope.cancel();
            }
        );
    }

    function loadDistributor() {
        $scope.proccessing = true;
        $scope.distributors = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'distributor',
                'subCategory': 'all'
            },
            function(data) {
                $scope.distributors = data.list;
                if ($scope.distributors == null) {
                    $scope.distributors = [];
                }
                $scope.distributors.unshift( { id: null, name: $filter('translate')('no.distributor') } )

                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $scope.proccessing = false;
                $scope.cancel();
            }
        );
    }

    function initMap() {
        var defaultCenter = {latitude: 21.084269026984, longitude: 105.82028125};
        var coords = {};// --> Ha Noi

        if (!angular.isUndefined($scope.record)
            && !angular.isUndefined($scope.record.latitude) && angular.isNumber($scope.record.latitude)
            && !angular.isUndefined($scope.record.longitude) && angular.isNumber($scope.record.longitude)) {

            defaultCenter = { latitude: $scope.record.latitude, longitude: $scope.record.longitude };
            coords = { latitude: $scope.record.latitude, longitude: $scope.record.longitude };
        }

        $scope.map = {
            center: defaultCenter,
            events: {
                click: function (mapModel, eventName, originalEventArgs) {
                    var e = originalEventArgs[0];
                    var lat = e.latLng.lat(),
                        lng = e.latLng.lng();

                    $scope.record.latitude = lat;
                    $scope.record.longitude = lng;

                    $scope.marker.coords.latitude = lat;
                    $scope.marker.coords.longitude = lng;

                    $scope.markAsChanged();
                }
            },
            zoom: 13
        };

        $scope.marker = {
            id: 0,
            title: 'customer',
            coords: coords,
            options: { draggable: true },
            windowOptions: {
                visible: false
            },
            events: {
                dragend: function (marker, eventName, args) {
                    var lat = marker.getPosition().lat();
                    var lng = marker.getPosition().lng();

                    $scope.record.latitude = lat;
                    $scope.record.longitude = lng;

                    $scope.markAsChanged();
                }
            }
        };
    }

    init();

});



