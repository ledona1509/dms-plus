app.controller('UserDetailPopupCtrl', function ($scope, $modalInstance, $filter, logger, record, categoryName, config, Factory) {

    function init() {
        initScopeForCategoryDetailPopup($scope, $modalInstance, $filter, logger, record, categoryName, config, Factory,
            null, initRoles);

        $scope.init();
    }

    $scope.resetPassword = function() {
        $scope.proccessing = true;

        Factory.doPut(
            {
                'who': $scope.who,
                'category': 'user',
                'id': $scope.record.id,
                'action': 'resetpassword'
            },
            {}
            ,
            function () {
                $scope.proccessing = false;
                logger.logSuccess($filter('translate')('reset.password.success'));
            },
            function () {
                $scope.proccessing = false;
                logger.logError($filter('translate')('reset.password.error'));
            }
        );
    };

    $scope.getRoleDisplay = function() {
        if ($scope.record.role != null && $scope.roles != null) {
            for (var i = 0; i < $scope.roles.length; i++) {
                if ($scope.roles[i].code === $scope.record.role) {
                    return $scope.roles[i].name;
                }
            }
        }

        return null;
    };

    function initRoles() {
        $scope.roles = [];
        $scope.proccessing = true;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'role'
            },
            function(data) {
                $scope.roles = data.list;
                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $scope.proccessing = false;
                $scope.cancel();
            }
        );
    }

    init();

});
