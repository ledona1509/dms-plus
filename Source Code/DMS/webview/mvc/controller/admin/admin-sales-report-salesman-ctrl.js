app.controller('AdminSalesReportSalesmanCtrl', function ($scope, $log, $filter, $state, $stateParams,
                                                               logger, Factory) {

    function init() {
        $scope.global = {};
        $scope.global.currentSalesmanId = $stateParams.salesmanId;
        $scope.global.month = $stateParams.month;
        $scope.global.year = $stateParams.year;

        reloadData();
    }

    function reloadData() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.records = [];

        var queryParams = {
            'who': 'admin',
            'category': 'report',
            'subCategory': 'sales',
            'param': 'salesman',
            'month': $scope.global.month,
            'year': $scope.global.year
        };

        if ($scope.global.currentSalesmanId != 'all') {
            queryParams.salesmanId = $scope.global.currentSalesmanId;
        }

        Factory.doGet(
            queryParams,
            function (data) {
                $scope.records = data.list;

                if (angular.isUndefinedOrNull($scope.records)) {
                    $scope.records = [];
                }

                $scope.proccessing = false;
            },
            function () {
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.refresh = function () {
        reloadData();
    };

    $scope.back = function () {
        $state.go('admin-sales-report');
    };

    $scope.getMonthDisplay = function () {
        var month = parseInt($scope.global.month) + 1;

        return $filter('translate')('report.salesman.sale.monthly.detail.month') + ' ' + month
            + '/' + $scope.global.year;
    };

    $scope.printReport = function () {
        var popupWin, printContents;
        return printContents = document.getElementById("report").innerHTML,
            popupWin = window.open(),
            popupWin.document.open(),
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="styles/main.css" /><style type="text/css" media="print">@page { size: landscape; } </style></head><body onload="window.print()">' + printContents + "</html>"),
            popupWin.document.close()
    };

    init();

});
