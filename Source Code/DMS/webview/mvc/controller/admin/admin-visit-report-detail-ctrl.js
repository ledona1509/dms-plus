app.controller('AdminVisitReportDetailCtrl', function ($scope, $log, $filter, $state, $stateParams, logger, Factory) {

    function init() {
        $scope.global = {};
        $scope.global.fromDate = $stateParams.fromDate;
        $scope.global.toDate = $stateParams.toDate;
        $scope.global.currentSalesmanId = $stateParams.salesmanId;

        reloadData();
    }

    function reloadData() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.records = [];

        var queryParams = {
            'who': 'admin',
            'category': 'report',
            'subCategory': 'visit',
            'fromDate': $scope.global.fromDate,
            'toDate': $scope.global.toDate,
            'salesmanId': $scope.global.currentSalesmanId
        };

        Factory.doGet(
            queryParams,
            function (data) {
                $scope.records = data.list;

                if (angular.isUndefinedOrNull($scope.records)) {
                    $scope.records = [];
                }

                $scope.proccessing = false;
            },
            function () {
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.refresh = function () {
        reloadData();
    };

    $scope.back = function () {
        $state.go('admin-visit-report');
    };

    $scope.getDateDisplay = function () {
        var fromDateO = angular.parseDate($scope.global.fromDate);
        var toDateO = angular.parseDate($scope.global.toDate);

        return $filter('translate')('report.visit.monthly.from.date') + ' ' + moment(fromDateO).format('DD/MM/YYYY')
            + ' '
            + $filter('translate')('report.visit.monthly.to.date') + ' ' + moment(toDateO).format('DD/MM/YYYY');
    };

    $scope.printReport = function () {
        var popupWin, printContents;
        return printContents = document.getElementById("report").innerHTML,
            popupWin = window.open(),
            popupWin.document.open(),
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="styles/main.css" /><style type="text/css" media="print">@page { size: landscape; } thead {display: table-header-group;}</style></head><body onload="window.print()">' + printContents + "</html>"),
            popupWin.document.close()
    };

    init();

});
