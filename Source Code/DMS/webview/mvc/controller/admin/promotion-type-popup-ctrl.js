app.controller('PromotionTypePopupCtrl', function ($scope, $log, $modalInstance, $filter, logger, promotionType, Factory, PATTERN) {

    function init() {
        if (promotionType == null) {
            $log.log("promotion type is null");
            $modalInstance.close();
        }

        $scope.title = promotionType.name;
        $scope.who = 'admin';
        $scope.promotionType = promotionType;

        $scope.pattern = PATTERN;

        initDetailMap();

        initProducts();
    }

    $scope.ok = function () {
        //Validate
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            $scope.promotionType.details = [];
            for (var i = 0; i < $scope.details.length; i++) {
                var detail = $scope.details[i];
                if ($scope.isUse(detail)) {
                    if (detail.reward.product != null && detail.reward.product.id != null) {
                        detail.reward.product = $scope.productMap[detail.reward.product.id];
                    }
                    $scope.promotionType.details.push(detail);
                }
            }

            $modalInstance.close();
        }
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.getNumberRewardColumn = function() {
        var numberColumn = 0;
        if ($scope.isDisplayRewardProduct()) {
            numberColumn++;
        }
        if ($scope.isDisplayRewardQuantity()) {
            numberColumn++;
        }
        if ($scope.isDisplayRewardPercentage()) {
            numberColumn++;
        }

        return numberColumn;
    };

    $scope.isDisplayRewardProduct = function() {
        return $.inArray($scope.promotionType.type, [1]) >= 0;
    };

    $scope.isDisplayRewardQuantity = function() {
        return $.inArray($scope.promotionType.type, [1]) >= 0;
    };

    $scope.isDisplayRewardPercentage = function() {
        return $.inArray($scope.promotionType.type, [0]) >= 0;
    };

    $scope.isUse = function(detail) {
        return detail.condition.quantity > 0;
    };

    $scope.sortDetail = function(detail) {
        if ($scope.isUse(detail)) {
            return 0;
        }

        return 1;
    };

    function initDetailMap() {
        $scope.detailMap = {};

        if ($scope.promotionType.details != null) {
            for (var i = 0; i < $scope.promotionType.details.length; i++) {
                var detail = $scope.promotionType.details[i];
                if (detail !=  null && detail.condition != null && detail.condition.product != null && detail.condition.product.id){
                    $scope.detailMap[detail.condition.product.id] = detail;
                }
            }
        }
    }

    function initProducts() {
        $scope.proccessing = true;

        $scope.productMap = {};
        $scope.products = [];
        $scope.details = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'product',
                'subCategory': 'all'
            },
            function(data) {
                $scope.products = data.list;
                if ($scope.products != null) {
                    for (var i = 0; i < $scope.products.length; i++) {
                        var product = $scope.products[i];
                        $scope.productMap[product.id] = product;
                        var detail = $scope.detailMap[product.id];
                        if (detail != null) {
                            $scope.details.push(detail);
                        } else {
                            $scope.details.push(
                                {
                                    type: $scope.promotionType.type,
                                    condition: { product: product, quantity: null },
                                    reward: {}
                                }
                            );
                        }
                    }
                }

                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $scope.proccessing = false;
                $scope.cancel();
            }
        );
    }

    init();

});
