app.controller('DistributorDetailCtrl', function ($scope, $filter, $log, logger, $stateParams, $state , Factory) {

    function init() {
        $scope.title = $filter('translate')('menu.distributor');
        $scope.categoryName = 'distributor';
        $scope.who = 'admin';

        $scope.recordId = $stateParams.id;
        if ($scope.recordId != 'new') {
            $scope.action = 'edit';
        } else {
            $scope.action = 'new';
        }

        loadSupervisors();

        $scope.refresh();
    }

    $scope.refresh = function() {
        $scope.isChanged = false;

        if ($scope.action == 'edit') {
            loadData($scope.recordId);
        } else {
            $scope.record = {};
        }
    };

    function loadData(id) {
        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': $scope.categoryName,
                id: id
            },
            function(data) {
                $scope.record = data;

                if (!angular.isUndefinedOrNull($scope.record.supervisor)) {
                    $scope.record.supervisorId = $scope.record.supervisor.id;
                }

                if (!$scope.isDraft()) {
                    loadSalesmans($scope.recordId);
                }

                $scope.proccessing = false;
            },
            function(error){
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.markAsChanged = function() {
        $scope.isChanged = true;
    };

    $scope.ok = function (isEnable) {
        //Validate
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            $scope.proccessing = true;

            // Edit record
            if ($scope.action === 'edit') {
                Factory.doPut(
                    {
                        'who': $scope.who,
                        'category': $scope.categoryName,
                        'id': $scope.record.id
                    },
                    $scope.record,
                    function () {
                        $scope.proccessing = false;
                        if (isEnable) {
                            enable($scope.record.id);
                        } else {
                            logger.logSuccess($filter('translate')('edit.success'));
                            $scope.refresh();
                        }
                    },
                    function () {
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
            // Create new record
            else {
                Factory.doPost(
                    {
                        'who': $scope.who,
                        'category': $scope.categoryName
                    },
                    $scope.record,
                    function(data) {
                        $scope.proccessing = false;
                        if (isEnable) {
                            enable(data.id);
                        } else {
                            logger.logSuccess($filter('translate')('create.success'));
                            $scope.cancel();
                        }
                    },
                    function(){
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
        }
    };

    $scope.cancel = function () {
        $state.go($scope.categoryName + '-list');
    };

    $scope.isDraft = function() {
        return $scope.record == null || $scope.record.draft == null ||  $scope.record.draft;
    };

    $scope.isEdit = function() {
        return $scope.action == 'edit';
    };

    $scope.isNew = function() {
        return $scope.action == 'new';
    };

    function enable(domainId) {
        $scope.proccessing = true;

        Factory.doPut(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: domainId,
                action: 'enable'
            },
            null,
            function() {
                $scope.proccessing = false;
                if ($scope.action === 'edit') {
                    $scope.refresh();
                } else {
                    $scope.cancel();
                }
                logger.logSuccess($filter('translate')('enable.success'));

            },
            function() {
                $scope.proccessing = false;
                logger.logError($filter('translate')('enable.error'));
            }
        );
    }

    function loadSupervisors() {
        $scope.proccessing = true;
        $scope.supervisors = [];

        Factory.doGet(
            {
                who: $scope.who,
                category: 'user',
                subCategory: 'supervisor'
            },
            function(data) {
                $scope.supervisors = data.list;
                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $scope.proccessing = false;
                $scope.cancel();
            }
        );
    }

    function loadSalesmans(distributorId) {
        $scope.proccessing = true;
        $scope.salesmen = [];
        Factory.doGet(
            {
                who: $scope.who,
                category: 'user',
                subCategory: 'salesman',
                param: 'withavailable',
                distributorId: distributorId
            },
            function(data) {
                $scope.salesmen = data.list;
                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $scope.proccessing = false;
                $scope.cancel();
            }
        );
    }

    init();

});



