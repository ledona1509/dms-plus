app.controller('AdminSalesReportDistributorCtrl', function ($scope, $log, $filter, $state, $stateParams, logger, Factory) {
    function init() {
        $scope.global = {};
        $scope.global.month = $stateParams.month;
        $scope.global.year = $stateParams.year;

        var date = new Date();
        date.setMonth($scope.global.month);
        date.setYear($scope.global.year);
        $scope.global.monthDisplay = moment(date).format('MM/YYYY');

        reloadData();
    }

    function reloadData() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.records = [];

        var queryParams = {
            'who': 'admin',
            'category': 'report',
            'subCategory': 'sales',
            'param': 'distributor',
            'month': $scope.global.month,
            'year': $scope.global.year
        };

        Factory.doGet(
            queryParams,
            function (data) {
                $scope.records = data.list;

                if (angular.isUndefinedOrNull($scope.records)) {
                    $scope.records = [];
                }

                loadChartData($scope.records);

                $scope.proccessing = false;
            },
            function () {
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.refresh = function () {
        reloadData();
    };

    $scope.displayIsoDate = function(isoDate) {
        return angular.displayIsoDate(isoDate);
    };

    function tooltip(label, xval, yval, flotItem) {
        return $scope.global.distributorName[xval - 1][1] + ': ' + $filter('number')(yval, 0);
    }

    function loadChartData(data) {
        var revenueData = [];
        var nbOrderDate = [];
        $scope.global.distributorName = [];
        if (data != null) {
            for (var i = 0; i < data.length; i++) {
                revenueData.push( [ i + 1, data[i].revenue ] );
                nbOrderDate.push( [ i + 1, data[i].nbOrder ] );
                $scope.global.distributorName.push( [ i + 1, angular.escapeHTML(data[i].name) ] );
            }
        }

        $scope.chart = {};
        $scope.chart.data = [
            {
                data: revenueData,
                label: $filter('translate')('admin.sales.report.revenue'),
                bars: { show: !0,
                    fill: 1,
                    barWidth: .3,
                    align: "center",
                    horizontal: !1,
                    order: 1
                },
                xaxis: 1
            },
            {   data: nbOrderDate,
                label: $filter('translate')('admin.sales.report.number.order'),
                lines: {
                    show: !0
                },
                points: {
                    show: !0,
                    lineWidth: 2,
                    fill: !0,
                    fillColor: "#ffffff",
                    symbol: "circle",
                    radius: 5
                },
                xaxis: 1,
                yaxis: 2
            }
        ];
        $scope.chart.options = {
            colors: ["#31C0BE", "#8170CA", "#E87352"],
            tooltip: {
                show: true,
                content: tooltip
            },
            tooltipOpts: {
                defaultTheme: !1
            },
            grid: {
                hoverable: !0,
                clickable: !0,
                tickColor: "#f9f9f9",
                borderWidth: 1,
                borderColor: "#eeeeee"
            },
            xaxis: {
                ticks: $scope.global.distributorName
            },
            yaxes: [ {}, {
                position: "right"
            }]
        };
    }

    init();

});
