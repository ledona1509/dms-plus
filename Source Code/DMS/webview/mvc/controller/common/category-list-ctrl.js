app.controller('CategoryListCtrl', function($scope, $log, $filter, $location, $state, logger, $modal, Factory, STATE_CONFIG) {

    function init() {
        initScopeForCategoryList($scope, $log, $filter, $location, $state, logger, $modal, Factory, STATE_CONFIG, null, null);

        $scope.init();
    }

    init();

});
