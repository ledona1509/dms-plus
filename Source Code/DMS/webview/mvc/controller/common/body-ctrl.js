app.controller('BodyCtrl', function ($rootScope, $scope, $token, $log, $location, Idle, Keepalive, $modal,
        $http, ADDRESS_PING, $translate) {

    $scope.main = {
        brand: 'DMS'
    };

    $scope.lang = 'vi';

    function closeModals() {
        if ($scope.warning) {
            $scope.warning.close();
            $scope.warning = null;
        }

        if ($scope.timedout) {
            $scope.timedout.close();
            $scope.timedout = null;
        }
    }

    $scope.$on('IdleStart', function() {
        closeModals();
        $scope.warning = $modal.open({
            templateUrl: 'template/logout-warning-dialog.html',
            controller: 'TimeoutWarnCtrl',
            windowClass: 'modal-danger'
        });
    });

    $scope.$on('IdleEnd', function() {
        closeModals();
    });

    $scope.$on('IdleTimeout', function() {
        closeModals();
        $scope.timedout = $modal.open({
            templateUrl: 'template/loggedout-dialog.html',
            windowClass: 'modal-danger'
        });
        $token.logout();
    });

    $scope.$on('Keepalive', function() {
        $http.get(ADDRESS_PING)
            .success(function(pingResult) {
                if (pingResult.sessionAlive === true) {
                    $log.debug("Session alive");
                } else {
                    $log.error("Session is not alive");
                }
            })
            .error(function(error) {
                $log.error("Cannot ping server to keep session alive");
            })
        ;
    });
    
    $scope.logout = function() {
        $token.logout();
    };

    $scope.isRole = function(roleCode) {
        var roleCodes = $token.getUserInfo().roleCodes;
        if (angular.isUndefinedOrNull(roleCodes) || roleCodes.length <= 0) {
            return false;
        }

        return roleCodes.indexOf(roleCode) >= 0;
    };

    $scope.getUsername = function() {
        return $token.getUserInfo().username;
    };

    $scope.setLang = function(lang) {
        $translate.use(lang);
        $token.setUserLanguage(lang);
    }

    $scope.getFlag = function() {
        if ($scope.lang === 'vi') {
            return 'flags-vietnam';
        }
        if ($scope.lang === 'zh') {
            return 'flags-china';
        }
        return 'flags-british';
    }

    $scope.$watch(
        function() {
            return $token.getUserLanguage();
        },
        function(newLang, oldLang){
            $scope.lang = newLang;
        }
    );
});