app.controller('SUPVisitScheduleCtrl', function($scope, $filter, $token, logger, dialogs, Factory) {

    function init() {
        $scope.title = $filter('translate')('visit.schedule.title');
        $scope.proccessing = false;
        $scope.error = false;

        $scope.currentUserId = $token.getUserInfo().id;

        $scope.global = {};

        //TODO load from client info
        $scope.numberWeekFrequency = 4;
        $scope.weeks = [];
        for (var i = 0; i < $scope.numberWeekFrequency; i++) {
            $scope.weeks.push( { label: 'W' + (i + 1), index: i } );
        }

        $scope.global.isChanged = false;
        $scope.global.editingMode = false;

        $scope.global.currentPage = 1;
        $scope.global.itemsPerPage = 10;
        $scope.global.maxSize = 5;
        $scope.global.searchText = null;

        $scope.tableCtrl = initTableCtrl(reloadDatas, null, null, null);

        initDistributors();
    }

    function reloadDatas() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.global.isChanged = false;
        $scope.global.editingMode = false;

        var queryString = {
            'who': 'supervisor',
            'category': 'visitschedule',
            'scheduled': false,
            'page': $scope.global.currentPage,
            'size': $scope.global.itemsPerPage,
            'distributorId': $scope.global.currentDistributorId
        };
        if (angular.isUndefinedOrNull($scope.global.currentSalesmanId) || $scope.global.currentSalesmanId.length == 0) {
            queryString.all = true;
        } else {
            if ($scope.global.currentSalesmanId != 'not-schedule') {
                queryString.salesmanId = $scope.global.currentSalesmanId;
            }
        }

        if ($scope.global.searchText && $scope.global.searchText.length > 0) {
            queryString.q = angular.encodeURI($scope.global.searchText);
        }

        Factory.doGet(
            queryString,
            function(data) {
                $scope.tableCtrl.records = data.list;
                $scope.global.totalItems = data.count;

                transformAfterLoadVisitSchedule();
                $scope.proccessing = false;
            },
            function(error) {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function initDistributors() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.distributors = [];
        $scope.global.currentDistributorId = null;
        $scope.global.currentSalesmanId = null;

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'distributor'
            },
            function(data){
                $scope.distributors = data.list;
                if ($scope.distributors.length > 0) {
                    $scope.reloadSchedule($scope.distributors[0]);
                }

                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function initSalesmen() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.salesmenFilter = [];
        $scope.salesmen = [];
        $scope.global.currentSalesmanId = null;

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'salesman',
                'distributorId' : $scope.global.currentDistributorId
            },
            function(data){
                angular.copy(data.list, $scope.salesmenFilter);
                angular.copy(data.list, $scope.salesmen);
                if (data.list == null) {
                    $scope.salesmenFilter = [];
                    $scope.salesmen = [];
                }
                $scope.salesmenFilter.unshift({
                    id: 'not-schedule',
                    fullname: '-- ' + $filter('translate')('visit.schedule.customer.not.scheduled') + ' --'
                });
                $scope.salesmen.unshift({
                    id: null,
                    fullname: '-- ' + $filter('translate')('visit.schedule.customer.not.scheduled') + ' --'
                });
                $scope.salesmenFilter.unshift({
                    id: null,
                    fullname: '-- ' + $filter('translate')('visit.schedule.all') + ' --'
                });

                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.getSalesmanName = function(salesmanName) {
        if (angular.isUndefinedOrNull(salesmanName)) {
            return '-- ' + $filter('translate')('visit.schedule.customer.not.scheduled') + ' --';
        }

        return salesmanName;
    };

    $scope.reloadSchedule = function($item) {
        $scope.global.currentDistributorId = $item.id;
        $scope.global.currentSalesmanId = null;

        $scope.global.currentPage = 1;
        $scope.global.searchText = null;

        initSalesmen();

        $scope.tableCtrl.refresh();
    };

    function transformAfterLoadVisitSchedule() {
        if (!angular.isUndefinedOrNull($scope.tableCtrl.records)) {
            angular.forEach($scope.tableCtrl.records, function(customerSchedule) {
                if (!angular.isUndefinedOrNull(customerSchedule.items)) {
                    angular.forEach(customerSchedule.items, function(item) {
                        item.isSunday = (item.days.indexOf(1) >= 0);
                        item.isMonday = (item.days.indexOf(2) >= 0);
                        item.isTuesday = (item.days.indexOf(3) >= 0);
                        item.isWednesday = (item.days.indexOf(4) >= 0);
                        item.isThursday = (item.days.indexOf(5) >= 0);
                        item.isFriday = (item.days.indexOf(6) >= 0);
                        item.isSaturday = (item.days.indexOf(7) >= 0);

                        item.weekFrequencys = [];
                        for (var i = 0; i < $scope.numberWeekFrequency; i++) {
                            var weekFrequency = {};
                            weekFrequency.selected = item.weeks.indexOf(i + 1) >= 0;
                            weekFrequency.label = 'W' + (i + 1);
                            weekFrequency.weekNumber = i + 1;
                            item.weekFrequencys.push(weekFrequency);
                        }

                    });
                }
            });
        }
    }

    function transformBeforeSaveVisitSchedule() {
        if (!angular.isUndefinedOrNull($scope.tableCtrl.records)) {
            angular.forEach($scope.tableCtrl.records, function(customerSchedule) {
                if (!angular.isUndefinedOrNull(customerSchedule.items)) {
                    angular.forEach(customerSchedule.items, function(item) {
                        item.days = [];
                        if (item.isSunday) { item.days.push(1); }
                        if (item.isMonday) { item.days.push(2); }
                        if (item.isTuesday) { item.days.push(3); }
                        if (item.isWednesday) { item.days.push(4); }
                        if (item.isThursday) { item.days.push(5); }
                        if (item.isFriday) { item.days.push(6); }
                        if (item.isSaturday) { item.days.push(7); }

                        if (!angular.isUndefinedOrNull(item.weekFrequencys)) {
                            item.weeks = [];
                            angular.forEach(item.weekFrequencys, function(weekFrequency) {
                                if (weekFrequency.selected) { item.weeks.push(weekFrequency.weekNumber); }
                            });
                        }
                    });
                }
            });
        }
    }

    function checkBeforeSave() {
        if (!angular.isUndefinedOrNull($scope.tableCtrl.records)) {
            for (var i = 0; i < $scope.tableCtrl.records.length; i++) {
                var customerSchedule = $scope.tableCtrl.records[i];

                console.log(customerSchedule);

                if (!angular.isUndefinedOrNull(customerSchedule.items)) {
                    for (var j = 0; j < customerSchedule.items.length; j++) {
                        var item = customerSchedule.items[j];

                        if (item.days != null && item.days.length > 0 && customerSchedule.salesmanId == null) {
                            return $filter('translate')('visit.schedule.salesman.null');
                        }
                    }
                }

                if (!angular.isUndefinedOrNull(customerSchedule.salesmanId)) {
                    var hasSchedule = false;

                    for (var j = 0; j < customerSchedule.items.length; j++) {
                        var item = customerSchedule.items[j];

                        if (item.days != null && item.days.length > 0 && item.weeks != null && item.weeks.length > 0) {
                            hasSchedule = true;
                            break;
                        }
                    }

                    if (!hasSchedule) {
                        return $filter('translate')('visit.schedule.schedule.null');
                    }
                }
            }
        }

        return null;
    }

    $scope.addNewItem = function(customerSchedule) {
        var item = {
            isMonday: false,
            isTuesday: false,
            isWednesday: false,
            isThursday: false,
            isFriday: false,
            isSaturday: false,
            isSunday: false
        };

        item.weekFrequencys = [];
        for (var i = 0; i < $scope.numberWeekFrequency; i++) {
            var weekFrequency = {};
            weekFrequency.selected = false;
            weekFrequency.label = 'W' + (i + 1);
            weekFrequency.weekNumber = i + 1;
            item.weekFrequencys.push(weekFrequency);
        }

        customerSchedule.items.push(item);
        $scope.markAsChanged();
    };

    $scope.clearCustomerSchedule = function(customerSchedule) {
        customerSchedule.items = [];
        $scope.addNewItem(customerSchedule);
        $scope.markAsChanged();
    };

    $scope.clearCustomerScheduleItem = function(customerSchedule, item) {
        if (customerSchedule.items.length > 1) {
            customerSchedule.items.splice(customerSchedule.items.indexOf(item), 1);
        } else {
            $scope.clearCustomerSchedule(customerSchedule);
        }
        $scope.markAsChanged();
    };

    $scope.markAsChanged = function() {
        $scope.global.isChanged = true;
    };

    $scope.save = function() {
        transformBeforeSaveVisitSchedule();

        if (checkBeforeSave() != null) {
            logger.logError(checkBeforeSave());
        } else {
            $scope.proccessing = true;
            $scope.error = false;

            Factory.doPut(
                {
                    'who': 'supervisor',
                    'category': 'visitschedule',
                    'subCategory': 'bydistributor',
                    'distributorId' : $scope.global.currentDistributorId
                },
                $scope.tableCtrl.records,
                function(){
                    $scope.proccessing = false;

                    reloadDatas();

                    logger.logSuccess($filter('translate')('save.success'));
                },
                function(){
                    $scope.proccessing = false;
                    logger.logError($filter('translate')('save.error'));
                }
            );
        }
    };

    $scope.search = function() {
        reloadDatas();
    };

    $scope.modeChange = function() {
        if (!$scope.global.editingMode && $scope.global.isChanged) {
            var dlg = dialogs.confirm(
                $filter('translate')('dialog.title.notice'),
                $filter('translate')('visit.schedule.data.unsaved.changing.mode'),
                {
                    size: 'md',
                    backdrop: false
                }
            );

            dlg.result.then(
                function() {
                    reloadDatas();
                },
                function() {
                    $scope.global.editingMode = true;
                }
            );
        }
    };

    init();

});


