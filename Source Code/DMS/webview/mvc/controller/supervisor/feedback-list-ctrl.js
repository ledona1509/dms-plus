app.controller('SUPFeedbackListCtrl', function($scope, $log, $filter, $location, $state, $token, logger, $modal, Factory) {

    function init() {
        $scope.currentUserId = $token.getUserInfo().id;

        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;
        $scope.searchText = null;

        $scope.today = new Date();

        $scope.tableCtrl = initTableCtrl(reloadData, null, null, null);

        $scope.tableCtrl.refresh();
    }

    function reloadData() {
        $scope.proccessing = true;
        $scope.error = false;

        var queryString = {
            'who': 'supervisor',
            'category': 'feedback',
            'page': $scope.currentPage,
            'size': $scope.itemsPerPage
        };

        Factory.doGet(
            queryString,
            function(data) {
                $scope.tableCtrl.records = data.list;
                $scope.totalItems = data.count;

                $scope.proccessing = false;
            },
            function(error) {
                logger.logError($filter('translate')('error.loading'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.getFeedbackDisplay = function(feedbacks) {
        if (feedbacks == null || feedbacks.length == 0) {
            return "N/A";
        }

        return feedbacks[0];
    };

    $scope.getTimeDisplay = function(isoTime) {
        var date = angular.parseDateTime(isoTime);

        if ($scope.today.getFullYear() == date.getFullYear()
        && $scope.today.getMonth() == date.getMonth()
        && $scope.today.getDate() == date.getDate() ) {
            return moment(date).format('HH:mm');
        } else {
            return moment(date).format('DD/MM/YYYY');
        }
    };

    $scope.viewDetail = function(feedback) {
        $state.go( 'feedback-detail', { id:feedback.id } );
    };

    init();
});
