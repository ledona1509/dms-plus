app.controller('SUPCustomerCreateCtrl', function ($scope, $filter, $log, logger, $stateParams, $state, Factory, $token) {

    function init() {
        $scope.title = $filter('translate')('customer.create.title');
        $scope.currentUserId = $token.getUserInfo().id;

        loadCustomerTypes();

        $scope.initData();
        initDistributors();
    }

    $scope.initData = function() {
        $scope.isChanged = false;
        $scope.record = {};
        $scope.global = {};
        initMap();
    };

    function initDistributors() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.distributors = [];
        $scope.global.currentDistributorId = null;

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'distributor'
            },
            function(data){
                $scope.distributors = data.list;
                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.ok = function () {
        //Validate
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
            return;
        }
        if (!$scope.global.currentDistributorId) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
            return;
        }
        $scope.proccessing = true;
        $scope.record.distributorId = $scope.global.currentDistributorId;
        // Create new record
        Factory.doPost(
            {
                'who': 'supervisor',
                'category': 'customer'
            },
            $scope.record,
            function() {
                $scope.proccessing = false;
                logger.logSuccess($filter('translate')('create.success'));
                $scope.clear();
            },
            function(){
                $scope.proccessing = false;
                logger.logError($filter('translate')('save.error'));
            }
        );
    };

    $scope.cancel = function () {
        $state.go('home');
    };

    $scope.clear = function () {
        $scope.initData();
    };

    function loadCustomerTypes() {
        $scope.proccessing = true;
        $scope.customerTypes = [];

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'customertype',
                'subCategory': 'all'
            },
            function(data) {
                $scope.customerTypes = data.list;
                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $scope.proccessing = false;
                $scope.cancel();
            }
        );
    }

    function initMap() {
        var defaultCenter = {latitude: 21.084269026984, longitude: 105.82028125};
        var coords = {};// --> Ha Noi

        $scope.map = {
            center: defaultCenter,
            events: {
                click: function (mapModel, eventName, originalEventArgs) {
                    var e = originalEventArgs[0];
                    var lat = e.latLng.lat(),
                        lng = e.latLng.lng();

                    $scope.record.latitude = lat;
                    $scope.record.longitude = lng;

                    $scope.marker.coords.latitude = lat;
                    $scope.marker.coords.longitude = lng;

                    //$scope.markAsChanged();
                }
            },
            zoom: 13
        };

        $scope.marker = {
            id: 0,
            title: 'customer',
            coords: coords,
            options: { draggable: true },
            windowOptions: {
                visible: false
            },
            events: {
                dragend: function (marker, eventName, args) {
                    var lat = marker.getPosition().lat();
                    var lng = marker.getPosition().lng();

                    $scope.record.latitude = lat;
                    $scope.record.longitude = lng;

                    //$scope.markAsChanged();
                }
            }
        };
    };

    init();

});
    


