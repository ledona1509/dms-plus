app.controller('SUPReportExhibitionListCtrl', function($scope, $token, $log, $filter, $location, $state, logger,
                                         $modal, STATE_CONFIG, Factory, ADDRESS_BACKEND) {

    function init() {
        $scope.currentUserId = $token.getUserInfo().id;

        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;

        $scope.tableCtrl = initTableCtrl(reloadDatas, null, null, null);

        $scope.tableCtrl.refresh();
    }

    function reloadDatas() {
        $scope.proccessing = true;
        $scope.error = false;

        var queryString = {
            'who': 'supervisor',
            'category': 'exhibition',
            'page': $scope.currentPage,
            'size': $scope.itemsPerPage
        };

        Factory.doGet(
            queryString,
            function(data) {
                $scope.tableCtrl.records = [];
                angular.copy(data.list, $scope.tableCtrl.records);
                $scope.totalItems = data.count;

                if ($scope.tableCtrl.records == null) {
                    $scope.tableCtrl.records = [];
                    $scope.totalItems = 0;
                }

                // Init status
                for (var i = 0; i < $scope.tableCtrl.records.length; i++) {
                    var record = $scope.tableCtrl.records[i];
                    record.status = $scope.getStatus(record);
                }

                initSelectableArray($scope.tableCtrl.records, null);

                $scope.tableCtrl.updateSelectAllStatus();

                $scope.proccessing = false;
            },
            function(error) {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.getStatus = function(record) {
        var currentDate = new Date();
        if (moment(currentDate).isBefore(record.startDate)) {
            return 0;
        }
        if (moment(record.endDate).isBefore(currentDate)) {
            return 2;
        }
        return 1;
    }

    $scope.search = function() {
        reloadDatas();
    };

    $scope.export = function(record) {
        location.href = ADDRESS_BACKEND + 'supervisor/report/exhibition/' + record.id + '/export?access_token=' + $token.getAccessToken();
    };

    init();

});