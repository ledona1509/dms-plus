app.controller('SUPHomepageCtrl', function ($scope, $log, $filter, $token, $location, $state, $stateParams, logger,
                                                   Factory) {

    function init() {
        $scope.global = {};

        $scope.data = {};

        $scope.refresh();
    }

    function reloadDatas() {
        $scope.data = {};

        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'homepage'
            },
            function (data) {
                $scope.data = data;

                $scope.proccessing = false;
            },
            function (error) {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.refresh = function () {
        reloadDatas();
    };

    $scope.today = function() {
        var date =  angular.parseDate($scope.data.today);

        if (date == null) {
            return '';
        }

        return moment(date).format('DD/MM/YYYY');

    };

    $scope.getComparisonIcon = function(thisMonth, lastMonth) {
        if (angular.isUndefinedOrNull(thisMonth)) {
            thisMonth = 0;
        }

        if (angular.isUndefinedOrNull(lastMonth)) {
            lastMonth = 0;
        }

        if (thisMonth < lastMonth) {
            return 'fa-arrow-circle-down';
        }

        if (thisMonth > lastMonth) {
            return 'fa-arrow-circle-up';
        }

        if (thisMonth == lastMonth) {
            return 'fa-arrow-circle-right';
        }
    };

    init();

});
