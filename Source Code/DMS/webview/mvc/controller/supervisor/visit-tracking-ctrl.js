app.controller('SUPVisitTrackingCtrl', function ($scope, $filter, $log, logger, $stateParams, $state, $token,
                                                 Factory, ImageService) {

    function init() {
        $scope.currentUserId = $token.getUserInfo().id;
        $scope.options = { draggable: false };

        $scope.initData();

        initDistributors();
    }

    $scope.initData = function() {
        $scope.isChanged = false;
        $scope.global = {};
        $scope.points = [];
        initMap();
    };

    function initDistributors() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.distributors = [];
        $scope.global.currentDistributorId = null;
        $scope.global.currentSalesmanId = null;

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'distributor'
            },
            function(data){
                angular.copy(data.list, $scope.distributors);
                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function initSalesmen() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.salesmen = [];
        $scope.global.currentSalesmanId = null;

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'salesman',
                'distributorId' : $scope.global.currentDistributorId
            },
            function(data){
                angular.copy(data.list, $scope.salesmen);
                if (data.list == null) {
                    $scope.salesmen = [];
                }
                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function initCustomers() {
        $scope.proccessing = true;
        $scope.error = false;
        $scope.currentPoint = undefined;

        $scope.customers = [];

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'customer',
                'subCategory': 'today',
                'salesmanId' : $scope.global.currentSalesmanId
            },
            function(data){
                angular.copy(data.list, $scope.customers);
                if (data.list == null) {
                    $scope.customers = [];
                }
                $scope.proccessing = false;
                createMarkers();
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function createMarkers() {
        $scope.points = [];

        for (var i = 0; i < $scope.customers.length; i++) {
            var customer = $scope.customers[i];
            var coords = {latitude: customer.latitude, longitude: customer.longitude};
            var imageUrl = customer.photos != null && customer.photos.length > 0 ? ImageService.getImageUrl(customer.photos[0]) : undefined;
            var point = {
                id: i,
                latitude: customer.latitude,
                longitude: customer.longitude,
                coords: coords,
                customer: customer,
                photoUrl: imageUrl
            };

            $scope.points.push(point);
        }
    }

    $scope.onClick = function(point) {
        $scope.currentPoint = point.model;
    }

    $scope.isUndefinedOrNull = function(a) {
        return angular.isUndefinedOrNull(a);
    }

    $scope.onDistributorSelected = function($item) {
        $scope.points = [];

        $scope.global.currentSalesmanId = null;
        if (angular.isUndefinedOrNull($item)) {
            $scope.global.currentDistributorId = null;
        } else {
            $scope.global.currentDistributorId = $item.id;
            initSalesmen();
        }
    };

    $scope.onSalesmanSelected = function($item) {
        $scope.global.currentSalesmanId = $item.id;

        initCustomers();
    };

    $scope.hasDistributor = function() {
        return angular.isUndefinedOrNull($scope.global.currentDistributorId);
    };

    function initMap() {
        var defaultCenter = {latitude: 21.084269026984, longitude: 105.82028125};

        $scope.map = {
            center: defaultCenter,
            zoom: 13
        };
    }

    init();

});
    


