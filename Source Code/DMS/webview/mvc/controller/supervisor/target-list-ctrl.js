app.controller('SUPTargetListCtrl', function($scope, $log, $filter, $location, $state, logger, $modal, Factory) {

    function init() {
        loadConfig();

        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;
        $scope.searchText = null;

        $scope.currentMonth = {};
        $scope.currentMonth.date = new Date();
        $scope.currentMonth.opened = null;

        $scope.tableCtrl = initTableCtrl(reloadDatas, deleteRecord, editRecord, createRecord);

        $scope.tableCtrl.refresh();
    }

    function loadConfig() {
        $scope.title = $filter('translate')('target.title');
    }

    function reloadDatas() {
        $scope.proccessing = true;
        $scope.error = false;

        var queryString = {
            'who': 'supervisor',
            'category': 'target',
            'month': ($scope.currentMonth.date.getMonth()),
            'year': $scope.currentMonth.date.getFullYear(),
            'page': $scope.currentPage,
            'size': $scope.itemsPerPage
        };
        if ($scope.searchText && $scope.searchText.length > 0) {
            queryString.q = $scope.searchText;
        }

        Factory.doGet(
            queryString,
            function(data) {
                $scope.tableCtrl.records = data.list;
                initSelectableArray($scope.tableCtrl.records, null);

                $scope.totalItems = data.count;

                $scope.tableCtrl.updateSelectAllStatus();

                $scope.proccessing = false;
            },
            function(error) {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function deleteRecord(record) {
        $scope.proccessing = true;

        Factory.doDelete(
            {
                'who': 'supervisor',
                'category': 'target',
                'id': record.id
            },
            function() {
                logger.logSuccess($filter('translate')('delete.success'));
                $scope.proccessing = false;
                $scope.tableCtrl.refresh();
            },
            function() {
                logger.logError($filter('translate')('delete.error'));
                $scope.proccessing = false;
            }
        );
    }

    function editRecord(record) {
        var modalInstance = $modal.open({
            templateUrl: 'mvc/view/supervisor/target-detail-popup.html',
            controller: 'SUPTargetDetailPopupCtrl',
            resolve: {
                record: function () {
                    return record;
                },
                categoryName: function () {
                    return $scope.categoryName
                },
                date: function () {
                    return null;
                }
            }
        });
        modalInstance.result.then(function () {
            $scope.tableCtrl.refresh();
        }, function () {
        });
    }

    function createRecord() {
        var modalInstance = $modal.open({
            templateUrl: 'mvc/view/supervisor/target-detail-popup.html',
            controller: 'SUPTargetDetailPopupCtrl',
            resolve: {
                record: function () {
                    return null;
                },
                categoryName: function() {
                    return $scope.categoryName
                },
                date: function() {
                    return { 'month': ($scope.currentMonth.date.getMonth()), 'year': $scope.currentMonth.date.getFullYear()};
                }
            }
        });
        modalInstance.result.then(function () {
            $scope.tableCtrl.refresh();
        }, function () {});
    }

    $scope.search = function() {
        reloadDatas();
    };

    $scope.open = function($event, data) {
        $event.preventDefault();
        $event.stopPropagation();
        data.opened = true;
    };

    init();

});
