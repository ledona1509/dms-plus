app.controller('SUPReportSaleSalesmanMonthly', function($scope, $log, $filter, $token, $location, $state, $stateParams,
                                                        logger, Factory) {

    function init() {
        $scope.currentUserId = $token.getUserInfo().id;

        $scope.global = {};

        $scope.global.currentMonth = {};
        $scope.global.currentMonth.date = new Date();
        $scope.global.currentMonth.opened = null;

        initSalesmen();
    }

    function initSalesmen() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.salesmen = [];

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'salesman'
            },
            function(data){
                $scope.salesmen = data.list;

                if (angular.isUndefinedOrNull($scope.salesmen)) {
                    $scope.salesmen = [];
                }

                $scope.salesmen.unshift({
                    id: 'all',
                    fullname: '-- ' + $filter('translate')('report.salesman.sale.monthly.all') + ' --'
                });

                $scope.global.currentSalesmanId = 'all';

                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.open = function($event, data) {
        $event.preventDefault();
        $event.stopPropagation();
        data.opened = true;
    };

    $scope.refresh = function() {
        init();
    };

    $scope.report = function() {
        $state.go('sup-report-sale-salesman-monthly-detail',
            {
                salesmanId: $scope.global.currentSalesmanId,
                month: $scope.global.currentMonth.date.getMonth(),
                year: $scope.global.currentMonth.date.getFullYear()
            });
    };

    init();

});
