app.controller('HeaderCtrl', function ($scope, $log, $filter, $token, $location, $state, $stateParams,
                                       logger, socketClient, Factory) {

    function init() {
        $scope.global = {};
        $scope.userid = $token.getUserInfo().id;

        $scope.numberFeedbackUnread = 0;
        $scope.new_po_count = 0;
        $scope.new_customer_count = 0;

        if ($scope.$parent.isRole('SUP')) {
            loadSupData();

            socketClient.registerSubscribe('/user/' + $scope.userid + '/topic/new/order').then(null, null, function (data) {
                $scope.new_po_count = data.message;

                if ($scope.userid != data.sender) {
                    var message = $filter('translate')('homepage.alert.message.new.po');
                    message = $filter('string_format')(message, $filter('number')($scope.new_po_count));
                    logger.log(message);
                }
            });

            socketClient.registerSubscribe('/user/' + $scope.userid + '/topic/changed/order').then(null, null, function (data) {
                $scope.new_po_count = data.message;
            });

            socketClient.registerSubscribe('/user/' + $scope.userid + '/topic/new/customer').then(null, null, function (data) {
                $scope.new_customer_count = data.message;

                if ($scope.userid != data.sender) {
                    var message = $filter('translate')('homepage.alert.message.new.customer');
                    message = $filter('string_format')(message, $filter('number')($scope.new_customer_count));
                    logger.log(message);
                }
            });

            socketClient.registerSubscribe('/user/' + $scope.userid + '/topic/changed/customer').then(null, null, function (data) {
                $scope.new_customer_count = data.message;
            });

            socketClient.registerSubscribe('/user/' + $scope.userid + '/topic/new/feedback').then(null, null, function (data) {
                $scope.numberFeedbackUnread = data.message;

                if ($scope.userid != data.sender) {
                    var message = $filter('translate')('homepage.alert.message.unread.feedback');
                    message = $filter('string_format')(message, $filter('number')($scope.numberFeedbackUnread));
                    logger.log(message);
                }
            });

            socketClient.registerSubscribe('/user/' + $scope.userid + '/topic/changed/feedback').then(null, null, function (data) {
                $scope.numberFeedbackUnread = data.message;
            });

            socketClient.start();
        }
    }

    function loadSupData() {

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'notification'
            },
            function (data) {
                $scope.new_customer_count = data.nbCustomerToApprove;
                $scope.numberFeedbackUnread = data.nbFeedbackToRead;
                $scope.new_po_count = data.nbPurchaseToApprove;
            },
            function (error) {
                logger.logError($filter('translate')('error.loading'));
                $log.log(error);
            }
        );
    }

    init();

});