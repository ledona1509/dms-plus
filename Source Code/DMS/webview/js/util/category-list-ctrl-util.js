function initScopeForCategoryList($scope, $log, $filter, $location, $state, logger, $modal, Factory, STATE_CONFIG, columns, transformAfterLoad) {
    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        $scope.config = STATE_CONFIG[path];

        $scope.title = $scope.config.title;
        $scope.categoryName = path;
        $scope.who = $scope.config.who;
    }

    $scope.init = function() {
        loadConfig();

        if (columns != null) {
            $scope.columns = columns;
        } else {
            $scope.columns = [ { header: 'name', property: 'name' } ];
            if ($scope.config.useCode) {
                $scope.columns.push({ header: 'code', property: 'code' });
            }
        }

        $scope.proccessing = false;
        $scope.error = false;

        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;
        $scope.searchText = null;

        $scope.refresh();
    };

    $scope.refresh = function() {
        $scope.proccessing = true;
        $scope.error = false;

        var params = {
            who: $scope.who,
            category: $scope.categoryName,
            page: $scope.currentPage,
            size: $scope.itemsPerPage
        };

        if ($scope.searchText && $scope.searchText.length > 0) {
            params.q = angular.encodeURI($scope.searchText);
        }

        Factory.doGet(
            params,
            function(data) {
                $scope.records = data.list;
                $scope.totalItems = data.count;

                if (transformAfterLoad != null && typeof(transformAfterLoad) == 'function') {
                    transformAfterLoad();
                }

                $scope.proccessing = false;
            },
            function(error) {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);

                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    };

    $scope.deleteRecord = function(record) {
        $scope.proccessing = true;

        Factory.doDelete(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: record.id
            },
            function() {
                logger.logSuccess($filter('translate')('delete.success'));
                $scope.proccessing = false;
                $scope.refresh();
            },
            function() {
                logger.logError($filter('translate')('delete.error'));
                $scope.proccessing = false;
            }
        );
    };

    $scope.enableRecord = function(record) {
        $scope.proccessing = true;

        Factory.doPut(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: record.id,
                action: 'enable'
            },
            null,
            function() {
                logger.logSuccess($filter('translate')('enable.success'));
                $scope.proccessing = false;
                $scope.refresh();
            },
            function() {
                logger.logError($filter('translate')('enable.error'));
                $scope.proccessing = false;
            }
        );
    };

    $scope.editRecord = function(record) {
        if ($scope.config.isUsePopup) {
            var detailTemplate = 'mvc/view/common/category-detail-popup.html';
            if ($scope.config.detailTemplate != null) {
                detailTemplate = $scope.config.detailTemplate;
            }

            var detailCtrl = 'CategoryDetailPopupCtrl';
            if ($scope.config.detailCtrl != null) {
                detailCtrl = $scope.config.detailCtrl;
            }


            var modalInstance = $modal.open({
                templateUrl: detailTemplate,
                controller: detailCtrl,
                resolve: {
                    record: function () {
                        return record;
                    },
                    categoryName: function() {
                        return $scope.categoryName;
                    },
                    config: function() {
                        return $scope.config;
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.refresh();
            }, function () {
            });
        } else {
            $state.go( $scope.categoryName + '-detail', { id: record.id } );
        }
    };

    $scope.createRecord = function() {
        if ($scope.config.isUsePopup) {
            var detailTemplate = 'mvc/view/common/category-detail-popup.html';
            if ($scope.config.detailTemplate != null) {
                detailTemplate = $scope.config.detailTemplate;
            }

            var detailCtrl = 'CategoryDetailPopupCtrl';
            if ($scope.config.detailCtrl != null) {
                detailCtrl = $scope.config.detailCtrl;
            }

            var modalInstance = $modal.open({
                templateUrl: detailTemplate,
                controller: detailCtrl,
                resolve: {
                    record: function () {
                        return null;
                    },
                    categoryName: function() {
                        return $scope.categoryName;
                    },
                    config: function() {
                        return $scope.config;
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.refresh();
            }, function () {});
        } else {
            $state.go( $scope.categoryName + '-detail', { id: 'new' } );
        }
    };

    $scope.getPropertyValue = function(column, record) {
        if (column != null && column.property != null) {
            if (typeof(column.property) == 'function') {
                return column.property(record);
            } else {
                return record[column.property];
            }
        }
    }

}
