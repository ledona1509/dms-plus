function initSelectableArray(records, isRecordSelected) {
    if (records != null && records.length > 0) {
        for (var i = 0; i < records.length; i++) {
            if (isRecordSelected != null && typeof(isRecordSelected) != 'function') {
                records[i].selected = isRecordSelected(records[i]);
            } else {
                records[i].selected = false;
            }
        }
    }
}

function initTableCtrl(reloadRecords, deleteRecord, editRecord, createRecord) {
    var tableCtrl = {};

    tableCtrl.refresh = function() {
        if (reloadRecords != null && typeof(reloadRecords) == 'function') {
            reloadRecords();
        }
    };

    tableCtrl.selectAllClick = function() {
        var selected = false;
        if (tableCtrl.selectAllStatus == 'T') {
            selected = true;
        }

        if (tableCtrl.records != null && tableCtrl.records.length > 0) {
            for (var i = 0; i < tableCtrl.records.length; i++) {
                tableCtrl.records[i].selected = selected;
            }
        }
    };

    tableCtrl.updateSelectAllStatus = function() {
        tableCtrl.selectAllStatus = 'F';
        if (tableCtrl.records != null && tableCtrl.records.length > 0) {
            var isT = false;
            var isF = false;

            for (var i = 0; i < tableCtrl.records.length; i++) {
                if (tableCtrl.records[i].selected) {
                    if (isF) {
                        tableCtrl.selectAllStatus = 'I';
                        return;
                    }
                    isT = true;
                } else {
                    if (isT) {
                        tableCtrl.selectAllStatus = 'I';
                        return;
                    }
                    isF = true;
                }
            }

            if (isT) {
                tableCtrl.selectAllStatus = 'T';
            }
        }
    };

    tableCtrl.getSelectedRecords = function() {
        if (tableCtrl.records || tableCtrl.records.length == 0 || tableCtrl.selectAllStatus === 'F') {
            return [];
        }

        if (tableCtrl.selectAllStatus === 'T') {
            return tableCtrl.records;
        }

        var selectedRecords = [];
        for (var i = 0; i < tableCtrl.records.length; i++) {
            if (tableCtrl.records[i].selected) {
                selectedRecords.push(tableCtrl.records[i]);
            }
        }
        return selectedRecords;
    };

    tableCtrl.pageChanged = function() {
        tableCtrl.refresh();
    };

    tableCtrl.deleteRecord = function(record) {
        if (deleteRecord != null && typeof(deleteRecord) == 'function') {
            deleteRecord(record);
        }
    };

    tableCtrl.editRecord = function(record) {
        if (editRecord != null && typeof(editRecord) == 'function') {
            editRecord(record);
        }
    };

    tableCtrl.createRecord = function() {
        if (createRecord != null && typeof(createRecord) == 'function') {
            createRecord();
        }
    };

    return tableCtrl;
}
