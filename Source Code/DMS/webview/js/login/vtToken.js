/**
 * @license haizpt
 * (c) 2014 haizpt, mail: haizpt@gmail.com
 * License: haizpt
 * @description
 * @requires base64 algorithms
 * 
 */

(function(window, angular, Base64, undefined) {'use strict';
    
    var tokenModule = angular.module('vtToken', []);

    tokenModule.provider('$token', function () {

        var OauthType = {
            ResourceOwnerPassword: 1,
            AuthorizationCode: 2,
            ImplicitGrant: 3
        };

        var LOGIN_TYPE = OauthType.ImplicitGrant;    

         var REQUIRED_AND_MISSING = {};

        // default for Implicit Grant
        var defaultConfigs = {
           client_id: REQUIRED_AND_MISSING,
           authorizationEndpoint: REQUIRED_AND_MISSING,
           scope: "",
           revokeTokenEndpoint: "",
           verifyTokenEndpoint:"",
           logoutUrl: ""
        };


        var objectToQueryString = function (obj) {
            var str = [];
            angular.forEach(obj, function (value, key) {
                str.push(encodeURIComponent(key) + "=" + encodeURIComponent(value));
            });
            return str.join("&");
        };

        var buildAuthorizationUrl = function (extraParams) {
            var params = defaultConfigs ;
            if(extraParams) {
                params = angular.extend(defaultConfigs, extraParams);
            }
            return defaultConfigs.authorizationEndpoint + '?' + objectToQueryString(params);
        };

        this.extendConfig = function(configExtension) {
            defaultConfigs = angular.extend(defaultConfigs, configExtension);
            return defaultConfigs;
        };
        
        this.set =
        this.Set = function ()
        {
            if (arguments.length !== 1 && typeof arguments[0] === 'object') {
                return this;
            }
            
            if(typeof arguments[0] !== 'string'){
                return this;
            }
            
            var value = typeof arguments[1] === 'undefined' ? null : arguments[1];
            var key = arguments[0];
            defaultConfigs[key] = value;
            
            return this;
        };
        
        this.loginHandler = function($token){
            $token.loginHandler();
        };
        
        this.$get = function ($http, $rootScope, $location, $q, $window, $log) {
            var requiredAndMissing = [];
            angular.forEach(defaultConfigs, function (value, key) {
                if (value === REQUIRED_AND_MISSING) {
                    requiredAndMissing.push(key);
                }
            });

            if (requiredAndMissing.length) {
                throw new Error("TokenProvider is insufficiently configured.  Please " +
                        "configure the following options using " +
                        "$tokenProvider.extendConfig: " + requiredAndMissing.join(", "));
            }

            function setAccessToken(params){
                localStorage['access_token'] = params['access_token'];
                localStorage['token_type'] = params['token_type'];
                localStorage['expires_in'] = params['expires_in'];
                localStorage['lang'] = params['lang'] ? params['lang'].toLowerCase() : null;
            }

            function getAccessToken() {
                 return localStorage["access_token"];
            }
            
            function getUserName(){
                var userInfo = getUserInfo();
                if(!userInfo) {
                    return null;
                }
                return userInfo["userName"];
            }
            
            function endsWith(str, suffix) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }
            
            function logout(){
                
                var doIt = function() {
                    clearToken();
                    clearUserInfo();

                    if(defaultConfigs.logoutUrl
                            && defaultConfigs.logoutUrl.length > 0) {
                        var lang = getUserLanguage();
                        if (!angular.isUndefinedOrNull(lang)) {
                            var logoutUrl = addUrlParam(defaultConfigs.logoutUrl, 'lang', lang);
                            $window.location.href = logoutUrl;
                        } else {
                            $window.location.href = defaultConfigs.logoutUrl;
                        }
                    }
                };
                
                if(getAccessToken() && defaultConfigs.revokeTokenEndpoint
                         && defaultConfigs.revokeTokenEndpoint.length > 0
                         && getUserName()) {
                    var url = defaultConfigs.revokeTokenEndpoint;

                    if(!endsWith(url,"/")) {
                        url += "/";
                    }
                    
                    url += 'users/' + getUserName() + '/tokens/' + getAccessToken();

                    var configs = {
                        method: 'DELETE',
                        url: url,
                        headers: {Authorization : 'Bearer ' + getAccessToken()}
                    };
                    
                    $http(configs).then(function(){
                        $log.info("logout ok!!!");
                    }, function(){
                        $log.error("logout error!!!");
                    }).finally(function(){
                        doIt();
                    });
                } else {
                    doIt();
                }
                
            }

            /**
             * Add a URL parameter (or changing it if it already exists)
             * @param {search} string  this is typically document.location.search
             * @param {key}    string  the key to set
             * @param {val}    string  value
             */
            function addUrlParam(search, key, val){
                var newParam = key + '=' + val,
                    params = '?' + newParam;

                // If the "search" string exists, then build params from it
                if (search) {
                    // Try to replace an existance instance
                    params = search.replace(new RegExp('[\?&]' + key + '[^&]*'), '$1' + newParam);

                    // If nothing was replaced, then add the new param to the end
                    if (params === search) {
                        if (params.indexOf('?') == -1) {
                            params += '?';
                        }
                        params += '&' + newParam;
                    }
                }

                return params;
            };
            
            function clearToken(){
                localStorage.removeItem("access_token");
            }
            
            function clearUserInfo() {
                localStorage.removeItem("userInfo");
            }
            
            function getUserInfo() {
                var userInfo = localStorage["userInfo"];
                if(!userInfo) {
                    return null;
                }
                try {
                    return JSON.parse(userInfo);
                } catch (e) {
                    localStorage["userInfo"] = null;
                }
                return null;
            }
            
            function setUserInfo(userInfo) {
                localStorage["userInfo"] = JSON.stringify(userInfo);
            }

            function getUserLanguage() {
                return localStorage["lang"];
            }

            function setUserLanguage(lang) {
                localStorage["lang"] = lang;
            }
            
            function verifyToken(){
                var defer = $q.defer();
                 
                if(angular.isNull(getAccessToken())) {
                   loginHandler();
                }
                
                if(angular.isNull(defaultConfigs.verifyTokenEndpoint)
                         || defaultConfigs.verifyTokenEndpoint.length === 0) {
                    throw new Error("Please config 'verifyTokenEndpoint' before use this function");
                }
                   
                $http.get(defaultConfigs.verifyTokenEndpoint).
                success(function(userinfo) {
                    setUserInfo(userinfo);
                    defer.resolve(userinfo);
                }).
                error(function(error) {
                    defer.reject(error);
                });
//                
                return defer.promise;
            }
            
            function setTokenFromLocation(){
                var search = $location.search();
                setAccessToken(search);
                    
                //remove access_token from url
                $location.search('access_token', null);
                $location.search('token_type', null);
                $location.search('expires_in', null);
                $location.search('flag', null);
                $location.search('lang', null);
            }
            
            function loginHandler(extendParams){
                if(LOGIN_TYPE === OauthType.ResourceOwnerPassword) {
                    $location.path("login");
                } else if (LOGIN_TYPE === OauthType.AuthorizationCode) {
                    $location.path("error");
                } else if (LOGIN_TYPE === OauthType.ImplicitGrant) {
                    var search = $location.search();
                    var search_define = angular.sizeOwnProperty(search);
                    if (search_define === 0) {
                        $location.search({flag: true});
                    }
                    
//                    var absUrl = $location.absUrl();
//                    var url = $location.url();
                    
//                    var redirect_uri = absUrl.replace(url,"");

                    var redirect_uri = $location.absUrl();

                    var loginParams = {
                        redirect_uri: redirect_uri
                    };

                    loginParams = angular.extend(defaultConfigs, loginParams);
                    
//                    extendParams = extendParams || {};
//                    
//                    extendParams['_redirect_uri'] = absUrl;
                    
                    login(loginParams, extendParams);
                }
            }

            /**
            * params contain username & password
            * var params = {
            *        username: "username",
            *        password: "password",
            *        client_id: "client_id",
            *        client_secret: "client_secret",
            *        authorizationEndpoint: "authorizationEndpoint"
            *   };
            *  @param {type} params 
            * */
            function loginByResourceOwnerPassword(params){
                var p = {};
                var url = params.authorizationEndpoint;

                p['grant_type'] = "password";
                p['username'] = params.username;
                p['password'] = params.password;

                var headers = {
                    'Authorization': "Basic " + Base64.encode(params.client_id + ":" + params.client_secret) 
                  };

                var username = p["username"];
                var password = p["password"];

                if (username == null 
                        || password == null ) {
                    throw new Error("username or password is null");
                    return;
                }

                $http({
                    method: "POST",
                    params: p,
                    url: url,
                    headers: headers,
                    isArray: false
                }).success(function (data, status) {
                     var access_token = data.access_token;
                     setAccessToken(access_token);
                     $location.path("/");
                }).error(function (error) {
                    console.log(error);
                });

            }

            function loginByAuthorizationCode(params) {
               var popupOptions = angular.extend({
                            name: 'AuthPopup',
                            openParams: {
                                width: 650,
                                height: 300,
                                resizable: true,
                                scrollbars: true,
                                status: true
                            }
                        }, {});

                var deferred = $q.defer(),
                    url = buildAuthorizationUrl(params);

                var formatPopupOptions = function (options) {
                    var pairs = [];
                    angular.forEach(options, function (value, key) {
                        if (value || value === 0) {
                            value = value === true ? 'yes' : value;
                            pairs.push(key + '=' + value);
                        }
                    });
                    return pairs.join(',');
                 };

                var popup = window.open(url, popupOptions.name, formatPopupOptions(popupOptions.openParams));

                angular.element($window).bind('message', function (event) {
                    // Use JQuery originalEvent if present
                    event = event.originalEvent || event;
                    if (event.source == popup && event.origin == window.location.origin) {
                        $rootScope.$apply(function () {
                            if (event.data.access_token) {
                                deferred.resolve(event.data)
                            } else {
                                deferred.reject(event.data)
                            }
                        });
                    }
                });

                return deferred.promise;
            }

            /**
             * @param {type} extendParams 
             * @param {type} params 
             *  params = {
             *               client_id: "client_id",
             *               redirect_uri: "redirect_uri",
             *               scope: "scope",
             *               authorizationEndpoint: "authorizationEndpoint"
             *           };
             * */
            function loginByImplicitGrant(params, extendParams){
                var search = $location.search();
                if(search.access_token) {
                    setTokenFromLocation();
                    return;
                }
                
                var p = {
                    response_type: "token",
                    client_id: params.client_id,
                    redirect_uri: params.redirect_uri,
                    scope: params.scope
                };

                if(extendParams) {
                    p = angular.extend(defaultConfigs, extendParams);
                }

                var url = params.authorizationEndpoint;

                var buildUrl = url + '?' + objectToQueryString(p);

                $window.location.href = buildUrl;
            }

            function login(params, extendParams){
                if(LOGIN_TYPE === OauthType.ResourceOwnerPassword) {
                    loginByResourceOwnerPassword(params, extendParams);
                } else if(LOGIN_TYPE === OauthType.AuthorizationCode){
                    loginByAuthorizationCode(params, extendParams);
                } else if(LOGIN_TYPE === OauthType.ImplicitGrant){
                    loginByImplicitGrant(params, extendParams);
                }
            }

            function refreshTokenLink(params) {
                var url = buildAuthorizationUrl(params);
                return url;
            }
            
            function getTokenHeaders(){
                var headers = {};
                if (LOGIN_TYPE === OauthType.ImplicitGrant) {
                    headers['Authorization'] = 'Bearer ' + getAccessToken(); 
                }
                
                return headers;
            }
            
            return {
                login: login,
                logout: logout,
                getAccessToken: getAccessToken,
                loginHandler: loginHandler,
                refreshTokenLink: refreshTokenLink,
                extendConfig: this.extendConfig,
                getTokenHeaders: getTokenHeaders,
                clearToken: clearToken,
                verifyToken: verifyToken,
                setTokenFromLocation : setTokenFromLocation,
                clearUserInfo: clearUserInfo,
                getUserInfo: getUserInfo,
                setUserInfo: setUserInfo,
                getUserLanguage: getUserLanguage,
                setUserLanguage: setUserLanguage
            };
        };
    });

    tokenModule.run(function($rootScope, $log, $location, $state, $token, NO_AUTH_PATH, $translate) {

        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {

                if($location.search() && $location.search().access_token){
                    $token.setTokenFromLocation();
                }

                // Load prefered language
                var lang = $token.getUserLanguage();
                if (!angular.isUndefinedOrNull(lang)) {
                    $translate.use(lang);
                }

                if ($rootScope.isNoAuthPage()) {
                    // No-auth, skip
                } else {
                    var access_token = $token.getAccessToken();
                    var tokenVerified = $rootScope.tokenVerified;

                    if (access_token) {
                        if ($token.getUserInfo() && tokenVerified) {
                            // It okay - do nothing
                            if (toState.name == 'home') {
                                event.preventDefault();
                                $state.go($rootScope.getHomeState($token.getUserInfo().roleCodes));
                            } else {
                                if (!$rootScope.checkStateByRole($token.getUserInfo().roleCodes, toState.name)) {
                                    event.preventDefault();
                                    $state.go('404');
                                }
                            }
                        } else {
                            var promise = $token.verifyToken();
                            promise.then(function (userinfo) {
                                $rootScope.tokenVerified = true;
                                if (toState.name == 'home') {
                                    event.preventDefault();
                                    $state.go($rootScope.getHomeState($token.getUserInfo().roleCodes));
                                } else {
                                    if (!$rootScope.checkStateByRole($token.getUserInfo().roleCodes, toState.name)) {
                                        event.preventDefault();
                                        $state.go('404');
                                    }
                                }
                                $log.log("Token was verified");
                                return;
                            }, function (error) {
                                $log.log("Cannot verify token: " + error);
                                event.preventDefault();

                                // If error is undefined, this usual because server is offline
                                if (!error) {
                                    $state.go('500');
                                } else if (error.status === 401) {
                                    // Do nothing - this case will be handled in interceptor
                                } else {
                                    $state.go('unknow_error');
                                }
                            });
                        }
                    } else {
                        event.preventDefault();
                        $token.loginHandler();
                    }
                }
            });
    });
    
    
    tokenModule.config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push(function ($injector) {
            return {
                request: function (config) {
                    config.headers = config.headers || {};

                    $injector.invoke([
                        '$token', function($token) {
                            if ($token.getAccessToken()) {
                                config.headers.Authorization = 'Bearer ' + $token.getAccessToken();
                            }
                        }
                    ]);

                    return config;
                },
                responseError: function(resp) {
                    if(resp.status === 401) {
                        $injector.invoke([
                          '$token', function($token) {
                            $token.clearToken();
                            $token.clearUserInfo();
                            $token.loginHandler();
                          }
                        ]);
                    }
                    return $injector.get('$q').reject(resp);
                }
            };
        });

        }
    ]);
 
})(window, window.angular, Base64);

function getValuesFromUrl(url, fragment){
    var search = url.indexOf(fragment);
    if(search < 0) { 
        return {};
    }
    
    search = url.substr(search + fragment.length);
    if(search.indexOf("/" === 0)) {
        search = search.substr(1);
    }
    var hash = search.indexOf(fragment);
    if(hash > 0) {
        hash = search.substr(hash);
        search = search.replace(hash,'');
    }
    
    search = search.split("&");
    var length = search.length;
    if(length < 0) return null;
    
    var obj = {};
  
    for(var i = 0; i < length; i++) {
        var key_value = search[i].split("=");
        obj[decodeURIComponent(key_value[0])] = decodeURIComponent(key_value[1]);
    }
    
    return obj;
}
