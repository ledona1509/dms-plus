/**
 * FILTER FOR UI SELECT
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
app.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
            items.forEach(function(item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    var value = '';
                    if (item != null && item[prop] != null) {
                        value = item[prop].toString().toLowerCase();
                    }

                    if (value.indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

app.filter('propsTranslateFilter', function($filter) {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
            items.forEach(function(item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    var value = $filter('translate')(item[prop].toString())
                    if (value.toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

app.filter("debug.print", [ function() {
        return function(str){
            console.log("ch.filters.debug.print", str);
            return str;
        }
    }
    ])
/***  Boolean Filters *****/
    .filter("boolean_YesNo", [ function() {
        return function(b){
            return b === true? 'Yes' : 'No';
        }
    }
    ])
/***  String Filters *****/
    .filter("string_format", [ function() {
        return function(str){
            if (!str || arguments.length <=1 ) return str;
            var args = arguments;
            for (var i = 1; i < arguments.length; i++) {
                var reg = new RegExp("\\{" + (i - 1) + "\\}", "gm");
                str = str.replace(reg, arguments[i]);
            }
            return str;
        }
    }
    ]).filter("string_html2string", [ function() {
        return function(str){
            if (!str) return str;
            return $('<div/>').html(str).text();
        }
    }
    ]).filter("string_shorten", [ function() {
        return function(str,length){
            if (!str || !length || str.length <= length) return (str || '');
            return  str.substr(0, length) + (length <= 3 ? '' : '...');
        }
    }
    ]).filter("string_lowercase", [ function() {
        return function(str){
            return (str || '').toLowerCase();
        }
    }
    ]).filter("string_uppercase", [ function() {
        return function(str){
            return (str || '').toUpperCase();
        }
    }
    ]).filter("string_camelcase", [ function(){
        return function(str){
            return (str || '').toLowerCase().replace(/(\s.|^.)/g, function(match, group) {
                return group ? group.toUpperCase() : '';
            });
        }
    }
    ]).filter("string_trim", [ function(){
        return function(str){
            return (str || '').replace(/(^\s*|\s*$)/g, function(match, group) {
                return '';
            });
        }
    }
    ]).filter("string_trimstart", [ function(){
        return function(str){
            return (str || '').replace(/(^\s*)/g, function(match, group) {
                return '';
            });
        }
    }
    ]).filter("string_trimend", [ function(){
        return function(str){
            return (str || '').replace(/(\s*$)/g, function(match, group) {
                return '';
            });
        }
    }
    ]).filter("string_replace", [ function(){
        return function(str, pattern, replacement){
            try {
                return (str || '').replace(pattern,replacement);
            } catch(e) {
                console.error("error in string.replace", e);
                return (str || '');
            }
        }
    }
    ]).filter("math_max", [ function(){
        return function(arr){
            if (!arr) return arr;
            return Math.max.apply(null, arr);
        }
    }
    ]).filter("math_min", [ function(){
        return function(arr){
            if (!arr) return arr;
            return Math.min.apply(null, arr);
        }
    }
    ]).filter("array_join", [ function(){
        return function(arr,seperator){
            if (!arr) return arr;
            return arr.join(seperator || ',');
        }
    }
    ]).filter("array_reverse", [ function(){
        return function(arr){
            if (!arr) return arr;
            return arr.reverse();
        }
    }
    ]);

app.filter("user_date_format", [ function() {
        return function(isoDateString){
            if (angular.isUndefinedOrNull(isoDateString)) {
                return '';
            }
            if (isoDateString.indexOf(':') > -1) {
                return moment(isoDateString, 'YYYY-MM-DDTHH:mm:ss').format('DD/MM/YYYY');
            } else {
                return moment(isoDateString, 'YYYY-MM-DD').format('DD/MM/YYYY');
            }
        }
    }
    ]).filter("user_date_time_format", [ function() {
        return function(isoDateString){
            if (angular.isUndefinedOrNull(isoDateString)) {
                return '';
            }
            return moment(isoDateString, 'YYYY-MM-DDTHH:mm:ss').format('DD/MM/YYYY HH:mm:ss');
        }
    }
]);

app.filter('bigNumber', function($filter) {
    return function(value) {
        var unit = '';
        if (value > 10000000000) {
            value = value / 1000000000;
            unit = ' B.';
        } else if (value > 10000000) {
            unit = ' M.';
            value = value / 1000000;
        } else if (value > 10000) {
            unit = ' k';
            value = value / 1000;
        }

        if (value > 99) {
            return $filter('number')(value, 0) + unit;
        } else if (value > 1) {
            return $filter('number')(value, 1) + unit;
        } else {
            return $filter('number')(value, 2) + unit;
        }
    };
});

/**
 ******************************************************************************
 * */