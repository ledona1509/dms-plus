
(function(angular, SockJS, Stomp, _, undefined) {'use strict';
    var module = angular.module('vt.push', ['vtToken']);

    module.provider('$vtpush', function () {
        var defaultConfigs = {
            RECONNECT_TIMEOUT : 30000,
            SOCKET_URL : ""
        };
        
        this.set =
        this.Set = function ()
        {
            if (arguments.length !== 1 && typeof arguments[0] === 'object') {
                return this;
            }
            
            if(typeof arguments[0] !== 'string'){
                return this;
            }
            
            var value = typeof arguments[1] === 'undefined' ? null : arguments[1];
            var key = arguments[0];
            defaultConfigs[key] = value;
            
            return this;
        };
        
        this.$get = function ($log) {
            var configs = {};
            
            configs.get = function() {
                var returnValue = null;
                if(arguments.length === 0) {
                    $log.error('arguments null');
                    throw 'arguments error';
                }
                
                var key = arguments[0];
                
                if (angular.isString(arguments[0])) {
                    returnValue = defaultConfigs[key];
                }
                return returnValue;
            };
            
            configs.getSocketUrl = function(){
                return configs.get('SOCKET_URL');  
            };
            
            return configs;
        };
    });

    module.service("_socket", function($q, $token, $log, $vtpush){
        var stomp = null;

        var reconnect = function() {
            $timeout(function() {
              //initialize();
            }, $vtpush.get('RECONNECT_TIMEOUT') );
        };
        
        this.connect = function(){
            var defer = $q.defer();
            if (!stomp && !angular.isUndefinedOrNull($token.getAccessToken())){
                var client = new SockJS($vtpush.getSocketUrl(), {},{});
                stomp = Stomp.over(client);
                stomp.onclose = reconnect;
                stomp.connect($token.getAccessToken(), $token.getAccessToken(), function(){
                    defer.resolve(stomp);
                });
            }
            return defer.promise;
        };
        
        this.getStomp = function (){
             return stomp;
        };
        
    });
    
    module.factory("socketClient", function($q, $token, _socket){
        var clientService = {
            broker: null
        };
        var subscribes = [];
        //var messageIds = [];

        clientService.registerSubscribe = function(topic) {
            var defer = $q.defer();

            subscribes.push({
                topic: topic,
                defer: defer
            });

            return defer.promise;
        };

        clientService.start = function() {
            _socket.connect().then(function(stomp){
                for (var i=0; i<subscribes.length; i++) {
                    subscribeStomp(stomp, subscribes[i].topic, subscribes[i].defer);
                }
            });
        };

        clientService.send = function(sender, message) {
            var id = Math.floor(Math.random() * 1000000);
            _socket.getStomp().send(clientService.broker, {},
                JSON.stringify({
                    sender: sender,
                    data: message,
                    id: id,
                    clientSentTime: new Date()
                })
            );
          
            //messageIds.push(id);
        };
        
        clientService.setBroker = function(broker) {
            clientService.broker = broker; 
        };

        var subscribeStomp = function(stomp, topic, defer) {
            stomp.subscribe(topic, function(data) {
                defer.notify(getMessage(data.body));
            });
        };

        var getMessage = function(data) {
            var message = JSON.parse(data);
            
            var out = {
                sender: message.sender,
                message: message.data,
                time: new Date(message.serverSentTime),
                self: false
            };

            /*if (_.contains(messageIds, message.id)) {
                out.self = true;
                messageIds = _.remove(messageIds, message.id);
            }*/

            return out;
        };

        return clientService;
    });

})(angular, SockJS, Stomp, _);