/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package log;

/**
 *
 * @author trungkh
 */
public class Logger {
    
    public static final void log(String message) {
        System.out.println(message);
    }
    
    public static final void log(String message, Throwable e) {
        System.out.println(message);
        e.printStackTrace();
    }
    
}
