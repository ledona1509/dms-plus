/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author trungkh
 */
public class ConvertDomainName {
    
    public static final String convertToLower(String className) {
        String[] splits = className.split("_");
        
        StringBuilder sb = new StringBuilder();
        if (splits.length > 1) {
            for (int i = 0; i < splits.length - 1; i++) {
                sb.append(splits[i].toLowerCase());
                sb.append("-");
            }
        }
        
        char[] chars = splits[splits.length - 1].toCharArray();
        boolean isFirst = true;
        for (char c : chars) {
            if (Character.isLetter(c) && Character.isUpperCase(c)) {
                if (!isFirst) {
                    sb.append("-");
                }
                sb.append(Character.toLowerCase(c));
            } else {
                sb.append(c);
            }
            
            isFirst = false;
        }
        
        return sb.toString();
    }
    
    public static final String convertToLowerOnlyLetter(String className) {
        return className.replace("-", "").replace("_", "").toLowerCase();
    }
    
    public static final String convertToOnlyLetter(String className) {
        return className.replace("-", "").replace("_", "");
    }
}
