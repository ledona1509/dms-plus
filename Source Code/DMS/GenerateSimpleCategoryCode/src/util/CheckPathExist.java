/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;

/**
 *
 * @author trungkh
 */
public class CheckPathExist {
    
    public static final boolean check(String path) {
        return new File(path).exists();
    }
    
}
