﻿http://buckinghaminquirer.blogspot.com/2011/11/why-you-shouldnt-use-mongodb-for-oltp.html

/* ********************************************************************************************
 *	Cài đặt 
 */

B1 : Create a /etc/yum.repos.d/mongodb-org-3.0.repo
[mongodb-org-3.0]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.0/x86_64/
gpgcheck=0
enabled=1
B2 : 
[root@localhost ~]# sudo yum install -y mongodb-org
--Lỗi khi cài đặt  
*Lỗi 1 : MongoDB_Log1

/* ********************************************************************************************
 *	Cấu hình và kiến trúc MongoDB
 */
--Các thư mục và file cấu hình của MongoDB
'/var/lib/mongo' : Chứa dữ liệu của database
'/var/log/mongodb' : chứa log của database
'/etc/mongodb.conf' : tập tin cấu hình (config) của MongoDB

--Start MongoDB 
[root@localhost ~]# sudo service mongod start
--Check MongoDB 
[root@localhost ~]# sudo service mongod status
--Stop MongoDB
[root@localhost ~]# sudo service mongod stop
--Restart MongoDB
[root@localhost ~]# sudo service mongod restart
--Config MongoDB start when server start
[root@localhost ~]# sudo chkconfig --levels 235 mongod on
-- Chạy mongo shelld
[root@localhost ~]# mongo

/* ********************************************************************************************
 *	Connect MongoDB
 */
--C1
mongo host:port/database-name -u user -ppassword
--C2

/* ********************************************************************************************
 *	Thao tác cơ bản với MongoDB
 */
http://www.daonho.com/2014/08/cac-lenh-co-ban-trong-mongodb-phan-1.html
http://www.daonho.com/2014/08/cac-lenh-co-ban-trong-mongodb-phan-2.html
http://vneconomics.com/30-lenh-thao-tac-voi-mongodb/
http://www.dtmi.net/tao-user-va-cap-phat-quyen-quan-tri-database-trong-mongodb/s

--Tạo cơ sở dữ liệu mydb
> use mydb
--Xem db hiện thời
> db
--Tạo các đối tượng
thing={x:4,j=100}
--Insert đối tượng vào collection things
db.things.insert(thing) //db.things.save(thing)
for (var i = 1; i<=20; i++) db.thing.save(x:4,j:i)
--Query đối tượng
db.things.find(<queries>)
--Làm việc với con trỏ
var c = db.things.find()
while ( c.hasNext() ) printjson( c.next() )
//gọi phần tử thứ 4
printjson( c [ 4 ] )

--Để xem danh sách database đã tạo trong MongoDB thì ta sử dụng lệnh sau:
> show dbs
--Xóa database
> use freetutsdb
> db.dropDatabase()

-- Lấy thông tin collection 
db.getCollection("demo");
-- Lấy thông tin collection 
db.version;

/* ********************************************************************************************
 *	Backup/Restore MongoDB
 */
http://www.thegeekstuff.com/2013/09/mongodump-mongorestore/
http://www.dtmi.net/tao-user-va-cap-phat-quyen-quan-tri-database-trong-mongodb/

--1.Backup raw.
copy toàn bộ thư mục /var/lib/mongodb hoặc /data/db hoặc tạo thành snapshot
--2.Dùng mongodump
#Backup toàn bộ cơ sở dữ liệu test, mặc định sẽ lưu vào /var/lib/mongo/dump/
mongodump --db test
#Backup cơ sở dữ liệu test và collection things lựa chọn và lưu vào /path/to/backup
mongodump --collection things --db test --dbpath /path/to/backup
#Backup thông qua mạng sử dụng username và password toàn bộ các cơ sở dữ liệu và lưu vào /opt/backup/mongodump-2012-12-24
mongodump --host mongodb1.example.net --port 3017 --username user --password pass --out /opt/backup/mongodump-2012-12-24
--3.Restore cơ sở dữ liệu
mongorestore dump-2012-12-24/
mongorestore --collection things --db test --dbpath /path/to/backup
mongorestore --host mongodb1.example.net --port 3017 --username user --password pass /opt/backup/mongodump-2012-12-24
 
 
Use mongorestore to restore data

The mongorestore utility uses the dump files to send a series of inserts to the target database, but ignoring duplicate records. If your data is largely "write once", this is a good way to merge backup data with production data. Caveat: This process will not apply updates that may happened since the backup was made, and will reinsert any since deleted documents.

You may run mongorestore against your target database. This example assumes dump/ is in your current working path.

    mongorestore --host walt.compose.io:<portnumber> --db <database_name> -u <username> -p<password> dump/<database_name>

Dump and restore speed will vary based on the total size of your data, the number of documents, and indexing schema on your database. For the best speed, you should run your commands from a server near your Compose database (an Amazon EC2 instance, for example).


 
/* ********************************************************************************************
 *	Tool For MongoDB
 */
http://robomongo.org/
http://www.mongodbmanager.com/
http://mongodb-tools.com/
http://www.infoworld.com/article/2610945/nosql/review--4-free--open-source-management-guis-for-mongodb.html

/* ********************************************************************************************
 *	Thao tác cơ bản với MongoDB trong Java
 */
 
http://laptrinh.vn/d/4337-vi-du-java-mongodb-query-document.html
http://iscclub.uit.edu.vn/qa/?qa=77/c%C3%A1ch-t%E1%BA%A1o-truy-v%E1%BA%A5n-d%E1%BB%AF-li%E1%BB%87u-trong-mongodb



/* ********************************************************************************************
 *	Tài liệu tham khảo
 */
http://docs.mongodb.org/manual/tutorial/install-mongodb-on-red-hat/
http://blog-xtraffic.pep.vn/huong-dan-cach-cai-dat-mongodb/
http://wikilinux.vn/mongodb-tren-linux-cho-nguoi-bat-dau/

http://www.qhonline.info/forum/showthread.php/7583-gioi-thieu-mongodb

http://code.freetuts.net/tong-quan-ve-mongodb-203.html
http://code.freetuts.net/lenh-tao-va-xoa-database-mongobb-332.html
http://code.freetuts.net/hoc-mongodb-online-voi-mongolab-com-va-robomongo-330.html

http://bigsonata.com/mongodb/
http://www.code4life.vn/2013/03/su-dung-java-e-lam-viec-voi-mongodb-nhu.html
http://www.dtmi.net/nosql-va-mongodb-la-gi/
